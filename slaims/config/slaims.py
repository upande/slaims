from __future__ import unicode_literals
from frappe import _
import frappe

def get_data():

	Organization_registrations_forms = [{
					"type": "doctype",
					"name": "International NGO Form",
					"description": _("Form That Allows the User to Register International NGO"),
				},
				{
					"type": "doctype",
					"name": "Local NGO Form English",
					"description": _("Form That Allows the User to Register UN System Organization"),
				},
				{
					"type": "doctype",
					"name": "Local NGO Form Somali",
					"description": _("Form That Allows the User to Register UN System Organization"),
				},
				{
					"type": "doctype",
					"name": "Somaliland Government Institution Form",
					"description": _("Form That Allows the User to Register Somaliland Government Institution"),
				},
				{
					"type": "doctype",
					"name": "Private Sector Company Form",
					"description": _("Form That Allows the User to Register Private Sector Company"),
				},
				{
					"type": "doctype",
					"name": "Bilateral Organization Form",
					"description": _("Form That Allows the User to Register Bilateral organization"),
				},
				{
					"type": "doctype",
					"name": "MultiLateral Organization Form",
					"description": _("Form That Allows the User to Register MultiLateral Organization"),
				},
				{
					"type": "doctype",
					"name": "UN System Organization Form",
					"description": _("Form That Allows the User to Register UN System Organization"),
				},
				{
					"type": "doctype",
					"name": "Umbrella or Consortium Organization Form",
					"description": _("Form That Allows the User to Register Umbrella or Consortium Organization"),
				}, 
			]
	
	Organization_renewal_forms =  [{
					"type": "doctype",
					"name": "International NGO Renewal Form",
					"description": _("Form That Allows the User to Register International NGO"),
				},
				{
					"type": "doctype",
					"name": "Local NGO Renewal Form English",
					"description": _("Form That Allows the User to Register UN System Organization"),
				},
				{
					"type": "doctype",
					"name": "Local NGO Renewal Form Somali",
					"description": _("Form That Allows the User to Register UN System Organization"),
				},
				{
					"type": "doctype",
					"name": "Somaliland Government Institution Renewal Form",
					"description": _("Form That Allows the User to Register Somaliland Government Institution"),
				},
				{
					"type": "doctype",
					"name": "Private Sector Company Renewal Form",
					"description": _("Form That Allows the User to Register Private Sector Company"),
				},
				{
					"type": "doctype",
					"name": "Bilateral Organization Renewal Form",
					"description": _("Form That Allows the User to Register Bilateral organization"),
				},
				{
					"type": "doctype",
					"name": "MultiLateral Organization Renewal Form",
					"description": _("Form That Allows the User to Register MultiLateral Organization"),
				},
				{
					"type": "doctype",
					"name": "UN System Organization Renewal Form",
					"description": _("Form That Allows the User to Register UN System Organization"),
				},
				{
					"type": "doctype",
					"name": "Umbrella or Consortium Organization Renewal Form",
					"description": _("Form That Allows the User to Register Umbrella or Consortium Organization"),
				},
			]


	Approved_forms = [{
					"type": "doctype",
					"name": "Organization",
					"description": _("Keep Details of an Organization"),
				},
                {
					"type": "doctype",
					"name": "Project",
					"description": _("Keep Details of a Project"),
				},
				{
					"type": "doctype",
					"name": "Annual Plan",
					"description": _("Keep Details of an Project's Annual Plan"),
				},
                {
					"type": "doctype",
					"name": "Annual Report",
					"description": _("Keep Details of an Project's Annual Report"),
				},
				{
					"type": "doctype",
					"name": "Non Project Related Expenditure",
					"description": _("Keeps a Lists of Non Project Related Expenditure"),
				}
		]

	Admin_forms =[{
					"type": "doctype",
					"name": "Administrative Unit",
					"description": _("Keep Details of Somaliland Administrative Units e.g Regions, Districts"),
				},
				{
					"type": "doctype",
					"name": "Organization Type",
					"description": _("Reirects you to all list of all the available Organization Type"),
				},
                {
					"type": "doctype",
					"name": "NDP",
					"description": _("Keep Details of Somaliland National Development Programs"),
				},
                {
					"type": "doctype",
					"name": "User",
					"description": _("Keep Details of Users in the System"),
				},
				{
					"type": "doctype",
					"name": "MDA",
					"description": _("Keep a List of MDA"),
				},
                {
					"type": "doctype",
					"name": "Program",
					"description": _("Keep a List of NDP Programs Under the NDP"),
				},
                {
					"type": "doctype",
					"name": "Outcome",
					"description": _("Keep a List of NDP Programs Results i.e Outcomes"),
				},
				{
					"type": "doctype",
					"name": "Country Code",
					"description": _("Keeps a Lists of Country Code"),
				},
				{
					"type": "doctype",
					"name": "Help",
					"description": _("Keeps a list of contact people"),
				},
				{
					"type": "doctype",
					"name": "Inquiry",
					"description": _("Link to inquiries"),
				},
				{
					"type": "doctype",
					"name": "Year",
					"description": _("Link to financial years"),
				},
				{
					"type": "doctype",
					"name": "FAQs",
					"description": _("Link to FAQs"),
				},
				{
					"type": "doctype",
					"name": "Global Settings",
					"description": _("Link to Global Settings"),
				},
				{
					"type": "doctype",
					"name": "SLAIMS Error Log",
					"description": _("Link to SLAIMS Error Logs")
				},
				
			]

	Project_forms = [{
					"type": "doctype",
					"name": "Project Registration Form",
					"description": _("Form That Allows the User to Register a Project"),
				},
			]

	Annual_plan_report_disbursment_forms = [{
						"type": "doctype",
						"name": "Annual Plan Form",
						"description": _("Form That Allows the User to Register,Renew or Update Registration Details of Projects"),
					},
					{
						"type": "doctype",
						"name": "Annual Report Form",
						"description": _("Form That Allows the User to Register,Renew or Update Registration Details of Annual Plan"),
					},
					{
						"type": "doctype",
						"name": "Non Project Related Expenditure",
						"description": _("Form for annual disbursement reports"),
					},
					
				]

	Other_forms = [{
						"type": "doctype",
						"name": "Data Import",
						"description": _("List of Data Imports"),
					},
					{
						"type": "doctype",
						"name": "Notification",
						"description": _("List of Notifications"),
					},
					{
						"type": "doctype",
						"name": "Email Account",
						"description": _("List of Email Accounts"),
					}
				]
	Reports = [{
				"type": "page",
				"label": _("SLAIMS Reports"),
				"name": "report",
			},
			{
				"type": "page",
				"label": _("SLAIMS Dashboard"),
				"name": "dashboard",
			},
			
	]

	# detemine which forms a user will see
	list_of_roles = frappe.get_roles() 
	if "Staff"  in list_of_roles or "Super Admin" in list_of_roles:
		all_forms = [
				{
				"label": _("Approved Forms"),
				"items": Approved_forms
				},
				{
				"label": _("Admin Forms"),
				"items": Admin_forms
				},
				{
				"label": _("Organization Registration Forms"),
				"items": Organization_registrations_forms
				},
				{
				"label": _("Organization Renewal Forms"),
				"items": Organization_renewal_forms
				},
				{
				"label": _("Project Forms"),
				"items": Project_forms
				},
				{
				"label": _("Annual Plan, Report & NPR Expenditure"),
				"items": Annual_plan_report_disbursment_forms
				},
				{
				"label": _("Other Forms"),
				"items": Other_forms
				},
				{
				"label": _("SLAIMS Reports"),
				"items": Reports
				}
			]
		
		return all_forms
	else:
		list_of_organizations_users = frappe.get_list("Organization",
			fields=["user","organization_type","name"],
			filters = {
				"user":frappe.session.user
		})

		if len(list_of_organizations_users) >0:
			user_org_type =  list_of_organizations_users[0].organization_type

			# get relevant registration forms
			user_registration_forms = []
			item_exceptions = {
					"Bi-lateral Organization":{
					"type": "doctype",
					"name": "Bilateral Organization Form",
					"description": _("Form That Allows the User to Register Bilateral organization"),
					},
					"Multi-Lateral Organizations":{
						"type": "doctype",
						"name": "MultiLateral Organization Form",
						"description": _("Form That Allows the User to Register MultiLateral Organization"),
					},
					"Bi-lateral Organization Renewal":{
						"type": "doctype",
						"name": "Bilateral Organization Renewal Form",
						"description": _("Form That Allows the User to Register Bilateral organization"),
					},
					"Multi-Lateral Organizations Renewal":{
						"type": "doctype",
						"name": "MultiLateral Organization Renewal Form",
						"description": _("Form That Allows the User to Register MultiLateral Organization"),
					},
				}

			if user_org_type == "Bi-lateral Organization":
				user_registration_forms.append(item_exceptions['Bi-lateral Organization'])
			elif user_org_type == "Multi-Lateral Organizations":
				user_registration_forms.append(item_exceptions['Multi-Lateral Organizations'])
			else:
				for item in Organization_registrations_forms:
						if user_org_type in item["name"]:
							user_registration_forms.append(item)

			# get renewal forms
			user_renewal_forms = []
			if user_org_type == "Bi-lateral Organization":
				user_renewal_forms.append(item_exceptions['Bi-lateral Organization Renewal'])
			elif user_org_type == "Multi-Lateral Organizations":
				user_renewal_forms.append(item_exceptions['Multi-Lateral Organizations Renewal'])
			else:
				for item in Organization_renewal_forms:
					if user_org_type in item["name"]:
						user_renewal_forms.append(item)

			filter_user_forms = [
				{
				"label": _("Organization Registration Forms"),
				"items": user_registration_forms
				},
				{
				"label": _("Organization Renewal Forms"),
				"items": user_renewal_forms
				},
				{
				"label": _("Project Forms"),
				"items": Project_forms
				},
				{
				"label": _("Annual Plan, Report & NPR Expenditure"),
				"items": Annual_plan_report_disbursment_forms
				}
			]

			return filter_user_forms

		else:
			# continue from here
			Unfiltered_user_forms = [
				{
				"label": _("Organization Registration Forms"),
				"items": Organization_registrations_forms
				},
				{
				"label": _("Organization Renewal Forms"),
				"items": Organization_renewal_forms
				},
				{
				"label": _("Project Forms"),
				"items": Project_forms
				},
				{
				"label": _("Annual Plan, Report & NPR Expenditure"),
				"items": Annual_plan_report_disbursment_forms
				}
			]

			return Unfiltered_user_forms

	