from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from .reusable_functions import get_projects_based_filters, get_years
from collections import Counter


@frappe.whitelist(allow_guest=True)
def get_search_results(args):
    '''
    Function that pulls data from the datatabase
    based on the given filters
    '''
    # given filters
    json_filters = json.loads(args)

    # fitler projects using different filters
    filtered_projects = get_projects_based_filters(
        json_filters['location'], json_filters['sector'], json_filters['year'], json_filters['type'])

    list_of_project_amount_n_locations = []
    # loop through projects getting reports

    for project in filtered_projects:
        if project.master_project:
            # holder of locations per project
            locations_holder_per_project = {
                "project": project, "report_locations": {}}

            # get all administrative unit
            list_of_adminisrative_units = frappe.get_list("Administrative Unit",
                                                          fields=["name"],
                                                          filters={
                                                              "type": "Region"
                                                          }
                                                          )

            # create a key holding all regions
            location_keys = {}
            for location in list_of_adminisrative_units:
                location_keys[location.name] = 0

            # add location keys
            locations_holder_per_project['report_locations'] = location_keys

            # get a list of all reports linked to the project
            list_of_reports = frappe.get_list("Annual Report",
                                              fields=["*"],
                                              filters={
                                                  "project_title": project.name,
                                              }
                                              )

            # if master project check if it has subprojects

            subproject_list = frappe.get_list('Project',
                                              fields=['*'],
                                              filters={
                                                  'sub_project': 1,
                                                  'link_to_master_project': project.name,
                                              }
                                              )

            if subproject_list:
                subproject_list = [sub.name for sub in subproject_list]
                for sub in subproject_list:
                    subproject_report = frappe.get_list('Annual Report',
                                                        fields=['*'],
                                                        filters={
                                                            'project_title': sub,
                                                        }
                                                        )

                    list_of_reports.extend(subproject_report)
            else:
                pass

            if list_of_reports:
            # loop through the found reports
                for report in list_of_reports:
                    # get a list of all locations linked to the project under Regional Distribution Table
                    list_of_locations = frappe.get_list("Regional Distribution Table",
                                                        fields=["*"],
                                                        filters={
                                                            "parent": report.name,
                                                            "parenttype": "Annual Report",
                                                            "parentfield": "regional_distribution_table",
                                                        }
                                                        )

                    # loop through linked projects
                    for location in list_of_locations:
                        # check if the project's key already exists
                        if location['region'] in locations_holder_per_project['report_locations'].keys():
                            locations_holder_per_project['report_locations'][location['region']
                                                                            ] += location['amount']
                        else:
                            pass

 
            else:
                pass
                           # append the dict to return list
            list_of_project_amount_n_locations.append(locations_holder_per_project)

        else:
            pass

    # return  list_of_project_reports
    return list_of_project_amount_n_locations
