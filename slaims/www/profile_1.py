from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json

def get_context(context):
    context['users'] = frappe.get_all('User')
    return context

@frappe.whitelist(allow_guest = True) 
def get_user():
    # get the logged in user
    username = frappe.session.user
    return username


@frappe.whitelist(allow_guest = True) 
def get_user_profile(args):
    # get all the pillars
    json_args = json.loads(args)

    # get related user doc
    linked_user_doc = frappe.get_doc("User",json_args['username'])

    # get linked user table
    organiation_user_table = frappe.get_list("Organization Users Table",
        fields=["*"],
        filters = {
            "parenttype":"Organization",
            "parentfield":"organization_users_table",
            "user":json_args['username']
        }
    )

    if len(organiation_user_table) > 0:
        linked_organization = organiation_user_table[0]["parent"]

        # get related organization
        linked_organization_doc = frappe.get_doc("Organization",linked_organization)
        return {"status":True,"organization":linked_organization_doc,"user_doc":linked_user_doc}
    else:
        return {"status":False,"user_doc":linked_user_doc}

    # frappe.throw("not alloed")
# @frappe.whitelist(allow_guest = True)
# def get_search_results_1(args):
#     # get all the pillars
#     json_filters = json.loads(args)
    
#     list_of_projects = frappe.get_list("Project",
#         fields=["*"],
#         filters = {
#             "master_project":1,
#         }
#     )

#     project_and_locations = []

#     for project in list_of_projects:
#         project_requested = False
#         locations_per_project = []
#         combined_key_valued = {}

#         # add filters below
#         if json_filters["location"] == "All Locations":
#             project_requested = True

#             list_of_locations = frappe.get_list("Project Location",
#                 fields=["*"],
#                 filters = {
#                     "parent":project.name,
#                     "parenttype":"Project",
#                     "parentfield":"project_location_table",
#                 }
#             )

#             for location in list_of_locations:
#                 full_location_name = location.region
#                 locations_per_project.append(full_location_name.split("/")[0])
#         else:
#             requested_location = json_filters["location"]

#             list_of_locations = frappe.get_list("Project Location",
#                 fields=["*"],
#                 filters = {
#                     "parent":project.name,
#                     "region":requested_location,
#                     "parenttype":"Project",
#                     "parentfield":"project_location_table",
#                 }
#             )

#             if len(list_of_locations) > 0:
#                 project_requested = True

#                 for location in list_of_locations:
#                     full_location_name = location.region
#                     locations_per_project.append(full_location_name.split("/")[0])

#             else:
#                 project_requested = False

        
#         if project_requested:
#             combined_key_valued["project"] = project
#             combined_key_valued["locations"] = locations_per_project
#             project_and_locations.append(combined_key_valued)
    
#     return project_and_locations