from __future__ import unicode_literals

# thirdn party imports
import json
import requests
from requests.exceptions import HTTPError
from html2text import html2text


# applications imports
import frappe
from frappe.utils.global_search import web_search
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
from .reusable_functions import get_sectors,get_organization_type,get_locations,get_years_n_global_financial_year

@frappe.whitelist(allow_guest = True)
def get_search_results(args):
    '''
    Function that pulls data from the datatabase
    based on the given filters
    NB:
        Please note that this report only works with 
        subprojects as there is no need to deal with master
        projects as may be the case for other reports because only
        the total matter
    '''
    # given filters
    json_filters = json.loads(args)
    # get all sub projects active within the given year and status == "Approved"
    projects_filtered_by_year = get_active_projects(json_filters['year'])
    # filter organization type
    projects_filtered_by_org_type = filter_by_org_type(json_filters['type'],projects_filtered_by_year)
    # filter by sector
    projects_filtered_by_sector = filter_by_org_sector(json_filters['sector'],projects_filtered_by_org_type)
    # filters by location 
    project_filtered_by_location = filter_by_location(json_filters['location'],projects_filtered_by_sector)
    # check that all the annual plans required for filtered projects are given
    projects_filtered_by_report_n_npre = filter_by_report_n_npre(json_filters['year'],project_filtered_by_location)
    # all projects that pass all the above filters should be used in the final report
    regions_to_add = get_locations()
    regions_with_values = {}
    total_expenditure = 0
    total_npre = 0
    total_amount_from_somaliland = 0
    total_amount_from_outside_somaliland = 0
    # loop through all the regions as keys to regions_with_values
    for region_to_add in regions_to_add:
        regions_with_values[region_to_add[0]] = {'expenditure':0,'npre':0,'disbursment':0,
            'disbursment_from_somaliland':0,'disbursment_from_outside_somaliland':0,
        }

    # get all the reports for the projects based on filters
    for filtered_project in projects_filtered_by_report_n_npre:
        # get funding sources and amounts for projects
        list_of_funding_sources = frappe.get_list("Source Organization",
            fields=["*"],
            filters={
                "parent":filtered_project[0],
            }
        )

        # loop through funding sources getting origins of sources either (somaliland/outside)
        for funding_source in list_of_funding_sources:
            if funding_source.funds_outside_sl_yes:
                total_amount_from_outside_somaliland += funding_source.amount
            elif funding_source.funds_outside_sl_no:
                total_amount_from_somaliland += funding_source.amount

        if json_filters['year'] == "All":
            # get all the reports for 
            list_of_reports = frappe.get_list("Annual Report",
                fields=["*"],
                filters={
                    "project_title": filtered_project[0],
                    "status":"Approved",
                }
            )

            for report in list_of_reports:
                # get amount allocated to each region
                list_of_reports_records = frappe.get_list("Regional Distribution Table",
                    fields=["*"],
                    filters={
                        "parent":report.name,
                    }
                )
                # loop through list of report records
                for report_record in list_of_reports_records:
                    if json_filters['location'] == "All":
                        region_name = report_record['region']
                        regions_with_values[region_name]['expenditure'] += report_record['amount']
                        total_expenditure += report_record['amount']
                    else:
                        region_name = report_record['region']
                        if region_name == json_filters['location']:
                            regions_with_values[region_name]['expenditure'] += report_record['amount']
                            total_expenditure += report_record['amount']
                    
        else:
            # get all the reports for given year
            list_of_reports = frappe.get_list("Annual Report",
                fields=["*"],
                filters={
                    "project_title": filtered_project[0],
                    "status":"Approved",
                    "annual_report_year":json_filters['year']
                }
            )

            for report in list_of_reports:
                # get amount allocated to each sector
                list_of_reports_records = frappe.get_list("Regional Distribution Table",
                    fields=["*"],
                    filters={
                        "parent":report.name,
                    }
                )
                # loop through list of report records
                for report_record in list_of_reports_records:
                    if json_filters['location'] == "All":
                        region_name = report_record['region']
                        regions_with_values[region_name]['expenditure'] += report_record['amount']
                        total_expenditure += report_record['amount']
                    else:
                        region_name = report_record['region']
                        if region_name == json_filters['location']:
                            regions_with_values[region_name]['expenditure'] += report_record['amount']
                            total_expenditure += report_record['amount']

    # get npres for organizations
    npre_filters = {"status":"Approved"}
    # check if year is defined
    if json_filters['year'] == "All":
        pass
    else:
        npre_filters['year'] = json_filters['year']
        
    # get npres
    npres_filtered_by_status_n_year = frappe.get_list("Non Project Related Expenditure",
        fields=["*"],
        filters = npre_filters
    )

    # filter by organization type
    npres_filtered_by_status_year_n_org_type = []
    if json_filters['type'] == "All":
        # add all npre to list
        npres_filtered_by_status_year_n_org_type = npres_filtered_by_status_n_year
    else:
        # filter npres by organization type
        for npre_filtered_by_status_n_year in npres_filtered_by_status_n_year:
            # get org doc
            org_doc_for_filtered_npre = frappe.get_doc("Organization",npre_filtered_by_status_n_year.full_name_of_the_organization)
            if org_doc_for_filtered_npre.organization_type == json_filters['type']:
                # add the npre to list
                npres_filtered_by_status_year_n_org_type.append(npre_filtered_by_status_n_year)

    # filter by location
    npres_filtered_by_status_year_org_type_n_location = []
    # check if location is defined
    if json_filters['location'] == "All":
        npres_filtered_by_status_year_org_type_n_location = npres_filtered_by_status_year_n_org_type
    else:
        # loop through filtered npres
        for npre_filtered_by_status_year_n_org_type in npres_filtered_by_status_year_n_org_type:
            location_distribution_records = frappe.get_list("Disbursement Report Table General",
                    fields=["*"],
                    filters={
                        "parent":npre_filtered_by_status_year_n_org_type.name,
                        "region":json_filters['location']
                    }
                )
            if len(location_distribution_records) > 0:
                npres_filtered_by_status_year_org_type_n_location.append(npre_filtered_by_status_year_n_org_type)

    # filter by sector
    npres_filtered_by_status_year_org_type_location_n_sector = []
    # check if sector is defined
    if json_filters['sector'] == "All":
        npres_filtered_by_status_year_org_type_location_n_sector = npres_filtered_by_status_year_org_type_n_location 
    else:
        # loop through filtered npres
        for npre_filtered_by_status_year_org_type_n_location in npres_filtered_by_status_year_org_type_n_location:
            sector_distribution_records = frappe.get_list("Disbursement Report Total Sectors",
                    fields=["*"],
                    filters={
                        "parent":npre_filtered_by_status_year_org_type_n_location.name,
                        "sector":json_filters['sector']
                    }
                )
            if len(sector_distribution_records) > 0:
                npres_filtered_by_status_year_org_type_location_n_sector.append(npre_filtered_by_status_year_org_type_n_location)

    # loop through filtered adding them to the specific regions they contribute to
    for approved_npre in npres_filtered_by_status_year_org_type_location_n_sector:
        # check if the organization had an npre
        if approved_npre.no:
            # no npre was available for the year/years
            pass
        else:
            try:
                # get the npre distribution per region
                if json_filters['location'] == "All":
                    location_distribution_child_records = frappe.get_list("Disbursement Report Table General",
                        fields=["*"],
                        filters={
                            "parent":approved_npre.name
                        }
                    )
                else:
                    location_distribution_child_records = frappe.get_list("Disbursement Report Table General",
                        fields=["*"],
                        filters={
                            "parent":approved_npre.name,
                            "region":json_filters['location']
                        }
                    )

                # loop through region distribution npre records to get total for each sector
                for location_distribution_child_record in location_distribution_child_records:
                    regions_with_values[location_distribution_child_record.region]['npre'] += location_distribution_child_record.total
                    total_npre += location_distribution_child_record.total
            except:
                pass

    # get the ponderated ratio share of funds from somaliland vs those from outside somaliland
    total_disbursement = total_expenditure + total_npre
    total_amount_from_somaliland_n_outside = total_amount_from_somaliland + total_amount_from_outside_somaliland
    ration_from_somaliland = 0
    ration_from_outside_somaliland = 0
    if total_amount_from_somaliland_n_outside <= 0:
        # a ration cannot be defined based on this
        pass
    else:
        if total_amount_from_somaliland >= 0:
            ration_from_somaliland = total_amount_from_somaliland/total_amount_from_somaliland_n_outside
        if total_amount_from_outside_somaliland >= 0:
            ration_from_outside_somaliland = total_amount_from_outside_somaliland/total_amount_from_somaliland_n_outside

    # loop through all the regions performing calculations for the amounts
    keys_for_region_with_values = regions_with_values.keys()
    for region_key in keys_for_region_with_values:
        region_value = regions_with_values[region_key]
        region_disbursment = region_value['expenditure'] + region_value['npre']
        disbursment_from_somaliland = ration_from_somaliland * region_disbursment
        disbursment_from_outside_somaliland = ration_from_outside_somaliland * region_disbursment

        # place the calculated values in the sector dictionary
        regions_with_values[region_key]['disbursment'] = region_disbursment
        regions_with_values[region_key]['disbursment_from_somaliland'] = disbursment_from_somaliland
        regions_with_values[region_key]['disbursment_from_outside_somaliland'] = disbursment_from_outside_somaliland
    
    # return sectors with their values and the ponderated ratios
    return {'regions_with_values':regions_with_values,
        'ponderated_disbursment_ration_from_somaliland':ration_from_somaliland,
        'ponderated_disbursment_ration_from_outside_somaliland':ration_from_outside_somaliland,
        'total_disbursement':total_disbursement,
        'total_expenditure':total_expenditure,
        'total_npre':total_npre,
        'total_ponderated_disbursment_somaliland':total_disbursement*ration_from_somaliland,
        'total_ponderated_disbursment_outside_somaliland':total_disbursement*ration_from_outside_somaliland
    }

def get_active_projects(financial_year):
    '''
    Function that gets all the current active projects i.e 
    Projects whose duration is within on covers the duration
    of the given financial year and status == active
    Input: 
        financial_year- str
    Output:
        list of projects 
    '''
    # check if year is given
    if financial_year == "All":
        # get all the projects regardless of the year
        sql_all_sub_projs = '''select name,status,your_organization \
            from tabProject where sub_project = 1 \
            and status = "Approved"'''
        all_sub_projs = frappe.db.sql(sql_all_sub_projs)
        # return all the subprojects
        return all_sub_projs
    else:
        # get start n end date of the given finacial year
        fin_year_doc = frappe.get_doc("Year",financial_year)
        start_date = fin_year_doc.start_date
        end_date = fin_year_doc.end_date
        
        # get projects the is the destination organizations
        # projects whose start or end dates are within financial year duration
        sql1 = '''select name,status,your_organization from \
            tabProject where project_start_date between \
            "{}" and "{}" and sub_project = 1 and status = "Approved" \
            || project_end_date between "{}" and "{}" and sub_project = 1 \
            and status = "Approved"'''.format(start_date,end_date,start_date,end_date) 
        project_list_sql1 = frappe.db.sql(sql1)
        # projects whose start or end dates are outside financial year duration
        sql2 = '''select name,status,your_organization from \
            tabProject where project_start_date < "{}" \
            and project_end_date > "{}" and sub_project = 1 and status = "Approved"\
            '''.format(start_date,end_date)
        project_list_sql2 = frappe.db.sql(sql2)
        # add the two to get all active projects within the given year(these are sub projects)
        list_of_active_projects_in_year = project_list_sql1 + project_list_sql2
        # return list of active project in the given year
        return list_of_active_projects_in_year

@frappe.whitelist(allow_guest = True)
def get_all_filters():
    '''
    Function that calls all the others independent
    filters collecting function and return all the 
    filters in a single dictionary
    '''
    filters_dict = {}
    # call the get organization type function
    filters_dict['sectors'] = get_sectors()
    filters_dict['types'] = get_organization_type()
    filters_dict['locations'] = get_locations()
    filters_dict['years'] = get_years_n_global_financial_year()
    # return the dictionary with all the filters
    return filters_dict

def filter_by_org_type(organization_type,list_of_projects):
    '''
    Function that loops through a list of projects 
    and return those whose destination organization
    are of the given type
    input:
        organization_type - str
        list_of_projects - list 
    output:
        list of projects
    '''
    if organization_type == "All":
        # return all since no  organization type has been specified
        return list_of_projects
    else:
        # initialize return dict
        return_filtered_proj_list = []
        for proj in list_of_projects:
            org_doc = frappe.get_doc("Organization",proj[2])
            if org_doc.organization_type == organization_type:
                # append to return list
                return_filtered_proj_list.append(proj)
        # return project filted by org type
        return return_filtered_proj_list


def filter_by_org_sector(sector,list_of_projects):
    '''
    Function that loops through a list of given 
    projects and determine the if the project 
    contributes to a given sector
    input:
        sector - str
        list_of_projects - list
    output:
        list of projects contributing to given sector
    '''
    if sector == "All":
        # return all since no organization sector has been specified
        return list_of_projects
    else:
        # initialize return dict
        return_filtered_proj_list = []
        for proj in list_of_projects:
            # check if there is a record of sector in child table
            sql_str = '''select name,parent from `tabSector Alignment Table` where \
                parenttype = 'Project' and parentfield = 'sector_alignment_table' and \
                parent = "{}" and sector = "{}"'''.format(proj[0],sector)
            found_child_records = frappe.db.sql(sql_str)
            # check if any record was found
            if len(found_child_records) > 0:
                return_filtered_proj_list.append(proj)

        # return filtered list
        return return_filtered_proj_list
            
def filter_by_location(location,list_of_projects):
    '''
    Function that loops through a list of given projects and 
    determine if the project is implemented in the given area
    input:
        location -str
        list_of_projects - list
    output:
        list of projects implemented in the location
    '''
    if location == "All":
        # return all since no organization location has been specified
        return list_of_projects
    else:
        # initialize return dict
        return_filtered_proj_list = []
        for proj in list_of_projects:
            # check if there is a record of location in child table
            sql_str = '''select name,parent from `tabProject Location` where \
                parenttype = 'Project' and parentfield = 'project_location_table' and \
                parent = "{}" and region = "{}"'''.format(proj[0],location)
            found_child_records = frappe.db.sql(sql_str)
            # check if any record was found
            if len(found_child_records) > 0:
                return_filtered_proj_list.append(proj)

        # return filtered list
        return return_filtered_proj_list

def filter_by_report_n_npre(year,list_of_projects):
    '''
    Function that loops through a list of projects checking
    if a NPRE has been filled for them and status = "Approved"
    input:
        year - str
        list_of_projects - list
    output:
        list of projects implemented in the location
    '''
    if year == "All":        
        # add code for all currently just returning all 
        return_filtered_proj_list = []
        for proj in list_of_projects:
            # get all required report records
            sql_str = '''select name,parent,implementing_organization from \
                `tabDestination Implementing Reporting`where \
                parenttype = 'Project' and parentfield = 'destination_organization_table' \
                and parent = "{}" '''.format(proj[0])
            required_reports = frappe.db.sql(sql_str)
            required_reports_available = False
            # get the start and end date of the project
            current_project_doc = frappe.get_doc("Project",proj[0])
            current_project_start_date = current_project_doc.project_start_date
            current_project_end_date = current_project_doc.project_end_date
            # get all the years within the given start and end date
            sql_str_year = '''select name from tabYear where start_date between "{}" \
                and "{}" or end_date between "{}" and "{}" or "{}" > start_date and \
                    "{}" < end_date
                ''' .format(current_project_start_date,current_project_end_date,current_project_start_date,current_project_end_date,current_project_start_date,current_project_end_date)
            years_reports_are_required = frappe.db.sql(sql_str_year)
            # loop through years required for reports
            for required_report in required_reports:
                # loop through all the years to determine if project has atleast one apparoved report
                for year_report_required in years_reports_are_required:
                    sql_str_1 = '''select name from \
                    `tabAnnual Report` where status = "Approved" and \
                    full_name_of_the_organization = "{}" and \
                    project_title = "{}" and annual_report_year = "{}"\
                    '''.format(required_report[2],required_report[1],year_report_required[0])
                    found_reports = frappe.db.sql(sql_str_1)
                    # check if a report was found
                    if len(found_reports) > 0:
                        required_reports_available = True
                        break
            # check whether to add the list to the return list
            if required_reports_available:
                return_filtered_proj_list.append(proj)   
        # return the filtered list
        return return_filtered_proj_list
    else:
        # initialize return dict
        return_filtered_proj_list = []
        for proj in list_of_projects:
            # get all required report records
            sql_str = '''select name,parent,implementing_organization from \
                `tabDestination Implementing Reporting`where \
                parenttype = 'Project' and parentfield = 'destination_organization_table' \
                and parent = "{}" '''.format(proj[0])
            required_reports = frappe.db.sql(sql_str)
            required_reports_available = True
            # loop through required reports
            for required_report in required_reports:
                sql_str_1 = '''select name from \
                    `tabAnnual Report` where status = "Approved" and \
                    full_name_of_the_organization = "{}" and \
                    project_title = "{}" and annual_report_year = "{}"\
                    '''.format(required_report[2],required_report[1],year)
                found_reports = frappe.db.sql(sql_str_1)
                # check if a report was found
                if len(found_reports) > 0:
                    pass
                else:
                    required_reports_available = False
                    break

                # check if NPRE of all implementing organizations have been submitted
                sql_str_2 = '''select name from `tabNon Project Related Expenditure` where \
                    status = "Approved" and full_name_of_the_organization = "{}" and year = "{}" \
                    '''.format(required_report[2],year)
                found_npres = frappe.db.sql(sql_str_2)
                # check if an NPRE was found
                if len(found_npres) > 0:
                    pass
                else:
                    required_reports_available = False
                    break

            # determine whether or not to add the project to the return list
            if required_reports_available:
                return_filtered_proj_list.append(proj)
        # return filtered list
        return return_filtered_proj_list


                 
            
            