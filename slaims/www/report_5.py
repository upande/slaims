from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from .reusable_functions import get_organizations_based_filters, get_projects_based_filters
from .reusable_functions import get_projects_based_filters


@frappe.whitelist(allow_guest=True)
def get_search_results(args):
    '''
    Function that pulls data from the datatabase
    based on the given filters
    '''
    # given filters
    json_filters = json.loads(args)

    # fitler projects using different filters
    filtered_projects = get_projects_based_filters(
        json_filters['location'], json_filters['sector'],  json_filters['year'], json_filters['type'])

    list_of_project_n_sources = []
    # loop through projects getting funding sources
    for project in filtered_projects:
        project_sources_dict = {}
        # fetch funding organization
        list_of_sources = frappe.get_list("Source Organization",
                                          fields = ["*"],
                                          filters = {
                                              "parent": project.name,
                                              "parentfield": "source_organization_table",
                                              "parenttype": "Project",
                                          }
                                          )

        project_sources_dict['project']=project
        project_sources_dict['sources']=list_of_sources
        list_of_project_n_sources.append(project_sources_dict)

    # return reulting list
    return list_of_project_n_sources
