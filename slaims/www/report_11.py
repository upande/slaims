from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json

# def get_context(context):
#     context['users'] = frappe.get_all('User')


@frappe.whitelist(allow_guest=True)
def get_search_results(args):
    '''
    Function that pulls data from the datatabase
    based on the given filters
    '''
    # get all the pillars
    json_filters = json.loads(args)
    filtered_list = []

    list_of_projects = frappe.get_list("Project",
                                       fields=["name", "project_start_date", "project_end_date",
                                               "total_project_amount_in_usd", "your_organization"],
                                       filters={
                                           "sub_project": 1
                                       }
                                       )

    for project in list_of_projects:
        project_qualifies = True
        # check sector qualification
        if json_filters["sector"] == "":
            pass
        else:
            # filter using sector
            list_of_projects_sector = frappe.get_list("Sector Alignment Table",
                                                      fields=["name"],
                                                      filters={
                                                          "parent": project.name,
                                                          "parenttype": "Project",
                                                          "parentfield": "sector_alignment_table",
                                                          "sector": json_filters["sector"]
                                                      }
                                                      )

            if len(list_of_projects_sector) == 0:
                #    falsify qualification
                project_qualifies = False

        # check if the type qualification
        if json_filters["type"]:
            project_org_type = frappe.get_doc(
                "Organization", project.your_organization).organization_type
            if project_org_type == json_filters["type"]:
                pass
            else:
                project_qualifies = False
        else:
            pass

        #!HACK: add year filter
        year = frappe.db.sql(
            'select current_registration_year from tabOrganization where name="{}"'.format(project.your_organization))

        if json_filters['year'] == '':
            pass
        elif json_filters['year'] == year[0][0]:
            pass
        else:
            continue

        # check location type qualification
        if json_filters["location"] == "":
            pass
        else:
            # filter using sector
            list_of_projects_sector = frappe.get_list("Project Location",
                                                      fields=["name"],
                                                      filters={
                                                          "parent": project.name,
                                                          "parenttype": "Project",
                                                          "parentfield": "project_location_table",
                                                          "region": json_filters["location"]
                                                      }
                                                      )

            if len(list_of_projects_sector) == 0:
                #    falsify qualification
                project_qualifies = False

        if project_qualifies:
            # get the implementing organizations
            list_of_sector_docs = frappe.get_list("Sector Alignment Table",
                                                  fields=["*"],
                                                  filters={
                                                      "parent": project.name,
                                                      "parenttype": "Project",
                                                      "parentfield": "sector_alignment_table",
                                                      "yes": 1
                                                  }
                                                  )
            list_of_relevant_sectors = []
            for sector_doc in list_of_sector_docs:
                list_of_relevant_sectors.append(sector_doc.sector)

            filtered_list.append(
                {"project": project, "sectors": list_of_relevant_sectors})

    return filtered_list
