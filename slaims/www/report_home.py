from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html

# def get_context(context):
#     context['users'] = frappe.get_all('User')

# @frappe.whitelist(allow_guest = True)
# def get_search_results_1():
#     # get all the pillars
#     list_of_projects = frappe.get_list("Project",
#         fields=["*"],
#         filters = {
#             "master_project":1,
#         }
#     )
    
#     print "*"*80
#     project_and_locations = []
#     for project in list_of_projects:

#         locations_per_project = []
#         combined_key_valued = {}
#         list_of_locations = frappe.get_list("Project Location",
#             fields=["*"],
#             filters = {
#                 "parent":project.name,
#                 "parenttype":"Project",
#                 "parentfield":"project_location_table"
#             }
#         )
    
#         for location in list_of_locations:
#             full_location_name = location.region
#             locations_per_project.append(full_location_name.split("/")[0])

#         # print locations_per_project
#         combined_key_valued["project"] = project
#         combined_key_valued["locations"] = locations_per_project
#         project_and_locations.append(combined_key_valued)

#     print project_and_locations

#     return project_and_locations