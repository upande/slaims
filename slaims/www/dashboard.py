from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from frappe.model.document import Document
import random
import requests
import pymysql.cursors
import json
from datetime import datetime

from collections import defaultdict, OrderedDict


def get_context(context):

    context.no_cache = True

    now = datetime.now()
    today_date = now.strftime('%Y-%m-%d')

    # organization_type_list = [org[0] for org in organization_type]

    orgs_by_type = frappe.db.sql(
        'select count(name), organization_type from `tabOrganization` group by organization_type')

    organizations_by_type = {}

    for i in range(len(orgs_by_type)):
        organizations_by_type.update({orgs_by_type[i][1]: orgs_by_type[i][0]})

    region_names = ["Marodijeh/Maroodijeex", "Sanag/Sanaag", "Central/Hargeisa",
                    "Sool/Sool", "Togdheer/Togdheer", "Awdal/Awdal", "Sahil/Saaxil",
                    ]

    year_list = [year[0]
                 for year in frappe.db.sql('select year from tabYear')]

    # def find_totals():

    #     region_total = []
    #     for region in regions:
    #         i = 0
    #         totals = frappe.db.sql(
    #             'select count(name) from `tabOrganization` where {}=1'.format(region))
    #         region_total.append(totals[i][0])
    #         i += 1

    #     return region_total

    def get_annual_project_expenditure():
        overall_dict = {}
        for year in year_list:
            amount_list = []
            # get total project amount for that year

            annual_report_amount = frappe.db.sql(
                'select total_project_amount from `tabAnnual Report Form` where annual_report_year={} and status="Approved"'.format(year))

            list_of_npres = frappe.db.sql(
                'SELECT name FROM `tabNon Project Related Expenditure` where year={} AND status="Approved"'.format(year))

            amount_list.extend(
                [i[0] for i in annual_report_amount if annual_report_amount])

            for npre in [npre[0] for npre in list_of_npres]:
                npre_region_totals = frappe.get_list('Disbursement Report Table General',
                                                     fields=[
                                                         'total'],
                                                     filters={
                                                         "parent": npre,
                                                         "parenttype": "Non Project Related Expenditure",
                                                         "parentfield": "disbursement_grid_total",
                                                     }
                                                     )
                amount_list.extend(
                    [i['total'] for i in npre_region_totals if 'total' in i])

            for npre in [npre[0] for npre in list_of_npres]:
                npre_sector_totals = frappe.get_list('Disbursement Report Table General',
                                                     fields=[
                                                         'total'],
                                                     filters={
                                                         "parent": npre,
                                                         "parenttype": "Non Project Related Expenditure",
                                                         "parentfield": "disbursement_total_sectors",
                                                     }
                                                     )
                amount_list.extend(
                    [i['total'] for i in npre_sector_totals if 'total' in i])
            if amount_list:
                overall_dict.update({year: sum(amount_list)})
            else:
                pass

        return overall_dict

    # get ongoing projects by funder type
    def get_ongoing_projects():

        ongoing_projects_list = []

        ongoing_projects = frappe.db.sql(
            'select name from `tabProject` where project_end_date >= "{}"'.format(today_date))

        for project in ongoing_projects:
            project = project[0]
            funder_organization = frappe.db.sql(
                'select your_organization from tabProject where name="{}"'.format(project))
            funder_organization = funder_organization[0][0]

            funder_organization_type = frappe.db.sql(
                'select organization_type from tabOrganization where name="{}"'.format(funder_organization))

            ongoing_projects_tuple = (funder_organization_type[0][0], project)

            ongoing_projects_list.append(ongoing_projects_tuple)

        ongoing_projects_dict = defaultdict(list)

        for key, value in ongoing_projects_list:
            ongoing_projects_dict[key].append(value)

        ongoing_projects_dict = dict(ongoing_projects_dict)

        for key in ongoing_projects_dict:
            ongoing_projects_dict[key] = len(ongoing_projects_dict[key])

        return ongoing_projects_dict

    def yearly_regional_expenditure():

        overall_dict = {}

        for year in year_list:
            regional_total = []
            list_of_reports = frappe.db.sql(
                'SELECT name from `tabAnnual Report` where annual_report_year={} AND status="Approved"'.format(year))

            list_of_npres = frappe.db.sql(
                'SELECT name FROM `tabNon Project Related Expenditure` where year={} AND status="Approved"'.format(year))

            for report in [report[0] for report in list_of_reports]:
                list_of_locations = frappe.get_list("Regional Distribution Table",
                                                    fields=[
                                                        "region", "amount"],
                                                    filters={
                                                        "parent": report,
                                                        "parenttype": "Annual Report",
                                                        "parentfield": "regional_distribution_table",
                                                    }
                                                    )

                regional_total.extend(
                    [region['amount'] for region in list_of_locations if 'amount' in region])

            for npre in [npre[0] for npre in list_of_npres]:
                organization_amounts_in_npre = frappe.get_list('Disbursement Report Table General',
                                                               fields=[
                                                                   'region', 'total'],
                                                               filters={
                                                                   "parent": npre,
                                                                   "parenttype": "Non Project Related Expenditure",
                                                                   "parentfield": "disbursement_grid_total",
                                                               }
                                                               )

                regional_total.extend(
                    [npre['total'] for npre in organization_amounts_in_npre if 'total' in npre])
            if regional_total:
                overall_dict.update(
                    {year: sum(regional_total)})
            else:
                pass

        return overall_dict

    def yearly_sector_expenditure():
        overall_dict = {}

        for year in year_list:
            sector_total = []

            list_of_reports = frappe.db.sql(
                'SELECT name from `tabAnnual Report` where annual_report_year={} AND status="Approved"'.format(year))

            list_of_npre_sectors = frappe.db.sql(
                'SELECT name FROM `tabNon Project Related Expenditure` where year={} AND status="Approved"'.format(year))

            for report in [report[0] for report in list_of_reports]:
                list_of_sectors = frappe.get_list("Pillar Budget Table",
                                                  fields=["sector", "amount"],
                                                  filters={
                                                      "parent": report,
                                                      "parenttype": "Annual Report",
                                                      "parentfield": "ndp_pillar_alignment",
                                                  }
                                                  )

                sector_total.extend(
                    [sector['amount'] for sector in list_of_sectors if 'amount' in sector])

            for npre in [npre[0] for npre in list_of_npre_sectors]:
                organization_amounts_in_npre = frappe.get_list('Disbursement Report Total Sectors',
                                                               fields=[
                                                                   "sector", "total"],
                                                               filters={
                                                                   "parent": npre,
                                                                   "parenttype": "Non Project Related Expenditure",
                                                                   "parentfield": "disbursement_total_sectors",
                                                               }
                                                               )

                sector_total.extend(
                    [npre['total'] for npre in organization_amounts_in_npre if 'total' in npre])

            if sector_total:
                overall_dict.update(
                    {year: sum(sector_total)})
            else:
                pass

        return overall_dict

    def yearly_staffing_numbers():
        local_staff = {}
        international_staff = {}

        year = frappe.db.sql('select year from tabYear')
        year_list = [year[0] for year in year]

        for year in year_list:
            local_staff_ = frappe.db.sql(
                'select sum(managerial_or_program_staff) + sum(admin_or_support_staff) from tabOrganization where current_registration_year={} and status="Approved"'.format(year))
            if local_staff_[0][0]:
                local_staff.update({year: local_staff_[0][0]})
            else:
                pass

            international_staff_ = frappe.db.sql(
                'select sum(managerial_or_program_staff_2) + sum(admin_or_support_staff_2) from tabOrganization where current_registration_year={} and status="Approved"'.format(year))
            if international_staff_[0][0]:
                international_staff.update({year: international_staff_[0][0]})
            else:
                pass

        return local_staff, international_staff

    annual_expenditure = get_annual_project_expenditure()
    ongoing_projects_by_funders = get_ongoing_projects()
    regional_expenditure = yearly_regional_expenditure()
    sector_expenditure = yearly_sector_expenditure()
    yearly_staffing_numbers = yearly_staffing_numbers()

    yearly_local_staff = yearly_staffing_numbers[0]
    yearly_international_staff = yearly_staffing_numbers[1]

    context['test'] = yearly_regional_expenditure()

    context['yearly_local_staff_year'] = [
        'Unspecified' if key is None else key for key, value in sorted(yearly_local_staff.items())]
    context['yearly_local_staff_total'] = [
        value[0] if isinstance(value, list) else value for key, value in sorted(yearly_local_staff.items())]

    context['yearly_international_staff_year'] = [
        'Unspecified' if key is None else key for key, value in sorted(yearly_international_staff.items())]
    context['yearly_international_staff_total'] = [
        value[0] if isinstance(value, list) else value for key, value in sorted(yearly_international_staff.items())]

    context['regional_expenditure_year'] = [key for key,
                                            value in sorted(regional_expenditure.items())]
    context['regional_expenditure_totals'] = [
        value for key, value in sorted(regional_expenditure.items())]

    context['sector_expenditure_year'] = [key for key,
                                          value in sorted(sector_expenditure.items())]
    context['sector_expenditure_totals'] = [
        value for key, value in sorted(sector_expenditure.items())]

    context['funder_type'] = ['Unspecified' if key is None else key for key,
                              value in ongoing_projects_by_funders.items()]
    context['ongoing_projects'] = [value[0] if isinstance(
        value, list) else value for key, value in ongoing_projects_by_funders.items()]

    context['annual_expenditure_year'] = [
        'Unspecified' if key is None else key for key, value in sorted(annual_expenditure.items())]
    context['annual_expenditure_total'] = [
        value for key, value in sorted(annual_expenditure.items())]
    context['regions'] = region_names
    context['no_of_projects'] = len(frappe.db.sql(
        'select name from `tabProject` where master_project=1'))
    context['no_of_orgs'] = len(frappe.db.sql(
        'select name from `tabOrganization`'))
    context['users'] = len(frappe.db.sql('select name from `tabUser`'))
    context['org_count'] = [value for key,
                            value in organizations_by_type.items()]
    context['org_type'] = [key for key, value in organizations_by_type.items()]
