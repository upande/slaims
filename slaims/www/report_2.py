from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json


# def get_context(context):
#     context['users'] = frappe.get_all('User')

@frappe.whitelist(allow_guest=True)
def get_search_results(args):
    # get all the pillars
    json_filters = json.loads(args)
    list_of_organization = frappe.get_list("Organization",
                                           fields=["*"],
                                           filters={
                                           }
                                           )

    requested_results = []
    # current section
    for organization in list_of_organization:
        organization_requested = False
        locations_per_organization = []
        combined_key_valued = {}

        # sectors filter
        if json_filters["sector"] == "":
            organization_requested = True
        else:
            requested_sector = json_filters["sector"]
            list_of_sectors = frappe.get_list("Sector Alignment Table",
                                              fields=["*"],
                                              filters={
                                                  "parent": organization.name,
                                                  "parenttype": "Organization",
                                                  "parentfield": "section_alignment_table",
                                                  "sector": requested_sector,
                                                  "yes": 1
                                              }
                                              )

            if len(list_of_sectors) > 0:
                organization_requested = True
            else:
                continue

        year = frappe.db.sql(
            'select current_registration_year from tabOrganization where name="{}"'.format(organization.name))

        if json_filters['year'] == '':
            organization_requested = True
        elif json_filters['year'] == year[0][0]:
            organization_requested = True
        else:
            continue

        # organization_type filter
        if json_filters["type"] == "":
            organization_requested = True
        else:
            requested_type = json_filters["type"]
            if organization.organization_type == requested_type:
                organization_requested = True
            else:
                continue

        # location filter
        requested_location = json_filters["location"]

        if requested_location == "":

            # add all the selected locations
            if organization.marodijeh_check == 1:
                locations_per_organization.append("Marodijeh")

            if organization.sanag_check == 1:
                locations_per_organization.append("Sanag")

            if organization.sool_check == 1:
                locations_per_organization.append("Sool")

            if organization.togdheer_check == 1:
                locations_per_organization.append("Togdheer")

            if organization.awdal_check == 1:
                locations_per_organization.append("Awdal")

            if organization.sahil_check == 1:
                locations_per_organization.append("Sahil")

            # add code for Central/Hargeisa Below
        else:
            # only add locations that have been selected
            if requested_location == "Marodijeh/Maroodijeex":
                if organization.marodijeh_check == 1:
                    locations_per_organization.append("Marodijeh")
                else:
                    continue

            elif requested_location == "Sanag/Sanaag":
                if organization.sanag_check == 1:
                    locations_per_organization.append("Sanag")
                else:
                    continue

            elif requested_location == "Sool/Sool":
                if organization.sool_check == 1:
                    locations_per_organization.append("Sool")
                else:
                    continue

            elif requested_location == "Togdheer/Togdheer":
                if organization.togdheer_check == 1:
                    locations_per_organization.append("Togdheer")
                else:
                    continue

            elif requested_location == "Awdal/Awdal":
                if organization.awdal_check == 1:
                    locations_per_organization.append("Awdal")
                else:
                    continue

            elif requested_location == "Sahil/Saaxil":
                if organization.sahil_check == 1:
                    locations_per_organization.append("Sahil")
                else:
                    continue

            elif requested_location == "Central/Hargeisa":
                if organization.sahil_check == 1:
                    locations_per_organization.append("Sahil")
                else:
                    continue

        if organization_requested:
            combined_key_valued["organization"] = organization
            combined_key_valued["locations"] = locations_per_organization
            requested_results.append(combined_key_valued)

    # finally return the results
    return requested_results
