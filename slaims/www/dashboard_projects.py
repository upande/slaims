from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from frappe.model.document import Document
import random
import requests
import pymysql.cursors
import json
from datetime import datetime
from .report_10 import get_search_results

from collections import Counter

# @frappe.whitelist(allow_guest = True)
# def get_project_location():

#     # get all master projects
#     list_of_projects = frappe.get_list('Project',
#         fields = ['name'],

#         filters = {
#             'master_project': 1,
#         }
#     )
#     # find all locations for the project
#     project_locations = []
#     for project in list_of_projects:
#         project_location = frappe.get_list('Project Location',
#             fields = ['region', 'district'],
#             filters = {
#                 'parent': project.name,
#                 'parenttype': 'Project',
#                 'parentfield': 'project_location_table',
#             }
#         )
#         if len(project_location) == 0:
#             pass
#         else:
#             for ln in range(len(project_location)):
#                 # project_location_tuple = (project_location[ln].region)
#                 project_locations.append(project_location[ln].region)

#     project_location_dict = dict(Counter(project_locations))

#     return project_location_dict


def get_context(context):

    context.no_cache = True

    organization_type = frappe.db.sql(
        'select distinct(organization_type) from `tabOrganization`')

    organization_type_list = [org[0] for org in organization_type]

    regions = ['marodijeh_check', 'sanag_check', 'hargeisa_region', 'sool_check',
               'togdheer_check', 'awdal_check', 'sahil_check', ]

    orgs_by_type = frappe.db.sql(
        'select count(name), organization_type from `tabOrganization` group by organization_type')
    org_count = []
    org_type = []

    for i in range(len(orgs_by_type)):
        org_count.append(orgs_by_type[i][0])

    for i in range(len(orgs_by_type)):
        org_type.append(orgs_by_type[i][1])

    # list of all current regions
    region_names = ["Marodijeh/Maroodijeex", "Sanag/Sanaag", "Central/Hargeisa",
                    "Sool/Sool", "Togdheer/Togdheer", "Awdal/Awdal", "Sahil/Saaxil",
                    ]

    # Function to find total expenditure per region
    def find_totals():

        region_total = []
        for region in regions:
            i = 0
            totals = frappe.db.sql(
                'select sum(total_project_amount) from `tabProject Registration Form` where {}=1 AND status="Approved"'.format(region))
            region_total.append(totals[i][0])
            i += 1
        region_total = [0 if i == None else i for i in region_total]
        return region_total

    # Function to determine total projects per region
    # ! Doubtful
    def find_region_project_totals():

        project_region_total = []
        for region in regions:
            i = 0
            totals = frappe.db.sql(
                'select count(name) from `tabProject Registration Form` where {}=1 AND status="Approved"'.format(region))
            project_region_total.append(totals[i][0])
            i += 1
        project_region_total = [
            0 if i == None else i for i in project_region_total]
        return project_region_total

    # Project completion by region
    def completed_project_by_org_type():
        completed_project_region = []
        for org in organization_type_list:
            i = 0
            totals = frappe.db.sql(
                'select sum(that_are_completed) from `tabOrganization` WHERE organization_type="{}"'.format(org))
            completed_project_region.append(totals[i][0])
            i += 1
        completed_project_region = [
            0 if i == None else i for i in completed_project_region]
        return completed_project_region

    def completion_status_numbers():
        status = ['confirmed_to_start_during_current_year', 'that_are_ongoing',
                  'that_are_completed', 'number_of_projects_confirmed_during_the_current_year']
        completion_status_numbers = []
        for sts in status:
            i = 0
            totals = frappe.db.sql(
                'select sum({}) from `tabOrganization`'.format(sts))
            completion_status_numbers.append(totals[i][0])
            i += 1
        completion_status_numbers = [i for i in completion_status_numbers]
        return completion_status_numbers

    #  # completed project by region
    # def completed_project_by_org_type():
    #     completed_project_region = []
    #     for org in organization_type_list:
    #         i = 0
    #         totals = frappe.db.sql('select sum(that_are_completed) from `tabOrganization` WHERE organization_type="{}"'.format(org))
    #         completed_project_region.append(totals[i][0])
    #         i += 1
    #     completed_project_region = [0 if i==None else i for i in completed_project_region]
    #     return completed_project_region

     # confirmed_during current year by region

    def confirmed_during_current_year():
        confirmed_current_year = []
        for org in organization_type_list:
            i = 0
            totals = frappe.db.sql(
                'select sum(number_of_projects_confirmed_during_the_current_year) from `tabOrganization` WHERE organization_type="{}"'.format(org))
            confirmed_current_year.append(totals[i][0])
            i += 1
        confirmed_current_year = [
            0 if i == None else i for i in confirmed_current_year]
        return confirmed_current_year

     # Project ongoing by region
    def ongoing_projects_region():
        ongoing_projects_region = []
        for org in organization_type_list:
            i = 0
            totals = frappe.db.sql(
                'select sum(that_are_ongoing) from `tabOrganization` WHERE organization_type="{}"'.format(org))
            ongoing_projects_region.append(totals[i][0])
            i += 1
        ongoing_projects_region = [
            0 if i == None else i for i in ongoing_projects_region]
        return ongoing_projects_region

     # Confirmed to start during the current year
    def confirmed_to_start():
        confirmed_to_start_region = []
        for org in organization_type_list:
            i = 0
            totals = frappe.db.sql(
                'select sum(confirmed_to_start_during_current_year) from `tabOrganization` WHERE organization_type="{}"'.format(org))
            confirmed_to_start_region.append(totals[i][0])
            i += 1
        confirmed_to_start_region = [
            0 if i == None else i for i in confirmed_to_start_region]
        return confirmed_to_start_region

    def get_project_location():

        # get all master projects
        list_of_projects = frappe.get_list('Project',
                                           fields=['name'],

                                           filters={
                                               'master_project': 1,
                                           }
                                           )
        # find all locations for the project
        project_locations = []
        for project in list_of_projects:
            project_location = frappe.get_list('Project Location',
                                               fields=['region', 'district'],
                                               filters={
                                                   'parent': project.name,
                                                   'parenttype': 'Project',
                                                   'parentfield': 'project_location_table',
                                               }
                                               )
            if len(project_location) == 0:
                pass
            else:
                for ln in range(len(project_location)):
                    # project_location_tuple = (project_location[ln].region)
                    project_locations.append(project_location[ln].region)

        project_location_dict = dict(Counter(project_locations))

        return project_location_dict

    # context variables passed on to dahboard_projects.html template
    context['project_location'] = get_project_location()
    context['complete_region'] = completed_project_by_org_type()
    context['confirmed_region'] = confirmed_during_current_year()
    context['ongoing_region'] = ongoing_projects_region()
    context['confirmed_to_start'] = confirmed_to_start()
    context['organization_totals_by_region'] = find_totals()
    context['project_region_totals'] = find_region_project_totals()
    context['regions'] = region_names
    context['no_of_projects'] = len(frappe.db.sql(
        'select name from `tabProject` where sub_project=1'))
    context['no_of_orgs'] = len(frappe.db.sql(
        'select name from `tabOrganization`'))
    context['users'] = len(frappe.db.sql('select name from `tabUser`'))
    context['org_count'] = org_count
    context['org_type'] = org_type
    context['org_type_2'] = organization_type_list
    context['completion_status_numbers'] = completion_status_numbers()
