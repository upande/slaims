from __future__ import unicode_literals
import frappe

def get_context(context):
    staff = False
    if "Staff" in frappe.get_roles(frappe.session.user):
        staff = True
    else:
        staff = False
    context["staff"] = staff
    
    # get all the relevant questions
    questions = frappe.get_list("FAQs",
        fields=["question","answer"],
        filters = {
    })

    organized_questions_n_answers = []
    # loop through found questions and answers
    count = 0
    for question in questions:
        count += 1
        question_dict = {'question':question['question'],\
            'answer':question['answer'],'count':count}

        # append to organized_questions_n_answers
        organized_questions_n_answers.append(question_dict)

    context['questions'] = organized_questions_n_answers
    # return the context
    return context