from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json

import requests
from collections import defaultdict, OrderedDict

from requests.exceptions import HTTPError


def get_organizations_based_filters(status, organization_type):
    '''
    Get all the organization based on given filters
    '''
    if status:
        filter_status = status
    else:
        filter_status = None

    if organization_type:
        filter_type = organization_type
    else:
        filter_type = None

    list_of_organizations = frappe.get_list("Organization",
                                            fields=["name", "acronym"],
                                            filters={
                                                "status": 'Approved',
                                                "organization_type": filter_type
                                            }
                                            )
    return list_of_organizations


def get_projects_based_filters(location, sector, year, organization):

    if organization:

        project_organization = frappe.db.sql(
            'SELECT your_organization from `tabProject`')

        organization_filtered = frappe.db.sql(
            'select name from tabOrganization where organization_type="{}"'.format(organization))

        filter_organization = [organization[0]
                               for organization in organization_filtered]

        list_of_projs = []

        for i in filter_organization:
            proj = frappe.get_list("Project",
                                   fields=["*"],
                                   filters={
                                       "your_organization": i,
                                       'status': 'Approved',
                                   }
                                   )

            list_of_projs.extend(proj)

        list_of_projects = list_of_projs
    else:
        list_of_projects = frappe.get_list("Project",
                                           fields=["*"],
                                           filters={
                                               'status': 'Approved',
                                           }
                                           )

    project_list = []

    list_of_sector_results = frappe.get_list("Sector Alignment Table",
                                             fields=["*"],
                                             filters={
                                                 "parent": 'Test Project Multilateral-tm2',
                                                 "parentfield": "sector_alignment_table",
                                                 "parenttype": "Project",
                                                 "sector": sector,
                                             }
                                             )

    # print(list_of_sector_results, sector)

    # loop through all projects
    for project in list_of_projects:

        # filter by sector
        if sector:
            list_of_sector_results = frappe.get_list("Sector Alignment Table",
                                                     fields=["*"],
                                                     filters={
                                                         "parent": project.name,
                                                         "parentfield": "sector_alignment_table",
                                                         "parenttype": "Project",
                                                         "sector": sector,
                                                     }
                                                     )

            if list_of_sector_results:
                pass
            else:
                continue

        if location:
            list_of_location_results = frappe.get_list("Project Location",
                                                       fields=["*"],
                                                       filters={
                                                           "parent": project.name,
                                                           "parentfield": "project_location_table",
                                                           "parenttype": "Project",
                                                           "region": location,
                                                       }
                                                       )

            if list_of_location_results:
                pass

            else:
                continue
            
        if year:
            try:
                # fin_year_doc = frappe.get_doc("Year",year)
                # start_date = fin_year_doc.start_date
                # end_date = fin_year_doc.end_date
                
                # projects whose start or end dates are within financial year duration
                query = ('SELECT name,status,your_organization FROM tabProject WHERE "{}"' 
                         'BETWEEN YEAR(project_start_date) AND YEAR(project_end_date)'
                         'AND status = "Approved"').format(year) 

                filtered_projects = frappe.db.sql(query)

                if year == '':
                    pass
                elif project.name in [project[0] for project in filtered_projects]:
                    pass
                else:
                    continue
            except:
                continue

        project_list.append(project)

    # return the filtered list
    return project_list


@frappe.whitelist(allow_guest=True)
def get_organization_type():
    '''
    Get all the organization types
    '''
    list_of_organizations = frappe.db.sql(
        "select name from `tabOrganization Type`")
    return list_of_organizations


@frappe.whitelist(allow_guest=True)
def get_sectors():
    '''
    Get all the organization sectors
    '''
    try:
        list_of_sectors = frappe.db.sql(
            "select name from `tabNDP` where type='Sector'")
    except:
        pass

    return list_of_sectors

@frappe.whitelist(allow_guest=True)
def get_locations():
    '''
    Get all the regions
    '''
    list_of_regions = frappe.db.sql(
        "select name from `tabAdministrative Unit`")
    return list_of_regions


@frappe.whitelist(allow_guest=True)
def get_years_n_global_financial_year():
    '''
    Get all the years including the current global
    financial year
    '''
    return_dict = {}
    try:
        list_of_years = frappe.db.sql(
            "SELECT  name FROM tabYear ORDER BY rank DESC")
        return_dict['list_of_years'] = list_of_years
    except:
        pass
    # get the global finacial year
    try:
        current_financial_year = frappe.db.sql("SELECT value FROM tabSingles WHERE field = 'current_financial_year' \
            && doctype = 'Global Settings'")
        return_dict['current_financial_year'] = current_financial_year[0][0]
    except:
        pass
    # now return the dict
    return return_dict


# get funding status
@frappe.whitelist(allow_guest=True)
def get_funding_status():
    return ["Pending Verification", "Verified", "Pending Validation",
            "Validated", "Approved", "Complete"]

# get funding status
@frappe.whitelist(allow_guest=True)
def get_Project_funding_status():
    return ["Pipeline", "Committed", "Operational"]


@frappe.whitelist(allow_guest=True)
def exclude_missing_values():
    return ['Yes', 'No']


@frappe.whitelist(allow_guest=True)
def get_years():
    year_list = frappe.db.sql(
        'select year from tabYear')
    return sorted([year[0] for year in year_list])

