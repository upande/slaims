from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from .reusable_functions import get_projects_based_filters, get_sectors


@frappe.whitelist(allow_guest=True)
def get_search_results(args):
    '''
    Function that pulls data from the datatabase
    based on the given filters
    '''
    # given filters
    json_filters = json.loads(args)

    # fitler projects using different filters
    filtered_projects = get_projects_based_filters(
        json_filters['location'], json_filters['sector'], json_filters['year'], json_filters['type'])

    # get all sectors
    all_sectors = get_sectors()

    sector_keys = {}
    for unique_sector in all_sectors:
        sector_keys[unique_sector[0]] = 0

    list_of_project_amount_n_organizations = {}
    # loop through projects getting reports
    for project in filtered_projects:
        current_organization = project['your_organization']
        current_organization_acronym = project['organization_acronym']
        # check if a key for the organization already exists in
        # the list_of_project_amount_n_organizations
        if current_organization in list_of_project_amount_n_organizations.keys():
            pass
        else:
            # add the keys to the list
            list_of_project_amount_n_organizations[current_organization] = {
                key: value for (key, value) in sector_keys.items()}

        # check if project has subproject
        sub_project_list = frappe.get_list('Project',
                                           fields=[
                                               'name', 'link_to_master_project'],

                                           filters={
                                               'sub_project': 1,
                                               'link_to_master_project': project.name,
                                           }
                                           )

        # if subprojects exist get associated annual reports
        subproject_reports = []
        if sub_project_list:
            for subproject in sub_project_list:
                # get associated annual report
                sub_project_report = frappe.get_list('Annual Report',
                                                     fields=['name'],
                                                     filters={
                                                         'project_title': subproject.name,
                                                     }
                                                     )

                if sub_project_report in subproject_reports:
                    pass
                else:
                    subproject_reports.extend(sub_project_report)

        else:
            pass

        list_of_reports = frappe.get_list("Annual Report",
                                          fields=["name"],
                                          filters={
                                              "project_title": project.name,
                                          }
                                          )

        # extend subproject reports if they exist
        if subproject_reports:
            list_of_reports.extend(subproject_reports)
        else:
            pass

        # get subproject npres
        subproject_npres = []
        if sub_project_list:
            for subproject in sub_project_list:
                # get associated annual report
                sub_project_npre = frappe.get_list('Non Project Related Expenditure',
                                                   fields=['*'],
                                                   filters={
                                                       'full_name_of_the_organization': subproject.your_organization,
                                                   }
                                                   )

                if sub_project_npre in subproject_npres:
                    pass
                else:
                    subproject_npres.extend(sub_project_npre)

        else:
            pass

        # get list of npres
        list_of_npres = frappe.get_list('Non Project Related Expenditure',
                                        fields=['*'],
                                        filters={
                                            'full_name_of_the_organization': project.your_organization,
                                        }
                                        )

        # extend list of npres if subproject npres exist
        if subproject_npres:
            list_of_npres.extend(subproject_npres)
        else:
            pass

        # loop through all linked reports
        # list_of_distribution_sectors = []
        for report in list_of_reports:
            # get a list of all sectors linked to the project under Pillar Budget Table
            list_of_distribution_sectors = frappe.get_list("Pillar Budget Table",
                                                           fields=[
                                                               "sector", "amount"],
                                                           filters={
                                                               "parent": report.name,
                                                               "parenttype": "Annual Report",
                                                               "parentfield": "ndp_pillar_alignment",
                                                           }
                                                           )

            for distribution_sector in list_of_distribution_sectors:
                list_of_project_amount_n_organizations[current_organization][
                    distribution_sector.sector] += distribution_sector.amount

        for npre in list_of_npres:
            sector_amounts_in_npre = frappe.get_list('Disbursement Report Total Sectors',
                                                     fields=['*'],
                                                     filters={
                                                         "parent": npre.name,
                                                         "parenttype": "Non Project Related Expenditure",
                                                         "parentfield": "disbursement_total_sectors",
                                                     }
                                                     )
            try:
                for sector_total in sector_amounts_in_npre:
                    list_of_project_amount_n_organizations[current_organization][
                        sector_total.sector] += sector_total.total
            except:
                pass

        list_of_project_amount_n_organizations[current_organization]['acronym'] = current_organization_acronym

    # return the final list
    return list_of_project_amount_n_organizations.items()
