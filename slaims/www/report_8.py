from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from .reusable_functions import get_projects_based_filters

@frappe.whitelist(allow_guest = True)
def get_search_results(args):
    '''
    Function that pulls data from the datatabase
    based on the given filters
    '''
    # given filters
    json_filters = json.loads(args)

    # fitler projects using different filters
    filtered_projects = get_projects_based_filters(json_filters['location'],json_filters['sector'], json_filters['year'], json_filters['type'])

    list_of_project_plans_reports = []
    # loop through projects getting reports
    for project in filtered_projects:
        # holder of report and plan amounts per organization
        plan_n_report_amount_per_organization = {"project":project,"plan_report":{}}

        # get a list of all plans linked to the project
        list_of_plans = frappe.get_list("Annual Plan",
            fields=["*"],
            filters = {
                "project_title":project.name,
            }
        ) 

        for plan in list_of_plans:
            # check if the organization's key already exists
            if plan['full_name_of_the_organization'] in plan_n_report_amount_per_organization['plan_report'].keys():
                plan_n_report_amount_per_organization['plan_report'][plan['full_name_of_the_organization']]['plan'] += plan['annual_budget_amount'] 
            else:
                # add the organization keys
                plan_n_report_amount_per_organization['plan_report'][plan['full_name_of_the_organization']] = {'plan':plan['annual_budget_amount'],'report':0}

        # get a list of all report linked to the project
        list_of_reports = frappe.get_list("Annual Report",
            fields=["*"],
            filters = {
                "project_title":project.name,
            }
        ) 

        # print plan_n_report_amount_per_organization
        for report in list_of_reports: 
            # check if the organization's key already exists
            if report['full_name_of_the_organization'] in plan_n_report_amount_per_organization['plan_report'].keys():
                plan_n_report_amount_per_organization['plan_report'][report['full_name_of_the_organization']]['report'] += report['annual_budget_amount'] 
            else:
                # add the organization keys
                plan_n_report_amount_per_organization['plan_report'][report['full_name_of_the_organization']] = {'plan':0,'report':report['annual_budget_amount']}

        # append the dict to return list
        list_of_project_plans_reports.append(plan_n_report_amount_per_organization)

    # return  list_of_project_reports
    return list_of_project_plans_reports
