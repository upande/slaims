from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from frappe.model.document import Document
import random
import requests
import pymysql.cursors
import json

def get_context(context):
    context['users'] = frappe.get_all('User')
    return context

@frappe.whitelist(allow_guest = True) 
def get_user():
    # get the logged in user
    username = frappe.session.user
    return username


@frappe.whitelist(allow_guest = True) 
def find_organizations(args):
    pass

@frappe.whitelist(allow_guest = True) 
def find_simalar_organization(args):
    json_args = json.loads(args)
    search_organization_name = json_args["organization_name"]
    search_acronymn = json_args["organization_acronym"]


	# holds all the organization found through this search
    all_organization = [] 
    
    def process_results(list_of_organizations):
        '''
        Function that will help to make the code Drier within
        this function by reducing repetitive tasks
        '''

        if len(list_of_organizations) > 0:
            for organization in list_of_organizations:
                organization_name = organization[0]
                all_organization.append(organization_name)
		
    
    list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '%{}%'".format(search_organization_name))
   
    # process the found results
    process_results(list_of_organizations)
    # full name of each word in the name query
    list_of_words_in_name = search_organization_name.split()
    # get rid of conjuctions
    common_conjuctions = ["of","the"]
    # loop through name parts
    for name_part in list_of_words_in_name:
        if name_part.lower() in common_conjuctions:
            pass
        else:
            # process results 
            list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '%{}%'".format(name_part))
            process_results(list_of_organizations)
    
    # starting 2 letters letter query
    first_two_letters = search_organization_name[:2]
    list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '{}%'".format(first_two_letters))
    if len(list_of_organizations) > 0:
        process_results(list_of_organizations)
    else:
        pass

    # Acronym matches
    # full name of acronym matches
    search_acronymn = search_organization_name
    list_of_organizations = frappe.db.sql("select name from `tabOrganization` where acronym like '%{}%'".format(search_acronymn))
    if len(list_of_organizations) > 0:
        process_results(list_of_organizations)
    else:
        pass

    # loop through unique organizations
    unique_results =  set(all_organization)
    return unique_results


@frappe.whitelist(allow_guest = True) 
def get_organization_type():
    '''
    Get all the organization types 
    '''
    list_of_organizations = frappe.db.sql("select name from `tabOrganization Type`")
    return list_of_organizations


@frappe.whitelist() 
# def create_org_form(args):
def find_create_org_form(args):
    '''
    Get all the organization types 
    '''
    json_args = json.loads(args)

    # use type
    org_type = json_args["new_org_type"]
    org_name = json_args["new_org_name"]
    org_acronym = json_args["new_org_acronym"]

    # check if an organization with the name exists
    list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name='{}'".format(org_name))
    if len(list_of_organizations)>0:
        return {"status":False,"error":"That Organization Name Already Exists"}
    else:
        pass
    
    def creating_name():
        '''
        Function that generates a unique name for the form
        '''
        # initialize the name as empty string
        form_name = ""
        complete_form_name = ""

        # loop through name parts
        for name_part in org_type.split():
            complete_form_name+=name_part+"-"

        # add the word form at the end
        form_name += org_type
        form_name += " Form"


        # if the form is a local form add a language
        if org_type == "Local NGO":
            # add the lanaguge of the form
            complete_form_name += "Form-"
            complete_form_name += json_args["language"]
            complete_form_name +=".BE."
            complete_form_name+=str(random.randint(10000,90000))

            # add language at the end of the form name
            form_name += " "
            form_name += json_args["language"]
        else:
            complete_form_name += "Form.BE."
            complete_form_name+=str(random.randint(10000,90000))

        # check if the complete name exists in the database
        list_of_organizations = frappe.db.sql("select name from `tab{}` where name='{}'".format(form_name,complete_form_name))
        if len(list_of_organizations)>0:
            creating_name()
        else:
            return {"name":complete_form_name,"form_name":form_name}
    
    validated_names = creating_name()

    # create connection
    connection = pymysql.connect(
            host='localhost',
            user='root',
            password='Empharse333',
            db='be19e6b811c911ad',
            charset='utf8mb4',
            cursorclass=pymysql.cursors.DictCursor
    )
    
    # create an organization
    try:
        with connection.cursor() as cursor: 
            # construct the sql syntax
            sql = "INSERT INTO `tabOrganization` (`name`,`full_name_of_the_organization`,`acronym`) VALUES ('{}','{}','{}')".format(org_name,org_name,org_acronym)
            # commit the changes
            cursor.execute(sql)

        # save changes to database
        connection.commit()
    finally:
        connection.close()

    connection = pymysql.connect(
            host='localhost',
            user='root',
            password='Empharse333',
            db='be19e6b811c911ad',
            charset='utf8mb4',
            cursorclass=pymysql.cursors.DictCursor
    )

    # create the organization form
    try:
        with connection.cursor() as cursor: 
            # construct the sql syntax
            sql = "INSERT INTO `tab{}` (`name`,`full_name_of_the_organization`,`acronym`) VALUES ('{}','{}','{}')".format(validated_names['form_name'],validated_names['name'],org_name,org_acronym)
            # commit the changes
            cursor.execute(sql)

        # save changes to database
        connection.commit()
    finally:
        connection.close()


    # assign the organization to the user
    assign_organization_to_user()

    # the form has been created now redirect the user
    return {"status":True,"form_name":validated_names['name']}


def assign_organization_to_user():
    '''
    Function that sets the users and the organization that the user
    is associated with
    '''
    pass



    