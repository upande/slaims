function extractTitle() {
  var title = "title";
  var temporalDivElement = document.createElement("div");
  temporalDivElement.innerHTML = html;
  return temporalDivElement.textContent || temporalDivElement.innerText || "";
}

function downloadCSV(csv, filename) {
  var csvFile;
  var downloadLink;

  var filename = filename ? filename + ".csv" : "csv_file.csv";

  csvFile = new Blob([csv], { type: "text/csv" });
  downloadLink = document.createElement("a");
  downloadLink.download = filename;
  downloadLink.href = window.URL.createObjectURL(csvFile);
  downloadLink.style.display = "none";
  document.body.appendChild(downloadLink);
  downloadLink.click();
}

function exportTableToExcel(tableID, filename) {
  var downloadLink;
  var dataType = "application/vnd.ms-excel";
  var tableSelect = document.getElementById(tableID);
  var tableHTML = tableSelect.outerHTML.replace(/ /g, "%20");

  // Specify file name
  filename = filename ? filename + ".xls" : "excel_data.xls";

  // Create download link element
  downloadLink = document.createElement("a");

  document.body.appendChild(downloadLink);

  if (navigator.msSaveOrOpenBlob) {
    var blob = new Blob(["\ufeff", tableHTML], {
      type: dataType,
    });
    navigator.msSaveOrOpenBlob(blob, filename);
  } else {
    downloadLink.href = "data:" + dataType + ", " + tableHTML;
    downloadLink.download = filename;
    downloadLink.click();
  }
}

function exportTableToPDF(id, reportTitle) {
  // var pdf = new jsPDF("l", "pt", "a4");
  // // var reportTitle = "Test";
  // var name = `${reportTitle}.pdf`;

  // source = $(`#${id}`)[0]; //table Id
  // specialElementHandlers = {
  //   "#bypassme": function (element, renderer) {
  //     return true;
  //   },
  // };

  // margins = {
  //   //table margins and width
  //   top: 80,
  //   bottom: 60,
  //   left: 40,
  //   width: 522,
  // };

  // pdf.fromHTML(
  //   source,
  //   margins.left,
  //   margins.top,
  //   {
  //     width: margins.width,
  //     elementHandlers: specialElementHandlers,
  //   },

  //   function (dispose) {
  //     pdf.save(`${name}`); //Filename
  //   },
  //   margins
  // );
  html2canvas(document.body, {
    onrendered: function (canvas) {
      var img = canvas.toDataURL("image/JPEG");
      var margin = 2;
      var imgWidth = 210 - 2 * margin;
      var pageHeight = 295 - 2 * margin;
      var imgHeight = (canvas.height * imgWidth) / canvas.width;
      var heightLeft = imgHeight;

      var doc = new jsPDF("p", "mm");
      var position = 0;

      doc.addImage(img, "JPEG", 0, position, imgWidth, imgHeight, "FAST");
      heightLeft -= pageHeight;

      while (heightLeft >= 0) {
        position = heightLeft - imgHeight;
        doc.addPage();
        doc.addImage(img, "JPEG", 0, position, imgWidth, imgHeight);
        heightLeft -= pageHeight;
      }
      doc.save(`${reportTitle}.pdf`);
    },
  });
}

function exportTableToCSV(id, filename) {
  var csv = [];
  var rows = document.querySelectorAll("table tr");

  for (var i = 0; i < rows.length; i++) {
    var row = [],
      cols = rows[i].querySelectorAll("td, th");

    for (var j = 0; j < cols.length; j++) row.push(cols[j].innerText);

    csv.push(row.join(","));
  }

  // Download CSV file
  downloadCSV(csv.join("\n"), filename);
}

function recordsPerPage() {
  if (localStorage.getItem("checked") == "true") {
    return 10000;
  } else {
    return 10;
  }
}

function thousandSeparator(x) {
  return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
}

// format currency to accounting standards in usd
const formatter = new Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
  minimumFractionDigits: 2,
});

function return_text_or_space(given_txt) {
  if (given_txt) {
    return given_txt;
  } else {
    return " ";
  }
}
