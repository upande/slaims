from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json

# def get_context(context):
#     context['users'] = frappe.get_all('User')

@frappe.whitelist(allow_guest = True)
def get_search_results(args):
    # get all the pillars
    json_filters = json.loads(args)
    
    list_of_organization = frappe.get_list("Organization",
        fields=["*"],
        filters = {
        }
    )

    requested_results = []
     # current section
    for organization in list_of_organization:
        organization_requested = False
        locations_per_organization = []
        combined_key_valued = {}

        # sectors filter
        if json_filters["sector"] == "All Sectors":
            organization_requested = True
        else:
            requested_sector = json_filters["sector"]
            list_of_sectors = frappe.get_list("Sector Alignment Table",
                fields=["*"],
                filters = {
                    "parent":organization.name,
                    "parenttype":"Organization",
                    "parentfield":"section_alignment_table",
                    "sector":requested_sector,
                    "yes":1
                }
            )

            if len(list_of_sectors) > 0:
                organization_requested = True
            else:
                continue

        # organization_type filter
        if json_filters["type"] == "All Organization Types":
            organization_requested = True

        else:
            requested_type = json_filters["type"]
            if organization.organization_type == requested_type:
                organization_requested = True
            else:
                continue


        if organization_requested:
            combined_key_valued["organization"] = organization
            combined_key_valued["locations"] = locations_per_organization
            requested_results.append(combined_key_valued)

    return requested_results


@frappe.whitelist(allow_guest = True) 
def get_organization_type():
    '''
    Get all the organization types 
    '''
    list_of_organizations = frappe.db.sql("select name from `tabOrganization Type`")
    return list_of_organizations