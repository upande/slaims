from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from .reusable_functions import get_projects_based_filters

@frappe.whitelist(allow_guest = True)
def get_search_results(args):
    '''
    Function that pulls data from the datatabase
    based on the given filters
    '''
    # given filters
    json_filters = json.loads(args)

    # fitler projects using different filters
    filtered_projects = get_projects_based_filters(json_filters['location'],json_filters['sector'], json_filters['year'], json_filters['type'])

    list_of_project_implementing_reporting_organizations = []
    # loop through projects getting implementing n reporting organizations
    for project in filtered_projects:
        project_implementors_n_reporting_dict = {}
        # fetch funding organization
        list_of_reporting_implemeting = frappe.get_list("Destination Implementing Reporting",
            fields=["*"],
            filters = {
                "parent":project.name,
                "parentfield":"destination_organization_table",
                "parenttype":"Project",
            }
        ) 

        project_implementors_n_reporting_dict['project'] = project
        project_implementors_n_reporting_dict['implementing_reporting'] = list_of_reporting_implemeting
        list_of_project_implementing_reporting_organizations.append(project_implementors_n_reporting_dict)  

    # return reulting list
    return  list_of_project_implementing_reporting_organizations

def get_context(context):

    total_implementing_partners = frappe.db.count('Destination Implementing Reporting',
     filters={
         'parentfield': 'destination_organization_table',
         'parenttype': 'Project'
     }
    )

    total_reporting_partners = frappe.db.count('Destination Implementing Reporting',
     filters={
         'parentfield': 'destination_organization_table',
         'parenttype': 'Project',
         'reporting_obligation': 1,
     }
    )

    total_projects = frappe.db.count('Project',
        filters={
            'sub_project': 1,
        }
    )

    context['total_projects'] = total_projects
    context['total_implementing_partners'] = total_implementing_partners
    context['total_reporting_partners'] = total_reporting_partners
