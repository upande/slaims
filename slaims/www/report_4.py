from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from .reusable_functions import get_projects_based_filters


@frappe.whitelist(allow_guest=True)
def get_search_results(args):
    # given filters
    json_filters = json.loads(args)

    # return filtered projects
    return get_projects_based_filters(json_filters['location'], json_filters['sector'], json_filters['year'], json_filters['type'])
