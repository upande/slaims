from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json

# def get_context(context):
#     context['users'] = frappe.get_all('User')

@frappe.whitelist(allow_guest = True)
def get_search_results_1(args):
    # print "*"*80

    # get all the pillars
    json_filters = json.loads(args)
    
    list_of_projects = frappe.get_list("Project",
        fields=["*"],
        filters = {
            "sub_project":1,
        }
    )

    project_and_locations = []

    for project in list_of_projects:
        project_requested = False
        locations_per_project = []
        combined_key_valued = {}

        # add sector filter
        if json_filters["sector"] == "":
            project_requested = True
        else:
            # list of sectors
            list_of_sectors = frappe.get_list("Sector Alignment Table",
                fields=["*"],
                filters = {
                    "parent":project.name,
                    "parenttype":"Project",
                    "parentfield":"sector_alignment_table",
                    "sector":json_filters["sector"]
                }
            )

            if len(list_of_sectors) > 0:
                project_requested = True
            else:
               continue 

        # add type filter
        if json_filters["type"] == "":
            project_requested = True
        else:
            # get the organization and its type here
            org_doc = frappe.get_doc("Organization",project['your_organization'])
            if org_doc.organization_type == json_filters["type"]:
                project_requested = True
            else:
                continue

        # add filters below
        if json_filters["location"] == "":
            project_requested = True

            list_of_locations = frappe.get_list("Project Location",
                fields=["*"],
                filters = {
                    "parent":project.name,
                    "parenttype":"Project",
                    "parentfield":"project_location_table",
                }
            )

            for location in list_of_locations:
                full_location_name = location.region
                locations_per_project.append(full_location_name.split("/")[0])
        else:
            requested_location = json_filters["location"]

            list_of_locations = frappe.get_list("Project Location",
                fields=["*"],
                filters = {
                    "parent":project.name,
                    "region":requested_location,
                    "parenttype":"Project",
                    "parentfield":"project_location_table",
                }
            )

            if len(list_of_locations) > 0:
                project_requested = True

                for location in list_of_locations:
                    full_location_name = location.region
                    locations_per_project.append(full_location_name.split("/")[0])
            else:
                continue
        
        if project_requested:
            combined_key_valued["project"] = project
            combined_key_valued["locations"] = locations_per_project
            project_and_locations.append(combined_key_valued)
    
    return project_and_locations