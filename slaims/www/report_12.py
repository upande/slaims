from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from .reusable_functions import get_projects_based_filters


@frappe.whitelist(allow_guest=True)
def get_search_results(args):
    '''
    Function that pulls data from the datatabase
    based on the given filters
    '''
    # given filters
    json_filters = json.loads(args)

    # fitler projects using different filters
    filtered_projects = get_projects_based_filters(
        json_filters['location'], json_filters['sector'], json_filters['year'], json_filters['type'])

    list_of_project_amount_n_sectors = []
    # loop through projects getting reports
    for project in filtered_projects:
        # holder of locations per project
        if project.master_project:
            sector_holder_per_project = {
                "project": project, "report_sectors": {}}

            # get all sectors
            list_of_sectors = frappe.get_list("NDP",
                                              fields=["name"],
                                              filters={
                                                  "type": "Sector"
                                              }
                                              )

            # create a key holding all sectors
            sector_keys = {}
            for unique_sector in list_of_sectors:
                sector_keys[unique_sector.name] = 0

            # add sector keys
            sector_holder_per_project['report_sectors'] = sector_keys

            # get a list of all reports linked to the project
            list_of_reports = frappe.get_list("Annual Report",
                                              fields=["*"],
                                              filters={
                                                  "project_title": project.name,
                                              }
                                              )

            subproject_list = frappe.get_list('Project',
                                              fields=['*'],
                                              filters={
                                                  'sub_project': 1,
                                                  'link_to_master_project': project.name,
                                              }
                                              )
            if subproject_list:
                subproject_list = [sub.name for sub in subproject_list]
                for sub in subproject_list:
                    subproject_report = frappe.get_list('Annual Report',
                                                        fields=['*'],
                                                        filters={
                                                            'project_title': sub,
                                                        }
                                                        )

                    list_of_reports.extend(subproject_report)
            else:
                pass

            if list_of_reports:
                # loop through the found reports
                for report in list_of_reports:
                    # get a list of all sectors linked to the project under Pillar Budget Table
                    list_of_sectors = frappe.get_list("Pillar Budget Table",
                                                      fields=["*"],
                                                      filters={
                                                          "parent": report.name,
                                                          "parenttype": "Annual Report",
                                                          "parentfield": "ndp_pillar_alignment",
                                                      }
                                                      )

                    # loop through linked projects
                    for sector in list_of_sectors:
                        # check if the project's key already exists
                        if sector['sector'] in sector_holder_per_project['report_sectors'].keys():
                            sector_holder_per_project['report_sectors'][sector['sector']
                                                                        ] += sector['amount']
                        else:
                            pass
            else:
                pass

            # append the dict to return list
            list_of_project_amount_n_sectors.append(sector_holder_per_project)

        else:
            pass

    # return  list_of_project_reports
    return list_of_project_amount_n_sectors
