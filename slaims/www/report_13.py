from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from .reusable_functions import get_projects_based_filters, get_locations


@frappe.whitelist(allow_guest=True)
def get_search_results(args):
    '''
    Function that pulls data from the datatabase
    based on the given filters
    '''
    # given filters
    json_filters = json.loads(args)

    # fitler projects using different filters
    filtered_projects = get_projects_based_filters(
        json_filters['location'], json_filters['sector'], json_filters['year'], json_filters['type'])

    # print('*' * 80)
    # print(json_filters)

    # get list of all regions
    all_locations = get_locations()

    location_keys = {}
    # create location keys
    for location in all_locations:
        location_keys[location[0]] = 0
    # print('g'*80)
    # print(location_keys)
    list_of_project_amount_n_organizations = {}

    # loop through projects getting reports
    for project in filtered_projects:
        current_organization = project['your_organization']
        current_organization_acronym = project['organization_acronym']
        # check if a key for the organization already exists in
        # the list_of_project_amount_n_organizations
        if project['your_organization'] in list_of_project_amount_n_organizations.keys():
            pass
        else:
            # add the key to the list
            list_of_project_amount_n_organizations[project['your_organization']] = {
                key: value for (key, value) in location_keys.items()}

        # get a list of all reports linked to the project
        list_of_reports = frappe.get_list("Annual Report",
                                          fields=["name", "project_title",
                                                  "full_name_of_the_organization"],
                                          filters={
                                              "project_title": project.name,
                                          }
                                          )

        list_of_npres = frappe.get_list('Non Project Related Expenditure',
                                        fields=['*'],
                                        filters={
                                            'full_name_of_the_organization': project.your_organization,
                                        }
                                        )

        for report in list_of_reports:
            # get a list of all locations linked to the project under Regional Distribution Table
            list_of_locations = frappe.get_list("Regional Distribution Table",
                                                fields=["region", "amount"],
                                                filters={
                                                    "parent": report.name,
                                                    "parenttype": "Annual Report",
                                                    "parentfield": "regional_distribution_table",
                                                }
                                                )

            try:
                # loop through linked locations
                for location in list_of_locations:
                    # add to the location amount for organization
                    list_of_project_amount_n_organizations[current_organization][location.region] += location.amount
            except:
                pass

        for npre in list_of_npres:
            organization_amounts_in_npre = frappe.get_list('Disbursement Report Table General',
                                                           fields=[
                                                               'region', 'total'],
                                                           filters={
                                                               "parent": npre.name,
                                                               "parenttype": "Non Project Related Expenditure",
                                                               "parentfield": "disbursement_grid_total",
                                                           }
                                                           )

           

            try:
                for location_amount in organization_amounts_in_npre:
                    list_of_project_amount_n_organizations[current_organization][location_amount.region] += int(
                        location_amount.total)
            except:
                pass

        # add organization acronym
        list_of_project_amount_n_organizations[current_organization]['acronym'] = current_organization_acronym

    return list_of_project_amount_n_organizations.items()
