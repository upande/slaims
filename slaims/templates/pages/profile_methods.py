from __future__ import unicode_literals

# stand python imports
from datetime import datetime
import json,random,requests

# frappe imports
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
from frappe.model.document import Document


# third party library imports
import pymysql.cursors

def get_user_organization(user_name):
    '''
    Function that gets the organization of the
    belonging to a given user
    '''
    list_of_organizations = frappe.get_list("Organization",
        fields=["*"],
        filters = {
            "user":user_name
    })

    if len(list_of_organizations) > 0:
        return list_of_organizations[0]
    else:
        return False


def get_organization(org_name):
    '''
    Function that gets the organization based on 
    the given name
    '''
    list_of_organizations = frappe.get_list("Organization",
                                fields=["*"],
                                filters = {
                                    "name":org_name
                            })

    if len(list_of_organizations) > 0:
        return list_of_organizations[0]
    else:
        return False

def check_if_Project_active(project_name,organization_name):
    '''
    Function that checks if a given project is active in a 
    given period i.e period between start_date and end_date
    input:
        project_name - string 
        organization_name - string
    output:
        boolean - True/False
    '''
    return_value = False
    # get start and end dates of org's financial year
    start_and_end_dates = get_start_n_end_date(organization_name)
    start_date = start_and_end_dates['start_date']
    end_date = start_and_end_dates['end_date']

    # projects whose start or end dates are within financial year duration
    sql1 = '''select name from tabProject where project_start_date between \
        "{}" and "{}" and sub_project = 1 and name = "{}" \
        || project_end_date between "{}" and "{}" and sub_project = 1 \
        and name = "{}"'''.format(start_date,end_date,project_name,\
        start_date,end_date,project_name) 
    project_list_sql1 = frappe.db.sql(sql1)
    # change the return value to True
    if len(project_list_sql1):
        return_value = True

    # projects whose start or end dates are outside financial year duration
    sql2 = '''select name from tabProject where project_start_date < "{}" \
        and project_end_date > "{}" and sub_project = 1 and name \
        = "{}"'''.format(start_date,end_date,project_name)
    project_list_sql2 = frappe.db.sql(sql2)
    # change the return value to True
    if len(project_list_sql2):
        return_value = True

    # return the value
    return return_value

def get_current_projects(organization_name):
    '''
    Function that gets all the current active projects i.e 
    projects whose duration is within the duration
    of the current finacial year of the organizaiton
    Input: 
        organization_name -string
    Output:
        list of current projects
    '''
    start_and_end_dates = get_start_n_end_date(organization_name)
    start_date = start_and_end_dates['start_date']
    end_date = start_and_end_dates['end_date']
    
    # get projects the is the destination organizations
    # projects whose start or end dates are within financial year duration
    sql1 = '''select name from tabProject where project_start_date between \
        "{}" and "{}" and sub_project = 1 and your_organization = "{}" \
        || project_end_date between "{}" and "{}" and sub_project = 1 \
        and your_organization = "{}"'''.format(start_date,end_date,organization_name,\
        start_date,end_date,organization_name) 
    project_list_sql1 = frappe.db.sql(sql1)
    # projects whose start or end dates are outside financial year duration
    sql2 = '''select name from tabProject where project_start_date < "{}" \
        and project_end_date > "{}" and sub_project = 1 and your_organization \
        = "{}"'''.format(start_date,end_date,organization_name)
    project_list_sql2 = frappe.db.sql(sql2)
    list_of_active_destination_projects = project_list_sql1 + project_list_sql2

    # lets get projects the organization is implementing
    implementing_projects_list = frappe.get_list("Destination Implementing Reporting",
        fields=["*"],
        filters = {
            'implementing_organization':organization_name,
            'parentfield':"destination_organization_table",
            'parenttype':"Project"
    })
    
    active_projects_org_is_implementing = []
    # loop through all the found project implementing docs
    for project_imp_doc in implementing_projects_list:
        project_name_from_imp_doc = project_imp_doc.parent
        # check if project is active
        project_is_active = check_if_Project_active(project_name_from_imp_doc,organization_name)
        if project_is_active:
            active_projects_org_is_implementing.append(project_name_from_imp_doc)
 
    # get all projects the user has reporting obligation
    reporting_projects_list = frappe.get_list("Destination Implementing Reporting",
        fields=["*"],
        filters = {
            'reporting_organization':organization_name,
            'parentfield':"destination_organization_table",
            'parenttype':"Project"
    })

    active_projects_org_is_reporting = []
    # loop through all the found project implementing docs
    for project_rep_doc in reporting_projects_list:
        project_name_from_rep_doc = project_rep_doc.parent
        # check if project is active
        project_is_active = check_if_Project_active(project_name_from_rep_doc,organization_name)
        if project_is_active:
            active_projects_org_is_reporting.append(project_name_from_rep_doc)

    # loop through projects to get all active projects
    list_of_unique_projects = []
    dict_of_unique_projects = {}
    # loop through list_of_active_destination_projects
    for active_destination_project in list_of_active_destination_projects:
        if active_destination_project[0] in list_of_unique_projects:
            # mark desitnation status as true
            dict_of_unique_projects[active_destination_project[0]]['destination_organization'] = True
        else:
            list_of_unique_projects.append(active_destination_project[0])
            # add the project name and status of destination as True
            dest_project_doc = frappe.get_doc("Project",active_destination_project[0])
            dict_of_unique_projects[active_destination_project[0]] = {'destination_organization':\
                True,'implementing_organization':False,'reporting_organization':False,\
                'project_doc':dest_project_doc,
            }

    # loop through active_projects_org_is_implementing
    for active_project_org_is_imp in active_projects_org_is_implementing:
        if active_project_org_is_imp in list_of_unique_projects:
            dict_of_unique_projects[active_project_org_is_imp]['implementing_organization'] = True    
        else:
            list_of_unique_projects.append(active_project_org_is_imp)
            # add the project name and status of destination as True
            imp_project_doc = frappe.get_doc("Project",active_project_org_is_imp)
            dict_of_unique_projects[active_project_org_is_imp] = {'destination_organization':\
                False,'implementing_organization':True,'reporting_organization':False,\
                'project_doc':imp_project_doc,
            }

    # loop through project the organizaiton is reporting for
    for active_project_org_is_rep in active_projects_org_is_reporting:
        if active_project_org_is_rep in list_of_unique_projects:
            dict_of_unique_projects[active_project_org_is_rep]['reporting_organization'] = True
        else:
            list_of_unique_projects.append(active_project_org_is_rep) 
            # add the project name and status of destination as True
            rep_project_doc = frappe.get_doc("Project",active_project_org_is_rep)
            dict_of_unique_projects[active_project_org_is_rep] = {'destination_organization':\
                False,'implementing_organization':False,'reporting_organization':True,\
                'project_doc':rep_project_doc
            }

    # create a list of key value pairs
    list_of_key_value_pairs = [v for k,v in dict_of_unique_projects.items()]
    # return list of key value pairs
    return list_of_key_value_pairs

def get_plans_for_each_project_obligation(current_organization,project_name,financial_year):
    '''
    Function that check a supposed plan linked to a project
    already exists
    input:
        project_name - str
        current_organization - str
        financial_year - str
    output:
        dictionary with
        status - Boolean (True/False)
        plan_status: str (depends on status)
    '''
    # check if any plan exists
    list_of_plans = frappe.get_list("Annual Plan",
        fields=["*"],
        filters = {
            "project_title": project_name,
            "full_name_of_the_organization":current_organization,
            "annual_plan_year":financial_year
    })

    if len(list_of_plans) > 0:
        return {'status':True,'plan_status':list_of_plans[0].status}
    else:
        return {'status':False}

def get_report_for_each_project_obligation(current_organization,project_name,financial_year):
    '''
    Function that check a supposed report linked to a project
    already exists
    input:
        project_name - str
        current_organization - str
        financial_year - str
    output:
        dictionary with
        status - Boolean (True/False)
        report_status: str (depends on status)
    '''
    # check if any report exists
    list_of_report = frappe.get_list("Annual Report",
        fields=["*"],
        filters = {
            "project_title": project_name,
            "full_name_of_the_organization":current_organization,
            "annual_report_year":financial_year
    })

    if len(list_of_report) > 0:
        return {'status':True,'report_status':list_of_report[0].status}
    else:
        return {'status':False}

def get_reports_linked_to_project(project,current_organization):
    '''
    Function that returns reports linked to a project
    if any exists
    '''
    # check the data type of organization passed to the 
    # function
    if type(current_organization) == str:
        requested_organization_name = current_organization
    else:
        requested_organization_name = current_organization['name']

    # get plans linked to a project from ints inception if any is missing
    project_start_date = project['project_start_date']
    project_start_year = project_start_date.year
    current_year = datetime.now().year

    list_of_reports_to_return = []

    def find_report_for_year(project_start_year,current_year):
        list_of_reports = frappe.get_list("Annual Report",
                fields=["*"],
                filters = {
                    "project_title": project['name'],
                    "annual_report_year":str(project_start_year)
            })

        # check if this is the current year
        if project_start_year == current_year:
            # check a report exists for each year
            if len(list_of_reports) > 0:
                # return the report for the current year
                list_of_reports_to_return.append({'current_year':current_year,'year':project_start_year,'report':list_of_reports[0]['status'],'project':project['name'],'status':True,'organization_name':requested_organization_name})
            else:
                # return False for the current year
                list_of_reports_to_return.append({'current_year':current_year,'year':project_start_year,'report':False,'project':project['name'],'status':False,'organization_name':requested_organization_name})

        else:
            # check a plan exists for previous years
            if len(list_of_reports) > 0:
                # plans exists for previous years
                pass  
            else:
                # return False for this year as a plan does not exist
                list_of_reports_to_return.append({'current_year':current_year,'year':project_start_year,'report':False,'project':project['name'],'status':False,'organization_name':requested_organization_name})
            
            # move the loop to the next year
            project_start_year += 1
            find_report_for_year(project_start_year,current_year)

    # call the find plans Function
    find_report_for_year(project_start_year,current_year)

    # now return the plans that the user needs to use
    return list_of_reports_to_return

@frappe.whitelist(allow_guest = True)
def get_link_org_renewal_form(args):
    '''
    Function that returns a link to organization renewal
    '''
    json_args = json.loads(args)
    pass


@frappe.whitelist(allow_guest = True)
def get_link_to_project_form(args):
    '''
    Function that returns a link to a project name
    '''
    json_args = json.loads(args)
    project_name = json_args['project_name']
    orgization_name = json_args['orgization_name']
 
    try:
        form_name = json_args['form_name']
        return frappe.utils.get_url_to_form("Project Registration Form",form_name)
    except:
        # check if renewal forms exist and arrange them in order tof phases
        list_of_renewal_forms = frappe.db.sql("select name from `tabProject Renewal Form` where phase_title = '{}' && status !='Cancelled' && select_your_organization = '{}'  ORDER BY phase DESC".format(project_name,orgization_name))
        
        if len(list_of_renewal_forms) > 0:
            # get the latest form on the list
            latest_renewal_form_name = list_of_renewal_forms[0][0]
            return frappe.utils.get_url_to_form("Project Renewal Form",latest_renewal_form_name)
        else:
            # check if registration forms exists
            list_of_registration_forms = frappe.db.sql("select name from `tabProject Registration Form` where sub_project_title = '{}' && status !='Cancelled' && select_your_organization = '{}'".format(project_name,orgization_name))
            if len(list_of_registration_forms) > 0:
                return frappe.utils.get_url_to_form("Project Registration Form",list_of_registration_forms[0][0])
            else:
                return False

@frappe.whitelist(allow_guest = True)
def get_link_to_plan_form(args):
    '''
    Function that returns a link to a plan name
    '''
    json_args = json.loads(args)
    project_name = json_args['project_name']
    orgization_name = json_args['organization_name']
    plan_year = json_args['year']

    try:
        form_name = json_args['form_name']
        return frappe.utils.get_url_to_form("Annual Plan Form",form_name)
    except:
        # check a annual plan form exists
        list_of_plan_forms = frappe.db.sql("select name from `tabAnnual Plan Form` where project_title = '{}' && status !='Cancelled' && full_name_of_the_organization = '{}' && annual_plan_year = '{}'".format(project_name,orgization_name,plan_year))
        
        if len(list_of_plan_forms) > 0:
            # get the latest form on the list
            form_name = list_of_plan_forms[0][0]
            return frappe.utils.get_url_to_form("Annual Plan Form",form_name)
        else:
            return False


@frappe.whitelist(allow_guest = True)
def get_link_to_report_form(args):
    '''
    Function that returns a link to a report name
    '''
    json_args = json.loads(args)
    project_name = json_args['project_name']
    orgization_name = json_args['organization_name']
    report_year = json_args['year']
    
    # check a annual plan form exists
    list_of_report_forms = frappe.db.sql("select name from `tabAnnual Report Form` where project_title = '{}' && status !='Cancelled' && full_name_of_the_organization = '{}' && annual_report_year = '{}'".format(project_name,orgization_name,report_year))
    
    if len(list_of_report_forms) > 0:
        # get the latest form on the list
        form_name = list_of_report_forms[0][0]
        return frappe.utils.get_url_to_form("Annual Report Form",form_name)
    else:
        return False

@frappe.whitelist(allow_guest = True)
def get_link_to_npre_form(args):
    '''
    Function that returns a link to the NPRE form
    '''
    json_args = json.loads(args)
    organization_name = json_args['organization_name']
    year = json_args['year']
    
    # check a annual plan form exists
    list_of_npre_forms = frappe.db.sql("select name from `tabNon Project Related Expenditure` where full_name_of_the_organization = '{}' && year = '{}'".format(organization_name,year))
    
    if len(list_of_npre_forms) > 0:
        # get the latest form on the list
        form_name = list_of_npre_forms[0][0]
        return frappe.utils.get_url_to_form("Non Project Related Expenditure",form_name)
    else:
        return False


@frappe.whitelist(allow_guest = True)
def get_link_to_org_form(args):
    '''
    Function that returns a link to the
    requested organization form
    '''
    json_args = json.loads(args)
    organization_name = json_args['org_name']
    organization_type = json_args['org_type']

    # check if the type is given in the first place
    if not organization_type:
        return False

    elif organization_type == "Local NGO":
        # check both english and somali forms
        req_org_type_form_english = organization_type +" Form English"
        req_org_type_form_renewal_english = organization_type +" Renewal Form English"
        req_org_type_form_somali = organization_type +" Form Somali"
        req_org_type_form_renewal_somali = organization_type +" Renewal Form Somali"

        def check_forms(req_org_type_form_renewal,req_org_type_form,organization_name):
            # check if renewal forms exists
            list_of_organization_renewal_forms = frappe.db.sql("select name from `tab{}` where full_name_of_the_organization = '{}'".format(req_org_type_form_renewal,organization_name))
            # check if any renewal forms were found
            if len(list_of_organization_renewal_forms) > 0:
                # get the latest form on the list
                # use -1 to the the last form created in the system
                form_name = list_of_organization_renewal_forms[-1][0]
                return frappe.utils.get_url_to_form(req_org_type_form_renewal,form_name)
            else:
                # check if organization forms exists
                list_of_organization_forms = frappe.db.sql("select name from `tab{}` where full_name_of_the_organization = '{}'".format(req_org_type_form,organization_name))
                
                if len(list_of_organization_forms):
                    form_name = list_of_organization_forms[0][0]
                    return frappe.utils.get_url_to_form(req_org_type_form,form_name)
                else:
                    return False

        # determine if renewal exist for english
        english_form = check_forms(req_org_type_form_renewal_english,req_org_type_form_english,organization_name)
        if english_form:
            return english_form
        else:
            somali_form = check_forms(req_org_type_form_renewal_somali,req_org_type_form_somali,organization_name)
            if somali_form:
                return somali_form
            else:
                return False

    else:
        # add a hack for multilateral organization since the name 
        # and the form names are different
        if organization_type == "Multi-Lateral Organizations":
            organization_type  = "MultiLateral Organization"
        elif organization_type == "Bi-lateral Organization":
            organization_type  = "Bilateral Organization"

        # create the name of the requested form
        req_org_type_form = organization_type +" Form"
        req_org_type_form_renewal = organization_type +" Renewal Form"
    
        # check if renewal forms exists
        list_of_organization_renewal_forms = frappe.db.sql("select name from `tab{}` where full_name_of_the_organization = '{}'".format(req_org_type_form_renewal,organization_name))

        # check if any renewal forms were found
        if len(list_of_organization_renewal_forms) > 0:
            # get the latest form on the list
            # use -1 to the the last form created in the system
            form_name = list_of_organization_renewal_forms[-1][0]
            return frappe.utils.get_url_to_form(req_org_type_form_renewal,form_name)
        else:
            # check if organization forms exists
            list_of_organization_forms = frappe.db.sql("select name from `tab{}` where full_name_of_the_organization = '{}'".format(req_org_type_form,organization_name))
            
            if len(list_of_organization_forms):
                form_name = list_of_organization_forms[0][0]
                return frappe.utils.get_url_to_form(req_org_type_form,form_name)
            else:
                return False

@frappe.whitelist(allow_guest = True)
def get_link_to_different_forms(args):
    '''
    Function that returns a link to a report name
    '''
    json_args = json.loads(args)
    
    # check requested form
    requested_form = json_args['req_form']

    if requested_form == "Organization Form":
        # call the get link to project form
        retrieved_link = get_link_to_org_form(args)
        # return the retrieved link
        return retrieved_link

    elif requested_form == "Project Form":
        # call the get link to project form
        retrieved_link = get_link_to_project_form(args)
        # return the retrieved link
        return retrieved_link

    elif requested_form == "Annual Plan Form":
        # call the get link to plan form function
        retrieved_link = get_link_to_plan_form(args)
        # return the retrieved link
        return retrieved_link

    elif requested_form == "Annual Report Form":
        # call the get link to report form function
        retrieved_link = get_link_to_report_form(args)
        # return the retrieved link
        return retrieved_link

    elif requested_form == "NPRE Form":
        # call the get link tothe npre form function
        retrieved_link = get_link_to_npre_form(args)
        # return the retrieved link
        return retrieved_link
    
    else:
        # return false
        return False

@frappe.whitelist(allow_guest = True)
def get_npre_for_specific_year(organization,year):
    '''
    Function that determines if a NPRE exist for a given year
    '''
    # check the data type of organization passed to the 
    # function
    if type(organization) == str:
        requested_organization_name = organization
    else:
        requested_organization_name = organization['name']
    
    # get list of npres
    list_of_NPRE_forms = frappe.db.sql("select name,year,status from `tabNon Project Related Expenditure` where full_name_of_the_organization = '{}' && year = '{}'".format(requested_organization_name,str(year)))
    # check if atleast one npre was found
    if len(list_of_NPRE_forms) > 0:
        # return the NPRE
        form_url = frappe.utils.get_url_to_form("Non Project Related Expenditure",list_of_NPRE_forms[0][0])
        return {'npre':list_of_NPRE_forms[0][0],'status':list_of_NPRE_forms[0][1],'url_form':form_url,'year':year,'form_status':list_of_NPRE_forms[0][2],'npre_status':True}
    else:
        return {'npre':False,'status':False,'url_form':False,'year':year,'npre_status':False}

# get the current year of the organization
def get_start_n_end_date(organization_name):
    '''
    Function that gets the start and end dates 
    of an organizations current finacial year
    input:
        organization_name - str
    output:
        dictionary with start and end dates of fin year
    '''
    organization_doc = frappe.get_doc("Organization",organization_name)

    org_financial_year = organization_doc.current_registration_year
    if org_financial_year:
        # get start and enddate of current financial year
        finacial_year_doc = frappe.get_doc("Year",org_financial_year)
        start_date = finacial_year_doc.start_date
        end_date = finacial_year_doc.end_date
    else:
        # get start and edn date from gloabl settings
        start_date_string = frappe.db.get_single_value('Global Settings','current_financial_start_date')
        start_date = datetime.strptime(str(start_date_string),'%Y-%m-%d').date()
        end_date_string = frappe.db.get_single_value('Global Settings','current_financial_end_date')
        end_date = datetime.strptime(str(end_date_string),'%Y-%m-%d').date()

    # return start and endates
    return {'start_date':start_date,'end_date':end_date}

def get_supposed_plans_or_reports_for_project(project_name,organization_name):
    '''
    Function that gets supposed plans/reports for a 
    given project
    input:
        project_name - string
    output:
        list of required plans/reports
    '''
    # get list of all implementing / reporting obligations of a project from the database
    sql_string_1 = '''select name,implementing_organization,reporting_organization \
        from `tabDestination Implementing Reporting` \
        where parent = "{}" && implementing_organization = "{}"\
        || parent = "{}" && reporting_organization = "{}"\
        '''.format(project_name,organization_name,project_name,organization_name) 
    sql_list_1 = frappe.db.sql(sql_string_1)

    # get list of all destination organizations who are neither reporting or implementing 
    sql_string_2 = '''select name,implementing_organization,reporting_organization \
        from `tabDestination Implementing Reporting` \
        where parent = "{}" && implementing_organization != "{}"\
        && reporting_organization != "{}"\
        '''.format(project_name,organization_name,organization_name) 
    sql_list_2 = frappe.db.sql(sql_string_2)
    # combine both lists
    list_of_imple_rep_obligations = sql_list_1 + sql_list_2
    # list_of_imple_rep_obligations = sql_list_1
    
    # loop through obligations
    list_of_imple_rep_obligations_for_project = []
    for imple_rep_obligation in list_of_imple_rep_obligations:
        imple_reporting_statuses = {"implementing_status":False,
            "reporting_status":False,
            "destination_organization":False,
            "project_name":project_name,
            "organization_name":organization_name,
            "implementing_orginization":imple_rep_obligation[1],
            "reporting_organization":imple_rep_obligation[2]
        }

        # check statuses for each obligation record
        # check if current organization has implementing obligations
        if imple_rep_obligation[1] == organization_name:
            imple_reporting_statuses["implementing_status"] = True
        # check if current organization has reporting obligations
        if imple_rep_obligation[2] == organization_name:
            imple_reporting_statuses["reporting_status"] = True
        # check if orgnization is only an implementing organization
        if imple_reporting_statuses['implementing_status'] or \
        imple_reporting_statuses["reporting_status"]:
            pass
        else:
            # the organization is only a destination organization
            imple_reporting_statuses["destination_organization"] = True

        # append status to list of implementing and reporting obligations
        list_of_imple_rep_obligations_for_project.append(imple_reporting_statuses)

    # return list of implementing reporting per project
    return list_of_imple_rep_obligations_for_project

def get_supposed_plans_or_reports_for_destionation_org_project(project_name):
    '''
    Function that gets all the supposed plans/reports for a 
    given project
    input:
        project_name - string
    output:
        list of required plans/reports
    '''
    # get list of all implementing /reporting obligations of a project from the database
    sql_string = '''select name,parent,implementing_organization,reporting_organization \
        from `tabDestination Implementing Reporting` \
        where parent = "{}"'''.format(project_name) 
    list_of_imple_rep_obligations = frappe.db.sql(sql_string)

    return_list = []
    # put the values in list of dictionaries
    for imple_rep_obligation in list_of_imple_rep_obligations:
        current_dict = {}
        current_dict['name'] = imple_rep_obligation[0]
        current_dict['project'] = imple_rep_obligation[1]
        current_dict['implementing_organization'] = imple_rep_obligation[2]
        current_dict['reporting_organization'] = imple_rep_obligation[3]
        # append to return list
        return_list.append(current_dict)
    # retrun the created list
    return return_list

def check_plan_or_report_exists(project_name,implementing_organization,financial_year):
    '''
    Function that checks if a plan and report exist for given project,
    implementing organization and year
    input:
        project_name - str
        implementing_organization - str
        financial_year - str
    output:
        return_dictionary = {'annual_plan_status': True/False,
                            'annual_report_status': True/False, 
                        }
    '''
    return_dictionary = {}
    # check if plan exist
    sql_str = '''select name,status from `tabAnnual Plan` where project_title = "{}"\
    and full_name_of_the_organization = "{}" and annual_plan_year = "{}"\
    '''.format(project_name,implementing_organization,financial_year)
    list_of_annual_plans = frappe.db.sql(sql_str)
    if len(list_of_annual_plans) > 0:
        return_dictionary['annual_plan_status'] = True
        return_dictionary['plan_doc_status'] = list_of_annual_plans[0][1]
    else:
        return_dictionary['annual_plan_status'] = False
    # check if report exist
    sql_str = '''select name,status from `tabAnnual Report` where project_title = "{}"\
    and full_name_of_the_organization = "{}" and annual_report_year = "{}"\
    '''.format(project_name,implementing_organization,financial_year)
    list_of_annual_reports = frappe.db.sql(sql_str)
    if len(list_of_annual_reports) > 0:
        return_dictionary['annual_report_status'] = True
        return_dictionary['report_doc_status'] = list_of_annual_reports[0][1]
    else:
        return_dictionary['annual_report_status'] = False
    # return the return dictionary
    return return_dictionary
     
def get_plan_obligations_n_statuses(current_organization):
    '''
    Function that gets all the plan obligations of an organization
    and return their statuses
    input:
        current_organization - class object
    output:
        list of obligation and status objects
    '''
    # get all the current active projects
    if current_organization:
        organization_name = current_organization.name
        list_of_active_projects = get_current_projects(organization_name)
    else:
        list_of_active_projects = []

    list_of_plans_obligations = []
    list_of_obligations_n_statues = []
    # get a list of supposed plans for each project
    for active_project in list_of_active_projects:
        list_of_plans_obligations += get_supposed_plans_or_reports_for_project(active_project['project_doc'].name,organization_name)

    # loop through plans to show checking if the required plan is given
    for plan_obligation in list_of_plans_obligations:
        # get current organization year
        plan_obligations_status = get_plans_for_each_project_obligation(plan_obligation['implementing_orginization'],plan_obligation['project_name'],current_organization.current_registration_year)
        plan_obligation['status'] = plan_obligations_status['status']
        plan_obligation['financial_year'] = current_organization.current_registration_year
        if plan_obligation['status']:
            # add plans status as well
            plan_obligation['plan_status'] = plan_obligations_status['plan_status']

        # append the new obligatin dict to list
        list_of_obligations_n_statues.append(plan_obligation)

    # return list of obligation and statuses
    return list_of_obligations_n_statues

def get_report_obligations_n_statuses(current_organization):
    '''
    Function that gets all the plan obligations of a project
    and return their statuses
    input:
        current_organization - class object
    output:
        list of obligation and status objects
    '''
    # get all the current active projects
    if current_organization:
        organization_name = current_organization.name
        list_of_active_projects = get_current_projects(organization_name)
    else:
        list_of_active_projects = []

    list_of_report_obligations = []
    list_of_obligations_n_statues = []
    # get a list of supposed reports for each project
    for active_project in list_of_active_projects:
        list_of_report_obligations += get_supposed_plans_or_reports_for_project(active_project['project_doc'].name,organization_name)

    # loop through plans to show checking if the required plan is given
    for report_obligation in list_of_report_obligations:
        # get current organization year
        report_obligations_status = get_report_for_each_project_obligation(report_obligation['implementing_orginization'],report_obligation['project_name'],current_organization.current_registration_year)
        report_obligation['status'] = report_obligations_status['status']
        report_obligation['financial_year'] = current_organization.current_registration_year
        if report_obligation['status']:
            # add plans status as well
            report_obligation['report_status'] = report_obligations_status['report_status']

        # append the new obligatin dict to list
        list_of_obligations_n_statues.append(report_obligation)

    # return list of obligation and statuses
    return list_of_obligations_n_statues