from __future__ import unicode_literals

# std library imports
from datetime import datetime
import json

# frappe imports
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
from frappe.model.document import Document

# imports from methods module
from .profile_methods import get_user_organization,get_current_projects,get_npre_for_specific_year

def get_context(context):
    pass

@frappe.whitelist(allow_guest = True)
def return_context():
    '''
    Function that returns the profile context for
    non project related expenditure
    '''
    return_dict = {}
    logged_in_user = frappe.session.user
    list_of_current_user_roles = frappe.get_roles(frappe.session.user)

    # get a list of roles
    list_of_current_user_roles = frappe.get_roles(logged_in_user)

    # check if the logged in user has role of "Organization Login"
    if "Organization Login" in list_of_current_user_roles:
        current_organization = get_user_organization(logged_in_user)
        return_dict['user'] = logged_in_user
        return_dict["organization"] = current_organization
        if current_organization:
            # check if the NPRE of current year
            return_dict["npre"] = get_npre_for_specific_year(current_organization.name,current_organization.current_registration_year)
        else:
            return_dict['npre'] = []
        # finally return the return dict
        return return_dict
    else:
        # throw an error if the logged in user is Staff
        frappe.throw("This is the basic user interface use the MoNDP interface instead") 
