// function that adds a loader to a page
function add_remove_loader(action){
    if(action== true){
        var loader_div = document.createElement("div");
        loader_div.setAttribute('class',"loader")
        loader_div.setAttribute('id',"loader")
        document.getElementById("loader_column").appendChild(loader_div)
    }else{
        var elem = document.getElementById("loader");
        elem.parentNode.removeChild(elem);
    }   
}


// function that adds certain text to a given section
function add_text_to_doc(text_to_add,where_to_add){
    if(text_to_add){
        var user_name = document.createTextNode(text_to_add);
        document.getElementById(where_to_add).appendChild(user_name)
    } 
}

// get values from url method and add to doc
function add_contexts_to_doc(message){
    // check if a user exists ie. the logged in user is not an admin
    if(message.user){
        // check if the user has an organization
        if(message.organization){
            // add user and organization details
            add_text_to_doc(message.user,"username_holder")
            add_text_to_doc(message.organization.name,"organization_holder")
            
            // check if any pending forms exist
            if(message.pending_forms.length > 0){
                // loop throogh the forms
                message.pending_forms.forEach(element => {
                    // create a div for each form
                    create_project_div(element)
                })
            }

            if(message.projects.length > 0 ){
                // loop through all found projects
                message.projects.forEach(element => {
                    // add each project to the document
                    create_project_div(element)
                });
            }else{
                // add the no projects found div
                var no_projects_found_inst = "No active registered projects for your organization"
                add_text_to_doc(no_projects_found_inst,"no_projects_found_p")
                no_projects_found_i.setAttribute("class","fa fa-check-circle-o")
                no_projects_div_hr.setAttribute("style","width:98%;")
            }
           
            // determine the instruction to display and and icons to show
            if(message.organization.status == "Pending Renewal"){
                var project_addition_inst = "Your organization in pending renewal please renew registration\
                in order to register new projects"
                add_text_to_doc(project_addition_inst,"project_addition_instruction")
                add_text_to_doc("Renew Organization","project_addition")
                // add link to renew button
                project_addition.setAttribute('class',"btn btn-success btn-sm step_button")
                project_addition.setAttribute("onclick",`location.replace('/profile_organization')`)

            }else if(message.organization.status == "Pending Verification"){
                var project_addition_inst = "Please verify your organization form in order to register new projects"
                add_text_to_doc(project_addition_inst,"project_addition_instruction")
                add_text_to_doc("Verify Organization","project_addition")
                // add link to renew button
                project_addition.setAttribute('class',"btn btn-success btn-sm step_button")
                project_addition.setAttribute("onclick",`location.replace('/profile_organization')`)

            }else{
                var project_addition_inst = "Click the button below to add a new project"
                add_text_to_doc(project_addition_inst,"project_addition_instruction")
                add_text_to_doc("Register Project","project_addition")
                // add link to renew button
                project_addition.setAttribute('class',"btn btn-success btn-sm step_button")
                project_addition.setAttribute("onclick",`location.replace('/desk#Form/Project%20Registration%20Form/New%20Project%20Registration%20Form')`)
            }
        }else{
            var user_option = confirm("You have not registered an organization, you will be redirected to the organization registration page")
            if(user_option){
                // redirect to organization page
                window.location.href = "/profile_organization";
            }else{
                // redirect to organization page
                window.location.href = "/profile_organization";
            }
        }
    }else{
        alert('This is basic user page, please use the MoNDP interface instead');
        window.location = '/desk';
    }
}

// get url to form
function get_url_to_form(clicked_button){
    var project_form_method_url = "/api/method/slaims.templates.pages.profile_methods.get_link_to_project_form?args="
    var project_form_url = project_form_method_url +clicked_button.name

    // pull information from ajax
    $.ajax({
        url:project_form_url,
        async: true,
        dataType: 'json',
        success: function (response) {
            // redirect the page to the recieved url
            if(response.message){
                window.location = response.message
            }else{
                alert("Registration form for this project does not exist please contact admin for assistance")
            }   
        },
        cache:false
    }); 
}

function create_project_div(project){
    // create  project row
    var project_row = document.createElement("div");
    project_row.setAttribute('class',"col-md-12")
    var project_details_column = document.createElement("div");
    project_details_column.setAttribute('class',"col-md-4")

    function create_label_n_text(label_name,value){
        var label_n_text_selement = document.createElement("h4");

        // create the label
        var label_span = document.createElement("span");
        label_span.setAttribute('style',"font-weight:bold")
        var label_name = document.createTextNode(label_name);
        var label_name_space = document.createTextNode(" :");
        label_span.appendChild(label_name)
        label_span.appendChild(label_name_space)
        
        // create the text
        if(value){
            var project_name_value = document.createTextNode(value);
        }else{
            var project_name_value = document.createTextNode("");
        }
        
        // append to label elements
        label_n_text_selement.appendChild(label_span)
        label_n_text_selement.appendChild(project_name_value)

        // finally retun the crated elements
        return label_n_text_selement
    }

    // add instructions details
    var instruction_paragraph = document.createElement("p");
    instruction_paragraph.setAttribute('style',"font-style:italic")
    var project_instructions_column = document.createElement("div");
    project_instructions_column.setAttribute('class',"col-md-6")
    var inst_action_button = document.createElement("button");
    var inst_button_text = document.createTextNode("View Form")
    inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
    
    // add status indicator for the project
    var project_status_value = document.createTextNode("Your project is already verified");
    var project_status_icon = document.createElement("i");

    // detemine if the instance is an actual project or a form
    if('form_type' in project){
        // add project-details
        project_details_column.appendChild(create_label_n_text("Project Name",project['new_project_title']))
        project_details_column.appendChild(create_label_n_text("Project Code",project['new_project_code']))
        project_details_column.appendChild(create_label_n_text("Project Status",project['status']))

        var project_instruction = document.createTextNode(`Your have an incomplete \
        project form for project ${project['new_project_title']} please complete and verify it`);
        var status_icon_class = "fa fa-times-circle-o"
        var status_icon_style = "color:red;font-size:25px;"

        // and url parameters to the button
        var name_proj_name = project['new_project_title']
        var name_proj_org = project['select_your_organization']
        var form_name = project['name']
        var full_name_attribute =  {'project_name':name_proj_name,'orgization_name':name_proj_org,'form_name':form_name}

    }else{
        // add project-details
        project_details_column.appendChild(create_label_n_text("Project Name",project.project_doc.name))
        project_details_column.appendChild(create_label_n_text("Project Code",project.project_doc.project_code))
        project_details_column.appendChild(create_label_n_text("Project Status",project.project_doc.status))

        // determine instructions to show and color to show on icons
        if(project.project_doc.status){
            // check if the user is the destination organization
            if(project.destination_organization){
                if(project.project_doc.status == "Pending Verification"){
                    var project_instruction = document.createTextNode("Your project is pending \
                    verification, please complete the form and verify it");
                }else if(project.project_doc.status == "Verified"){
                    var project_instruction = document.createTextNode("Your project form is Verified \
                    pending Approval");
                }else if(project.project_doc.status == "Validated"){
                    var project_instruction = document.createTextNode("Your project form is Validated \
                    pending Approval");
                }else if(project.project_doc.status == "Approved"){
                    var project_instruction = document.createTextNode("Your project has been approved");                    
                }
            }else{
                // determine instructions to show
                if(project.implementing_organization){
                // check if the user is also reporting
                    if(project.reporting_organization){
                        var project_instruction = document.createTextNode("You have both \
                        implementing and reporting obligations for this project ,\
                        please make sure to submit an Annual Plan and Report form for this project");
                    }else{
                        var project_instruction = document.createTextNode("You are an \
                        implementing patner for this project,but you do not have any reporting obligation");
                    }
                }else if(project.reporting_organization){
                    var project_instruction = document.createTextNode("You have reporting \
                    obligations for this ,\
                    please make sure to submit an Annual Plan and Report form for this project"); 
                }
            }

            // determine status icon to show
            if(project.project_doc.status == "Pending Verification"){
                var status_icon_class = "fa fa-times-circle-o"
                var status_icon_style = "color:red;font-size:25px;"
            }else if(project.project_doc.status == "Verified"){
                var status_icon_class = "fa fa-exclamation-circle"
                var status_icon_style = "color:#B7481D;font-size:25px;"
            }else if(project.project_doc.status == "Validated"){
                var status_icon_class = "fa fa-exclamation-circle"
                var status_icon_style = "color:#B7481D;font-size:25px;"
            }else if(project.project_doc.status == "Approved"){
                var status_icon_class = "fa fa-check-circle"
                var status_icon_style = "color:green;font-size:25px;"
            } 
        }else{
            var project_instruction = document.createTextNode("Your project is pending \
            verification, please complete the form and verify it");
            var status_icon_class = "fa fa-times-circle-o"
            var status_icon_style = "color:red;font-size:25px;"
        }

        // and url parameters to the button
        var name_proj_name = `${project.project_doc.name}`
        var name_proj_org = `${project.project_doc.your_organization}`
        var full_name_attribute =  {'project_name':name_proj_name,'orgization_name':name_proj_org}
    }

    // set url attribute for the button
    inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))
    inst_action_button.setAttribute('onclick','get_url_to_form(this)')
    inst_action_button.appendChild(inst_button_text)
    
    // append project details elements
    instruction_paragraph.appendChild(project_instruction)
    project_instructions_column.appendChild(instruction_paragraph)
    // only add the button if the user is a destination organization
    if(project.destination_organization){
        project_instructions_column.appendChild(inst_action_button)
    }else if('form_type' in project){
        // if form is pending approval also  create a button
        project_instructions_column.appendChild(inst_action_button)
    }
    
    // create status column
    var project_status_column = document.createElement("div");
    project_status_column.setAttribute('class',"col-md-2")
    project_status_icon.setAttribute('class',status_icon_class)
    project_status_icon.setAttribute('style',status_icon_style)
    // append project details elements
    instruction_paragraph.appendChild(project_instruction)
    project_status_column.appendChild(project_status_icon)

    // append elements to row
    project_row.appendChild(project_details_column)
    project_row.appendChild(project_instructions_column)
    project_row.appendChild(project_status_column)
    
    // Append the row to dom
    document.getElementById("project_div").appendChild(project_row)
    var project_hr = document.createElement("hr");
    project_hr.setAttribute("width", "98%");
    document.getElementById("project_div").appendChild(project_hr)
}

function redirect_url(new_url){
    // allow the user to view page
    window.location = new_url
}

function main_function(){
    // add a loader to the contents tables
    add_remove_loader(true)
    // // url to the method
    var url = "/api/method/slaims.templates.pages.profile_project.return_context"
    
    // ajax calls to response
    $.ajax({
        url:url,
        async: true,
        dataType: 'json',
        success: function (response) {
            add_remove_loader(false)
            add_contexts_to_doc(response.message)  
        },
        cache:false
    });
}

// call the main function
main_function()
