// function that adds a loader to a page
function add_remove_loader(action){
    if(action== true){
        var loader_div = document.createElement("div");
        loader_div.setAttribute('class',"loader")
        loader_div.setAttribute('id',"loader")
        document.getElementById("loader_column").appendChild(loader_div)
    }else{
        var elem = document.getElementById("loader");
        elem.parentNode.removeChild(elem);
    }   
}

// function that adds certain text to a given section
function add_text_to_doc(text_to_add,where_to_add){
    if(text_to_add){
        var user_name = document.createTextNode(text_to_add);
        document.getElementById(where_to_add).appendChild(user_name)
    } 
}

function create_plan_div(plan){
    // create  plan row
    var plan_row = document.createElement("div");
    plan_row.setAttribute('class',"col-md-12")
    var plan_details_column = document.createElement("div");
    plan_details_column.setAttribute('class',"col-md-4");

    function create_label_n_text(label_name,value){
        var label_n_text_selement = document.createElement("h4");
        // create the label
        var label_span = document.createElement("span");
        label_span.setAttribute('style',"font-weight:bold")
        var label_name = document.createTextNode(label_name);
        var label_name_space = document.createTextNode(" :");
        label_span.appendChild(label_name)
        label_span.appendChild(label_name_space)
        
        // create the text
        if(value){
            var project_name_value = document.createTextNode(value);
        }else{
            var project_name_value = document.createTextNode("");
        }
        
        // append to label elements
        label_n_text_selement.appendChild(label_span)
        label_n_text_selement.appendChild(project_name_value)

        // finally retun the crated elements
        return label_n_text_selement
    }

    // add instructions details
    var instruction_paragraph = document.createElement("p");
    instruction_paragraph.setAttribute('style',"font-style:italic")
    var plan_instructions_column = document.createElement("div");
    plan_instructions_column.setAttribute('class',"col-md-6")
    

    if('form_type' in plan){
        // add project-details
        plan_details_column.appendChild(create_label_n_text("Plan For Project:",plan.project_title))
        plan_details_column.appendChild(create_label_n_text("Status",plan.status))
        plan_details_column.appendChild(create_label_n_text("Year",plan.annual_plan_year))
        var name_proj_name = `${plan.project_title}`
        var name_plan_org = `${plan.organization_name}`
        var name_plan_year = `${plan.annual_plan_year}`
        var form_name = `${plan.name}`
        // create instructions the user will see
        var plan_instruction = document.createTextNode(`Your have an incomplete \
        annual plan form for project ${name_proj_name} please complete and verify it`);
        var status_icon_class = "fa fa-times-circle-o"
        var status_icon_style = "color:red;font-size:25px;"
        // add details to form
        var inst_action_button = document.createElement("button");
        inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
        var inst_button_text = document.createTextNode("View Form")
        inst_action_button.setAttribute('onclick','get_url_to_form(this)')
        inst_action_button.appendChild(inst_button_text)
        // button attribute
        var full_name_attribute = {'project_name':name_proj_name,'organization_name':name_plan_org,'year':name_plan_year,'form_name':form_name}
        inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))
    }else{
        // add plan details
        plan_details_column.appendChild(create_label_n_text("Plan For Project:",plan.project_name))
        plan_details_column.appendChild(create_label_n_text("Status",plan.plan_status))
        plan_details_column.appendChild(create_label_n_text("Year",plan.financial_year))  
        plan_details_column.appendChild(create_label_n_text("Implementing Organization",plan.implementing_orginization))
        plan_details_column.appendChild(create_label_n_text("Reporting Organization",plan.reporting_organization))
        var name_proj_name = `${plan.project_name}`
        var name_plan_org = `${plan.implementing_orginization}`
        var name_plan_year = `${plan.financial_year}`
        // determine instructions to show
        if(plan.reporting_status){
            // check if the plan has been created
            if(plan.plan_status){
                if(plan.plan_status == "Pending Verification"){
                    // check if user has reporting obligations
                    var plan_instruction = document.createTextNode("This plan is pending \
                    verification, please complete the form and verify it");
                    var status_icon_class = "fa fa-times-circle"
                    var status_icon_style = "color:red;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
    
                }else if(plan.plan_status == "Verified"){
                    var plan_instruction = document.createTextNode("This Annual Plan is Verified \
                    pending Approval");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
 
                }else if(plan.plan_status == "Validated"){
                    var plan_instruction = document.createTextNode("This Annual Plan is Validated \
                    pending Approval");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")

                }else if(plan.plan_status == "Approved"){
                    var plan_instruction = document.createTextNode("This Annual Plan has been Approved");
                    var status_icon_class = "fa fa-check-circle-o"
                    var status_icon_style = "color:green;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
                    
                }
                // create the name attribute for the action button
                var full_name_attribute = {'project_name':name_proj_name,'organization_name':name_plan_org,'year':name_plan_year}
            }else{
                var plan_instruction = document.createTextNode(`You have not submitted an Annual Plan for \
                the project ${plan.project_name} for the year ${plan.financial_year} `);
                var status_icon_class = "fa fa-times-circle-o"
                var status_icon_style = "color:red;font-size:25px;"
                var inst_button_text = document.createTextNode("Create Plan")
                var onclick_attribute = "redirect_url('/desk#Form/Annual%20Plan%20Form/New%20Annual%20Plan%20Form')" 
            }

            // since the user has reporting obligation create an action button for the user
            var inst_action_button = document.createElement("button");
            inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
            inst_action_button.appendChild(inst_button_text)
            if(onclick_attribute){
                inst_action_button.setAttribute('onclick',onclick_attribute)
            }else{
                inst_action_button.setAttribute('onclick','get_url_to_form(this)')
                inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))
            }
        }else{
            // this handles situations when the organization is not the reporting organization
            // check if the plan has been created
            if(plan.plan_status){
                if(plan.plan_status == "Pending Verification"){
                    var plan_instruction = document.createTextNode(`This plan is pending \
                    verification, please request the reporting organization ${plan.reporting_organization}\
                    to complete the form and verify it`);
                    var status_icon_class = "fa fa-times-circle"
                    var status_icon_style = "color:red;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
    
                }else if(plan.plan_status == "Verified"){
                    var plan_instruction = document.createTextNode("This Annual Plan is Verified \
                    pending Approval");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
 
                }else if(plan.plan_status == "Validated"){
                    var plan_instruction = document.createTextNode("This Annual Plan is Validated \
                    pending Approval");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")

                }else if(plan.plan_status == "Approved"){
                    var plan_instruction = document.createTextNode("This Annual Plan has been Approved");
                    var status_icon_class = "fa fa-check-circle-o"
                    var status_icon_style = "color:green;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
                    
                }
                // create the name attribute for the action button
                var full_name_attribute = {'project_name':name_proj_name,'organization_name':name_plan_org,'year':name_plan_year}
            }else{
                var plan_instruction = document.createTextNode(`An 'Annual Plan' for \
                the project '${plan.project_name}',year-'${plan.financial_year}' \
                for the Implementing Organization '${plan.implementing_orginization}' \
                has not been submitted. Please request \
                the reporting Organization '${plan.reporting_organization}' to submit this Plan `);
                var status_icon_class = "fa fa-times-circle-o"
                var status_icon_style = "color:red;font-size:25px;"
                var inst_button_text = document.createTextNode("Create Plan")
                var onclick_attribute = "frappe.throw('Your organization does not have reporting obligation for this project\
                .Please request the reporting organization to create the plan')"
            }

            // crete action button
            var inst_action_button = document.createElement("button");
            inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
            inst_action_button.appendChild(inst_button_text)
            if(onclick_attribute){
                inst_action_button.setAttribute('onclick',onclick_attribute)
            }else{
                inst_action_button.setAttribute('onclick','get_url_to_form(this)')
                inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))
            }
        }
    }
   
    // append project details elements
    instruction_paragraph.appendChild(plan_instruction)
    plan_instructions_column.appendChild(instruction_paragraph)
    plan_instructions_column.appendChild(inst_action_button)
    
    // create status column
    var plan_status_icon = document.createElement("i");
    var plan_status_column = document.createElement("div");
    plan_status_column.setAttribute('class',"col-md-2")
    plan_status_icon.setAttribute('class',status_icon_class)
    plan_status_icon.setAttribute('style',status_icon_style)
    plan_status_column.appendChild(plan_status_icon)

    // append elements to row
    plan_row.appendChild(plan_details_column)
    plan_row.appendChild(plan_instructions_column)
    plan_row.appendChild(plan_status_column)
    
    // Append the row to dom
    document.getElementById("plan_div").appendChild(plan_row)
    var plan_hr = document.createElement("hr");
    plan_hr.setAttribute("width", "98%");
    document.getElementById("plan_div").appendChild(plan_hr)
}

function redirect_url(new_url){
    // allow the user to view page
    window.location = new_url
}

// get url to form
function get_url_to_form(clicked_button){
    var project_form_method_url = "/api/method/slaims.templates.pages.profile_methods.get_link_to_plan_form?args="
    var project_form_url = project_form_method_url +clicked_button.name

    // pull information from ajax
    $.ajax({
        url:project_form_url,
        async: true,
        dataType: 'json',
        success: function (response) {
            // redirect the page to the recieved url
            if(response.message){
                window.location = response.message
            }else{
                alert("Registration form for this plan does not exist please contact admin for assistance")
            }   
        },
        cache:false
    }); 
}

// get values from url method and add to doc
function add_contexts_to_doc(message){
    // check if a user exists ie. the logged in user is not an admin
    if(message.user){
        // check if the user has an organization
        if(message.organization){
            // add user and organization details
            add_text_to_doc(message.user,"username_holder")
            add_text_to_doc(message.organization.name,"organization_holder")  
            
            // check if the user has any pending forms
            if(message.pending_forms.length >0 ){
                // loop through all found plans
                message.pending_forms.forEach(form_per_project => {
                    // loop throgh all the relavant plans for each project
                    create_plan_div(form_per_project)
                });
            }
            
            if(message.plans.length > 0 ){
                // loop through all found plans
                message.plans.forEach(plans_per_project => {
                    create_plan_div(plans_per_project)
                });
            }else{
                // add to no plans required div
                var no_plans_required_inst = "You do not need to create any any annual plans \
                as you do not have any currently active projects"
                add_text_to_doc(no_plans_required_inst,"no_plans_required_p")
                no_plans_required_i.setAttribute("class","fa fa-check-circle-o")
            }
        }else{
            var user_option = confirm("You have not registered an organization, you will be redirected to the organization registration page")
            if(user_option){
                // redirect to organization page
                window.location.href = "/profile_organization";
            }else{
                // redirect to organization page
                window.location.href = "/profile_organization";
            }
        }

    }else{
        alert('This is basic user page, please use the MoNDP interface instead');
        window.location = '/desk';
    }
}

function main_function(){
    // add a loader to the contents tables
    add_remove_loader(true)
    // // url to the method
    var url = "/api/method/slaims.templates.pages.profile_plan.return_context"
    
    // ajax calls to response
    $.ajax({
        url:url,
        async: true,
        dataType: 'json',
        success: function (response) {
            add_remove_loader(false)
            add_contexts_to_doc(response.message)  
        },
        cache:false
    });
}

// call the main function
main_function()