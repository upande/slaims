// function that adds a loader to a page
function add_remove_loader(action){
    if(action== true){
        var loader_div = document.createElement("div");
        loader_div.setAttribute('class',"loader")
        loader_div.setAttribute('id',"loader")
        document.getElementById("loader_column").appendChild(loader_div)
    }else{
        var elem = document.getElementById("loader");
        elem.parentNode.removeChild(elem);
    }   
}

// function that adds certain text to a given section
function add_text_to_doc(text_to_add,where_to_add){
    if(text_to_add){
        var user_name = document.createTextNode(text_to_add);
        document.getElementById(where_to_add).appendChild(user_name)
    } 
}

function create_report_div(report){
    // create  report row
    var report_row = document.createElement("div");
    report_row.setAttribute('class',"col-md-12")
    var report_details_column = document.createElement("div");
    report_details_column.setAttribute('class',"col-md-4");

    function create_label_n_text(label_name,value){
        var label_n_text_selement = document.createElement("h4");
        // create the label
        var label_span = document.createElement("span");
        label_span.setAttribute('style',"font-weight:bold")
        var label_name = document.createTextNode(label_name);
        var label_name_space = document.createTextNode(" :");
        label_span.appendChild(label_name)
        label_span.appendChild(label_name_space)
        
        // create the text
        if(value){
            var project_name_value = document.createTextNode(value);
        }else{
            var project_name_value = document.createTextNode("");
        }
        
        // append to label elements
        label_n_text_selement.appendChild(label_span)
        label_n_text_selement.appendChild(project_name_value)

        // finally retun the crated elements
        return label_n_text_selement
    }

    // add instructions details
    var instruction_paragraph = document.createElement("p");
    instruction_paragraph.setAttribute('style',"font-style:italic")
    var report_instructions_column = document.createElement("div");
    report_instructions_column.setAttribute('class',"col-md-6")
    

    if('form_type' in report){
        // add project-details
        report_details_column.appendChild(create_label_n_text("Report For Project:",report.project_title))
        report_details_column.appendChild(create_label_n_text("Status",report.status))
        report_details_column.appendChild(create_label_n_text("Year",report.annual_report_year))
        var name_proj_name = `${report.project_title}`
        var name_report_org = `${report.organization_name}`
        var name_report_year = `${report.annual_report_year}`
        var form_name = `${report.name}`
        // create instructions the user will see
        var report_instruction = document.createTextNode(`Your have an incomplete \
        annual report form for project ${name_proj_name} please complete and verify it`);
        var status_icon_class = "fa fa-times-circle-o"
        var status_icon_style = "color:red;font-size:25px;"
        // add details to form
        var inst_action_button = document.createElement("button");
        inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
        var inst_button_text = document.createTextNode("View Form")
        inst_action_button.setAttribute('onclick','get_url_to_form(this)')
        inst_action_button.appendChild(inst_button_text)
        // button attribute
        var full_name_attribute = {'project_name':name_proj_name,'organization_name':name_report_org,'year':name_report_year,'form_name':form_name}
        inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))
    }else{
        // add report details
        report_details_column.appendChild(create_label_n_text("Report For Project:",report.project_name))
        report_details_column.appendChild(create_label_n_text("Status",report.report_status))
        report_details_column.appendChild(create_label_n_text("Year",report.financial_year))  
        report_details_column.appendChild(create_label_n_text("Implementing Organization",report.implementing_orginization))
        report_details_column.appendChild(create_label_n_text("Reporting Organization",report.reporting_organization))
        var name_proj_name = `${report.project_name}`
        var name_report_org = `${report.implementing_orginization}`
        var name_report_year = `${report.financial_year}`
        // determine instructions to show
        if(report.reporting_status){
            // check if the report has been created
            if(report.report_status){
                if(report.report_status == "Pending Verification"){
                    // check if user has reporting obligations
                    var report_instruction = document.createTextNode("This report is pending \
                    verification, please complete the form and verify it");
                    var status_icon_class = "fa fa-times-circle"
                    var status_icon_style = "color:red;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
    
                }else if(report.report_status == "Verified"){
                    var report_instruction = document.createTextNode("This Annual Report is Verified \
                    pending Approval");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
 
                }else if(report.report_status == "Validated"){
                    var report_instruction = document.createTextNode("This Annual Report is Validated \
                    pending Approval");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")

                }else if(report.report_status == "Approved"){
                    var report_instruction = document.createTextNode("This Annual Report has been Approved");
                    var status_icon_class = "fa fa-check-circle-o"
                    var status_icon_style = "color:green;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
                    
                }
                // create the name attribute for the action button
                var full_name_attribute = {'project_name':name_proj_name,'organization_name':name_report_org,'year':name_report_year}
            }else{
                var report_instruction = document.createTextNode(`You have not submitted an Annual Report for \
                the project ${report.project_name} for the year ${report.financial_year}`);
                var status_icon_class = "fa fa-times-circle-o"
                var status_icon_style = "color:red;font-size:25px;"
                var inst_button_text = document.createTextNode("Create Report")
                var onclick_attribute = "redirect_url('/desk#Form/Annual%20Report%20Form/New%20Annual%20Report%20Form')" 
            }

            // since the user has reporting obligation create an action button for the user
            var inst_action_button = document.createElement("button");
            inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
            inst_action_button.appendChild(inst_button_text)
            if(onclick_attribute){
                inst_action_button.setAttribute('onclick',onclick_attribute)
            }else{
                inst_action_button.setAttribute('onclick','get_url_to_form(this)')
                inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))
            }
        }else{
            // this handles situations when the organization is not the reporting organization
            // check if the report has been created
            if(report.report_status){
                if(report.report_status == "Pending Verification"){
                    var report_instruction = document.createTextNode(`This report is pending \
                    verification, please request the reporting organization ${report.reporting_organization}\
                    to complete the form and verify it`);
                    var status_icon_class = "fa fa-times-circle"
                    var status_icon_style = "color:red;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
    
                }else if(report.report_status == "Verified"){
                    var report_instruction = document.createTextNode("This Annual Report is Verified \
                    pending Approval");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
 
                }else if(report.report_status == "Validated"){
                    var report_instruction = document.createTextNode("This Annual Report is Validated \
                    pending Approval");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")

                }else if(report.report_status == "Approved"){
                    var report_instruction = document.createTextNode("This Annual Report has been Approved");
                    var status_icon_class = "fa fa-check-circle-o"
                    var status_icon_style = "color:green;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
                    
                }
                // create the name attribute for the action button
                var full_name_attribute = {'project_name':name_proj_name,'organization_name':name_report_org,'year':name_report_year}
            }else{
                var report_instruction = document.createTextNode(`An 'Annual Report' for \
                the project '${report.project_name}',year-'${report.financial_year}' \
                for the Implementing Organization '${report.implementing_orginization}' \
                has not been submitted. Please request \
                the reporting Organization '${report.reporting_organization}' to submit this Report `);
                var status_icon_class = "fa fa-times-circle-o"
                var status_icon_style = "color:red;font-size:25px;"
                var inst_button_text = document.createTextNode("Create Report")
                var onclick_attribute = "frappe.throw('Your organization does not have reporting obligation for this project\
                .Please request the reporting organization to create the report')"
            }

            // since the user has reporting obligation create an action button for the user
            var inst_action_button = document.createElement("button");
            inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
            inst_action_button.appendChild(inst_button_text)
            if(onclick_attribute){
                inst_action_button.setAttribute('onclick',onclick_attribute)
            }else{
                inst_action_button.setAttribute('onclick','get_url_to_form(this)')
                inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))
            }

        }
    }
   
    // append project details elements
    instruction_paragraph.appendChild(report_instruction)
    report_instructions_column.appendChild(instruction_paragraph)
    report_instructions_column.appendChild(inst_action_button)
    
    // create status column
    var report_status_icon = document.createElement("i");
    var report_status_column = document.createElement("div");
    report_status_column.setAttribute('class',"col-md-2")
    report_status_icon.setAttribute('class',status_icon_class)
    report_status_icon.setAttribute('style',status_icon_style)
    report_status_column.appendChild(report_status_icon)

    // append elements to row
    report_row.appendChild(report_details_column)
    report_row.appendChild(report_instructions_column)
    report_row.appendChild(report_status_column)
    
    // Append the row to dom
    document.getElementById("report_div").appendChild(report_row)
    var report_hr = document.createElement("hr");
    report_hr.setAttribute("width", "98%");
    document.getElementById("report_div").appendChild(report_hr)
}

function redirect_url(new_url){
    // allow the user to view page
    window.location = new_url
}

// get url to form
function get_url_to_form(clicked_button){
    var project_form_method_url = "/api/method/slaims.templates.pages.profile_methods.get_link_to_report_form?args="
    var project_form_url = project_form_method_url +clicked_button.name

    // pull information from ajax
    $.ajax({
        url:project_form_url,
        async: true,
        dataType: 'json',
        success: function (response) {
            // redirect the page to the recieved url
            if(response.message){
                window.location = response.message
            }else{
                alert("Registration form for this report does not exist please contact admin for assistance")
            }   
        },
        cache:false
    }); 
}

// get values from url method and add to doc
function add_contexts_to_doc(message){
    // check if a user exists ie. the logged in user is not an admin
    if(message.user){
        // check if the user has an organization
        if(message.organization){
            // add user and organization details
            add_text_to_doc(message.user,"username_holder")
            add_text_to_doc(message.organization.name,"organization_holder")  
            
            // check if the user has any pending forms
            if(message.pending_forms.length >0 ){
                // loop through all found pending reports
                message.pending_forms.forEach(form_per_project => {
                    create_report_div(form_per_project)
                });
            }
            
            if(message.reports.length > 0 ){
                // loop through all found reports
                message.reports.forEach(report_per_project => {
                    create_report_div(report_per_project)
                });
            }else{
                // add to no reports required div
                var no_report_required_inst = "You do not need to create any any Annual Reports \
                as you do not have any currently active projects"
                add_text_to_doc(no_report_required_inst,"no_report_required_p")
                no_report_required_i.setAttribute("class","fa fa-check-circle-o")
            }
        }else{
            var user_option = confirm("You have not registered an organization, you will be redirected to the organization registration page")
            if(user_option){
                // redirect to organization page
                window.location.href = "/profile_organization";
            }else{
                // redirect to organization page
                window.location.href = "/profile_organization";
            }
        }

    }else{
        alert('This is basic user page, please use the MoNDP interface instead');
        window.location = '/desk';
    }
}

function main_function(){
    // add a loader to the contents tables
    add_remove_loader(true)
    // // url to the method
    var url = "/api/method/slaims.templates.pages.profile_report.return_context"
    
    // ajax calls to response
    $.ajax({
        url:url,
        async: true,
        dataType: 'json',
        success: function (response) {
            add_remove_loader(false)
            add_contexts_to_doc(response.message)  
        },
        cache:false
    });
}

// call the main function
main_function()