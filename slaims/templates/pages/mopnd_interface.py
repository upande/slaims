from __future__ import unicode_literals

# std lib imports
import datetime

# application imports
import frappe
from frappe.utils.global_search import web_search
from frappe import _
from frappe.utils import sanitize_html
from frappe.model.document import Document
from .profile_methods import get_current_projects,get_plan_obligations_n_statuses,\
    get_report_obligations_n_statuses,get_npre_for_specific_year

# global variables
status_icon_list = ['red','#B7481D','green']
status_icon_color  = {'Pending Verification':['red','fa fa-times-circle-o'],
    'Pending Renewal':['red','fa fa-times-circle-o'],
    'Registration Expired':['red','fa fa-times-circle-o'],
    'Verified':['#B7481D','fa fa-exclamation-circle'],
    'Pending Validation':['#B7481D','fa fa-exclamation-circle'],
    'Validated':['#B7481D','fa fa-exclamation-circle'],
    'Pending Approval':['#B7481D','fa fa-exclamation-circle'],
    'Approved':['green','fa fa-check-circle'],
    'Complete':['green','fa fa-check-circle'],
    'Admin Approved':['green','fa fa-check-circle']   
}

def get_context(context):
    pass

@frappe.whitelist(allow_guest = True)
def return_context():
    return_dict = {}
    logged_in_user = frappe.session.user
    return_dict['user'] = logged_in_user

    # determine if the user is allowed to view this page
    list_of_current_user_roles = frappe.get_roles(logged_in_user)
    if "Staff" in list_of_current_user_roles or \
    "Super Admin" in list_of_current_user_roles:
        return_dict['view_status'] = True
        return_dict["organizations"] = main_function()
    else:
        return_dict['view_status'] = False

    # finally return the dictionary
    return return_dict
 
def save_organization(args):
    pass

def get_organizations():
    '''
    Function that gets all organizations from the database
    '''
    list_of_organizations = frappe.get_list("Organization",
        fields=["*"],
        filters = {      
    })
    # return a list of organizations
    return list_of_organizations


def get_projects_for_organization(organization):
    '''
    Function that gets all the projects belonging to an organization
    '''
    list_of_projects = frappe.get_list("Project",
        fields=["name","status"],
        filters = {  
            "your_organization":organization,
            "sub_project":1
        }
    )
    return list_of_projects

def get_annual_plans_for_project(project):
    '''
    Function that gets all the annual plans of a project
    '''
    list_of_plans = frappe.get_list("Annual Plan",
        fields=["name"],
        filters = {  
            "project_title":project,
        }
    )

    if len(list_of_plans)>0:
        return 'green'
    else:
        return 'red'

def get_annual_reports_for_project(project):
    '''
    Function that gets all the annual reports of a project
    '''
    list_of_reports = frappe.get_list("Annual Report",
        fields=["name"],
        filters = {  
            "project_title":project,
        }
    )

    if len(list_of_reports)>0:
        return 'green'
    else:
        return 'red'

def get_non_project_related_expenditure(organization,year):
    '''
    Function that get the non project related expenditure for
    a given year
    '''
    # get all the non project related expenditures
    list_of_npre = frappe.get_list("Non Project Related Expenditure",
        fields=["name","status"],
        filters = {  
            "full_name_of_the_organization":organization,
            "year":year
        }
    )

    # return the list of npre
    if len(list_of_npre) > 0:
        # check the status
        current_npre_status = list_of_npre[0].status
        npre_status_color = status_icon_color[current_npre_status][0]
        return npre_status_color
    else:
        return 'red'

def main_function():
    '''
    This is the main function that returns results to be displayed
    in the template
    '''
    print("*"*80)
    return_list = []

    # get a list of all organizations in the system
    list_of_orgs = get_organizations()

    # loop through found organizations
    for organization in list_of_orgs:
        print(organization["name"])
        # place the key in organizations holder
        org_holder = {"organization_name":organization["name"]}
        # determine organization status and color
        # add organization details
        org_holder['org_status_color'] = status_icon_color[organization['status']][0]

        # get all the current projects
        list_of_project_key_value_pairs = get_current_projects(organization["name"])

        # check the statuses of projects annual plans and annual reports
        project_status_color = None
        plan_status_color = None
        report_status_color = None
    
        # loop through the projects
        for project in list_of_project_key_value_pairs:
            # determine the status color
            current_project_doc = project["project_doc"]
            if project_status_color:
                # check if its index is lower than the current index in the list
                current_proj_color = status_icon_color[current_project_doc.status][0]
                if status_icon_list.index(project_status_color) > status_icon_list.index(current_proj_color):
                    # if the latest color index is lower than project_status_color index
                    # set the latest color as the project_status_color
                    project_status_color = current_proj_color
                else:
                    pass
            else:
                # set latest color as the new project_status_color
                project_status_color = status_icon_color[current_project_doc.status][0]

        
        # get all current Annual Plan obligations and statuses for the organization
        list_of_plan_obligation_n_statuses = get_plan_obligations_n_statuses(organization)
        for plan_obligation_n_status in list_of_plan_obligation_n_statuses:
            # check if the plan exists
            if plan_obligation_n_status['status']:
                current_plan_color = status_icon_color[plan_obligation_n_status['plan_status']][0]
                # check if plan status color is already defined
                if plan_status_color:
                    # check if the current plan status color is less than the global color
                    if status_icon_list.index(plan_status_color) > status_icon_list.index(current_plan_color):
                        # make the new color the global color since it is lower than the global color index
                        plan_status_color = current_plan_color
                else:
                    # curren the current color as organization's global
                    plan_status_color = current_plan_color
            else:
                # plan status color is not yet defined
                plan_status_color = status_icon_list[0]

        # get all current Annual Report obligations and statuses for the organization
        list_of_report_obligation_n_statuses = get_report_obligations_n_statuses(organization)
        for report_obligation_n_status in list_of_report_obligation_n_statuses:
            # check if the report exists
            if report_obligation_n_status['status']:
                current_report_color = status_icon_color[report_obligation_n_status['report_status']][0]
                # check if report status color is already defined
                if report_status_color:
                    # check if the current report status color is less than the global color
                    if status_icon_list.index(report_status_color) > status_icon_list.index(current_report_color):
                        # make the new color the global color since it is lower than the global color index
                        report_status_color = current_report_color
                else:
                    # make the current color as organization's global
                    report_status_color = current_report_color
            else:
                # plan status color is not yet defined
                report_status_color = status_icon_list[0]

        # check if a project,plan,report status color was 
        # if it was not defined so far set it to red
        if not project_status_color:
            project_status_color = status_icon_list[0]
        if not plan_status_color:
            plan_status_color = status_icon_list[0]
        if not report_status_color:
            report_status_color = status_icon_list[0]
      
        # place statuses of annual plan and annaul reports as well as projects
        org_holder["project_status_color"] = project_status_color
        org_holder["plan_status_color"] = plan_status_color
        org_holder["report_status_color"] = report_status_color

        # Get the organization's NPRE
        # check if the organization has a current financial year
        if organization.current_registration_year:
            current_organization_year = organization.current_registration_year
        else:
            global_settings_doc = frappe.get_single("Global Settings")
            current_organization_year = global_settings_doc.current_financial_year
        # check if the NPRE of current year
        organization_npre_n_status = get_npre_for_specific_year(organization.name,current_organization_year)
        # check if the npre exists
        if organization_npre_n_status['status']:
            current_npre_status_color = status_icon_color[organization_npre_n_status['form_status']][0]
            org_holder["npre_status_color"] = current_npre_status_color 
        else:
            # npre does not exist
            org_holder["npre_status_color"] = status_icon_list[0]

        # return values for each organization
        return_list.append(org_holder) 

    # return a list of organizations with statuses
    return return_list