// function that adds a loader to a page
function add_remove_loader(action){
    if(action== true){
        var loader_div = document.createElement("div");
        loader_div.setAttribute('class',"loader")
        loader_div.setAttribute('id',"loader")
        document.getElementById("instruction_column").appendChild(loader_div)
    }else{
        var elem = document.getElementById("loader");
        elem.parentNode.removeChild(elem);
    }   
}


// function that adds certain text to a given section
function add_text_to_doc(text_to_add,where_to_add){
    if(text_to_add){
        var user_name = document.createTextNode(text_to_add);
        document.getElementById(where_to_add).appendChild(user_name)
    } 
}

// get values from url method and add to doc
function add_contexts_to_doc(message){
    // check if a user exists ie. the logged in user is not an admin
    if(message.role == "Sign Up Role"){
        alert("Your user name is not yet verified,Please submit an inquiry if you haven't done so already")
        window.location = '/desk';
    }else if(message.role == "Staff"){
        alert('This is basic user page, please use the MoNDP interface instead');
        window.location = '/desk';
    }else{
        // check if the user has an organization
        if(message.organization){
            // add user and organization details
            add_text_to_doc(message.user,"username_holder")
            add_text_to_doc(message.organization.name,"organization_name")
            add_text_to_doc(message.organization.acronym,"organization_acronym")
            add_text_to_doc(message.organization.organization_type,"organization_type")
            add_text_to_doc(message.organization.status,"organization_status")
            add_text_to_doc(message.organization.current_registration_year,"organization_year")

            // determine the instruction to display and and icons to show
            if(message.organization.status == "Registration Expired"){
                var current_finacial_year = `${message.organization.current_registration_year}`
                // set values for the instruction section
                var instruction_to_show = `Your Organization's Registration has expired \
                Please fill in all the required forms for the year ${current_finacial_year} and contact admin for further\
                assistance on renewing your registration.`
                add_text_to_doc(instruction_to_show,"instructions_p")

                // set values for the status section
                status_icon.setAttribute("class","fa fa-times-circle-o")
                status_icon.setAttribute("style","color:red;font-size:25px;")

                // add the action button
                action_button.setAttribute('class',"btn btn-success btn-sm step_button")
                add_text_to_doc("View Registration/Renewal Form","action_button")
                action_button.setAttribute("onclick",'get_url_to_form(this)')
                // set the name value attributes
                var name_org_name = `${message.organization.name}`
                var name_org_type = `${message.organization.organization_type}`
                var name_requested_form = 'Organization Form'
                var full_name_attribute =  {'org_name':name_org_name,'org_type':name_org_type,'req_form':name_requested_form}
                action_button.setAttribute('name',JSON.stringify(full_name_attribute))

            }else if(message.organization.status == "Pending Renewal"){
                // set values for the instruction section
                var instruction_to_show = "Your Organization is pending renewal,click on the button below to renew your registration"
                add_text_to_doc(instruction_to_show,"instructions_p")
                // add the renew button
                action_button.setAttribute('class',"btn btn-success btn-sm step_button")

                function getRenewalForm(){
                    if(message.organization.organization_type == 'Local NGO'){

                        function getLocalRenewal(){
                            if(confirm('Would you like to fill out the renewal form in English?')){
                                // action_button.setAttribute("onclick", `location.replace('${message.english_renewal}')`)
                                location.replace('desk#Form/Local%20NGO%20Renewal%20Form%20English/New%20Local%20NGO%20Renewal%20Form%20English%201')
                            }else{
                                location.replace('desk#Form/Local%20NGO%20Renewal%20Form%20Somali/New%20Local%20NGO%20Renewal%20Form%20Somali%201')
                            }
                        }

                        add_text_to_doc("Renew Organization","action_button")
                        action_button.onclick = function() {
                            getLocalRenewal()
                        }

                    }else if(message.organization.organization_type == 'Multi-Lateral Organizations'){
                        add_text_to_doc("Renew Organization","action_button")
                        action_button.setAttribute("onclick", `location.replace('desk#Form/MultiLateral%20Organization%20Renewal%20Form/New%20MultiLateral%20Organization%20Renewal%20Form%201')`)
                    }else if(message.organization.organization_type == 'International NGO'){
                        add_text_to_doc("Renew Organization","action_button")
                        action_button.setAttribute("onclick", `location.replace('desk#Form/International%20NGO%20Renewal%20Form/New%20International%20NGO%20Renewal%20Form%201')`)
                    }
                    else if(message.organization.organization_type == 'Somaliland Government Institution'){
                        add_text_to_doc("Renew Organization","action_button")
                        action_button.setAttribute("onclick", `location.replace('/desk#Form/Somaliland%20Government%20Institution%20Renewal%20Form/New%20Somaliland%20Government%20Institution%20Renewal%20Form%202')`)
                    }
                    else if(message.organization.organization_type == 'Bi-lateral Organization'){
                        add_text_to_doc("Renew Organization","action_button")
                        action_button.setAttribute("onclick", `location.replace('desk#Form/Bilateral%20Organization%20Renewal%20Form/New%20Bilateral%20Organization%20Renewal%20Form%201')`)
                    }
                    else if(message.organization.organization_type == 'Private Sector Company'){
                        add_text_to_doc("Renew Organization","action_button")
                        action_button.setAttribute("onclick", `location.replace('desk#List/Private%20Sector%20Company%20Renewal%20Form/List')`)
                    }
                    else if(message.organization.organization_type == 'UN System Organization'){
                        add_text_to_doc("Renew Organization","action_button")
                        action_button.setAttribute("onclick", `location.replace('desk#Form/UN%20System%20Organization%20Renewal%20Form/New%20UN%20System%20Organization%20Renewal%20Form%201')`)
                    }
                    else if(message.organization.organization_type == 'Umbrella or Consortium Organization'){
                        add_text_to_doc("Renew Organization","action_button")
                        action_button.setAttribute("onclick", `location.replace('desk#Form/Umbrella%20or%20Consortium%20Organization%20Form/New%20Umbrella%20or%20Consortium%20Organization%20Form%201')`)
                    }
                    else{
                        add_text_to_doc("Renew Organization","action_button")
                        action_button.setAttribute("onclick", `location.replace('${message.link_to_renewals}')`)
                    }
                }

               getRenewalForm()

                // set values for the status section
                status_icon.setAttribute("class","fa fa-times-circle-o")
                status_icon.setAttribute("style","color:red;font-size:25px;")
                

            }else if(message.organization.status == "Pending Verification"){
                // set values for the instruction section
                var instruction_to_show = "Your organization form is pending verification,please complete and verify it"
                add_text_to_doc(instruction_to_show,"instructions_p")
                
                // check if button should be added
                if(message.view_form){
                    // add the renew button
                    add_text_to_doc("View Form","action_button")
                    action_button.setAttribute('class',"btn btn-success btn-sm step_button")
                    action_button.setAttribute("onclick",`location.replace('${message.view_form}')`)
                }else{

                }
                
                // set values for the status section
                status_icon.setAttribute("class","fa fa-times-circle-o")
                status_icon.setAttribute("style","color:red;font-size:25px;")
            
            }else if(message.organization.status == "Verified"){
                // set values for the instruction section
                var instruction_to_show = "Your organization form is verified,\
                pending Approval"
                add_text_to_doc(instruction_to_show,"instructions_p")

                // check if button should be added
                if(message.view_form){
                    // add the renew button
                    add_text_to_doc("View Form","action_button")
                    action_button.setAttribute('class',"btn btn-success btn-sm step_button")
                    action_button.setAttribute("onclick",`location.replace('${message.view_form}')`)
                }else{

                }
                // set values for the status section
                status_icon.setAttribute("class","fa fa-exclamation-circle")
                status_icon.setAttribute("style","color:#B7481D;font-size:25px;")

            }else if(message.organization.status == "Validated"){
                // set values for the instruction section
                var instruction_to_show = "Your organization form is Validated,\
                pending Approval"
                add_text_to_doc(instruction_to_show,"instructions_p")

                // check if button should be added
                if(message.view_form){
                    // add the renew button
                    add_text_to_doc("View Form","action_button")
                    action_button.setAttribute('class',"btn btn-success btn-sm step_button")
                    action_button.setAttribute("onclick",`location.replace('${message.view_form}')`)
                }else{

                }
                // set values for the status section
                status_icon.setAttribute("class","fa fa-exclamation-circle")
                status_icon.setAttribute("style","color:#B7481D;font-size:25px;")
            }else if(message.organization.status == "Approved"){
                // set values for the instruction section
                var instruction_to_show = "Your organization has been approved"
                add_text_to_doc(instruction_to_show,"instructions_p")

                // check if button should be added
                if(message.view_form){
                    // add the renew button
                    add_text_to_doc("View Form","action_button")
                    action_button.setAttribute('class',"btn btn-success btn-sm step_button")
                    action_button.setAttribute("onclick",`location.replace('${message.view_form}')`)
                }else{

                }
                // set values for the status section
                status_icon.setAttribute("class","fa fa-check-circle")
                status_icon.setAttribute("style","color:green;font-size:25px;")
            }
            else{
                
                
            }
        }else{
           
            if (message.pending_url) {
                 // set values for the instruction section
                var instruction_to_show = `You have a pending ${message.org}, please click on the button below to complete registration`
                add_text_to_doc(instruction_to_show,"instructions_p")
                add_text_to_doc("Complete Registration","action_button")
                action_button.setAttribute('class',"btn btn-success btn-sm step_button")
                action_button.setAttribute("onclick",`location.replace('${message.pending_url}')`)

            }else{
                 // set values for the instruction section
                var instruction_to_show = "You have not registered any organization,please click on the button below to register"
                add_text_to_doc(instruction_to_show,"instructions_p")
                add_text_to_doc("Register Organization","action_button")
                action_button.setAttribute('class',"btn btn-success btn-sm step_button")
                action_button.setAttribute("onclick","redirect_url('/organization_type')")
            }
        }
    }
}

function redirect_url(new_url){
    // allow the user to view page
    window.location = new_url
}

// get url to form
function get_url_to_form(clicked_button){
    var project_form_method_url = "/api/method/slaims.templates.pages.profile_methods.get_link_to_different_forms?args="
    var project_form_url = project_form_method_url +clicked_button.name

    // pull information from ajax
    $.ajax({
        url:project_form_url,
        async: true,
        dataType: 'json',
        success: function (response) {
            // redirect the page to the recieved url
            if(response.message){
                window.location = response.message
            }else{
                alert("An error occured while trying to retrive link to this form")
            }   
        },
        cache:false
    }); 
}

function main_function(){
    // add a loader to the contents tables
    add_remove_loader(true)
    // url to the method
    var url = "/api/method/slaims.templates.pages.profile_organization.return_context"
    
    // ajax calls to response
    $.ajax({
        url:url,
        async: true,
        dataType: 'json',
        success: function (response) {
            add_remove_loader(false)
            add_contexts_to_doc(response.message)  
        },
        cache:false
    });
}

// call the main function
main_function()


