// call the main function on refresh
main()

// ************************************ general functions section **************************************************
// function that calls all the other functions for the single view MoNDP interface
function main(){
    // add a loader to the contents tables
    add_remove_loader(true)

    // get requested organization
    var requested_organization_name = getUrlParameter('org_name')

    if(requested_organization_name){
        // url to the method
        var url = "/api/method/slaims.templates.pages.mopnd_interface_single.return_context?args="
        var arguments = {'organization_name':requested_organization_name}
        var url_with_args = url +JSON.stringify(arguments)

        // ajax calls to response
        $.ajax({
            url:url_with_args,
            async: true,
            dataType: 'json',
            success: function (response) {
                console.log("response from python")
                console.log(response)
                // remove the loader function
                add_remove_loader(false)
                // check if the user has privillages to view page
                if(response.message.view_status){
                    // add organization details to doc
                    add_contexts_to_doc(response.message) 
                }else{
                    // user does not have the prillage to view this page
                    alert("You do not have the privillage to view this page")
                    location.href = '/desk';
                }
            }
        });
    }else{
        // redirect to MoNDP interface
        location.href = '/mopnd_interface';
    } 
}

// function that gets parameters passed to the url 
function getUrlParameter(name){
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

// function that adds pulled data to doc
function add_contexts_to_doc(message){
    // add add general details
    add_text_to_doc(message.organization.name,"organization_name_holder")    
    add_text_to_doc(message.organization.acronym,"organization_acronym_holder")
    // add context to organization details
    add_organization_details_to_doc(message)
    // add context to project details
    add_project_details_to_doc(message)
    // add context to plan details
    add_plan_details_to_doc(message)
    // add context to report details
    add_report_details_to_doc(message)
    // add npre context to doc
    add_npre_details_to_doc(message)
}

// function that adds certain text to a given section
function add_text_to_doc(text_to_add,where_to_add){
    if(text_to_add){
        var user_name = document.createTextNode(text_to_add);
        document.getElementById(where_to_add).appendChild(user_name)
    }
}

function add_organization_details_to_doc(message){
    add_text_to_doc(message.organization.name,"organization_name") 
    add_text_to_doc(message.organization.acronym,"organization_acronym") 
    add_text_to_doc(message.organization.organization_type,"organization_type") 
    add_text_to_doc(message.organization.status,"organization_status") 

    var org_status = message.organization.status
    // determine the instruction to display and and icons to show
    if(org_status == "Registration Expired"){
        // set values for the instruction section
        var instruction_to_show = "The organization's registration has expired"
        add_text_to_doc(instruction_to_show,"instructions_p")
        
        // add the renew button
        add_text_to_doc("View Previous Registration Form","action_button")
        action_button.setAttribute('class',"btn btn-success btn-sm step_button")
        action_button.setAttribute("onclick",'get_url_to_form(this)')
        // set the name value attributes
        var name_org_name = `${message.organization.name}`
        var name_org_type = `${message.organization.organization_type}`
        var name_requested_form = 'Organization Form'
        var full_name_attribute =  {'org_name':name_org_name,'org_type':name_org_type,'req_form':name_requested_form}
        action_button.setAttribute('name',JSON.stringify(full_name_attribute))

        // set values for the status section
        status_icon.setAttribute("class","fa fa-times-circle-o")
        status_icon.setAttribute("style","color:red;font-size:25px;")
    
    }else if(org_status == "Pending Renewal"){
        // set values for the instruction section
        var instruction_to_show = "The organization's registration is Pending Renewal"
        add_text_to_doc(instruction_to_show,"instructions_p")
        
        // add the renew button
        add_text_to_doc("View Previous Registration Form","action_button")
        action_button.setAttribute('class',"btn btn-success btn-sm step_button")
        action_button.setAttribute("onclick",'get_url_to_form(this)')
        // set the name value attributes
        var name_org_name = `${message.organization.name}`
        var name_org_type = `${message.organization.organization_type}`
        var name_requested_form = 'Organization Form'
        var full_name_attribute =  {'org_name':name_org_name,'org_type':name_org_type,'req_form':name_requested_form}
        action_button.setAttribute('name',JSON.stringify(full_name_attribute))

        // set values for the status section
        status_icon.setAttribute("class","fa fa-times-circle-o")
        status_icon.setAttribute("style","color:red;font-size:25px;")
    
    }else if(org_status == "Pending Verification"){
        // set values for the instruction section
        var instruction_to_show = "The organization's registration form is Pending Verification"
        add_text_to_doc(instruction_to_show,"instructions_p")
        // add the renew button
        add_text_to_doc("View Form","action_button")
        action_button.setAttribute('class',"btn btn-success btn-sm step_button")
        action_button.setAttribute("onclick",'get_url_to_form(this)')
        // set the name value attributes
        var name_org_name = `${message.organization.name}`
        var name_org_type = `${message.organization.organization_type}`
        var name_requested_form = 'Organization Form'
        var full_name_attribute =  {'org_name':name_org_name,'org_type':name_org_type,'req_form':name_requested_form}
        action_button.setAttribute('name',JSON.stringify(full_name_attribute))

        // set values for the status section
        status_icon.setAttribute("class","fa fa-times-circle-o")
        status_icon.setAttribute("style","color:red;font-size:25px;")
    }else if(org_status == "Verified" || org_status =="Pending Validation"){
        // set values for the instruction section
        var instruction_to_show = "The organization's registration form is Verified ,please  \
        click on the button below to  View/Validate/Approve the form"
        add_text_to_doc(instruction_to_show,"instructions_p")
        // add the renew button
        add_text_to_doc("View Form","action_button")
        action_button.setAttribute('class',"btn btn-success btn-sm step_button")
        action_button.setAttribute("onclick",'get_url_to_form(this)')
        // set the name value attributes
        var name_org_name = `${message.organization.name}`
        var name_org_type = `${message.organization.organization_type}`
        var name_requested_form = 'Organization Form'
        var full_name_attribute =  {'org_name':name_org_name,'org_type':name_org_type,'req_form':name_requested_form}
        action_button.setAttribute('name',JSON.stringify(full_name_attribute))

        // set values for the status section
        status_icon.setAttribute("class","fa fa-exclamation-circle")
        status_icon.setAttribute("style","color:#B7481D;font-size:25px;")
    }else if(org_status == "Validated"){
        // set values for the instruction section
        var instruction_to_show = "The organization's registration form is Validated ,please  \
        click on the button below to  View/Validate/Approve the form"
        add_text_to_doc(instruction_to_show,"instructions_p")
        // add the renew button
        add_text_to_doc("View Form","action_button")
        action_button.setAttribute('class',"btn btn-success btn-sm step_button")
        action_button.setAttribute("onclick",'get_url_to_form(this)')
        // set the name value attributes
        var name_org_name = `${message.organization.name}`
        var name_org_type = `${message.organization.organization_type}`
        var name_requested_form = 'Organization Form'
        var full_name_attribute =  {'org_name':name_org_name,'org_type':name_org_type,'req_form':name_requested_form}
        action_button.setAttribute('name',JSON.stringify(full_name_attribute))

        // set values for the status section
        status_icon.setAttribute("class","fa fa-exclamation-circle")
        status_icon.setAttribute("style","color:#B7481D;font-size:25px;")
    }else if(org_status == "Approved" || org_status == "Admin Approved"){
        // set values for the instruction section
        var instruction_to_show = "This Organization is Approved"
        add_text_to_doc(instruction_to_show,"instructions_p")
        // add the renew button
        add_text_to_doc("View Form","action_button")
        action_button.setAttribute('class',"btn btn-success btn-sm step_button")
        action_button.setAttribute("onclick",'get_url_to_form(this)')
        // set the name value attributes
        var name_org_name = `${message.organization.name}`
        var name_org_type = `${message.organization.organization_type}`
        var name_requested_form = 'Organization Form'
        var full_name_attribute =  {'org_name':name_org_name,'org_type':name_org_type,'req_form':name_requested_form}
        action_button.setAttribute('name',JSON.stringify(full_name_attribute))

        // set values for the status section
        status_icon.setAttribute("class","fa fa-check-circle")
        status_icon.setAttribute("style","color:green;font-size:25px;")
    }
}

// function that adds a loader to a page
function add_remove_loader(action){
    if(action== true){        
        var loader_div_1 = document.createElement("div");
        loader_div_1.setAttribute('class',"loader")
        loader_div_1.setAttribute('id',"loader_1")

        var loader_div_2 = document.createElement("div");
        loader_div_2.setAttribute('class',"loader")
        loader_div_2.setAttribute('id',"loader_2")

        var loader_div_3 = document.createElement("div");
        loader_div_3.setAttribute('class',"loader")
        loader_div_3.setAttribute('id',"loader_3")

        var loader_div_4 = document.createElement("div");
        loader_div_4.setAttribute('class',"loader")
        loader_div_4.setAttribute('id',"loader_4")

        var loader_div_5 = document.createElement("div");
        loader_div_5.setAttribute('class',"loader")
        loader_div_5.setAttribute('id',"loader_5")

        document.getElementById("loader_column_1").appendChild(loader_div_1)
        document.getElementById("loader_column_2").appendChild(loader_div_2)
        document.getElementById("loader_column_3").appendChild(loader_div_3)
        document.getElementById("loader_column_4").appendChild(loader_div_4)
        document.getElementById("loader_column_5").appendChild(loader_div_5)
        
    }else{
        var elem = document.getElementById("loader_1");
        elem.parentNode.removeChild(elem);

        var elem = document.getElementById("loader_2");
        elem.parentNode.removeChild(elem);

        var elem = document.getElementById("loader_3");
        elem.parentNode.removeChild(elem);

        var elem = document.getElementById("loader_4");
        elem.parentNode.removeChild(elem);

        var elem = document.getElementById("loader_5");
        elem.parentNode.removeChild(elem);
    }   
}


function add_project_details_to_doc(message){
    // check if any pending forms exist
    if(message.pending_project_forms.length > 0){
        // loop throogh the forms
        message.pending_project_forms.forEach(element => {
            // create a div for each form
            create_project_div(element)
        })
    }
    if(message.projects.length > 0 ){
        // loop through all found projects
        message.projects.forEach(element => {
            // add each project to the document
            create_project_div(element)
        });
    }else{
        // add the no projects found div
        var no_projects_found_inst = "There are currently no active projects registered by the organization"
        add_text_to_doc(no_projects_found_inst,"no_projects_found_p")
        no_projects_found_i.setAttribute("class","fa fa-check-circle-o")
        no_projects_div_hr.setAttribute("style","width:98%;")
    }
}
    
function create_project_div(project){
    // create  project row
    var project_row = document.createElement("div");
    project_row.setAttribute('class',"col-md-12")
    var project_details_column = document.createElement("div");
    project_details_column.setAttribute('class',"col-md-4")

    function create_label_n_text(label_name,value){
        var label_n_text_selement = document.createElement("h5");
        // create the label
        var label_span = document.createElement("span");
        label_span.setAttribute('style',"font-weight:bold")
        var label_name = document.createTextNode(label_name);
        var label_name_space = document.createTextNode(" :");
        label_span.appendChild(label_name)
        label_span.appendChild(label_name_space)
        // create the text
        if(value){
            var project_name_value = document.createTextNode(value);
        }else{
            var project_name_value = document.createTextNode("");
        }
        
        // append to label elements
        label_n_text_selement.appendChild(label_span)
        label_n_text_selement.appendChild(project_name_value)

        // finally retun the crated elements
        return label_n_text_selement
    }

    // add instructions details
    var instruction_paragraph = document.createElement("p");
    instruction_paragraph.setAttribute('style',"font-style:italic")
    var project_instructions_column = document.createElement("div");
    project_instructions_column.setAttribute('class',"col-md-6")
    var inst_action_button = document.createElement("button");
    var inst_button_text = document.createTextNode("View Form")
    inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
    
    // add status indicator for the project
    var project_status_value = document.createTextNode("This project is already verified, \
    please Validate it");
    var project_status_icon = document.createElement("i");

    // detemine if the instance is an actual project or a form
    if('form_type' in project){
        // add project-details
        project_details_column.appendChild(create_label_n_text("Project Name",project['new_project_title']))
        project_details_column.appendChild(create_label_n_text("Project Code",project['new_project_code']))
        project_details_column.appendChild(create_label_n_text("Project Status",project['status']))

        var project_instruction = document.createTextNode(`The organization has an incomplete \
        project form for project '${project['new_project_title']}'`);
        var status_icon_class = "fa fa-times-circle-o"
        var status_icon_style = "color:red;font-size:25px;"

        // and url parameters to the button
        var name_proj_name = project['new_project_title']
        var name_proj_org = project['select_your_organization']
        var form_name = project['name']
        var full_name_attribute =  {'project_name':name_proj_name,'orgization_name':name_proj_org,'form_name':form_name,'req_form':'Project Form'}

    }else{
        // add project-details
        project_details_column.appendChild(create_label_n_text("Project Name",project.project_doc.name))
        project_details_column.appendChild(create_label_n_text("Project Code",project.project_doc.project_code))
        project_details_column.appendChild(create_label_n_text("Project Status",project.project_doc.status))

        // determine instructions to show and color to show on icons
        if(project.project_doc.status){
            // check if the organization's user is the destination organization
            if(project.destination_organization){
                if(project.project_doc.status == "Pending Verification"){
                    var project_instruction = document.createTextNode("This project is Pending \
                    Verification by the organization");
                }else if(project.project_doc.status == "Verified"){
                    var project_instruction = document.createTextNode("This project has been Verified\
                    by the organization please validate it");
                }else if(project.project_doc.status == "Validated"){
                    var project_instruction = document.createTextNode("This project is Validated \
                    please Approve it");
                }else if(project.project_doc.status == "Approved"){
                    var project_instruction = document.createTextNode("This project has been Approved");                    
                }
            }else{
                // determine instructions to show
                if(project.implementing_organization){
                // check if the user is also reporting
                    if(project.reporting_organization){
                        var project_instruction = document.createTextNode("The organization has both \
                        implementing and reporting obligations for this project ,\
                        please check if the organization has submitted both this project");
                    }else{
                        var project_instruction = document.createTextNode("The organization is an \
                        implementing patner for this project,but does not have any reporting obligation");
                    }
                }else if(project.reporting_organization){
                    var project_instruction = document.createTextNode("The organization has reporting \
                    obligations for this project,please check that the organization has submitted and \
                    Annual Plan and Report for this project"); 
                }
            }

            // determine status icon to show
            if(project.project_doc.status == "Pending Verification"){
                var status_icon_class = "fa fa-times-circle-o"
                var status_icon_style = "color:red;font-size:25px;"
            }else if(project.project_doc.status == "Verified"){
                var status_icon_class = "fa fa-exclamation-circle"
                var status_icon_style = "color:#B7481D;font-size:25px;"
            }else if(project.project_doc.status == "Validated"){
                var status_icon_class = "fa fa-exclamation-circle"
                var status_icon_style = "color:#B7481D;font-size:25px;"
            }else if(project.project_doc.status == "Approved"){
                var status_icon_class = "fa fa-check-circle"
                var status_icon_style = "color:green;font-size:25px;"
            } 
        }else{
            var project_instruction = document.createTextNode("This project is Pending \
            Verification by the organization");
            var status_icon_class = "fa fa-times-circle-o"
            var status_icon_style = "color:red;font-size:25px;"
        }

        // and url parameters to the button
        var name_proj_name = `${project.project_doc.name}`
        var name_proj_org = `${project.project_doc.your_organization}`
        var full_name_attribute =  {'project_name':name_proj_name,'orgization_name':name_proj_org,'req_form':'Project Form'}
    }

    // set url attribute for the button
    inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))
    inst_action_button.setAttribute('onclick','get_url_to_form(this)')
    inst_action_button.appendChild(inst_button_text)
    
    // append project details elements
    instruction_paragraph.appendChild(project_instruction)
    project_instructions_column.appendChild(instruction_paragraph)
    // only add the button if the user is a destination organization
    if(project.destination_organization){
        project_instructions_column.appendChild(inst_action_button)
    }else if('form_type' in project){
        // if form is pending approval also  create a button
        project_instructions_column.appendChild(inst_action_button)
    }
    
    // create status column
    var project_status_column = document.createElement("div");
    project_status_column.setAttribute('class',"col-md-2")
    project_status_icon.setAttribute('class',status_icon_class)
    project_status_icon.setAttribute('style',status_icon_style)
    // append project details elements
    instruction_paragraph.appendChild(project_instruction)
    project_status_column.appendChild(project_status_icon)

    // append elements to row
    project_row.appendChild(project_details_column)
    project_row.appendChild(project_instructions_column)
    project_row.appendChild(project_status_column)
    
    // Append the row to dom
    document.getElementById("project_div").appendChild(project_row)
    var project_hr = document.createElement("hr");
    project_hr.setAttribute("width", "98%");
    document.getElementById("project_div").appendChild(project_hr)
}

function add_plan_details_to_doc(message){
    // check if the user has any pending forms
    if(message.pending_plan_forms.length >0 ){
        // loop through all found plans
        message.pending_plan_forms.forEach(form_per_project => {
            // loop throgh all the relavant plans for each project
            create_plan_div(form_per_project)
        });
    }
    
    if(message.plans.length > 0 ){
        // loop through all found plans
        message.plans.forEach(plans_per_project => {
            create_plan_div(plans_per_project)
        });
    }else{
        // add to no plans required div
        var no_plans_required_inst = "This organization does not need to create any any annual plans \
        as it does not have any currently active projects"
        add_text_to_doc(no_plans_required_inst,"no_plans_required_p")
        no_plans_required_i.setAttribute("class","fa fa-check-circle-o")
    }
}

function create_plan_div(plan){
    // create  plan row
    var plan_row = document.createElement("div");
    plan_row.setAttribute('class',"col-md-12")
    var plan_details_column = document.createElement("div");
    plan_details_column.setAttribute('class',"col-md-4");

    function create_label_n_text(label_name,value){
        var label_n_text_selement = document.createElement("h5");
        // create the label
        var label_span = document.createElement("span");
        label_span.setAttribute('style',"font-weight:bold")
        var label_name = document.createTextNode(label_name);
        var label_name_space = document.createTextNode(" :");
        label_span.appendChild(label_name)
        label_span.appendChild(label_name_space)
        
        // create the text
        if(value){
            var project_name_value = document.createTextNode(value);
        }else{
            var project_name_value = document.createTextNode("");
        }
        
        // append to label elements
        label_n_text_selement.appendChild(label_span)
        label_n_text_selement.appendChild(project_name_value)

        // finally retun the crated elements
        return label_n_text_selement
    }

    // add instructions details
    var instruction_paragraph = document.createElement("p");
    instruction_paragraph.setAttribute('style',"font-style:italic")
    var plan_instructions_column = document.createElement("div");
    plan_instructions_column.setAttribute('class',"col-md-6")

    if('form_type' in plan){
        // add project-details
        plan_details_column.appendChild(create_label_n_text("Plan For Project:",plan.project_title))
        plan_details_column.appendChild(create_label_n_text("Status",plan.status))
        plan_details_column.appendChild(create_label_n_text("Year",plan.annual_plan_year))
        var name_proj_name = `${plan.project_title}`
        var name_plan_org = `${plan.organization_name}`
        var name_plan_year = `${plan.annual_plan_year}`
        var form_name = `${plan.name}`
        // create instructions the user will see
        var plan_instruction = document.createTextNode(`This organization has an incomplete \
        annual plan form for project'${name_proj_name}'`);
        var status_icon_class = "fa fa-times-circle-o"
        var status_icon_style = "color:red;font-size:25px;"
        // add details to form
        var inst_action_button = document.createElement("button");
        inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
        var inst_button_text = document.createTextNode("View Form")
        inst_action_button.setAttribute('onclick','get_url_to_form(this)')
        inst_action_button.appendChild(inst_button_text)
        // button attribute
        var full_name_attribute = {'project_name':name_proj_name,'organization_name':name_plan_org,'year':name_plan_year,'form_name':form_name,'req_form':'Annual Plan Form'}
        inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))
    }else{
        // add plan details
        plan_details_column.appendChild(create_label_n_text("Plan For Project:",plan.project_name))
        plan_details_column.appendChild(create_label_n_text("Status",plan.plan_status))
        plan_details_column.appendChild(create_label_n_text("Year",plan.financial_year))  
        plan_details_column.appendChild(create_label_n_text("Implementing Organization",plan.implementing_orginization))
        plan_details_column.appendChild(create_label_n_text("Reporting Organization",plan.reporting_organization))
        var name_proj_name = `${plan.project_name}`
        var name_plan_org = `${plan.implementing_orginization}`
        var name_plan_year = `${plan.financial_year}`
        // initialize inst_button_text as NaN so if they are not 
        // defined the button will be ignored
        inst_button_text = NaN
        inst_action_button = NaN
        // determine instructions to show
        if(plan.reporting_status){
            // check if the plan has been created
            if(plan.plan_status){
                if(plan.plan_status == "Pending Verification"){
                    // check if user has reporting obligations
                    var plan_instruction = document.createTextNode("This plan is pending \
                    Verification by the reporting organization");
                    var status_icon_class = "fa fa-times-circle"
                    var status_icon_style = "color:red;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
    
                }else if(plan.plan_status == "Verified"){
                    var plan_instruction = document.createTextNode("This Annual Plan is Verified \
                    please Validate it");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
 
                }else if(plan.plan_status == "Validated"){
                    var plan_instruction = document.createTextNode("This Annual Plan is Validated \
                    please Approve it");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")

                }else if(plan.plan_status == "Approved"){
                    var plan_instruction = document.createTextNode("This Annual Plan has been Approved");
                    var status_icon_class = "fa fa-check-circle-o"
                    var status_icon_style = "color:green;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
                }
                // create the name attribute for the action button
                var full_name_attribute = {'project_name':name_proj_name,'organization_name':name_plan_org,'year':name_plan_year,'req_form':'Annual Plan Form'}
            }else{
                var plan_instruction = document.createTextNode(`This organization has not submitted an Annual Plan for \
                the project ${plan.project_name} for the year ${plan.financial_year} for the implementing organization \
                ${plan.reporting_organization}`);
                var status_icon_class = "fa fa-times-circle-o"
                var status_icon_style = "color:red;font-size:25px;"
            }

            // since the user has reporting obligation create an action button for the user
            if(inst_button_text){
                var inst_action_button = document.createElement("button");
                inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
                inst_action_button.appendChild(inst_button_text)
                inst_action_button.setAttribute('onclick','get_url_to_form(this)')
                inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))
            }
            
        }else{
            // this handles situations when the organization is not the reporting organization
            // check if the plan has been created
            if(plan.plan_status){
                if(plan.plan_status == "Pending Verification"){
                    var plan_instruction = document.createTextNode(`This plan is pending \
                    Verification.The reporting organization ${plan.reporting_organization}\
                    needs to complete the form and verify it`);
                    var status_icon_class = "fa fa-times-circle"
                    var status_icon_style = "color:red;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
    
                }else if(plan.plan_status == "Verified"){
                    var plan_instruction = document.createTextNode("This Annual Plan is Verified \
                    please Validate it");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
 
                }else if(plan.plan_status == "Validated"){
                    var plan_instruction = document.createTextNode("This Annual Plan is Validated \
                    please Approve it");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")

                }else if(plan.plan_status == "Approved"){
                    var plan_instruction = document.createTextNode("This Annual Plan has been Approved");
                    var status_icon_class = "fa fa-check-circle-o"
                    var status_icon_style = "color:green;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
                }
                // create the name attribute for the action button
                var full_name_attribute = {'project_name':name_proj_name,'organization_name':name_plan_org,'year':name_plan_year,'req_form':'Annual Plan Form'}
            }else{
                var plan_instruction = document.createTextNode(`An Annual Plan for \
                the project '${plan.project_name}',year-'${plan.financial_year}' \
                for the Implementing Organization '${plan.implementing_orginization}' \
                has not been submitted. Please request \
                the reporting Organization '${plan.reporting_organization}' to submit this Plan `);
                var status_icon_class = "fa fa-times-circle-o"
                var status_icon_style = "color:red;font-size:25px;"
            }

            if(inst_button_text){
                // since the user has reporting obligation create an action button for the user
                var inst_action_button = document.createElement("button");
                inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
                inst_action_button.appendChild(inst_button_text)
                inst_action_button.setAttribute('onclick','get_url_to_form(this)')
                inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))
            }
        }
    }
   
    // append project details elements
    instruction_paragraph.appendChild(plan_instruction)
    plan_instructions_column.appendChild(instruction_paragraph)
    if(inst_action_button){
        plan_instructions_column.appendChild(inst_action_button)
    }
    
    // create status column
    var plan_status_icon = document.createElement("i");
    var plan_status_column = document.createElement("div");
    plan_status_column.setAttribute('class',"col-md-2")
    plan_status_icon.setAttribute('class',status_icon_class)
    plan_status_icon.setAttribute('style',status_icon_style)
    plan_status_column.appendChild(plan_status_icon)

    // append elements to row
    plan_row.appendChild(plan_details_column)
    plan_row.appendChild(plan_instructions_column)
    plan_row.appendChild(plan_status_column)
    
    // Append the row to dom
    document.getElementById("plan_div").appendChild(plan_row)
    var plan_hr = document.createElement("hr");
    plan_hr.setAttribute("width", "98%");
    document.getElementById("plan_div").appendChild(plan_hr)
}

function add_report_details_to_doc(message){
    // check if the user has any pending forms
    if(message.pending_report_forms.length >0 ){
        // loop through all found pending reports
        message.pending_report_forms.forEach(form_per_project => {
            create_report_div(form_per_project)
        });
    }
    
    if(message.reports.length > 0 ){
        // loop through all found reports
        message.reports.forEach(report_per_project => {
            create_report_div(report_per_project)
        });
    }else{
        // add to no reports required div
        var no_report_required_inst = "This organization does not need to create any Annual Reports \
        as it does not have any currently active projects"
        add_text_to_doc(no_report_required_inst,"no_report_required_p")
        no_report_required_i.setAttribute("class","fa fa-check-circle-o")
    }
}

function create_report_div(report){
    // create  report row
    var report_row = document.createElement("div");
    report_row.setAttribute('class',"col-md-12")
    var report_details_column = document.createElement("div");
    report_details_column.setAttribute('class',"col-md-4");

    function create_label_n_text(label_name,value){
        var label_n_text_selement = document.createElement("h5");
        // create the label
        var label_span = document.createElement("span");
        label_span.setAttribute('style',"font-weight:bold")
        var label_name = document.createTextNode(label_name);
        var label_name_space = document.createTextNode(" :");
        label_span.appendChild(label_name)
        label_span.appendChild(label_name_space)
        
        // create the text
        if(value){
            var project_name_value = document.createTextNode(value);
        }else{
            var project_name_value = document.createTextNode("");
        }
        
        // append to label elements
        label_n_text_selement.appendChild(label_span)
        label_n_text_selement.appendChild(project_name_value)

        // finally retun the crated elements
        return label_n_text_selement
    }

    // add instructions details
    var instruction_paragraph = document.createElement("p");
    instruction_paragraph.setAttribute('style',"font-style:italic")
    var report_instructions_column = document.createElement("div");
    report_instructions_column.setAttribute('class',"col-md-6")
    

    if('form_type' in report){
        // add project-details
        report_details_column.appendChild(create_label_n_text("Report For Project:",report.project_title))
        report_details_column.appendChild(create_label_n_text("Status",report.status))
        report_details_column.appendChild(create_label_n_text("Year",report.annual_report_year))
        var name_proj_name = `${report.project_title}`
        var name_report_org = `${report.organization_name}`
        var name_report_year = `${report.annual_report_year}`
        var form_name = `${report.name}`
        // create instructions the user will see
        var report_instruction = document.createTextNode(`This organization has an incomplete \
        annual report form for project '${name_proj_name}'`);
        var status_icon_class = "fa fa-times-circle-o"
        var status_icon_style = "color:red;font-size:25px;"
        // add details to form
        var inst_action_button = document.createElement("button");
        inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
        var inst_button_text = document.createTextNode("View Form")
        inst_action_button.setAttribute('onclick','get_url_to_form(this)')
        inst_action_button.appendChild(inst_button_text)
        // button attribute
        var full_name_attribute = {'project_name':name_proj_name,'organization_name':name_report_org,'year':name_report_year,'form_name':form_name,'req_form':'Annual Report Form'}
        inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))
    }else{
        // add report details
        report_details_column.appendChild(create_label_n_text("Report For Project:",report.project_name))
        report_details_column.appendChild(create_label_n_text("Status",report.report_status))
        report_details_column.appendChild(create_label_n_text("Year",report.financial_year))  
        report_details_column.appendChild(create_label_n_text("Implementing Organization",report.implementing_orginization))
        report_details_column.appendChild(create_label_n_text("Reporting Organization",report.reporting_organization))
        var name_proj_name = `${report.project_name}`
        var name_report_org = `${report.implementing_orginization}`
        var name_report_year = `${report.financial_year}`
        // initialize the variables to NaN
        inst_button_text = NaN
        inst_action_button = NaN

        // determine instructions to show
        if(report.reporting_status){
            // check if the report has been created
            if(report.report_status){
                if(report.report_status == "Pending Verification"){
                    // check if user has reporting obligations
                    var report_instruction = document.createTextNode('This report is pending \
                    Verification by this organization');
                    var status_icon_class = "fa fa-times-circle"
                    var status_icon_style = "color:red;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
    
                }else if(report.report_status == "Verified"){
                    var report_instruction = document.createTextNode("The Annual Report is Verified \
                    please Validate it");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
 
                }else if(report.report_status == "Validated"){
                    var report_instruction = document.createTextNode("This Annual Report is Validated \
                    please Approve it");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")

                }else if(report.report_status == "Approved"){
                    var report_instruction = document.createTextNode("This Annual Report is Approved");
                    var status_icon_class = "fa fa-check-circle-o"
                    var status_icon_style = "color:green;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
                    
                }
                // create the name attribute for the action button
                var full_name_attribute = {'project_name':name_proj_name,'organization_name':name_report_org,'year':name_report_year,'req_form':'Annual Report Form'}
            }else{
                var report_instruction = document.createTextNode(`This organization has not submitted an Annual Report for \
                the project '${report.project_name}' for the year ${report.financial_year} for the implementing \
                organization '${report.implementing_orginization}'`);
                var status_icon_class = "fa fa-times-circle-o"
                var status_icon_style = "color:red;font-size:25px;"
            }

            //add a view button if there is a form to view
            if(inst_button_text){
                var inst_action_button = document.createElement("button");
                inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
                inst_action_button.appendChild(inst_button_text)
                inst_action_button.setAttribute('onclick','get_url_to_form(this)')
                inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))
            }
            
        }else{
            // this handles situations when the organization is not the reporting organization
            // check if the report has been created
            if(report.report_status){
                if(report.report_status == "Pending Verification"){
                    var report_instruction = document.createTextNode(`This report is pending \
                    Verification by the reporting organization '${report.reporting_organization}'`);
                    var status_icon_class = "fa fa-times-circle"
                    var status_icon_style = "color:red;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
    
                }else if(report.report_status == "Verified"){
                    var report_instruction = document.createTextNode("This Annual Report is Verified \
                    please Validate it");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
 
                }else if(report.report_status == "Validated"){
                    var report_instruction = document.createTextNode("This Annual Report is Validated \
                    please Approve it");
                    var status_icon_class = "fa fa-exclamation-circle"
                    var status_icon_style = "color:#B7481D;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")

                }else if(report.report_status == "Approved"){
                    var report_instruction = document.createTextNode("This Annual Report is Approved");
                    var status_icon_class = "fa fa-check-circle-o"
                    var status_icon_style = "color:green;font-size:25px;"
                    var inst_button_text = document.createTextNode("View Form")
                    
                }
                // create the name attribute for the action button
                var full_name_attribute = {'project_name':name_proj_name,'organization_name':name_report_org,'year':name_report_year,'req_form':'Annual Report Form'}
            }else{
                var report_instruction = document.createTextNode(`The reporting organization \
                '${report.reporting_organization}' has not submitted an Annual Report' for \
                the project '${report.project_name}' for the year year-'${report.financial_year}' \
                for the Implementing Organization '${report.implementing_orginization}'`);
                var status_icon_class = "fa fa-times-circle-o"
                var status_icon_style = "color:red;font-size:25px;"
            }

            // check if there is a  form to view
            if(inst_button_text){
                inst_action_button = document.createElement("button");
                inst_action_button.setAttribute('class',"btn btn-success btn-sm step_button")
                inst_action_button.appendChild(inst_button_text)
                inst_action_button.setAttribute('onclick','get_url_to_form(this)')
                inst_action_button.setAttribute('name',JSON.stringify(full_name_attribute))                
            }
        }
    }
   
    // append project details elements
    instruction_paragraph.appendChild(report_instruction)
    report_instructions_column.appendChild(instruction_paragraph)
    if(inst_action_button){
        report_instructions_column.appendChild(inst_action_button)
    }
    
    // create status column
    var report_status_icon = document.createElement("i");
    var report_status_column = document.createElement("div");
    report_status_column.setAttribute('class',"col-md-2")
    report_status_icon.setAttribute('class',status_icon_class)
    report_status_icon.setAttribute('style',status_icon_style)
    report_status_column.appendChild(report_status_icon)

    // append elements to row
    report_row.appendChild(report_details_column)
    report_row.appendChild(report_instructions_column)
    report_row.appendChild(report_status_column)
    
    // Append the row to dom
    document.getElementById("report_div").appendChild(report_row)
    var report_hr = document.createElement("hr");
    report_hr.setAttribute("width", "98%");
    document.getElementById("report_div").appendChild(report_hr)
}

function add_npre_details_to_doc(message){
    add_text_to_doc(message.organization.name,"organization_name_holder_npre")
    add_text_to_doc(message.npre.year,"npre_year_holder")
    add_text_to_doc(message.npre.form_status,"npre_status")

    // check if npre exists
    if(message.npre.npre){
        // based on status determine message
        if(message.npre.form_status == "Pending Verification"){
            status_icon_indicator.setAttribute("class","fa fa-times-circle")
            status_icon_indicator.setAttribute("style","color:red;font-size:15px;")

            // add instructions to the instructions section
            var instructions_to_display = `Non project related expenditure for year ${message.npre.year} \
            for this organization is Pending Verification`
            // style the status indicator
            npre_status_indicator.setAttribute("class","fa fa-times-circle-o")
            npre_status_indicator.setAttribute("style","color:red;font-size:25px;")

        }else if(message.npre.form_status == "Verified"){
            status_icon_indicator.setAttribute("class","fa fa-exclamation-circle")
            status_icon_indicator.setAttribute("style","color:#B7481D;font-size:15px;")

            // add instructions to the instructions section
            var instructions_to_display = `Non project related expenditure for year ${message.npre.year} \
            for this organization is Verified please validate it`
            // style the status indicator
            npre_status_indicator.setAttribute("class","fa fa-exclamation-circle")
            npre_status_indicator.setAttribute("style","color:#B7481D;font-size:25px;")

        }else if(message.npre.form_status == "Validated"){
            status_icon_indicator.setAttribute("class","fa fa-exclamation-circle")
            status_icon_indicator.setAttribute("style","color:#B7481D;font-size:15px;")

            // add instructions to the instructions section
            var instructions_to_display = `Non project related expenditure for year ${message.npre.year} \
            for this organization is Validated please approve it`
            // style the status indicator
            npre_status_indicator.setAttribute("class","fa fa-exclamation-circle")
            npre_status_indicator.setAttribute("style","color:#B7481D;font-size:25px;")

        }else if(message.npre.form_status == "Approved"){
            status_icon_indicator.setAttribute("class","fa fa-exclamation-circle")
            status_icon_indicator.setAttribute("style","color:green;font-size:15px;")

            // add instructions to the instructions section
            var instructions_to_display = `Non project related expenditure for year ${message.npre.year} \
            for this organization is Approved`
            // style the status indicator
            npre_status_indicator.setAttribute("class","fa fa-check-circle")
            npre_status_indicator.setAttribute("style","color:green;font-size:25px;")

        }
        else{
            status_icon_indicator.setAttribute("class","fa fa-check-circle-o")
            status_icon_indicator.setAttribute("style","color:green;")

            // add instructions to the instructions section
            var instructions_to_display = `Non project related expenditure for year ${message.npre.year} \
            for this organization exist but no action has been taken on it such as Verifiation`
            // style the status indicator
            npre_status_indicator.setAttribute("class","fa fa-check-circle-o")
            npre_status_indicator.setAttribute("style","color:green;font-size:25px;")
        }
        
        add_text_to_doc(instructions_to_display,"instructions_npre") 
        instruction_action_button.setAttribute("class","btn btn-success btn-sm step_button")
        add_text_to_doc("View Form","instruction_action_button")
        instruction_action_button.setAttribute("onclick",`location.replace('${message.npre.url_form}')`)

    }else{
        status_icon_indicator.setAttribute("class","fa fa-exclamation-circle")
        status_icon_indicator.setAttribute("style","color:red;font-size:15px;")
        // add instructions to the instructions section
        var instructions_to_display = `Non project related expenditure for year ${message.npre.year} \
        for this organization has not been submitted`
        add_text_to_doc(instructions_to_display,"instructions_npre")
        // style the status indicator
        npre_status_indicator.setAttribute("class","fa fa-times-circle-o")
        npre_status_indicator.setAttribute("style","color:red;font-size:25px;")
    }
}

// get url to form
function get_url_to_form(clicked_button){
    var project_form_method_url = "/api/method/slaims.templates.pages.profile_methods.get_link_to_different_forms?args="
    var project_form_url = project_form_method_url +clicked_button.name

    // pull information from ajax
    $.ajax({
        url:project_form_url,
        async: true,
        dataType: 'json',
        success: function (response) {
            // redirect the page to the recieved url
            if(response.message){
                window.location = response.message
            }else{
                alert("An error occured while trying to retrive link to this form")
            }   
        },
        cache:false
    }); 
}