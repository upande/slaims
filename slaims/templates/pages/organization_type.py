from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from frappe.model.document import Document
import random
import requests
import pymysql.cursors
import json


user = frappe.session.user

# linked_organizations = frappe.get_list('Organization', filters=[''])


def get_context(context):
    if frappe.session.user == 'Administrator':
        pass
    else:
        context['organization_types'] = frappe.get_all('Organization Type')
        context['user'] = frappe.session.user
        