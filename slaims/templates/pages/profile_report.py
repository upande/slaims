from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from frappe.model.document import Document
import random
import requests
import pymysql.cursors
import json


# imports from methods module
from .profile_methods import get_user_organization,get_current_projects,get_reports_linked_to_project,\
    get_report_obligations_n_statuses

def get_context(context):
    pass

@frappe.whitelist(allow_guest = True)
def return_context():
    '''
    Function that returns the context for 
    annual reports page
    '''
    return_dict = {}
    logged_in_user = frappe.session.user
    # get a list of roles
    list_of_current_user_roles = frappe.get_roles(logged_in_user)

    # check is the logged in user has role of "Organization Login"
    if "Organization Login" in list_of_current_user_roles:
        current_organization = get_user_organization(logged_in_user)
        return_dict['user'] = logged_in_user
        return_dict["organization"] = current_organization

        # get all current obligations and statuses
        list_of_obligation_n_statuses = get_report_obligations_n_statuses(current_organization)
        # add plans to return dict
        return_dict['reports'] = list_of_obligation_n_statuses

        # get plans pending verification and have no reports
        list_of_supposed_report_forms = frappe.get_list("Annual Report Form",
            fields=["*"],
            filters = {
                "status":"Pending Verification",
		})

        list_of_pending_forms = []
        # loop through all the pending verifications forms to determine which does 
        # not have a report
        for form in list_of_supposed_report_forms:
            report_form_name = form.project_title+"-"+form.organization_acronym+"-("+str(form.annual_report_year)+")-AR"
            # check if plan exist
            try:
                frappe.get_doc('Annual Report',report_form_name)
            except:
                list_of_pending_forms.append(form)

        return_dict['pending_forms'] = list_of_pending_forms

        # return the dictionry with reports and form
        return return_dict
    else:
        # throw an error if the logged in user is Staff
        frappe.throw("This is the basic user interface use the MoNDP interface instead") 