import json,frappe
from datetime import datetime
from .profile_methods import get_current_projects,get_plan_obligations_n_statuses,\
    get_report_obligations_n_statuses,get_npre_for_specific_year

# user = frappe.session.user

def get_context(context):
    pass

@frappe.whitelist(allow_guest = True)
def return_context(args):
    '''
    Function that returns context for the MoNDP interface
    using javascript
    '''
    return_dict = {}
    logged_in_user = frappe.session.user
    # get list of user roles and add the different roles to the return dict
    list_of_current_user_roles = frappe.get_roles(logged_in_user)

    if "Staff" in list_of_current_user_roles or "Super Admin" in list_of_current_user_roles:
        return_dict['view_status'] = True
        json_args = json.loads(args)

        # add organmization details to return dict
        requested_organization = json_args['organization_name']
        current_organization = frappe.get_doc("Organization",requested_organization)
        return_dict['organization'] = current_organization
        
        # get all current active projects based on organization 
        # current financial year
        list_of_project_key_value_pairs = get_current_projects(requested_organization)
        # add key value pairs to return dict
        return_dict['projects'] = list_of_project_key_value_pairs
        # get all the pending 
        list_of_pending_project_forms = frappe.get_list("Project Registration Form",
            fields=["name","new_project_code","status","new_project_title","form_type","select_your_organization"],
            filters = {
                "status":"Pending Verification",
                "sub_project_title":["=",""],
                "select_your_organization":requested_organization
        })
        return_dict['pending_project_forms'] = list_of_pending_project_forms

        # get all current Annual Plan obligations and statuses
        list_of_obligation_n_statuses = get_plan_obligations_n_statuses(current_organization)
        # add plans to return dict
        return_dict['plans'] = list_of_obligation_n_statuses
        # get plan forms pending verification and have no plans linked to them
        # initialize an empty list of pending forms
        list_of_pending_plan_forms = []
        # check if the current organization has user asscociated with it
        if current_organization.user:
            # get Annual Plan Forms pending verification
            list_of_supposed_plan_forms = frappe.get_list("Annual Plan Form",
                fields=["*"],
                filters = {
                    "status":"Pending Verification",
                    "owner":current_organization.user
            })
            # loop through all the pending verifications forms to determine which does 
            # not have a plan
            for form in list_of_supposed_plan_forms:
                plan_form_name = form.project_title+"-"+form.organization_acronym+"-("+str(form.annual_plan_year)+")-AP"
                # check if plan exist
                try:
                    frappe.get_doc('Annual Plan',plan_form_name)
                except:
                    list_of_pending_plan_forms.append(form)

        return_dict['pending_plan_forms'] = list_of_pending_plan_forms

        # get all current Annual Report obligations and statuses
        list_of_obligation_n_statuses = get_report_obligations_n_statuses(current_organization)
        # add plans to return dict
        return_dict['reports'] = list_of_obligation_n_statuses
        # get report forms pending verification and have no reports linked to them
        # initialize an empty list of pending forms
        list_of_pending_report_forms = []
        # check if the current organization has user asscociated with it
        if current_organization.user:
            # get report forms pending verification and have no reports
            list_of_supposed_report_forms = frappe.get_list("Annual Report Form",
                fields=["*"],
                filters = {
                    "status":"Pending Verification",
                    "owner":current_organization.user
            })
            # loop through all the pending verifications forms to determine which does 
            # not have a report
            for form in list_of_supposed_report_forms:
                report_form_name = form.project_title+"-"+form.organization_acronym+"-("+str(form.annual_report_year)+")-AR"
                # check if plan exist
                try:
                    frappe.get_doc('Annual Report',report_form_name)
                except:
                    list_of_pending_report_forms.append(form)

        return_dict['pending_report_forms'] = list_of_pending_report_forms
        
        # Get the organization's NPRE
        # check if the organization has a current financial year
        if current_organization.current_registration_year:
            current_organization_year = current_organization.current_registration_year
        else:
            global_settings_doc = frappe.get_single("Global Settings")
            current_organization_year = global_settings_doc.current_financial_year
        # check if the NPRE of current year
        return_dict["npre"] = get_npre_for_specific_year(current_organization.name,current_organization_year)

        # return the return dictionary
        return return_dict
    else:
        # user is not allowed to view this page
        return_dict['view_status'] = False

    # return the dict
    return return_dict

def get_plans_linked_to_project(project,current_organization):
    '''
    Function that returns a plan linked to a project
    if any exists
    '''
    # check the data type of organization passed to the 
    # function
    if type(current_organization) == str:
        requested_organization_name = current_organization
    else:
        requested_organization_name = current_organization['name']
    # get plans linked to a project from ints inception if any is missing
    project_start_date = project['project_start_date']
    project_end_date = project['project_end_date']
    project_start_year = project_start_date.year
    current_year = datetime.now().year
    list_of_plans_to_return = []

    def find_plan_for_year(project_start_year,current_year):
        list_of_plans = frappe.get_list("Annual Plan",
                fields=["*"],
                filters = {
                    "project_title": project['name'],
                    "annual_plan_year":str(project_start_year)
            })
        
        # check if this is the current year
        if project_start_year == current_year:
            # check a plan exists for each year
            if len(list_of_plans) > 0:
                # return the plan for the current year
                list_of_plans_to_return.append({'current_year':current_year,\
                    'year':project_start_year,'plan':list_of_plans[0]['status'],\
                    'project':project['name'],'status':True,\
                    'organization_name':requested_organization_name})
            else:
                # return False for the current year
                list_of_plans_to_return.append({'current_year':current_year,'year':project_start_year,'plan':False,'project':project['name'],'status':False,'organization_name':requested_organization_name})
        else:
            # check a plan exists for previous years
            if len(list_of_plans) > 0:
                # plans exists for previous years
                pass  
            else:
                # return False for this year as a plan does not exist
                list_of_plans_to_return.append({'current_year':current_year,'year':project_start_year,'plan':False,'project':project['name'],'status':False,'organization_name':current_organization['name']})
            
            # move the loop to the next year
            project_start_year += 1
            # curcusivelly call the function
            find_plan_for_year(project_start_year,current_year)

    # call the find plans Function
    find_plan_for_year(project_start_year,current_year)
    # now return the plans that the user needs to use
    return list_of_plans_to_return

def get_reports_linked_to_project(project,current_organization):
    '''
    Function that returns reports linked to a project
    if any exists
    '''
    # check the data type of organization passed to the 
    # function
    if type(current_organization) == str:
        requested_organization_name = current_organization
    else:
        requested_organization_name = current_organization['name']

    # get plans linked to a project from ints inception if any is missing
    project_start_date = project['project_start_date']
    project_start_year = project_start_date.year
    current_year = datetime.now().year

    list_of_reports_to_return = []

    def find_report_for_year(project_start_year,current_year):
        list_of_reports = frappe.get_list("Annual Report",
                fields=["*"],
                filters = {
                    "project_title": project['name'],
                    "annual_report_year":str(project_start_year)
            })

        # check if this is the current year
        if project_start_year == current_year:
            # check a report exists for each year
            if len(list_of_reports) > 0:
                # return the report for the current year
                list_of_reports_to_return.append({'current_year':current_year,'year':project_start_year,'report':list_of_reports[0]['status'],'project':project['name'],'status':True,'organization_name':requested_organization_name})
            else:
                # return False for the current year
                list_of_reports_to_return.append({'current_year':current_year,'year':project_start_year,'report':False,'project':project['name'],'status':False,'organization_name':requested_organization_name})

        else:
            # check a plan exists for previous years
            if len(list_of_reports) > 0:
                # plans exists for previous years
                pass  
            else:
                # return False for this year as a plan does not exist
                list_of_reports_to_return.append({'current_year':current_year,'year':project_start_year,'report':False,'project':project['name'],'status':False,'organization_name':requested_organization_name})
            
            # move the loop to the next year
            project_start_year += 1
            find_report_for_year(project_start_year,current_year)

    # call the find plans Function
    find_report_for_year(project_start_year,current_year)

    # now return the plans that the user needs to use
    return list_of_reports_to_return

@frappe.whitelist(allow_guest = True)
def get_link_org_renewal_form(args):
    '''
    Function that returns a link to organization renewal
    '''
    json_args = json.loads(args)
    pass


@frappe.whitelist(allow_guest = True)
def get_link_to_project_form(args):
    '''
    Function that returns a link to a project name
    '''
    json_args = json.loads(args)
    project_name = json_args['project_name']
    orgization_name = json_args['orgization_name']
 
    try:
        form_name = json_args['form_name']
        return frappe.utils.get_url_to_form("Project Registration Form",form_name)
    except:
        # check if renewal forms exist and arrange them in order tof phases
        list_of_renewal_forms = frappe.db.sql("select name from `tabProject Renewal Form` where phase_title = '{}' && status !='Cancelled' && select_your_organization = '{}'  ORDER BY phase DESC".format(project_name,orgization_name))
        
        if len(list_of_renewal_forms) > 0:
            # get the latest form on the list
            latest_renewal_form_name = list_of_renewal_forms[0][0]
            return frappe.utils.get_url_to_form("Project Renewal Form",latest_renewal_form_name)
        else:
            # check if registration forms exists
            list_of_registration_forms = frappe.db.sql("select name from `tabProject Registration Form` where sub_project_title = '{}' && status !='Cancelled' && select_your_organization = '{}'".format(project_name,orgization_name))
            if len(list_of_registration_forms) > 0:
                return frappe.utils.get_url_to_form("Project Registration Form",list_of_registration_forms[0][0])
            else:
                return False

@frappe.whitelist(allow_guest = True)
def get_link_to_plan_form(args):
    '''
    Function that returns a link to a plan name
    '''
    json_args = json.loads(args)
    project_name = json_args['project_name']
    orgization_name = json_args['organization_name']
    plan_year = json_args['year']

    try:
        form_name = json_args['form_name']
        return frappe.utils.get_url_to_form("Annual Plan Form",form_name)
    except:
        # check a annual plan form exists
        list_of_plan_forms = frappe.db.sql("select name from `tabAnnual Plan Form` where project_title = '{}' && status !='Cancelled' && full_name_of_the_organization = '{}' && annual_plan_year = '{}'".format(project_name,orgization_name,plan_year))
        
        if len(list_of_plan_forms) > 0:
            # get the latest form on the list
            form_name = list_of_plan_forms[0][0]
            return frappe.utils.get_url_to_form("Annual Plan Form",form_name)
        else:
            return False


@frappe.whitelist(allow_guest = True)
def get_link_to_report_form(args):
    '''
    Function that returns a link to a report name
    '''
    json_args = json.loads(args)
    project_name = json_args['project_name']
    orgization_name = json_args['organization_name']
    report_year = json_args['year']
    
    # check a annual plan form exists
    list_of_report_forms = frappe.db.sql("select name from `tabAnnual Report Form` where project_title = '{}' && status !='Cancelled' && full_name_of_the_organization = '{}' && annual_report_year = '{}'".format(project_name,orgization_name,report_year))
    
    if len(list_of_report_forms) > 0:
        # get the latest form on the list
        form_name = list_of_report_forms[0][0]
        return frappe.utils.get_url_to_form("Annual Report Form",form_name)
    else:
        return False

@frappe.whitelist(allow_guest = True)
def get_link_to_npre_form(args):
    '''
    Function that returns a link to the NPRE form
    '''
    json_args = json.loads(args)
    organization_name = json_args['organization_name']
    year = json_args['year']
    
    # check a annual plan form exists
    list_of_npre_forms = frappe.db.sql("select name from `tabNon Project Related Expenditure` where full_name_of_the_organization = '{}' && year = '{}'".format(organization_name,year))
    
    if len(list_of_npre_forms) > 0:
        # get the latest form on the list
        form_name = list_of_npre_forms[0][0]
        return frappe.utils.get_url_to_form("Non Project Related Expenditure",form_name)
    else:
        return False


@frappe.whitelist(allow_guest = True)
def get_link_to_org_form(args):
    '''
    Function that returns a link to the
    requested organization form
    '''
    json_args = json.loads(args)
    organization_name = json_args['org_name']
    organization_type = json_args['org_type']

    # check if the type is given in the first place
    if not organization_type:
        return False

    elif organization_type == "Local NGO":
        # check both english and somali forms
        req_org_type_form_english = organization_type +" Form English"
        req_org_type_form_renewal_english = organization_type +" Renewal Form English"
        req_org_type_form_somali = organization_type +" Form Somali"
        req_org_type_form_renewal_somali = organization_type +" Renewal Form Somali"

        def check_forms(req_org_type_form_renewal,req_org_type_form,organization_name):
            # check if renewal forms exists
            list_of_organization_renewal_forms = frappe.db.sql("select name from `tab{}` where full_name_of_the_organization = '{}'".format(req_org_type_form_renewal,organization_name))
            # check if any renewal forms were found
            if len(list_of_organization_renewal_forms) > 0:
                # get the latest form on the list
                # use -1 to the the last form created in the system
                form_name = list_of_organization_renewal_forms[-1][0]
                return frappe.utils.get_url_to_form(req_org_type_form_renewal,form_name)
            else:
                # check if organization forms exists
                list_of_organization_forms = frappe.db.sql("select name from `tab{}` where full_name_of_the_organization = '{}'".format(req_org_type_form,organization_name))
                
                if len(list_of_organization_forms):
                    form_name = list_of_organization_forms[0][0]
                    return frappe.utils.get_url_to_form(req_org_type_form,form_name)
                else:
                    return False

        # determine if renewal exist for english
        english_form = check_forms(req_org_type_form_renewal_english,req_org_type_form_english,organization_name)
        if english_form:
            return english_form
        else:
            somali_form = check_forms(req_org_type_form_renewal_somali,req_org_type_form_somali,organization_name)
            if somali_form:
                return somali_form
            else:
                return False

    else:
        # create the name of the requested form
        req_org_type_form = organization_type +" Form"
        req_org_type_form_renewal = organization_type +" Renewal Form"
    
        # check if renewal forms exists
        list_of_organization_renewal_forms = frappe.db.sql("select name from `tab{}` where full_name_of_the_organization = '{}'".format(req_org_type_form_renewal,organization_name))

        # check if any renewal forms were found
        if len(list_of_organization_renewal_forms) > 0:
            # get the latest form on the list
            # use -1 to the the last form created in the system
            form_name = list_of_organization_renewal_forms[-1][0]
            return frappe.utils.get_url_to_form(req_org_type_form_renewal,form_name)
        else:
            # check if organization forms exists
            list_of_organization_forms = frappe.db.sql("select name from `tab{}` where full_name_of_the_organization = '{}'".format(req_org_type_form,organization_name))
            
            if len(list_of_organization_forms):
                form_name = list_of_organization_forms[0][0]
                return frappe.utils.get_url_to_form(req_org_type_form,form_name)
            else:
                return False

@frappe.whitelist(allow_guest = True)
def get_link_to_different_forms(args):
    '''
    Function that returns a link to a report name
    '''
    json_args = json.loads(args)
    
    # check requested form
    requested_form = json_args['req_form']

    if requested_form == "Organization Form":
        # call the get link to project form
        retrieved_link = get_link_to_org_form(args)
        # return the retrieved link
        return retrieved_link

    elif requested_form == "Project Form":
        # call the get link to project form
        retrieved_link = get_link_to_project_form(args)
        # return the retrieved link
        return retrieved_link

    elif requested_form == "Annual Plan Form":
        # call the get link to plan form function
        retrieved_link = get_link_to_plan_form(args)
        # return the retrieved link
        return retrieved_link

    elif requested_form == "Annual Report Form":
        # call the get link to report form function
        retrieved_link = get_link_to_report_form(args)
        # return the retrieved link
        return retrieved_link

    elif requested_form == "NPRE Form":
        # call the get link tothe npre form function
        retrieved_link = get_link_to_npre_form(args)
        # return the retrieved link
        return retrieved_link
    
    else:
        # return false
        return False

# @frappe.whitelist(allow_guest = True)
# def get_npre_for_specific_year(organization,year):
#     '''
#     Function that determines if a NPRE exist for a given year
#     '''
#     # check the data type of organization passed to the 
#     # function
#     if type(organization) == str:
#         requested_organization_name = organization
#     else:
#         requested_organization_name = organization['name']
    
#     # get list of npres
#     list_of_NPRE_forms = frappe.db.sql("select name,year,status from `tabNon Project Related Expenditure` where full_name_of_the_organization = '{}' && year = '{}'".format(requested_organization_name,str(year)))
#     # check if atleast one npre was found
#     if len(list_of_NPRE_forms) > 0:
#         # return the NPRE
#         form_url = frappe.utils.get_url_to_form("Non Project Related Expenditure",list_of_NPRE_forms[0][0])
#         return {'npre':list_of_NPRE_forms[0][0],'status':list_of_NPRE_forms[0][1],'url_form':form_url,'year':year,'form_status':list_of_NPRE_forms[0][2]}
#     else:
#         return {'npre':False,'status':False,'url_form':False,'year':year}

def get_pending_project_forms(organization_name):
    '''
    Function that gets of all the pending project forms
    belonging to an organization
    input:
        organization_name - str
    output:
        list of pending project forms
    '''
    # get pending forms belonging to the user
    # get all the pending forms belonging to the current user
    list_of_pending_forms = frappe.get_list("Project Registration Form",
        fields=["name","new_project_code","status","new_project_title","form_type","select_your_organization"],
        filters = {
            "status":"Pending Verification",
            "sub_project_title":["=",""],
            "select_your_organization":organization_name
    })
    

    # get user linked to a given organization
    try:
        frappe
    except:
        print("An error occured")
