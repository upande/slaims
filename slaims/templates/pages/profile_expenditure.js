// function that adds a loader to a page
function add_remove_loader(action){
    if(action== true){
        var loader_div = document.createElement("div");
        loader_div.setAttribute('class',"loader")
        loader_div.setAttribute('id',"loader")
        document.getElementById("loader_column").appendChild(loader_div)
    }else{
        var elem = document.getElementById("loader");
        elem.parentNode.removeChild(elem);
    }   
}

// function that adds certain text to a given section
function add_text_to_doc(text_to_add,where_to_add){
    if(text_to_add){
        var user_name = document.createTextNode(text_to_add);
        document.getElementById(where_to_add).appendChild(user_name)
    } 
}

// get values from url method and add to doc
function add_contexts_to_doc(message){
    console.log(message)
    // check if a user exists ie. the logged in user is not an admin
    if(message.npre){
        // check if the user has an organization
        if(message.organization){
            // add user and organization details
            add_text_to_doc(message.user,"username_holder")
            add_text_to_doc(message.organization.name,"organization_holder")     
            
            add_text_to_doc(message.organization.name,"organization_name_holder")
            add_text_to_doc(message.npre.year,"npre_year_holder")
            add_text_to_doc(message.npre.form_status, "npre_status") 
            // check if npre exists
            if(message.npre.npre){
                // based on status determine message
                if(message.npre.form_status == "Pending Verification"){
                    status_icon_indicator.setAttribute("class","fa fa-times-circle")
                    status_icon_indicator.setAttribute("style","color:red;font-size:15px;")

                    // add instructions to the instructions section
                    var instructions_to_display = `Non project related expenditure for year ${message.npre.year} \
                    is pending verification ,please complete the form and verify it`
                    // style the status indicator
                    npre_status_indicator.setAttribute("class","fa fa-times-circle-o")
                    npre_status_indicator.setAttribute("style","color:red;font-size:25px;")

                }else if(message.npre.form_status == "Verified"){
                    status_icon_indicator.setAttribute("class","fa fa-exclamation-circle")
                    status_icon_indicator.setAttribute("style","color:#B7481D;font-size:15px;")

                    // add instructions to the instructions section
                    var instructions_to_display = `Non project related expenditure for year ${message.npre.year} \
                    is Verified pending Approval`
                    // style the status indicator
                    npre_status_indicator.setAttribute("class","fa fa-exclamation-circle")
                    npre_status_indicator.setAttribute("style","color:#B7481D;font-size:25px;")

                }else if(message.npre.form_status == "Validated"){
                    status_icon_indicator.setAttribute("class","fa fa-exclamation-circle")
                    status_icon_indicator.setAttribute("style","color:#B7481D;font-size:15px;")

                    // add instructions to the instructions section
                    var instructions_to_display = `Non project related expenditure for year ${message.npre.year} \
                    is Validated pending Approval`
                    // style the status indicator
                    npre_status_indicator.setAttribute("class","fa fa-exclamation-circle")
                    npre_status_indicator.setAttribute("style","color:#B7481D;font-size:25px;")

                }else if(message.npre.form_status == "Approved"){
                    status_icon_indicator.setAttribute("class","fa fa-exclamation-circle")
                    status_icon_indicator.setAttribute("style","color:green;font-size:15px;")

                    // add instructions to the instructions section
                    var instructions_to_display = `Non project related expenditure for year ${message.npre.year} \
                    has been Approved`
                    // style the status indicator
                    npre_status_indicator.setAttribute("class","fa fa-check-circle")
                    npre_status_indicator.setAttribute("style","color:green;font-size:25px;")

                }
                else{
                    status_icon_indicator.setAttribute("class","fa fa-check-circle-o")
                    status_icon_indicator.setAttribute("style","color:green;")

                    // add instructions to the instructions section
                    var instructions_to_display = `Non project related expenditure for year ${message.npre.year} \
                    has been created`
                    // style the status indicator
                    npre_status_indicator.setAttribute("class","fa fa-check-circle-o")
                    npre_status_indicator.setAttribute("style","color:green;font-size:25px;")
                }
                
                add_text_to_doc(instructions_to_display,"instructions_p") 
                instruction_action_button.setAttribute("class","btn btn-success btn-sm step_button")
                add_text_to_doc("View Form","instruction_action_button")
                instruction_action_button.setAttribute("onclick",`location.replace('${message.npre.url_form}')`)

            }else{
                status_icon_indicator.setAttribute("class","fa fa-exclamation-circle")
                status_icon_indicator.setAttribute("style","color:red;font-size:15px;")

                // add instructions to the instructions section
                var instructions_to_display = `Non project related expenditure for year ${message.npre.year} \
                does not exist please click on the button below to create it`
                add_text_to_doc(instructions_to_display,"instructions_p")

                instruction_action_button.setAttribute("class","btn btn-success btn-sm step_button")
                add_text_to_doc("Create NPRE","instruction_action_button")

                // style the status indicator
                // status_icon_indicator(status_icon_indicator,"npre_status_indicator")
                npre_status_indicator.setAttribute("class","fa fa-times-circle-o")
                npre_status_indicator.setAttribute("style","color:red;font-size:25px;")
            }
        }else{
            var user_option = confirm("You have not registered an organization, you will be redirected to the organization registration page")
            if(user_option){
                // redirect to organization page
                window.location.href = "/profile_organization";
            }else{
                // redirect to organization page
                window.location.href = "/profile_organization";
            }
        }
    }else{
        alert('This is basic user page, please use the MoNDP interface instead');
        window.location = '/desk';
    }
}

function main_function(){
    // add a loader to the contents tables
    add_remove_loader(true)
    // url to the method
    var url = "/api/method/slaims.templates.pages.profile_expenditure.return_context"
    
    // ajax calls to response
    $.ajax({
        url:url,
        async: true,
        dataType: 'json',
        success: function (response) {
            add_remove_loader(false)
            add_contexts_to_doc(response.message)  
        },
        cache:false
    });
}

// call the main function
main_function()