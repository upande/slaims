from __future__ import unicode_literals

# Python 2 and 3: easiest option 
# from future.standard_library import install_aliases 
# install_aliases()
# from urllib.parse import urlparse, urlencode 
# from urllib.request import urlopen, Request 
# from urllib.error import HTTPError

import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from frappe.model.document import Document
import pymysql.cursors
import json,urllib,re,random,requests
from . profile_methods import get_link_to_org_form

user = frappe.session.user

def get_context(context):
    pass
    
@frappe.whitelist(allow_guest = True)
def return_context():
    '''
    Function required values for the profile page and returns them through
    a whitelisted method
    '''
    return_dict = {}
    logged_in_user = frappe.session.user
    # get list of user roles and add the different roles to the return dict
    list_of_current_user_roles = frappe.get_roles(logged_in_user)

    if "Staff" in list_of_current_user_roles or "Super Admin" in list_of_current_user_roles:
        return_dict['role'] = "Staff"
    elif "Organization Login" not in list_of_current_user_roles:
        return_dict['role'] = "Sign Up Role"
    else:
        return_dict['role'] = "Organization Login"
        # return the logged in user
        return_dict['user'] = logged_in_user
        # get a list of organizations the user is linked to i.e 
        # which should be only one
        list_of_organizations = frappe.get_list("Organization",
                                fields=["*"],
                                filters = {
                                    "user":logged_in_user
                            })
        # if the user is linked to atleast one organization the length
        # will be greater than zero 
        if len(list_of_organizations) > 0:
            # add the linked organization to the return dictionary
            return_dict['organization'] = list_of_organizations[0]
            current_org = list_of_organizations[0]
            # get link to form
            found_url = get_link_to_org_form(json.dumps({'org_name':current_org['name'],'org_type':current_org["organization_type"]}))
            return_dict['view_form'] = found_url
        else:
            # list of organization forms
            organizations = ['International NGO Form',
                            'Local NGO Form English',
                            'Local NGO Form Somali',
                            'Somaliland Government Institution Form',
                            'Private Sector Company Form',
                            'Bilateral Organization Form',
                            'MultiLateral Organization Form',
                            'UN System Organization Form',
                            'Umbrella or Consortium Organization Form',
                        ]                           

            # loop through all the forms to find any pending form the 
            # organization is linked to
            for org in organizations:
                pending = frappe.get_list(org,
                    fields=["*"],
                    filters = {
                        'status':'Pending Verification',
                        'owner': logged_in_user,
                })
                
                # check if the pending form exist
                if len(pending) == 0:
                    pass
                else:
                    try: 
                        if return_dict['pending_url']:
                            # determine which form is the latest to be modified
                            initial_found_form = return_dict['form_name']
                            initial_found_type = return_dict['org']
                            current_form_name = pending[0]['name']
                            current_form_type = org
                
                            # initial form
                            list_of_initial_form = frappe.get_list(initial_found_type,
                                fields=["name","modified"],
                                filters = {
                                    "name":initial_found_form
                            })
                        
                            # make the form latest modified as the default form to be completed
                            if list_of_initial_form[0]['modified'] > pending[0]['modified']:
                                pass
                            else:
                                # place the latest form to return dict 
                                return_dict['org'] = org
                                return_dict['form_name'] = pending[0]['name']
                                return_dict['pending_url'] = frappe.utils.get_url_to_form(org,pending[0]['name'])
                    except:
                        # this is the first form to be found 
                        return_dict['org'] = org
                        return_dict['form_name'] = pending[0]['name']
                        return_dict['pending_url'] = frappe.utils.get_url_to_form(org,pending[0]['name'])
    
    # now return the contect dict
    return return_dict
