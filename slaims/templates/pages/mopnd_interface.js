// function that adds a loader to a page
function add_remove_loader(action){
    if(action== true){
        // hide the table
        document.getElementById("mopndInterfaceTable").setAttribute('style',"display:none;")
        var loader_div = document.createElement("div");
        loader_div.setAttribute('class',"loader")
        loader_div.setAttribute('id',"loader")
        document.getElementById("loader_column").appendChild(loader_div)
    }else{
        // show table
        document.getElementById("mopndInterfaceTable").setAttribute('style',"display:true;")
        var elem = document.getElementById("loader");
        elem.parentNode.removeChild(elem);
    }   
}


// get values from url method and add to doc
function add_contexts_to_doc(message){
    // loop through each found organization
    message.organizations.forEach(org_details => {
        create_org_row(org_details)
    }); 
    // call the table function
    table_function()
}

function redirectFuction(organization_name){
    var request_url = `/mopnd_interface_single?org_name=${organization_name}`
    location.href = request_url
}

function create_org_row(org_details){
    // create  organization row
    var org_row = document.createElement("tr");

    function create_status_icon(color){
        var current_icon_column = document.createElement("td");
        current_icon_column.setAttribute("style","text-align:center")
        var current_icon = document.createElement("i")
        current_icon.setAttribute("style",`color:${color};font-size:20px;`)
        // determine icon to show
        if(color == 'red'){
            current_icon.setAttribute("class","fa fa-times-circle-o")
        }else if(color == '#B7481D'){
            current_icon.setAttribute("class","fa fa-exclamation-circle")
        }else if(color == 'green'){
            current_icon.setAttribute("class","fa fa-check-circle")
        }else{
            current_icon.setAttribute("class","fa fa-times-circle-o")
        }
        current_icon_column.append(current_icon)
        // retrun the status icon column
        return current_icon_column
    }

    // add organization name
    var orgnization_name_column = document.createElement("td");
    var current_orgnization_name = document.createElement("a")
    current_orgnization_name.innerHTML = org_details.organization_name
    orgnization_name_column.append(current_orgnization_name)
    orgnization_name_column.setAttribute("onclick",`redirectFuction('${org_details.organization_name}')`)
    org_row.append(orgnization_name_column)

    // add orgnization status icon
    var org_status_column = create_status_icon(org_details.org_status_color)
    org_row.append(org_status_column)
    // add project status icon
    var project_status_column = create_status_icon(org_details.project_status_color)
    org_row.append(project_status_column)
    // add plan status icon
    var plan_status_column = create_status_icon(org_details.plan_status_color)
    org_row.append(plan_status_column)
    // add report status icon
    var report_status_column = create_status_icon(org_details.report_status_color)
    org_row.append(report_status_column)
    // add npre status icon
    var npre_status_column = create_status_icon(org_details.npre_status_color)
    org_row.append(npre_status_column)
    // append the row to the table
    document.getElementById('table_body').append(org_row) 
}

// Create dynamic datatable
function table_function() {
    $.noConflict()
    var table = $('#mopndInterfaceTable').DataTable({
   
    // Options for entry display size
    aLengthMenu: [
        [10, 20, 30, 100, -1],
        [10, 20, 30, 100, "All"]
    ],
    iDisplayLength: 10,

    // Set default message on zero records
    "language":{
        "zeroRecords": "<h1>Sorry, no matching organization found</h1>",
        "emptyTable": "No organization data available"
    },

    // Enable smart search
    "search": {
        "smart": true,
    },

    "ordering": false,
   });

   // Modify component styles here
   $('div.dataTables_filter input').addClass("form-control");
   $('div.dataTables_filter input').addClass("input-md");
}

function main_function(){
    // add a loader to the contents tables
    add_remove_loader(true)
    // url to the method
    var url = "/api/method/slaims.templates.pages.mopnd_interface.return_context"

    // ajax calls to response
    $.ajax({
        url:url,
        async: true,
        dataType: 'json',
        success: function (response) {
            // remove loader
            add_remove_loader(false)
            // check if the user has view privillages
            if(response.message.view_status){
                // logged in user is staff call the main function
                add_contexts_to_doc(response.message)

            }else{
                alert("You do not have privillages to access this page")
                location.href = '/desk'
            } 
        },
        cache:false
    });
}

// call the main function
main_function()

