from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from frappe.model.document import Document
import random
import requests
import pymysql.cursors
import json

# imports from methods module
from .profile_methods import get_user_organization,get_current_projects,get_supposed_plans_or_reports_for_project,\
    get_plans_for_each_project_obligation,get_plan_obligations_n_statuses

def get_context(context):    
    pass

@frappe.whitelist(allow_guest = True)
def return_context():
    '''
    Function that returns context for the plan interface
    using javascript
    '''
    return_dict = {}
    logged_in_user = frappe.session.user
    # get a list of roles
    list_of_current_user_roles = frappe.get_roles(logged_in_user)

    if "Organization Login" in list_of_current_user_roles:
        # user has the role organization login
        current_organization = get_user_organization(logged_in_user)
        return_dict['user'] = logged_in_user
        return_dict["organization"] = current_organization

        # get all current obligations and statuses
        list_of_obligation_n_statuses = get_plan_obligations_n_statuses(current_organization)
        # add plans to return dict
        return_dict['plans'] = list_of_obligation_n_statuses
    
        # get plans pending verification and have no plans
        list_of_supposed_plan_forms = frappe.get_list("Annual Plan Form",
            fields=["*"],
            filters = {
                "status":"Pending Verification",
		})
        list_of_pending_forms = []
        
        # loop through all the pending verifications forms to determine which does 
        # not have a plan
        for form in list_of_supposed_plan_forms:
            plan_form_name = form.project_title+"-"+form.organization_acronym+"-("+str(form.annual_plan_year)+")-AP"
            # check if plan exist
            try:
                frappe.get_doc('Annual Plan',plan_form_name)
            except:
                list_of_pending_forms.append(form)
        return_dict['pending_forms'] = list_of_pending_forms
        # return the dict
        return return_dict
    else:
        return "The user does not have the role of organization login"

