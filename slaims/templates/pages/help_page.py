from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from frappe.model.document import Document
import random
import requests
import pymysql.cursors
import json
from datetime import date

today = date.today()

day_of_week = ['monday', 'tuesday', 'wednesday',
             'thursday', 'friday', 'saturday',
             'sunday'
              ]

week_day = day_of_week[today.weekday()]

def get_context(context):
    pass

@frappe.whitelist(allow_guest = True)
def get_contacts_on_call():
    on_duty = frappe.db.sql('select first_name, last_name, email, telephone_no from `tabHelp` where {} = 1'.format(week_day))
    return list(on_duty)


    

        