from __future__ import unicode_literals
import frappe
from frappe.utils.global_search import web_search
from html2text import html2text
from frappe import _
from jinja2 import utils
from frappe.utils import sanitize_html
import json
from frappe.model.document import Document
import random
import requests
import pymysql.cursors
import json
from datetime import datetime

# application imports
from .profile_methods import get_start_n_end_date,get_current_projects

def get_context(context):
    pass

@frappe.whitelist(allow_guest = True)
def return_context():
    '''
    Function that returns context from the project page
    from javascript
    '''
    return_dict = {}
    logged_in_user = frappe.session.user
    list_of_current_user_roles = frappe.get_roles(logged_in_user)

    # check is the logged in user has role of "Organization Login"
    if "Organization Login" in list_of_current_user_roles:
        return_dict['user'] = logged_in_user
        # get organizations linked to the user
        list_of_organizations = frappe.get_list("Organization",
            fields=["*"],
            filters = {
                "user":logged_in_user
        })

        # set organization and user for the projects
        if len(list_of_organizations) > 0:
            return_dict["organization"] = list_of_organizations[0]
            current_organization_name = list_of_organizations[0].name
            # get all current active projects based on organization 
            # current financial year
            list_of_project_key_value_pairs = get_current_projects(current_organization_name)
            # add key value pairs to return dict
            return_dict['projects'] = list_of_project_key_value_pairs

            # get all the pending forms belonging to the current user
            list_of_pending_forms = frappe.get_list("Project Registration Form",
                fields=["name","new_project_code","status","new_project_title","form_type","select_your_organization"],
                filters = {
                    "status":"Pending Verification",
                    "sub_project_title":["=",""],
            })
            # add the list of pending forms to the return dict
            return_dict['pending_forms'] = list_of_pending_forms
        else:
            return_dict['projects'] = []
    else:
        # throw an error if the logged in user is Staff
        frappe.throw("This is the basic user interface use the MoNDP interface instead")

    # now return the context dictionary
    return return_dict
    
