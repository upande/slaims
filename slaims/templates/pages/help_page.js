function create_contact_table(contacts){
    if(contacts.length > 0){
        var node_table = document.createElement("table");
        node_table.setAttribute('class','table table-striped')
        var table_head = document.createElement("thead");
        

        // add table headings
        for (var rows = 0; rows < 1; rows++){
            var table_row = document.createElement("tr");
            var th = document.createElement("th");
            th.setAttribute('scope',"col")
            var th1 = document.createElement("th");
            th1.setAttribute('scope',"col")
            var th2 = document.createElement("th");
            th2.setAttribute('scope',"col")
            var content = document.createTextNode("Name");
            var content1 = document.createTextNode("Telephone Number");
            var content2 = document.createTextNode("Email");

            th.appendChild(content)
            th1.appendChild(content1)
            th2.appendChild(content2)
            table_row.appendChild(th)
            table_row.appendChild(th1)
            table_row.appendChild(th2)
            table_head.appendChild(table_row)
            node_table.appendChild(table_head)
        }

        // add rows of contacts
        var list_of_names = ["James","Mians"]

        // loop through the list of available contacts
        contacts.forEach(element => {
            var table_body = document.createElement("tbody");
            var table_row = document.createElement("tr");
            table_row.setAttribute('class','row_style');
            var td_name = document.createElement("td");
            td_name.setAttribute('scope',"col")
            var td_email = document.createElement("td");
            td_email.setAttribute('scope',"col")
            var td_no = document.createElement("td");
            td_no.setAttribute('scope',"col")
            var content_name = document.createTextNode(element[0]);
            var content_email = document.createTextNode(element[2]);
            var content_number = document.createTextNode(element[3]);

            td_name.appendChild(content_name)
            td_email.appendChild(content_number)
            td_no.appendChild(content_email)
            table_row.appendChild(td_name)
            table_row.appendChild(td_email)
            table_row.appendChild(td_no)
            table_head.appendChild(table_row)
            node_table.appendChild(table_body)
        });
    }else{
        var node_table = document.createElement("h1");
        var content_none = document.createTextNode("No staff is on call today");
        node_table.appendChild(content_none)
    }
    // append the created table to DOM
    document.getElementById("table_div").appendChild(node_table) 
}




// add the loader
function add_remove_loader(action){
    if(action== true){
        var loader_div = document.createElement("div");
        loader_div.setAttribute('class',"loader")
        loader_div.setAttribute('id',"loader")
        content_div.appendChild(loader_div)
    }else{
        var elem = document.getElementById("loader");
        elem.parentNode.removeChild(elem);
    }   
}

function main_function(){
    // add a loader to the contents tables
    add_remove_loader(true)

    // url to the method
    var url = "/api/method/slaims.templates.pages.help_page.get_contacts_on_call"
    
    // ajax calls to response
    $.ajax({
        url:url,
        async: true,
        dataType: 'json',
        success: function (response) {
            // remove the loader
            add_remove_loader(false)
            // add contacts to the content section
            create_contact_table(response.message)
        },
        cache:false
    });
}


// call the main function
main_function()

