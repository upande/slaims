from __future__ import unicode_literals

# std lib imports
import datetime
import re

# frappe imports
import frappe
from frappe import _
import frappe.model
import frappe.utils
import json, os
from six import iteritems, string_types, integer_types
from frappe.utils.file_manager import save_file

# local imports
from .templates.pages.profile_methods import get_current_projects,\
	get_supposed_plans_or_reports_for_destionation_org_project,\
	check_plan_or_report_exists,get_npre_for_specific_year
@frappe.whitelist()
def check_if_project_is_active(organization_name,user_role):
	'''
	Custom method filter subprojects Annual Plans and 
	Annula Report where an orgization has a reporting 
	obligation
	'''
	if user_role == "Staff":
		# get all active projects
		list_of_found_projects = frappe.db.get_list('Project',
					filters={
						"project_end_date":[">=",datetime.datetime.now().date().replace(month=1, day=1)],
						"sub_project":1
					},
					fields=['name','project_end_date'],
					as_list=True
			)
		
		# put the select fields as a 
		filtered_projects = [""]
		for project in list_of_found_projects:
			filtered_projects.append(project[0])
			
		# return list of project names
		return filtered_projects
	else:
		# get a list of projects with reporting organizations
		reporting_projects_list = frappe.get_list("Destination Implementing Reporting",
						fields=["*"],
						filters = {
							'reporting_organization':organization_name,
							'parentfield':"destination_organization_table",
							'parenttype':"Project"
					})
		
		# filter out only the active project
		filtered_projects = [""]
		for project in reporting_projects_list:
			# check if project is active for current year
			list_of_found_projects = frappe.db.get_list('Project',
								filters={
									'name':project['parent'],
									"project_end_date":[">=",datetime.datetime.now().date().replace(month=1, day=1)],
									"sub_project":1
								},
								fields=['name','project_end_date'],
								as_list=True
						)
			# append active projects
			if len(list_of_found_projects) > 0:
				filtered_projects.append(list_of_found_projects[0][0])
			else:
				pass

		# return the list of filtered projects
		return filtered_projects

@frappe.whitelist()
def get_project_implementors(project_name,organization_name):
	'''
	Custom method filter subprojects for annual plans and 
	reports based on the selected project
	'''
	if organization_name:
		# list of implementing organizations
		list_of_implementing_orgs = frappe.get_list("Destination Implementing Reporting",
				fields=["*"],
				filters = {
					'parent':project_name,
					'reporting_organization':organization_name,
					'parentfield':"destination_organization_table",
					'parenttype':"Project"
			})

		# filter out only the active project
		list_of_organization_names = [""]
		for implementor in list_of_implementing_orgs:
			list_of_organization_names.append(implementor.implementing_organization)
		# return list of linked organization
		return list_of_organization_names
	else:
		# list of implementing organizations
		list_of_implementing_orgs = frappe.get_list("Destination Implementing Reporting",
				fields=["*"],
				filters = {
					'parent':project_name,
					'parentfield':"destination_organization_table",
					'parenttype':"Project"
			})

		# filter out only the active project
		list_of_organization_names = [""]
		for implementor in list_of_implementing_orgs:
			list_of_organization_names.append(implementor.implementing_organization)
		# return list of linked organization
		return list_of_organization_names

def close_open_year():
	'''
	Function that is called to close the previous
	year and open a new global year
	input:
	'''
	frappe.msgprint("Starting the process of closing the current year <hr> \
		You will be notified once the process is complete")

	
def close_open_year_for_all_organizations(financial_year):
	'''
	Function that closes financial year for all organization
	within a given year
	input:
		financial_year -str
	output:
		bolean - True (when done)
	'''
	frappe.publish_realtime('msgprint', 'Closing financial year {}\
        ,you will be nofied once the process is complete'.format(financial_year))
	# get all the organizations within a given financial year
	list_of_all_organizations = frappe.get_list("Organization",
		fields=["*"],
		filters = {
			'current_registration_year':financial_year
	})
	# get the year with the next rank
	current_year_doc = frappe.get_doc("Year",financial_year)
	next_year_rank = current_year_doc.rank + 1
	# get the year with the new rank
	list_of_years_with_rank = frappe.db.get_list('Year',
		filters={
			"rank":next_year_rank
		},
		fields=['*']
	)
	# check if a year with a greater rank exist
	if len(list_of_years_with_rank) > 0:
		# loop through all the organizations
		for organization in  list_of_all_organizations:
			# check if the organization has submitted all the required then close year
			close_open_year_for_an_organization(organization.name)
	else:
		# A financial year after the current year has not been created
		slaims_error_doc = frappe.new_doc("SLAIMS Error Log")
		error_title = "A financial year after '{}' has not been created".format(current_year_doc.name)
		slaims_error_doc.title = error_title 
		# add the encountered errors in the databases
		slaims_error_doc.append("description",{
			"error":"Please create the next year after {} in order to close the year".format(current_year_doc.name)
		})
		# save the error log to the database
		slaims_error_doc.insert()
		frappe.db.commit()
	# throw a message to indicate that the process is complete
	frappe.publish_realtime('msgprint', 'Closing of financial year {}\
        is complete'.format(financial_year))
	# once the process is complete return true
	return True
	
def close_open_year_for_an_organization(organization_name):
	'''
	Function that checks if a given organization has submitted 
	all the required forms for its financial year and closes
	the year else adds the founds errors under SLAIMS Error log
	list
	input:
		organization_name - str
	'''
	organization_doc = frappe.get_doc("Organization",organization_name)
	# get the year with the next rank
	current_year_doc = frappe.get_doc("Year",organization_doc.current_registration_year)
	next_year_rank = current_year_doc.rank + 1
	# get the year with the new rank
	list_of_years_with_rank = frappe.db.get_list('Year',
		filters={
			"rank":next_year_rank
		},
		fields=['*']
	)
	# check if a year with a greater rank exist
	if len(list_of_years_with_rank) > 0:
		next_year = list_of_years_with_rank[0]
		# check if the organization has submitted all the required 
		# for the year
		forms_submitted = check_if_organization_has_submitted_all_forms(organization_doc.name)
		# check if all the required forms for the years have been submitted
		if forms_submitted['status']:
			# close the organization's financial year
			organization_doc = frappe.get_doc("Organization",organization_doc.name)
			organization_doc.current_registration_year = next_year.name
			organization_doc.status = "Pending Renewal"
			organization_doc.financial_year_not_closed = 0
			# save the year
			organization_doc.save()
			frappe.db.commit()
		else:
			# save the errors
			slaims_error_doc = frappe.new_doc("SLAIMS Error Log")
			error_title = "Fail Closing Financial Year {} for {}".format(organization_doc.current_registration_year,organization_doc.name)
			slaims_error_doc.title = error_title 
			# add the encountered errors in the databases
			for error in forms_submitted['list_of_errors']:
				clean_message = " ".join(error.split())
				slaims_error_doc.append("description",{
					"error":clean_message
				})
			# save the error log to the database
			slaims_error_doc.insert()
			frappe.db.commit()
			# change the organization's status
			organization_doc = frappe.get_doc("Organization",organization_doc.name)
			organization_doc.status = "Pending Renewal"
			organization_doc.financial_year_not_closed = 1
			# save changes
			organization_doc.save()
			frappe.db.commit()	
	else:
		# A financial year after the current year has not been created
		slaims_error_doc = frappe.new_doc("SLAIMS Error Log")
		error_title = "A financial year after '{}' has not been created".format(current_year_doc.name)
		slaims_error_doc.title = error_title 
		# add the encountered errors in the databases
		slaims_error_doc.append("description",{
			"error":"Please create the next year after {} in order to close the year".format(current_year_doc.name)
		})
		# save the error log to the database
		slaims_error_doc.insert()
		frappe.db.commit()
		
def check_if_organization_has_submitted_all_forms(organization_name):
	'''
	Function that checks that an organization has submitted all the 
	required forms it is supposed to submit for its financial year
	and return True else return a list of errors
	input:
		organization_name - str
		year - str 
	output:
		dictionary {'status':True/False,'errors':list of errors}
	'''
	status = True
	list_of_errors = []
	# get the organization approved forms
	organization_doc = frappe.get_doc("Organization",organization_name)
	if organization_doc.status == "Approved":
		pass
	else:
		status = False
		list_of_errors.append("This organization's registration is not \
			Approved for the year {}".format(organization_doc.current_registration_year))

	# check that all the active projects the organization is linked to are approved as well
	organization_active_projects = get_current_projects(organization_name)
	list_of_projects_already_checked = []
	# loop through the found projects
	for project in organization_active_projects:
		# check if the organization is the destination organization
		project_doc = project['project_doc']
		if project_doc.your_organization == organization_name:
			# check if the project is already checked
			if project_doc.name in list_of_projects_already_checked:
				pass
			else:
				# place the name of the project in the list
				list_of_projects_already_checked.append(project_doc.name)
				# check if the current project is approved
				if project_doc.status == "Approved":
					pass
				else:
					status = False
					list_of_errors.append("This organization's is the destination organization \
						for the project: '{}' which has not be Approved".format(project_doc.name))

				# get a list of all required annual plans and reports
				supposed_plans_reports = get_supposed_plans_or_reports_for_destionation_org_project(project_doc.name)
				# loop through the found supposed plans/reports
				for supposed_plan_report in supposed_plans_reports:
					# fetch plans ad reports
					plan_report_status = check_plan_or_report_exists(project_doc.name,supposed_plan_report['implementing_organization'],organization_doc.current_registration_year)
					# check if plan exist
					if plan_report_status['annual_plan_status']:
						# check if the plan is approved
						if plan_report_status['plan_doc_status'] == "Approved":
							pass
						else:
							status = False
							list_of_errors.append("An Annual Plan for the Project:'{}',year:'{}',for implementing \
								organization '{}' has not been Approved.The organization: '{}' is the destination organization \
								for the project".format(project_doc.name,organization_doc.current_registration_year,supposed_plan_report['implementing_organization'],organization_doc.name))
					else:
						status = False
						list_of_errors.append("An Annual Plan for the Project:'{}',year:'{}',for implementing \
							organization '{}' has not been submitted.The organization: '{}' is the destination organization \
							for the project".format(project_doc.name,organization_doc.current_registration_year,supposed_plan_report['implementing_organization'],organization_doc.name))
						
					# check if the report is given
					if plan_report_status['annual_report_status']:
						# check if the report is approved
						if plan_report_status['report_doc_status'] == "Approved":
							pass
						else:
							status = False
							list_of_errors.append("An Annual Report for the Project:'{}',year:'{}',for implementing \
								organization '{}' has not been Approved.The organization:'{}' is the destination organization \
								for the project".format(project_doc.name,organization_doc.current_registration_year,supposed_plan_report['implementing_organization'],organization_doc.name))
					else:
						status = False
						list_of_errors.append("An Annual Report for the Project:'{}',year:'{}',for implementing \
							organization '{}' has not been submittedThe organization: '{}' is the destination organization \
							for the project".format(project_doc.name,organization_doc.current_registration_year,supposed_plan_report['implementing_organization'],organization_doc.name))
		
		# else the organization is not a destionation organization either implementing or reporting
		else:
			# check if the project is already checked for obligations
			if project_doc.name in list_of_projects_already_checked:
				pass
			else:
				# check if organization has reporting obligation
				if project['reporting_organization']:
					# check if plan exist
					plan_report_status = check_plan_or_report_exists(project_doc.name,organization_doc.name,organization_doc.current_registration_year)
					# check if the plan exist
					if plan_report_status['annual_plan_status']:
						# check if the plan approved 
						if plan_report_status['plan_doc_status'] == "Approved":
							pass
						else:
							list_of_errors.append("An Annual Plan for the Project:'{}',year:'{}',for implementing \
								organization '{}' has not been Approved - this organization:'{}' has reporting obligation \
								for the project".format(project_doc.name,organization_doc.current_registration_year,organization_doc.name,organization_doc.name))			
					else:
						status = False
						list_of_errors.append("An Annual Plan for the Project:'{}',year:'{}',for implementing \
						organization '{}' has not been submitted - this organization:'{}' has reporting obligation \
						for the project".format(project_doc.name,organization_doc.current_registration_year,organization_doc.name,organization_doc.name))			

					# check if annual report 
					if plan_report_status['annual_report_status']:
						# check if plan is approved
						if plan_report_status['report_doc_status'] == "Approved":
							pass
						else:
							list_of_errors.append("An Annual Report for the Project:'{}',year:'{}',for implementing \
								organization '{}' has not been Approved - this organization:'{}' has reporting obligation \
								for the project".format(project_doc.name,organization_doc.current_registration_year,organization_doc.name,organization_doc.name))			
					else:
						status = False
						list_of_errors.append("An Annual Report for the Project:'{}',year:'{}',for implementing \
						organization '{}' has not been submitted - this organization:'{}' has reporting obligation \
						for the project".format(project_doc.name,organization_doc.current_registration_year,organization_doc.name,organization_doc.name))			

	# check if user has submitted an NPRE
	npre_status = get_npre_for_specific_year(organization_doc.name,organization_doc.current_registration_year)
	if npre_status['npre_status']:
		# check if the npre has been approved
		if npre_status['form_status'] == "Approved":
			pass
		else:
			# npre does not exist
			status = False
			list_of_errors.append("Non Project Related Expinditure for this organization:'{}'\
				for the year: '{}' is not Approved".format(organization_doc.name,organization_doc.current_registration_year))
	else:
		# npre does not exist
		status = False
		list_of_errors.append("Non Project Related Expinditure for this organization:'{}'\
			for the year: '{}' has not been submitted".format(organization_doc.name,organization_doc.current_registration_year))
	
	# return result
	return {'status':status,'list_of_errors':list_of_errors}
	