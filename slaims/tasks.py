# frappe imports
import frappe
from frappe import enqueue

def closing_organizations_financial_years(financial_year):
    '''
    Function that add the functionality for closing 
    financial years of all organizations within a 
    year into a long queue i.e background task
    '''
    enqueue('slaims.custom_methods.close_open_year_for_all_organizations', financial_year = financial_year)
