# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt
from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import datetime

# global varibles
list_of_sections = ["general_organizaiton_details","organization_suggestion_section","organization_address",
	"address_of_main_office_section_section","somaliland_branch_offices_section",
	"country_director","most_senior_executive_section","three_alternative_contact_persons_section",
	"part_two","pillar_alignment_section","section_alignment_section","themes_alignment_section",
	"geographical_area",
	"accounting_and_reporting_period_section","total_number_of_staff_section",
	"declaration","attachments_section_section","verification",
]

class InternationalNGOForm(Document):
	def validate(self):
		# checks for ordinary form filling
		if self.full_name_of_the_organization  and self.acronym:
			# check if the user has an organization
			check_if_user_has_organization(self)
		else:
			frappe.throw("Please give the name and acronym of organization in order to continue")			
		
		# run the validation function 
		validation_function(self)

		# call the excute function
		execute_actions(self)

		# set the next page
		set_next_page(self)
	
		# add pillars to the pillars table
		add_pillars_to_table(self)

		# add sectors to the sectors table
		add_sectors_to_table(self)

		# add themes to themes table
		add_themes_to_table(self)

		# add contact persons
		add_contact_spaces(self)

		# upgrade the status for imported documents
		if self.imported:
			upgrade_status_for_imported_documents(self)

	def on_update(self):
		
		#update the data to organization form
		map_data_to_organization(self)

	def on_trash(self):
		'''
		Function that runs when the delete document
		function is called
		'''
		pass

def add_pillars_to_table(self):
	'''
	Function that collects all the pillars and places them 
	in the Pillars table
	'''
	if self.current_section == "pillar_alignment_section":
		# hold all marked pillars
		pillars_holder = []
		for item in self.pillar_alignment_table:
			if item.yes:
				pillars_holder.append(item.pillar)
		
		# first clear out the table
		self.pillar_alignment_table = []
		# get all the pillars
		list_of_pillars = frappe.get_list("NDP",
			fields=["name"],
			filters = {
				"type":"Pillar"
		})

		# add the pillars back to the table
		for pillar in list_of_pillars:
			# check if that pillar is marked
			if pillar["name"] in pillars_holder:
				self.append("pillar_alignment_table", {
					"pillar":pillar["name"],
					"yes":1
				})
			else:
				self.append("pillar_alignment_table", {
					"pillar":pillar["name"],
					"yes":0
				})


def add_sectors_to_table(self):
	'''
	Function that collects Sectors Related to Pillars Selected
	in the pillars section and places them in the Sectors table
	'''
	if self.current_section == "section_alignment_section":
		# hold all marked sectors
		sectors_holder = []
		for item in self.section_alignment_table:
			if item.yes:
				sectors_holder.append(item.sector)
		# first clear the sector table
		self.section_alignment_table = []
	
		# get the relavant sectors
		list_of_relevant_sectors = []

		# get sectors for each selected pillar
		for pillar in self.pillar_alignment_table:
			# get all the sectors for each ticked pillar
			if pillar.yes:
				sectors_for_pillar = frappe.get_list("NDP",
					fields=["name","parent_ndp"],
					filters = {
						"type":"Sector",
						"parent_ndp":pillar.pillar
				})

				# append the sectors to the list of pillars
				list_of_relevant_sectors += sectors_for_pillar

		# append the sectors to the sectors table
		for sector in list_of_relevant_sectors:
			# check if that sector is marked
			if sector["name"] in sectors_holder:
				self.append("section_alignment_table", {
					"pillar":sector["parent_ndp"],
					"sector":sector["name"],
					"yes":1
				})
			else:
				self.append("section_alignment_table", {
					"pillar":sector["parent_ndp"],
					"sector":sector["name"]
				})


def add_themes_to_table(self):
	'''
	Function that collects Themes Related to Sectors Selected
	in the Sectors section and places them in the Themes table
	'''
	if self.current_section == "themes_alignment_section":
		themes_holder = []
		for item in self.themes_alignment_table:
			if item.yes:
				themes_holder.append(item.theme)
		# first clear the pillars table
		self.themes_alignment_table = []

		# holds a list of relevant themes
		list_of_relevant_themes = []

		# get sectors for each selected pillar
		for sector in self.section_alignment_table:
			# get all the themes for each ticked sector
			if sector.yes:
				themes_for_sector = frappe.get_list("NDP",
					fields=["name","parent_ndp"],
					filters = {
						"type":"Theme",
						"parent_ndp":sector.sector
				})

				# append the sectors to the list of sectors
				list_of_relevant_themes += themes_for_sector

		# append the themes to the themes table
		for theme in list_of_relevant_themes:
			# check if that sector is marked
			if theme["name"] in themes_holder:
				self.append("themes_alignment_table", {
					"sector":theme["parent_ndp"],
					"theme":theme['name'],
					"yes":1
				})
			else:
				self.append("themes_alignment_table", {
					"sector":theme["parent_ndp"],
					"theme":theme["name"]
				})

def add_contact_spaces(self):
	'''
	Add the Correct Number of Spaces for Connection
	'''
	if len(self.alternative_contact_person) < 3:
		rows_to_add = 3 - len(self.alternative_contact_person)
		for i in range(rows_to_add):
			self.append("alternative_contact_person", {
				# do not add any fields
			})

def check_if_organization_exists(self):
	'''
	Checks if an organization exists else create one
	'''
	
	list_of_organizations = frappe.get_list("Organization",
		fields=["*"],
		filters = {
			"name":self.full_name_of_the_organization
		})
	
	if len(list_of_organizations) == 1:
		# update the status for the organization
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
		doc.status = "Verified"
		doc.save(ignore_permissions = True)

	elif len(list_of_organizations) == 0:
		# create an organization
		doc = frappe.get_doc({"doctype":"Organization"})
		doc.name = self.full_name_of_the_organization
		doc.status = self.status
		doc.organization_type = "International NGO"
		doc.full_name_of_the_organization = self.full_name_of_the_organization
		doc.acronym = self.acronym
		doc.country_of_origin = self.country_of_origin
		
		# check if the user is not the admin
		if "Staff" in frappe.get_roles(frappe.session.user):
			pass
		else:
			# assign the current non-staff user to the organization
			doc.user = frappe.session.user
			doc.owner = frappe.session.user

		# save the organization
		doc.insert(ignore_permissions=True)

		# set the correct organization in the link field
		self.link_to_org_name = self.full_name_of_the_organization
		

def find_simalar_organizations(self):
	'''
	Function That Check if There Are Any Organization with Similar
	Names and Acronyms to those given in the Form
	'''
	# holds all the organization found through this search
	all_organization = [] 

	def process_results(list_of_organizations):
		'''
		Function that will help to make the code Drier within
		this function by reducing repetitive tasks
		'''

		if len(list_of_organizations) > 0:
			for organization in list_of_organizations:
				organization_name = organization[0]
				all_organization.append(organization_name)	
	
	# check if the user gave organizaion name and acronym
	if self.full_name_of_the_organization and self.acronym:
		# set the given name as the search name
		search_organization_name  = self.full_name_of_the_organization
		search_acronymn = self.acronym

		# search based on full name likeness
		list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '%{}%'".format(self.full_name_of_the_organization))
		
		# process the found results
		process_results(list_of_organizations)

		# full name of each word in the name query
		list_of_words_in_name = search_organization_name.split()
		# get rid of conjuctions
		common_conjuctions = ["of","the"]
		# loop through name parts
		for name_part in list_of_words_in_name:
			if name_part.lower() in common_conjuctions:
				pass
			else:
				# process results 
				list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '%{}%'".format(name_part))
				process_results(list_of_organizations)
		
		# starting 2 letters letter query
		first_two_letters = search_organization_name[:2]
		list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '{}%'".format(first_two_letters))
		if len(list_of_organizations) > 0:
			process_results(list_of_organizations)
		else:
			pass

		# Acronym matches
		# full name of acronym matches
		search_acronymn = self.acronym
		list_of_organizations = frappe.db.sql("select name from `tabOrganization` where acronym like '%{}%'".format(search_acronymn))
		if len(list_of_organizations) > 0:
			process_results(list_of_organizations)
		else:
			pass
	
		# loop through unique organizations
		unique_results =  set(all_organization)
		# retun unique results
		return unique_results
	else:
		frappe.throw("Please give the organization name and acronym in order to continue")

def set_next_page(self):
	# skip or show the organization suggestion section
	if self.current_section == "organization_suggestion_section":
		if not self.link_to_org_name:
			# find matches
			found_organizations = find_simalar_organizations(self)
			if len(found_organizations) > 0:
				# clear the tables first
				self.organization_suggestion_table = []
				# add each unique result to table
				for organization in found_organizations:
					# append to select table
					self.append("organization_suggestion_table", {
						"organization_name":organization
					})	

				# unselect the yes and no
				self.yes_found_org_match = 0 
				self.did_not_find_org_match	= 0
			else:
				# create the organization
				new_org_doc  = frappe.get_doc({"doctype":"Organization"})
				new_org_doc.name = self.full_name_of_the_organization
				new_org_doc.full_name_of_the_organization = self.full_name_of_the_organization
				new_org_doc.acronym = self.acronym
				new_org_doc.organization_type = "International NGO"

				# check if the user is not the admin
				if "Staff" in frappe.get_roles(frappe.session.user):
					pass
				else:
					# assign the current non-staff user to the organization
					new_org_doc.user = frappe.session.user
					new_org_doc.owner = frappe.session.user

				new_org_doc.insert(ignore_permissions=True)

				# set the link field to organization name
				self.link_to_org_name = self.full_name_of_the_organization

				# skip this section
				current_section_index = list_of_sections.index("organization_suggestion_section")
				# place new page for user to see depending on direction
				if self.direction == "Forward":
					self.previous_section = list_of_sections[current_section_index]
					self.current_section = list_of_sections[current_section_index+1]
					self.next_section = list_of_sections[current_section_index + 2]
				else:
					self.previous_section = list_of_sections[current_section_index - 2]
					self.current_section = list_of_sections[current_section_index-1]
					self.next_section = list_of_sections[current_section_index]
		else:
			# skip this section
			current_section_index = list_of_sections.index("organization_suggestion_section")
			# place new page for user to see depending on direction
			if self.direction == "Forward":
				self.previous_section = list_of_sections[current_section_index]
				self.current_section = list_of_sections[current_section_index+1]
				self.next_section = list_of_sections[current_section_index + 2]
			else:
				self.previous_section = list_of_sections[current_section_index - 2]
				self.current_section = list_of_sections[current_section_index-1]
				self.next_section = list_of_sections[current_section_index]

	# check if a suggestion has been selected and the link field has not been selected
	if not self.imported and not self.link_to_org_name and self.yes_found_org_match:
		# place the correct match in the link field take user to first page
		for organization in self.organization_suggestion_table:
			if organization.yes:
				organization_suggestion_doc = frappe.get_doc("Organization",organization.organization_name)
				# get the organizaion acronym
				# set the selected organization to name field
				self.full_name_of_the_organization = organization.organization_name
				self.acronym = organization_suggestion_doc.acronym 

				# check if the user is not the admin
				if "Staff" in frappe.get_roles(frappe.session.user):
					pass
				else:
					# check if the organization has been assigned to a user
					if organization_suggestion_doc.user:
						pass
					else:
						# assign the current non-staff user to the organization
						organization_suggestion_doc.user = frappe.session.user
						organization_suggestion_doc.owner = frappe.session.user
						organization_suggestion_doc.save(ignore_permissions=True)

				# set the current page to first page
				self.previous_section = ""
				self.current_section = list_of_sections[0]
				self.next_section = list_of_sections[1]

def check_if_user_has_organization(self):
	'''
	Function that ensures that a user is only linked to one 
	organization
	'''
	if "Staff" in frappe.get_roles(frappe.session.user):
		pass
	else:
		# first check if the user is already assigned to an organization
		list_of_organization_user_is_assigned_to = frappe.get_list("Organization",
			fields=["*"],
			filters = {
				"user":frappe.session.user
		})

		if len(list_of_organization_user_is_assigned_to) > 0:
			# check if the organization is the same as the that on the form
			if list_of_organization_user_is_assigned_to[0]['name'] == self.full_name_of_the_organization:
				pass
			else:
				frappe.throw("You username '{}' is already assigned to the organization '{}'\
					<hr> Contact the administrator for assistance if this is wrong  \
						".format(frappe.session.user,list_of_organization_user_is_assigned_to[0]['name']))

		# check if the data is not imported and the link name does not exist
		if not self.imported and not self.link_to_org_name:
			# check that organization name and acronym exists
			if self.full_name_of_the_organization and self.acronym:
				# check if the organization already exists
				list_of_organizations_registered = frappe.get_list("Organization",
						fields=["*"],
						filters = {
							"name":self.full_name_of_the_organization
					})
			
				if len(list_of_organizations_registered) == 1:
					# check if the organization is linked to a user
					if list_of_organizations_registered[0].user:
						# check if that user is the current user
						if list_of_organizations_registered[0].user == frappe.session.user:
							# assign any form if any exists
							assign_registration_forms_to_correct_users(self,list_of_organizations_registered)
							# set the correct organization in the link field
							self.link_to_org_name = self.full_name_of_the_organization
						else:
							frappe.msgprint("The organization {} is linked to a different user,contact administrator for assistance".format(self.full_name_of_the_organization))
							# cancell the document
							self.status = "Cancelled"
							# continue from here 		
					else:
						# update the doctype
						document_to_update = frappe.get_doc("Organization",self.full_name_of_the_organization)
						document_to_update.user = frappe.session.user
						document_to_update.owner = frappe.session.user
						document_to_update.save(ignore_permissions=True)

						# assign any form if any exists
						assign_registration_forms_to_correct_users(self,list_of_organizations_registered)
						# set the correct organization in the link field
						self.link_to_org_name = self.full_name_of_the_organization
				else:
					# allow the user to continue normally
					pass
			else:
				frappe.msgprint("Please provide the organization name and acronym in screen one in order to continue")
				# take the users to screen one
				self.previous_section = ""
				self.current_section = list_of_sections[0]
				self.next_section = list_of_sections[1]
		else:
			pass

def upgrade_status_for_imported_documents(self):
	if self.status == "Admin Verified":
		# progress the form to Verified for imported data
		self.status = "Verified"

	elif self.status == "Verified":
		# progress the form to Validated for imported data
		self.status = "Validated"

	elif self.status == "Validated":
		# progress the form to Approved for imported data
		self.status = "Approved"


def map_data_to_organization(self):
	'''
	Function that Updates the details of the organization
	to the organization form once the form is approved
	'''

	if self.status == "Verified":
		check_if_organization_exists(self)

	if self.status == "Validated":
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
		# save the status
		doc.status = "Validated"
		doc.save()

	# now update the other remaining fields
	# if approved update details to linked organization
	if(self.status == "Approved"):
		# get related organization
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
		
		# ORGANIZATION TYPE SECTION
		doc.status = "Approved"

		# GENERAL ORGANIZATION DETAILS SECTION
		doc.full_name_of_the_organization = self.full_name_of_the_organization
		doc.acronym = self.acronym
		doc.country_of_origin = self.country_of_origin

		# ORGANIZATION ADDRESS SECTION
		doc.address = self.address
		doc.city = self.city
		doc.origin_country = self.origin_country
		doc.postcode = self.postcode

		# ADDRESS OF MAIN OFFICE SECTION
		doc.area_neighborhood = self.area_neighborhood
		doc.town = self.town
		doc.region = self.region
		doc.district = self.district

		# SOMALILAND BRANCH OFFICES
		doc.number_of_branches = self.number_of_branches
		
		# update Somaliland table
		doc.somaliland_branch_offices_table = []
		for branch in self.somaliland_branch_offices_table:
			doc.append("somaliland_branch_offices_table", {
				"region": branch.region,
				"district":branch.district,
				"town":branch.town
			})

		# Organization Director Section
		doc.full_name = self.full_name
		doc.director_country_code = self.director_country_code
		doc.tel_no_1 = self.tel_no_1
		doc.complete_director_tel_no = self.complete_director_tel_no
		doc.director_email = self.director_email

		doc.select_one_director_address = self.select_one_director_address
		doc.directors_address = self.directors_address
		doc.director_city = self.director_city
		doc.director_code = self.director_code
		doc.country = self.country
		doc.region_director_address = self.region_director_address
		doc.district_director_address = self.district_director_address

		# Somaliland Most Senior Executive Section
		doc.if_senior_executive_same_director =  self.if_senior_executive_same_director
		doc.full_name_2 =  self.full_name_2
		doc.country_code_senior_most_executive =  self.country_code_senior_most_executive
		doc.director_tel_no =  self.director_tel_no
		doc.complete_senior_tel_no =  self.complete_senior_tel_no
		doc.email_2 =  self.email_2

		# Three Alternative Contact Persons Section
		doc.alternative_contact_person = []
		for contact in self.alternative_contact_person:
			doc.append("alternative_contact_person", {
				"full_name": contact.full_name,
				"title":contact.title,
				"tel_no":contact.tel_no,
				"email":contact.email
			})

		# Number of Projects Section
		doc.number_of_projects_confirmed_during_the_current_year = self.number_of_projects_confirmed_during_the_current_year

		# Pillar Alignment Section
		doc.pillar_alignment_table = []
		for pillar in self.pillar_alignment_table:
			if pillar.yes:
				doc.append("pillar_alignment_table", {
					"pillar": pillar.pillar,
					"yes": pillar.yes
				})

		# Sectors Alignment Section
		doc.section_alignment_table = []
		for sector in self.section_alignment_table:
			if sector.yes:
				doc.append("section_alignment_table", {
					"pillar": sector.pillar,
					"sector":sector.sector,
					"yes": sector.yes
				})

		# Themes Alignment Table
		doc.themes_alignment_table = []
		for theme in self.themes_alignment_table:
			if theme.yes:
				doc.append("themes_alignment_table", {
					"sector":theme.sector,
					"theme": theme.theme,
					"yes": theme.yes
				})

		# Geographical area(s) of operation
		doc.marodijeh_check = self.marodijeh_check
		doc.hargeisa = self.hargeisa
		doc.gabiley = self.gabiley
		doc.baligubadle = self.baligubadle
		doc.salahlay = self.salahlay

		doc.sanag_check = self.sanag_check
		doc.erigavo = self.erigavo
		doc.badhan = self.badhan
		doc.las_qoray = self.las_qoray
		doc.ela_fweyn = self.ela_fweyn
		doc.dhahar = self.dhahar
		doc.gar_adag = self.gar_adag
		
		doc.sool_check = self.sool_check
		doc.las_anod = self.las_anod
		doc.hudun = self.hudun
		doc.taleh = self.taleh
		doc.aynabo = self.aynabo

		doc.togdheer_check = self.togdheer_check
		doc.burao = self.burao
		doc.odwayne = self.odwayne
		doc.buhodle = self.buhodle

		doc.awdal_check = self.awdal_check
		doc.borama = self.borama
		doc.zeila = self.zeila
		doc.baki = self.baki
		doc.lughaya = self.lughaya

		doc.sahil_check = self.sahil_check
		doc.berbera = self.berbera
		doc.sheekh = self.sheekh

		doc.hargeisa_region = self.hargeisa_region
		
		# Accounting and reporting period
		doc.closing_month = self.closing_month
		doc.closing_day = self.closing_day

		# Total Number of Staff
		doc.managerial_or_program_staff = self.managerial_or_program_staff
		doc.admin_or_support_staff = self.admin_or_support_staff
		doc.managerial_or_program_staff_2 = self.managerial_or_program_staff_2
		doc.admin_or_support_staff_2 = self.admin_or_support_staff_2

		# Attachments Section
		doc.attachments_table = []
		all_attachments_table = 'attachments_table'

		# attach application requests
		for application_request in self.attach_application_request:
			doc.append(all_attachments_table,{
				'attachment_name':application_request.attachment_name,
				'attachment':application_request.attachment,
				'field_name':'attach_application_request'
			})
		# attach certificate from origin country
		for cert in self.cert_from_origin_country:
			doc.append(all_attachments_table, {
				'attachment_name': cert.attachment_name,
				'attachment': cert.attachment,
				'field_name':'cert_from_origin_country'
			})

		# attach as attestation of your existence
		for attestation in self.an_attestation_of_your_existence:
			doc.append(all_attachments_table, {
				'attachment_name': attestation.attachment_name,
				'attachment': attestation.attachment,
				'field_name':'an_attestation_of_your_existence'
			})

		# attach detailed organization info
		for org_info in self.detailed_organization_info:
			doc.append(all_attachments_table, {
				'attachment_name': org_info.attachment_name,
				'attachment': org_info.attachment,
				'field_name':'detailed_organization_info'
			})

		# attach evidence of similar work
		for evidence in self.evidence_of_similar_work:
			doc.append(all_attachments_table, {
				'attachment_name': evidence.attachment_name,
				'attachment': evidence.attachment,
				'field_name':'evidence_of_similar_work'
			})

		# attach founders cv certs
		for cert in self.founders_cv_certs:
			doc.append(all_attachments_table, {
				'attachment_name': cert.attachment_name,
				'attachment': cert.attachment,
				'field_name':'founders_cv_certs'
			})

		# attach cvs of international staff 
		for int_cv in self.cvs_of_international:
			doc.append(all_attachments_table, {
				'attachment_name': int_cv.attachment_name,
				'attachment': int_cv.attachment,
				'field_name':'cvs_of_international'
			})
		
		# attach activity plan
		for activity_plan in self.attach_activity_plan:
			doc.append(all_attachments_table, {
				'attachment_name': activity_plan.attachment_name,
				'attachment': activity_plan.attachment,
				'field_name':'attach_activity_plan'
			})
		
		# attach letter of intent
		for intent_letter in self.intent_letter:
			doc.append(all_attachments_table, {
				'attachment_name': intent_letter.attachment_name,
				'attachment': intent_letter.attachment,
				'field_name':'intent_letter'
			})

		# attach evidence of own office 
		for evidence in self.country_office_evidence:
			doc.append(all_attachments_table, {
				'attachment_name': evidence.attachment_name,
				'attachment': evidence.attachment,
				'field_name':'country_office_evidence'
			})

		# Other (Project Log form, Partner Info Sheet, etc)
		print('*'*80)
		print(self.other_attachments)
		for other_attachment in self.other_attachments:
			doc.append(all_attachments_table, {
				'attachment_name': other_attachment.attachment_name,
				'attachment': other_attachment.attachment,
				'field_name':'other_attachments'
			})

		# attach payment reciepts
		for reciept_of_payment in self.reciept_of_payment:
			doc.append(all_attachments_table, {
				'attachment_name': reciept_of_payment.attachment_name,
				'attachment': reciept_of_payment.attachment,
				'field_name':'reciept_of_payment'
			})

		# add the form as a child table to organization
		doc.append("attached_organization_forms_table", {
			"name_of_form":self.name,
			"year_of_form":self.year_of_registration,
		})

		# save the added changes
		doc.save()

def validation_function(self):
	'''
	Function that checks that all the required fields 
	are given
	'''

	# verification fields
	list_of_verification_fields = [
		{"field":self.full_name_of_the_organization,"name":"Name of Organization"},
		{"field":self.acronym,"name":"Acronym"},
		{"field":self.status,"name":"Status"},
		{"field":self.country_of_origin,"name":"* Country of origin"},

		# add more validation required fields below

	]

	# validation fields
	list_of_validation_fields = [
		{"field":self.full_name_of_the_organization,"name":"Name of Organization"},
		{"field":self.acronym,"name":"Acronym"},
		{"field":self.status,"name":"Status"}

		# add more validation required fields below
	]

	

	# check approval fields
	list_of_approval_fields = [
		# add fields required for approval below
	]


	if self.status == "Verified":
		# loop through list of saving fields
		for field in list_of_verification_fields:
			if field["field"]:
				pass
			else:
				# value for the field is not given throw an errror
				frappe.throw("The Field '{}' is Required Before Verification".format(field["name"]))

	if self.status == "Validated":
		# loop through list of saving fields
		for field in list_of_validation_fields:
			if field["field"]:
				pass
			else:
				# value for the field is not given throw an errror
				frappe.throw("The Field '{}' is Required Before Verification".format(field["name"]))

	if self.status == "Approved":
		# loop through list of saving fields
		for field in list_of_approval_fields:
			if field["field"]:
				pass
			else:
				# value for the field is not given throw an errror
				frappe.throw("The Field '{}' is Required Before Verification".format(field["name"]))

def execute_actions(self):
	'''
	Function that executes some functions once the documents
	is updated
	'''
	# check if the organization has been created
	if self.link_to_org_name:
		pass
	else:
		# check if the user already checked that the organization was
		# not found in the list of suggestions
		if self.did_not_find_org_match:
			found_organizations = frappe.get_list("Organization",
				fields=["name"],
				filters = {
					"name":self.full_name_of_the_organization
			})
			# check the length of found organizations
			if len(found_organizations) > 0:
				# set the link organ name as orgs full name since it been saved to database
				self.link_to_org_name = self.full_name_of_the_organization
			else:
				# create this new organization
				create_new_organization(self,"International NGO")
				# set the link organ name as orgs full name since it been saved to database
				self.link_to_org_name = self.full_name_of_the_organization
		
		# check if the user selected and organization from the suggestion table
		for organization in self.organization_suggestion_table:
			if organization.yes:
				organization_suggestion_doc = frappe.get_doc("Organization",organization.organization_name)
				# get the organizaion acronym
				# set the selected organization to name field
				self.full_name_of_the_organization = organization.organization_name
				self.acronym = organization_suggestion_doc.acronym 
				self.link_to_org_name =  organization.organization_name
			

def create_new_organization(self,organization_type):
	'''
	Function that creates a new organization
	based on a given name and type
	'''
	# create the organization
	new_org_doc  = frappe.get_doc({"doctype":"Organization"})
	new_org_doc.name = self.full_name_of_the_organization
	new_org_doc.full_name_of_the_organization = self.full_name_of_the_organization
	new_org_doc.acronym = self.acronym
	new_org_doc.organization_type = organization_type

	# check if the user is not the admin
	if "Staff" in frappe.get_roles(frappe.session.user):
		pass
	else:
		# assign the current non-staff user to the organization
		new_org_doc.user = frappe.session.user
		new_org_doc.owner = frappe.session.user
	# now save the document
	new_org_doc.insert(ignore_permissions=True)

def assign_registration_forms_to_correct_users(self,list_of_organizations_registered):
	'''
	Function that assigns registration forms to the correct user 
	in that owns the organization
	'''
	# check if the organization has linked registration forms							
	if list_of_organizations_registered[0].organization_type != "Local NGO":
		type_of_registration_form = list_of_organizations_registered[0].organization_type + " Form"

		# get registration forms of given type
		list_of_registration_forms = frappe.db.sql("select name from `tab{}` where full_name_of_the_organization = '{}' && status !='Cancelled'".format(type_of_registration_form,self.full_name_of_the_organization))

		if len(list_of_registration_forms) > 0:
			# check if the form is the same as the first form
			if self.name == list_of_registration_forms[0][0]:	
				pass
			else:
				# change the ownership of the form
				form_doc = frappe.get_doc(type_of_registration_form,list_of_registration_forms[0][0])
				form_doc.owner = frappe.session.user
				form_doc.save(ignore_permissions=True)
				frappe.msgprint("A registration for this organization form has been assigned to you find it under list of '{}' Forms".format(list_of_organizations_registered[0].organization_type + " Form"))
				self.status = "Cancelled"
		else:
			# allow the user to continue with the process but set correct values
			self.full_name_of_the_organization = list_of_organizations_registered[0].name
			self.acronym = list_of_organizations_registered[0].acronym
			self.link_to_org_name = list_of_organizations_registered[0].name

			# take the user to page 1
			self.previous_section = ""
			self.current_section = list_of_sections[0]
			self.next_section = list_of_sections[1]

	elif list_of_organizations_registered[0].organization_type == "Local NGO":
		# check somali forms
		type_of_registration_form = "Local NGO Form Somali"
		# get registration forms of given type
		list_of_registration_forms_somali = frappe.db.sql("select name,full_name_of_the_organization from `tabLocal NGO Form Somali` where full_name_of_the_organization = '{}' && status !='Cancelled'".format(self.full_name_of_the_organization))

		if len(list_of_registration_forms_somali) > 0:
			# check if the form is the same as the first form
			if self.name == list_of_registration_forms_somali[0][0]:
				pass
			else:
				# change the ownership of the form
				# continue from here
				form_doc = frappe.get_doc("Local NGO Form Somali",list_of_registration_forms_somali[0][0])
				form_doc.owner = frappe.session.user
				form_doc.save(ignore_permissions=True)
				frappe.msgprint("A registration for this organization form has been assigned to you find it under list of 'Local NGO Form Somali' List")
				self.status = "Cancelled"

		# check somali forms
		type_of_registration_form = "Local NGO Form English"
		# get registration forms of given type
		list_of_registration_forms_english = frappe.db.sql("select name,full_name_of_the_organization from `tabLocal NGO Form English` where full_name_of_the_organization = '{}' && status !='Cancelled'".format(self.full_name_of_the_organization))

		if len(list_of_registration_forms_english) > 0:
			# check if the form is the same as the first form
			if self.name == list_of_registration_forms_english[0][0]:
				pass
			else:
				# change the ownership of the form
				form_doc = frappe.get_doc("Local NGO Form Somali",list_of_registration_forms_somali[0][0])
				form_doc.owner = frappe.session.user
				form_doc.save(ignore_permissions=True)
				frappe.msgprint("A registration for this organization form has been assigned to you find it under list of 'Local NGO Form Somali' List")
				self.status = "Cancelled"

		if len(list_of_registration_forms_english) > 0 and len(list_of_registration_forms_somali) > 0:
			pass
		else:
			# allow the user to continue with the process but set correct values
			self.full_name_of_the_organization = list_of_organizations_registered[0].name
			self.acronym = list_of_organizations_registered[0].acronym
			self.link_to_org_name = list_of_organizations_registered[0].name
			
			# take the user to page 1
			self.previous_section = ""
			self.current_section = list_of_sections[0]
			self.next_section = list_of_sections[1]		
