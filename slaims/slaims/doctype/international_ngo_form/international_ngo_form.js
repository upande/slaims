// Copyright (c) 2019, upande and contributors
// For license information, please see license.txt


// list of organization form sections in organization
var list_of_sections = {
	"International NGO": [
		"general_organizaiton_details","organization_suggestion_section","organization_address",
		"address_of_main_office_section_section","somaliland_branch_offices_section",
		"country_director","most_senior_executive_section","three_alternative_contact_persons_section",
		"part_two","pillar_alignment_section","section_alignment_section","themes_alignment_section",
		"geographical_area",
		"accounting_and_reporting_period_section","total_number_of_staff_section",
		"attachments_section_section",
		"verification",
	],
	"View All":[
		"general_organizaiton_details","organization_address",
		"address_of_main_office_section_section","somaliland_branch_offices_section",
		"country_director","most_senior_executive_section","three_alternative_contact_persons_section",
		"part_two","pillar_alignment_section","section_alignment_section","themes_alignment_section",
		"geographical_area",
		"accounting_and_reporting_period_section","total_number_of_staff_section",
		"attachments_section_section",
		"validation","verification","approval"
	],
	"All": [
		"organization_type_section_section",
		"general_organizaiton_details","organization_suggestion_section","organization_address",
		"address_of_main_office_section_section","somaliland_branch_offices_section",
		"country_director","most_senior_executive_section","three_alternative_contact_persons_section",
		"part_two","pillar_alignment_section","section_alignment_section","themes_alignment_section",
		"geographical_area",
		"accounting_and_reporting_period_section","total_number_of_staff_section",
		"attachments_section_section",
		"validation","verification","approval"
	]
}

// attachments_section_section
var attachments_per_organization_type = {
	"Somaliland Government Institution":[],
	"International NGO":[
		"inst_attachment_ingo","cert_from_origin_country","an_attestation_of_your_existence",
		"detailed_organization_info","evidence_of_similar_work","founders_cv_certs",
		"cvs_of_international","activity_plan","intent_letter","country_office_evidence",
		"reciept_of_payment"
	],
	"Local NGO":[],
	"Private Sector Company":[
		"private_sector_company_attachements","valid_registrationlicence_from_the_mot",
		"certificate_of_tax_clearance","a_copy_of_articles_of_incorporation",
		"standard_letter_of_agreement"
	],
	"Multi-Lateral Organizations":[],
	"Development Partner":[],
	"UN system organization":[],
	"Other":[],
	"All":[
		// Somaliland Government Institution
		"private_sector_company_attachements","valid_registrationlicence_from_the_mot",
		"certificate_of_tax_clearance","a_copy_of_articles_of_incorporation",
		"standard_letter_of_agreement",

		// International NGO
		"inst_attachment_ingo","cert_from_origin_country","an_attestation_of_your_existence",
		"detailed_organization_info","evidence_of_similar_work","founders_cv_certs",
		"cvs_of_international","activity_plan","intent_letter","country_office_evidence",
		"reciept_of_payment"
	]
}


var list_of_international_fields = [
	// organization details section
	"full_name_of_the_organization","acronym","country_of_origin",

	// suggestion section
	"organization_suggestion_table","yes_found_org_match","did_not_find_org_match",

	// organization address section
	"address","city","origin_country","postcode",

	// Organization details section
	"area_neighborhood", "town", "region", "district",
	
	// Somaliland branch officess section
	"number_of_branches", "somaliland_branch_offices_table", "full_name", "director_country_code",
	"tel_no_1", "director_email", "select_one_director_address",
	"directors_address","area_neighborhood_senior_most","director_city", "director_code", "country", "region_director_address",
	"district_director_address",

	// Most senior executive section
	"if_senior_executive_same_director", "full_name_2", "country_code_senior_most_executive",
	"director_tel_no", "email_2", 
	
	//Three alternative contacts section
	"alternative_contact_person", "number_of_projects_confirmed_during_the_current_year", 

	//Pillar alignment section
	"pillar_alignment_table", "confirm_pillars_check", 
	
	// Section alignment secton
	"section_alignment_table", "confirm_sectors_check", 
	
	// Themes alignment section
	"themes_alignment_table", 
	
	//Geographical area section
	"marodijeh_check", "hargeisa", "gabiley", "baligubadle", "salahlay", "sanag_check",
	"erigavo", "badhan", "las_qoray", "ela_fweyn", "dhahar", "gar_adag", "hargeisa_region",
	"sool_check", "las_anod", "hudun", "taleh", "aynabo", "togdheer_check", "burao",
	"odwayne", "buhodle", "awdal_check", "borama", "zeila", "baki", "lughaya", "sahil_check",
	"berbera", "sheekh",
	
	// Accounting and reporting period section
	"closing_month", "closing_day", "managerial_or_program_staff",
	"admin_or_support_staff", "managerial_or_program_staff_2", "admin_or_support_staff_2",
	
	// Attachments section
	"cert_from_origin_country", "an_attestation_of_your_existence", "detailed_organization_info",
	"evidence_of_similar_work", "founders_cv_certs", "cvs_of_international", "activity_plan",
	"intent_letter", "country_office_evidence", "reciept_of_payment", 

	// Verification and approval section
	"verifier_name","verifier_designation","verification_date",	
]

// function that sets custom buttons
function add_custom_buttons(button_name,action){
	cur_frm.add_custom_button(__(button_name), function(){

		// only show this buttons to users with administrative privillages
		if(action=="Unverify"){
			unverify_button()
		}else if (action == "Unvalidate"){
			unvalidate_button()
		}else if(action=="Edit"){
			// check user is allowed to edit
			if(cur_frm.doc.verified){
				frappe.throw("You cannot edit a form that is already verified \
				,contact the admin for assistance")
			}else{
				// make the forms readble for user
				cur_frm.set_value("viewing_information",0)
				cur_frm.set_value("current_section",list_of_sections[0])
				cur_frm.save()
			}
		}else if(action = "Go To Page"){
			goToPage()
		}
	},__("Form Menu"));
}

// function that unverifies and unvalidates the form
function unverify_button(){
	cur_frm.set_value("information_checked",0)
	cur_frm.set_value("verifier_name","")
	cur_frm.set_value("verifier_designation","")
	cur_frm.set_value("verification_date","")
	cur_frm.set_value("verifier_confirmation_of_inst","")
	cur_frm.set_value("verified","")
	cur_frm.set_value("validated","")
	cur_frm.set_value("validated_by","")
	cur_frm.set_value("verified","")
	cur_frm.set_value("information_checked_by_staff",0)
	cur_frm.set_value("information_checked_by","")
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Pending Verification")
	cur_frm.save()
}

// function that unvalidates the form
function unvalidate_button(){
	cur_frm.set_value("information_checked_by_staff",0)
	cur_frm.set_value("information_checked_by","")
	cur_frm.set_value("validated","")
	cur_frm.set_value("validated_by","")
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Pending Validation")
	cur_frm.save()
}

// function that unverifies and unapprovess the form
function unapprove_button(){
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Verified")
	cur_frm.save()
}

/*function that toogles field to hide or unhide*/
function hide_unhide_fields(frm, list_of_fields, hide_or_unhide) {
	for (var i = 0; i < list_of_fields.length; i++) {
		frm.toggle_display(list_of_fields[i], hide_or_unhide)
	}
}

// function that determines which section the user should see
function navigation_function(frm,list_of_sections){
	// hide all the fields
	hide_unhide_fields(frm, list_of_sections["All"], false)

	if(cur_frm.doc.viewing_information){
		// allow user to see all section
		hide_unhide_fields(frm, list_of_sections["View All"], true)
	}else{
		// if form has a current section
		if(frm.doc.current_section){
			// unhide the current section
			frm.toggle_display(frm.doc.current_section,true)
		}else{
			frm.doc.current_section = list_of_sections["International NGO"][0]
			frm.doc.next_section = list_of_sections["International NGO"][1]

			// refresh form
			cur_frm.refresh()
		}

		// fill in the section page numbers
		if(frm.doc.current_section){
			// do nothing for now
			var number_of_sections = list_of_sections["International NGO"].length
			var current_section_index = list_of_sections["International NGO"].indexOf(cur_frm.doc.current_section)
			// fill in the section page numbers
			cur_frm.set_value("pagesection","Screen: "+String(parseInt(current_section_index)+1)+" of "+String(number_of_sections))
		}else{
			cur_frm.set_value("pagesection","Screen: 1")
		}
	}
}


// function that determines the next section the user need to fill and unhides it
function unhide_next_section(frm,steps){
	var current_section_index = list_of_sections["International NGO"].indexOf(cur_frm.doc.current_section)

	var current_user_section = list_of_sections["International NGO"][current_section_index]
	var next_user_section = list_of_sections["International NGO"][current_section_index + steps]
	var future_user_section = list_of_sections["International NGO"][current_section_index + steps+1]

	// set next sections
	if(next_user_section){
		cur_frm.set_value("previous_section",current_user_section)
		cur_frm.set_value("current_section",next_user_section)
		cur_frm.set_value("next_section",future_user_section)
		cur_frm.set_value("direction","Forward")
	}else{
		msgprint("You Reached the End of the Form")
	}
	// save the form
	cur_frm.save()
}

// function that determines the previous section the user filled and unhides it
function unhide_previous_section(frm){
	// unhide_next_field(list_of_fields)	
	var current_section_index = list_of_sections["International NGO"].indexOf(frm.doc.current_section)

	var current_user_section = list_of_sections["International NGO"][current_section_index - 1]
	var previous_user_section = list_of_sections["International NGO"][current_section_index - 2]
	var future_user_section = list_of_sections["International NGO"][current_section_index]

	// set next sections
	if(current_user_section){
		cur_frm.set_value("previous_section",previous_user_section)
		cur_frm.set_value("current_section",current_user_section)
		cur_frm.set_value("next_section",future_user_section)
		cur_frm.set_value("direction","Backward")
	}else{
		msgprint("You Reached the Beginning of the Form")
	}

	// save the form
	cur_frm.save()
}

// function that sets custom buttons
function add_go_to_buttons(button_name,action){
	cur_frm.add_custom_button(__(button_name), function(){

	if(action = "Go To Page"){
		frappe.prompt([
			{'fieldname': 'page', 'fieldtype': 'Int', 'label': 'Go to page', 'reqd': 1}  
		],
		function(values){
			if(values.page <= list_of_sections["International NGO Form"].length &&  values.page > 0 ){
				// set the current page
				cur_frm.set_value("previous_section",list_of_sections["International NGO Form"][values.page -2])
				cur_frm.set_value("current_section",list_of_sections["International NGO Form"][values.page-1])
				cur_frm.set_value("next_section",list_of_sections["International NGO Form"][values.page ])

				// // save the form in order to go to that page
				cur_frm.save()

			}else{
				msgprint("Please provide a page betweeen 1 and "+String(list_of_sections["International NGO"].length))
			}
		},
		'Go to page',
		'Go'
		)
	}

},__("Go to page"));
}

function exitButton(){
	
	cur_frm.add_custom_button('Exit', function(){
		
		if(frappe.user.has_role('Staff') || frappe.user.has_role('Super Admin')){
			window.location = '/desk#List/International%20NGO%20Form/List'
		} else{
			window.location = '/profile_organization'
		}
	})
	
}

// function that checks to ensure that all the required fields are
// given before an action is undertaken e.g verification, validation etc.
function check_required_fields(action){
	var return_value = true
	if(action == "verification"){
		// fields required before verification
		var verification_required_fields = [
			{"field_name":"* Country of origin","field_value":cur_frm.doc.country_of_origin}
		]

		// loop through checking if all the required fields are given
		verification_required_fields.forEach(function(required_field){
			if(required_field["field_value"]){
				// do nothing
			}else{
				return_value = false
				frappe.msgprint(`The field ${required_field['field_name']} is required before verification`)
			}
		})
	}else if (action == 'validation'){

	}else if (action == 'approval'){

	}
	
	// return the return value
	return return_value
}


var check_required_organization_type_section_section = function(){
	return {status:true,steps:1}
}

var check_required_general_organizaiton_details = function(){
	// check has given all the required fields
	if(cur_frm.doc.full_name_of_the_organization && cur_frm.doc.acronym && cur_frm.doc.country_of_origin ){
		return {status:true,steps:1}	
	}else{
		frappe.throw("Please Give Values for The Fields '* Name','* Acronym','* Country of Origin (Select One)' In Order to Continue")
	}
}

var check_required_organization_suggestion_section = function(){
	// check has given all the required field
	if(cur_frm.doc.yes_found_org_match || cur_frm.doc.did_not_find_org_match){
		return {status:true,steps:1}	
	}else{
		frappe.throw("Please select 'Yes' or 'No' in order to continue")
	}
}

var check_required_organization_address = function(){
	return {status:true,steps:1}
}

var check_required_address_of_main_office_section_section = function(){
	return {status:true,steps:1}
}

var check_required_somaliland_branch_offices_section = function(){
	// check if the user has added the exact number of branches in the details table
	if(cur_frm.doc.number_of_branches == cur_frm.doc.somaliland_branch_offices_table.length){
		// correct number of branches
		return {status:true,steps:1}
	}else{
		frappe.throw("You have indicated that apart from the main office you have "+cur_frm.doc.number_of_branches+" branches, please provide details\
		for the same number of offices")
	}
}

var check_required_country_director = function(){
	return {status:true,steps:1}
}

var check_required_most_senior_executive_section = function(){
	return {status:true,steps:1}
}

var check_required_three_alternative_contact_persons_section = function(){
	return {status:true,steps:1}
}


var check_required_part_two = function(){
	return {status:true,steps:1}
}

var check_required_pillar_alignment_section = function(){
	return {status:true,steps:1}
}

var check_required_section_alignment_section = function(){
	return {status:true,steps:1}
}

var check_required_themes_alignment_section = function(){
	return {status:true,steps:1}
}

var check_required_themes_alignment_section = function(){
	return {status:true,steps:1}
}

var check_required_section_break_47 = function(){
	return {status:true,steps:1}
}

var check_required_geographical_area = function(){
	return {status:true,steps:1}
}

var check_required_accounting_and_reporting_period_section = function(){
	return {status:true,steps:1}
}

var check_required_total_number_of_staff_section = function(){
	return {status:true,steps:1}
}

var check_required_attachments_section_section = function(){
	return {status:true,steps:1}
}

var check_required_verification_section_section = function(){
	return {status:true,steps:1}
}


var validation_function_per_section ={
	"organization_type_section_section":check_required_organization_type_section_section,
	"general_organizaiton_details":check_required_general_organizaiton_details,
	"organization_suggestion_section":check_required_organization_suggestion_section,
	"organization_address":check_required_organization_address,
	"address_of_main_office_section_section":check_required_address_of_main_office_section_section,
	"somaliland_branch_offices_section":check_required_somaliland_branch_offices_section,
	"country_director":check_required_country_director,
	"most_senior_executive_section":check_required_most_senior_executive_section,
	"three_alternative_contact_persons_section":check_required_three_alternative_contact_persons_section,
	"part_two":check_required_part_two,
	"pillar_alignment_section": check_required_pillar_alignment_section,
	"section_alignment_section": check_required_section_alignment_section,
	"themes_alignment_section": check_required_themes_alignment_section,
	"section_alignment_section":check_required_section_alignment_section,
	"section_break_47":check_required_section_break_47,
	"geographical_area":check_required_geographical_area,
	"accounting_and_reporting_period_section":check_required_accounting_and_reporting_period_section,
	"total_number_of_staff_section":check_required_total_number_of_staff_section,
	"attachments_section_section":check_required_attachments_section_section,
	"verification":check_required_verification_section_section
}

// function filter relevant fields
function filter_fields(){

	if(frappe.user.has_role("Staff")){
		// filter organization based on those assigned to user
		cur_frm.set_query("full_name_of_the_organization", function() {
			return {
				"filters": {
					"organization_type": "International NGO",
				}
			}
		});
	}else{
		// filter organization based on those assigned to user
		cur_frm.set_query("full_name_of_the_organization", function() {
			return {
				"filters": {
					"organization_type": "International NGO",
					"user":frappe.session.user
				}
			}
		});
	}

	// filter regions
	cur_frm.set_query("region", function() {
		return {
			"filters": {
				"type": "Region",
				name:["!=","Central/Hargeisa"]
			}
		}
	});

	// filter districts
	cur_frm.set_query("district", function() {
		return {
			"filters": {
				"type": "District"
			}
		}
	});

	// filter office branches field table
	cur_frm.set_query("region", "somaliland_branch_offices_table", function(doc, cdt, cdn) {
		return {
			filters: {
				type: "Region",
				name:["!=","Central/Hargeisa"]
			}
		};
	});

	cur_frm.set_query("district", "somaliland_branch_offices_table", function(doc, cdt, cdn) {
		return {
			filters: {
				type: "District",
			}
		};
	});

	
	cur_frm.set_query("country_code_senior_most_executive", function() {
		return {
			filters: {
				name_of_country: "Somaliland",
			}
		};
	});
}

// function that hides or unhides certain fields based on what the user selects while filling the form
function options_based_hide_unhide(){
	// determine whether to show the project management menu
	if(frappe.user.has_role("Staff")){
		add_custom_buttons("Unverify Form","Unverify")
	}

	if(frappe.user.has_role("Documents Approver")){
		add_custom_buttons("Unvalidate Form","Unvalidate")
	}

	// allow all users to seee this menu items
	add_custom_buttons("Edit Form","Edit")
	add_custom_buttons("Go to screen","Go To Page")

	// first hide all the address fields for senior most executive
	cur_frm.toggle_display("directors_address", false)
	cur_frm.toggle_display("director_city", false)
	cur_frm.toggle_display("director_code", false)
	cur_frm.toggle_display("country", false)
	cur_frm.toggle_display("region_director_address", false)
	cur_frm.toggle_display("district_director_address", false)
	cur_frm.toggle_display("area_neighborhood_senior_most", false)

	// now hide unhide based on selected option
	if (cur_frm.doc.select_one_director_address == "Same address as Somaliland main office") {
		cur_frm.toggle_display("directors_address", false)
		cur_frm.toggle_display("director_city", true)
		cur_frm.toggle_display("director_code", false)
		cur_frm.toggle_display("country", false)
		cur_frm.toggle_display("region_director_address", true)
		cur_frm.toggle_display("district_director_address", true)
		cur_frm.toggle_display("area_neighborhood_senior_most", true)
		

	} else if (cur_frm.doc.select_one_director_address == "Same address as origin country main office"){
		cur_frm.toggle_display("directors_address", true)
		cur_frm.toggle_display("director_city", true)
		cur_frm.toggle_display("director_code", true)
		cur_frm.toggle_display("country", true)
		cur_frm.toggle_display("region_director_address", false)
		cur_frm.toggle_display("district_director_address", false)
		cur_frm.toggle_display("area_neighborhood_senior_most", false)
		
	} else if (cur_frm.doc.select_one_director_address == "Other") {
		cur_frm.toggle_display("directors_address", true)
		cur_frm.toggle_display("director_city", true)
		cur_frm.toggle_display("director_code", false)
		cur_frm.toggle_display("country", true)
		cur_frm.toggle_display("region_director_address", false)
		cur_frm.toggle_display("district_director_address", false)
		cur_frm.toggle_display("area_neighborhood_senior_most", false)
	
	}

	// SOMALILAND MOST SENIOR EXECUTIVE
	if(cur_frm.doc.if_senior_executive_same_director == "Yes"){
		// unhide senior most executive details fields
		cur_frm.toggle_display("instruction_senior_executive_details", true);
		cur_frm.toggle_display("full_name_2", true);
		cur_frm.toggle_display("director_tel_no", true);
		cur_frm.toggle_display("email_2", true);
		cur_frm.toggle_display("country_code_senior_most_executive", true);
		cur_frm.toggle_display("complete_senior_tel_no", true);

	}else{
		// unhide senior most executive details fields
		cur_frm.toggle_display("instruction_senior_executive_details", false);
		cur_frm.toggle_display("full_name_2", false);
		cur_frm.toggle_display("director_tel_no", false);
		cur_frm.toggle_display("email_2", false);
		cur_frm.toggle_display("country_code_senior_most_executive", false);
		cur_frm.toggle_display("complete_senior_tel_no", false);
	}

	// REGISTRATION YEARS SECTION
	if(cur_frm.doc.were_there_years_unregistered == "Yes"){
		// show the unregistered years table
		cur_frm.toggle_display("inst_unregistered_2", true);
		cur_frm.toggle_display("unregistered_years", true);
	}else{
		// hide the unregistered years table
		cur_frm.toggle_display("inst_unregistered_2", false);
		cur_frm.toggle_display("unregistered_years", false);
		// clear the unregistered  years table
		cur_frm.clear_table("unregistered_years"); 
	}

	// approval sections
	if(frappe.user.has_role("Staff")){
		// check if form is already verified
		if(cur_frm.doc.verified == 1){
			// show all fields
			cur_frm.toggle_display("validation", true);
			// check if user has the role of documents approver
			if(frappe.user.has_role("Documents Approver")){
				cur_frm.toggle_display("approval", true);
			}else{
				cur_frm.toggle_display("approval", false);
			}
		}else{
			// hide the validation ,approval section
			cur_frm.toggle_display("validation", false);
			cur_frm.toggle_display("approval", false);
		}
	}else{
		// hide the validation ,approval section
		cur_frm.toggle_display("validation", false);
		cur_frm.toggle_display("approval", false);
	}	

	// add go to menu button
	add_go_to_buttons("Go To Page","Go To Page")

	// Add exit button to forms of basic users.
	if (frappe.user_roles.includes('Staff') || frappe.user_roles.includes('Super Admin')) {
		// Dont add exit button
	} else {
		exitButton()
	}


}

// Email validation function
function ValidateEmail(mail){
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
	{
		return true
	}
		return false
}


 // Function to navigate through screens
function goToPage(){
frappe.prompt([
	{'fieldname': 'page', 'fieldtype': 'Int', 'label': 'Go to page', 'reqd': 1}  
],
function(values){
	if(values.page <= list_of_sections['International NGO'].length &&  values.page > 0 ){
		// set the current page
		cur_frm.set_value("previous_section",list_of_sections['International NGO'][values.page -2])
		cur_frm.set_value("current_section",list_of_sections['International NGO'][values.page-1])
		cur_frm.set_value("next_section",list_of_sections['International NGO'][values.page ])

		// // save the form in order to go to that page
		cur_frm.save()

	}else{
		msgprint("Please provide a page betweeen 1 and "+String(list_of_sections['International NGO'].length))
	}
},
'Go to page',
'Go'
)
}


function check_if_user_has_organization(){
	
	// check if the user already has an organization
	if(frappe.user.has_role("Staff")){
		// do not do anything	
	}else{
		if(cur_frm.doc.status == "Cancelled"){
			// redirect the user back to the list
			frappe.confirm(
				"You do not have sufficient permissions to view a cancelled document, you are being redirected back to the list",
				function(){
					frappe.set_route("List", "International NGO Form")
				},
				function(){
					frappe.set_route("List", "International NGO Form")
				}
			)
		}else{
			// check if the form is already saved
			if(cur_frm.doc.__islocal ? 0 : 1){
				// allow user to continue
			}else{
				// check if the user is associated with any organization
				frappe.call({
					method: "frappe.client.get_list",
					args: 	{
							doctype:"Organization",
							filters: {
								user:frappe.session.user
							},
					fields:["*"]
					},
					callback: function(response) {
						if(response.message.length > 0){
							// check if the user has a form
							frappe.confirm(
								"You have already registered your organization '"+response.message[0].name+"' use the renewal forms under "+response.message[0].organization_type +" Renewal Forms to renew/update details",
								function(){
									location.replace("/desk#modules/SLAIMS")
								},
								function(){
									location.replace("/desk#modules/SLAIMS")
								}
							)
						}
					}
				})
			}
		}	
	}
}


function make_fields_read_only(list_of_fields){
	if(frappe.user.has_role("Super Admin")){
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",0)	
		})
	}
	else if(frappe.user.has_role("Staff")){
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",1)	
		})
	}else{
		if(cur_frm.doc.verified == 1 || cur_frm.doc.viewing_information){
			// check if user has validation rights
			if(frappe.user.has_role("Super Admin")){
				list_of_fields.forEach(function(v,i){
					cur_frm.set_df_property(v,"read_only",0)	
				})
			}else{
				list_of_fields.forEach(function(v,i){
					cur_frm.set_df_property(v,"read_only",1)	
				})
			}
		}else{
			list_of_fields.forEach(function(v,i){
				cur_frm.set_df_property(v,"read_only",0)	
			})
		}
	}	
}

// function that determines if  user has privillages to Validite , approve,verify info
function check_privillages(frm,action){
	// check if doc is saved
	if(frm.doc.__islocal ? 0 : 1){
		// check privillages absed on action
		if(action == "checked"){
			// check if the user is the owner of the doc
			if(frappe.user.has_role("Staff")){
				// allow administrator to mark as checked
				cur_frm.set_value("information_checked",1)
				cur_frm.save()
			}
			else if(frm.doc.owner){
				// check if the current user is also the owner
				if(frappe.session.user == frm.doc.owner){
					// allow administrator to validate
					cur_frm.set_value("information_checked",1)
					cur_frm.save()
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}else{
				// do not do anything for now
			}

		}else if(action == "verify"){
			// check if the user is the owner of the doc
			if(frappe.user.has_role("Staff")){
				if (frm.doc.verifier_name && frm.doc.verifier_designation && frm.doc.verification_date && frm.doc.verifier_confirmation_of_inst) {
					// allow administrator to validate
					cur_frm.set_value("verified",1)
					cur_frm.set_value("status","Verified")
					cur_frm.save()
				} else {
					cur_frm.set_value('verified', 0);
					// check if designation fields have been given
					if(cur_frm.doc.viewing_information){
						frappe.throw('Please switch the mode to edit mode, in order to add name,designation,confirmation and date');
					}else{
						frappe.throw('Fill in <b>Name</b>,<b>Designation</b>,<b>Date</b> and <b>Confirm</b> fields before verifying');
					}
				}
			}
			else if(frm.doc.owner){
				// check if the current user is also the owner
				if(frappe.session.user == frm.doc.owner){
					if (frm.doc.verifier_name && frm.doc.verifier_designation && frm.doc.verification_date && frm.doc.verifier_confirmation_of_inst) {
						// allow user to validate their organization
						cur_frm.set_value("verified",1);
						cur_frm.set_value("status","Verified");
						cur_frm.save();
					} else {
						cur_frm.set_value('verified', 0);
						// check if designation fields have been given
						if(cur_frm.doc.viewing_information){
							frappe.throw('Please switch the mode to edit mode, in order to add name and designation');
						}else{
							frappe.throw('Fill in <b>Name</b>, <b>Designation</b>, <b>Date</b> and <b>Confirm</b> fields before verifying');
						}
					}
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}else{
				if(frappe.session.user == frm.doc.created_by){
					if (frm.doc.verifier_name && frm.doc.verifier_designation && frm.doc.verification_date && frm.doc.verifier_confirmation_of_inst) {
						// allow user to validate their organization
						cur_frm.set_value("verified",1)
						cur_frm.set_value("status","Verified")
						cur_frm.save()
					} else {
						cur_frm.set_value('verified', 0);
						// check if designation fields have been given
						if(cur_frm.doc.viewing_information){
							frappe.throw('Please switch the mode to edit mode, in order to add name and designation');
						}else{
							frappe.throw('Fill in <b>Name</b>, <b>Designation</b>, <b>Date</b> and <b>Confirm</b> fields before verifying');
						}
					}
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}
		}else if(action == "checked_by_staff"){
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Staff")){
				// the user has privillages allow
				// check if the if its validated
				if(frm.doc.verified == 1){
					// information_checked_by_mopnd_staff
					cur_frm.set_value("information_checked_by_staff",1)
					cur_frm.set_value("information_checked_by",frappe.session.user)
					cur_frm.save()
				}else{
					cur_frm.set_value("information_checked_by_staff",0)
					cur_frm.set_value("information_checked_by","")
					frappe.throw("Form need to be Verified before Staff can Mark it as Checked")
				}	
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
		}else if(action == "validate"){
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Staff")){
				// check if staff has marked the form as checked
				if(frm.doc.information_checked_by_staff == 1){
					cur_frm.set_value("validated",1)
					cur_frm.set_value("status","Validated")
					cur_frm.set_value("validated_by",frappe.session.user)
					cur_frm.save()
				}else{
					frappe.throw("Form need to be Checked by Staff before Validation")
				}
				
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
	
		}else if(action == "approve"){
			// check if the user has the Role "Documents Approver"
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Documents Approver")){
				// the user has privillages allow
				// check if the if its validated
				if(frm.doc.validated == 1){
					cur_frm.set_value("approved",1)
					cur_frm.set_value("status","Approved")
					cur_frm.set_value("approved_by",frappe.session.user)
					cur_frm.save()
				}else{
					frappe.throw("Form need to be validated before approval")
				}
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
		}
	}else{
		frappe.throw("Please save the document first")
	}
}

function redirect_url(new_url){
    // allow the user to view page
    window.location = new_url
}
/*****************************************************************************************/
// Trigger based function
frappe.ui.form.on('International NGO Form', {
	onload: function(frm) {
	// do not allow users to add rows in fields they are not supposed to add tables to 
	cur_frm.get_field("alternative_contact_person").grid.cannot_add_rows = true;
	cur_frm.get_field("pillar_alignment_table").grid.cannot_add_rows = true;
	cur_frm.get_field("section_alignment_table").grid.cannot_add_rows = true;
	cur_frm.get_field("themes_alignment_table").grid.cannot_add_rows = true;
	
	}
});

frappe.ui.form.on('International NGO Form', {
	refresh: function(frm) {
		// navigation code
		navigation_function(frm,list_of_sections)
		filter_fields()
		options_based_hide_unhide()

		// do not allow users to add rows in fields they are not supposed to add tables to 
		cur_frm.get_field("organization_suggestion_table").grid.cannot_add_rows = true;
		cur_frm.get_field("alternative_contact_person").grid.cannot_add_rows = true;
		cur_frm.get_field("pillar_alignment_table").grid.cannot_add_rows = true;
		cur_frm.get_field("section_alignment_table").grid.cannot_add_rows = true;
		cur_frm.get_field("themes_alignment_table").grid.cannot_add_rows = true;

		// check if the user already registered an organization if so redirect to list
		check_if_user_has_organization()

		// make fields read only based on role and status
		make_fields_read_only(list_of_international_fields)
	}
});

// the first section
frappe.ui.form.on('International NGO Form', 'full_name_of_the_organization', function(frm){
	// check if the full name and link name are equal
	if(cur_frm.doc.full_name_of_the_organization && cur_frm.doc.link_to_org_name){
		if(cur_frm.doc.full_name_of_the_organization == cur_frm.doc.link_to_org_name){
			// pass
		}else{
			cur_frm.set_value("link_to_org_name","")
		}
	}
})

// Hide organization suggestion table on selecting ''NO
frappe.ui.form.on('International NGO Form', 'did_not_find_org_match', function(frm){
	if(frm.doc.did_not_find_org_match){
		cur_frm.toggle_display(['organization_suggestion', 'organization_suggestion_table'], false)
		cur_frm.set_df_property('yes_found_org_match', 'read_only', 1)
	} else{
		cur_frm.toggle_display(['organization_suggestion', 'organization_suggestion_table'], true)
		cur_frm.set_df_property('yes_found_org_match', 'read_only', 0)
	}
})

// function that redirects the user to the profile page organization 
// oon form verification
frappe.ui.form.on('International NGO Form', {
	after_save:function(frm){
		// check if user is not part of the admin
		if(frappe.user.has_role("Staff") || frappe.user.has_role("Super Admin") || frappe.user.has_role("Documents Approver")){
			// do no do anything
		}else{
			// check if the user is a basic user and the status is verified
			if(frappe.user.has_role("Organization Login")){
				if(cur_frm.doc.status == "Verified"){
					// redirect to profile page
					setTimeout(function(){redirect_url('/profile_organization');}, 950);
				}
			}
		}
	}
})

/*****************************************************************************************/
// Alternative Contact persons section
frappe.ui.form.on('Alternative Contact Persons','email',function(frm,cdt,cdn){
	var row = locals[cdt][cdn]

	if(row.email){
		if(ValidateEmail(row.email)){
			// allow the user to continue
		}else{
			row.email = ""
			frappe.throw("Please enter a valid email")
		}
	}
})

/*****************************************************************************************/
// organization suggestion table
frappe.ui.form.on("International NGO Form", "yes_found_org_match", function(frm){
	if(cur_frm.doc.yes_found_org_match){
		var user_selected_match = false
		var number_selected = 0
		var found_match = ""
		
		// loop through the child table
		$.each(cur_frm.doc.organization_suggestion_table,function(i,v){
			if(v.yes == 1){
				user_selected_match = true
				number_selected += 1
				found_match = v.organization_name
			}
			
		})

		// check if the user selected  a match
		if(user_selected_match){
			// check if the user selected one 
			if(number_selected == 1){
				// allow user to continue
			}else if (number_selected > 1){
				cur_frm.set_value("yes_found_org_match",0)
				frappe.throw("You can only select one match for your organization in the table above")
			}
		}else{
			cur_frm.set_value("yes_found_org_match",0)
			frappe.throw("You have not selected any match for you organization in the table")
		}
	}else{
		// dont do anything
	}
})

/*****************************************************************************************/
// Address of main office in Somaliland section

// function called when  the region field clicked under Navigation Section
frappe.ui.form.on("International NGO Form", "region", function(frm){

	if(cur_frm.doc.region){
		// check if a district is already given
		if(cur_frm.doc.district){
			// check if district is within region
			frappe.call({
				method: "frappe.client.get_list",
				args: 	{
						doctype: "Administrative Unit",
						filters: {
							name:frm.doc.district
						},
				fields:["*"]
				},
				callback: function(response) {	
					if(response.message[0]["parent_administrative_unit"] == cur_frm.doc.region){
						// this is correct
					}else{
						// clear the field of district
						cur_frm.set_value("district","")
					}
				}
			})

		}else{
			// set query
			cur_frm.set_query("district", function() {
				return {
					"filters": {
						"type": "District",
						"parent_administrative_unit":cur_frm.doc.region
					}
				}
			});
		}
	}
})

// function called when  the region field clicked under Navigation Section
frappe.ui.form.on("International NGO Form", "district", function(frm){
	// check if a district is already given
	if(cur_frm.doc.region){
		// check if district is within region
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Administrative Unit",
					filters: {
						name:frm.doc.district
					},
			fields:["*"]
			},
			callback: function(response) {	
				if(response.message[0]["parent_administrative_unit"] == cur_frm.doc.region){
					// this is correct
				}else{
					var district_name_holder = cur_frm.doc.district
					// clear the field of district
					cur_frm.set_value("district","")
					frappe.throw("'"+district_name_holder+"' is not a '"+cur_frm.doc.region +"' District")
					
				}
			}
		})

	}else{
		// pass
	}
})

/*****************************************************************************************
Somaliland Branches Section */
 
frappe.ui.form.on("Somaliland Branch Offices", "region", function(frm,cdt,cdn){

	var child = locals[cdt][cdn];

	if(child.region && child.district){
		// check if district is within region
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Administrative Unit",
					filters: {
						name:child.district
					},
			fields:["*"]
			},
			callback: function(response) {	
				if(response.message[0]["parent_administrative_unit"] == child.region){
					// this is correct
				}else{
					// clear the field of district
					cur_frm.set_value("district","")
					child.district = ""
					cur_frm.refresh_field("somaliland_branch_offices_table")
					frappe.throw("The Select District is Not in The Selected Region")
				}
			}
		})
	}
})

frappe.ui.form.on("Somaliland Branch Offices", "district", function(frm,cdt,cdn){

	var child = locals[cdt][cdn];

	if(child.region && child.district){
		// check if district is within region
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Administrative Unit",
					filters: {
						name:child.district
					},
			fields:["*"]
			},
			callback: function(response) {	
				if(response.message[0]["parent_administrative_unit"] == child.region){
					// this is correct
				}else{
					// clear the field of district
					cur_frm.set_value("district","")
					child.district = ""
					cur_frm.refresh_field("somaliland_branch_offices_table")
					frappe.throw("The selected district is not in the selected region")
				}
			}
		})
	}
})

/*****************************************************************************************
ORGANIZATION DIRECTOR SECTION */
// function called when  the '* Country Code (Select One)' is filled in the ORGANIZATION DIRECTOR SECTION 
frappe.ui.form.on("International NGO Form", "director_country_code", function(frm){
	if(frm.doc.director_country_code && frm.doc.tel_no_1){
		// combine the code and telephone into complete telephone number
		var full_telephone_number = frm.doc.director_country_code+String(frm.doc.tel_no_1)
		cur_frm.set_value("complete_director_tel_no",full_telephone_number)
	}
})


// function called when  the '* Tel  No.' is filled in the ORGANIZATION DIRECTOR SECTION 
frappe.ui.form.on("International NGO Form", "tel_no_1", function(frm){
	if(frm.doc.director_country_code && frm.doc.tel_no_1){
		// combine the code and telephone into complete telephone number
		var full_telephone_number = frm.doc.director_country_code+String(frm.doc.tel_no_1)
		cur_frm.set_value("complete_director_tel_no",full_telephone_number)
	}
})

// validate the email
frappe.ui.form.on("International NGO Form", "director_email", function(frm){

	if(cur_frm.doc.director_email){
		if(ValidateEmail(cur_frm.doc.director_email)){
			// allow 
		}else{
			cur_frm.set_value("director_email","")
			frappe.throw("Please Enter A Valid Email Address")
		}
	}

})

/*****************************************************************************************
SOMALILAND MOST SENIOR EXECUTIVE */

// function called when  the if_senior_executive_same_director is filled in the SOMALILAND MOST SENIOR EXECUTIVE 
frappe.ui.form.on("International NGO Form", "if_senior_executive_same_director", function(frm){
	// refresh the form so as to hide/unhide fields based on selected option
	cur_frm.refresh()
})

// function called when  the '* Country Code' is filled in the SOMALILAND MOST SENIOR EXECUTIVE 
frappe.ui.form.on("International NGO Form", "country_code_senior_most_executive", function(frm){
	if(frm.doc.country_code_senior_most_executive && frm.doc.director_tel_no){
		// combine the code and telephone into complete telephone number
		var full_telephone_number_director = frm.doc.country_code_senior_most_executive+String(frm.doc.director_tel_no)
		cur_frm.set_value("complete_senior_tel_no",full_telephone_number_director)
	}
})

// function called when  the '* Tel No' is filled in the SOMALILAND MOST SENIOR EXECUTIVE 
frappe.ui.form.on("International NGO Form", "director_tel_no", function(frm){
	if(frm.doc.country_code_senior_most_executive && frm.doc.director_tel_no){
		// combine the code and telephone into complete telephone number
		var full_telephone_number_director = frm.doc.country_code_senior_most_executive+String(frm.doc.director_tel_no)
		cur_frm.set_value("complete_senior_tel_no",full_telephone_number_director)
	}
})

// validate the email
frappe.ui.form.on("International NGO Form", "email_2", function(frm){

	if(cur_frm.doc.email_2){
		if(ValidateEmail(cur_frm.doc.email_2)){
			// allow 
		}else{
			cur_frm.set_value("email_2","")
			frappe.throw("Please Enter A Valid Email Address")
		}
	}

})

/*****************************************************************************************
COUNTRY/DEPARTMENT DIRECTOR ADDRESS SECTION */

// function called when  the '* Office Address (Select One)' is filled in the COUNTRY/DEPARTMENT DIRECTOR ADDRESS SECTION 
frappe.ui.form.on("International NGO Form", "select_one_director_address", function(frm){
	// refresh the form so as to hide/unhide fields based on selected option
	if(cur_frm.doc.select_one_director_address){
		if(cur_frm.doc.select_one_director_address == "Same address as origin country main office"){
			// autofill from origin country address

			cur_frm.set_value("directors_address",cur_frm.doc.address)
			cur_frm.set_value("director_city",cur_frm.doc.city)
			cur_frm.set_value("director_code",cur_frm.doc.postcode)
			cur_frm.set_value("country",cur_frm.doc.origin_country)
			cur_frm.set_value("region_director_address","")
			cur_frm.set_value("district_director_address","")
			cur_frm.set_value("area_neighborhood_senior_most","")

		}else if(cur_frm.doc.select_one_director_address == "Same address as Somaliland main office"){
			// autofill from origin country address
			cur_frm.set_value("area_neighborhood_senior_most",cur_frm.doc.area_neighborhood)
			cur_frm.set_value("director_city",cur_frm.doc.town)
			cur_frm.set_value("region_director_address",cur_frm.doc.region)
			cur_frm.set_value("district_director_address",cur_frm.doc.district)
			cur_frm.set_value("directors_address","")
			cur_frm.set_value("country","")
			cur_frm.set_value("director_code","")

		
		}else{
			// hide_unhide_fields(cur_frm, ["region_director_address","district_director_address"], false)
			cur_frm.set_value("directors_address","")
			cur_frm.set_value("director_city","")
			cur_frm.set_value("director_code","")
			cur_frm.set_value("country","")
			cur_frm.set_value("region_director_address","")
			cur_frm.set_value("district_director_address","")
			cur_frm.set_value("area_neighborhood_senior_most","")
		}
	}

	// refresh to hide unhide fields
	cur_frm.refresh()
})

/*****************************************************************************************
PILLAR ALIGNMENT SECTION */

// function called when  the confirm_pillars_check field clicked under PILLAR ALIGNMENT SECTION
frappe.ui.form.on("International NGO Form", "confirm_pillars_check", function(frm){
	// check if the user has selected any pillar
	var pillar_selected = false

	if(frm.doc.confirm_pillars_check){
		$.each(frm.doc.pillar_alignment_table, function(i,v){
			if(v["yes"]){
				pillar_selected = true
			}else{
				// do nothing
			}
		});
	
		if(pillar_selected){
			// allow user to continues
		}else{
			// check if there is any pillar in the table
			if(frm.doc.pillar_alignment_table.length == 0){
				// do nothing
			}else{
				cur_frm.set_value("confirm_pillars_check","")
				frappe.throw("You Have Not Ticked Any Pillar In The Table"+"<hr>"+"Please Tick 'Yes' Against Pillars That The Organization Contributes to In Order to Continue")
			}	
		}
	}
})




/*****************************************************************************************
SECTION ALIGNMENT SECTION */

// function called when  the confirm_sectors_check field clicked under SECTION ALIGNMENT SECTION
frappe.ui.form.on("International NGO Form", "confirm_sectors_check", function(frm){
	// check if the user has selected any pillar
	var selectors_selected = false

	if(frm.doc.confirm_sectors_check){
		$.each(frm.doc.section_alignment_table, function(i,v){
			if(v["yes"]){
				selectors_selected = true
			}else{
				// do nothing
			}
		});
	
		if(selectors_selected){
			// allow user to continues
		}else{
			// check if there is any sector in the table
			if(frm.doc.section_alignment_table.length == 0){
				// do nothing
			}else{
				cur_frm.set_value("confirm_sectors_check","")
				frappe.throw("You Have Not Ticked Any Sector In The Table"+"<hr>"+"Please Tick 'Yes' Against Sectors That The Organization Contributes to In Order to Continue")
			}
		}
	}
})

/*****************************************************************************************
THEMES ALIGNMENT SECTION */

// function called when  the confirm_themes_check field clicked under THEMES ALIGNMENT SECTION
frappe.ui.form.on("International NGO Form", "confirm_themes_check", function(frm){
	// check if the user has selected any pillar
	var themes_selected = false

	if(frm.doc.confirm_themes_check){
		$.each(frm.doc.themes_alignment_table, function(i,v){
			if(v["yes"]){
				themes_selected = true
			}else{
				// do nothing
			}
		});
	
		if(themes_selected){
			// allow user to continues
		}else{
			// check if there is any themes in the table
			if(frm.doc.themes_alignment_table.length == 0){
				// do nothing
			}else{
				cur_frm.set_value("confirm_themes_check","")
				frappe.throw("You Have Not Ticked Any Themes In The Table"+"<hr>"+"Please Tick 'Yes' Against Themes That The Organization Contributes to In Order to Continue")
			}
		}
	}
})

/*****************************************************************************************
ACCOUNTING AND REPORTING PERIOD */

// function called when  the Month Field is Filled under ACCOUNTING AND REPORTING PERIOD
frappe.ui.form.on("International NGO Form", "closing_month", function(frm){
	// check if the user has given a day and month
	if(frm.doc.closing_month && frm.doc.closing_day){
		
		// month is correct
		var months_and_days = {
			"January":31,"February":29,"March":31,"April":30,"May":31,"June":30,"July":31,
			"August":31,"September":30,"October":31,"November":30,"December":31
		}

		// check if given day is correct
		if(frm.doc.closing_day <= months_and_days[String(frm.doc.closing_month)]){
			// allow
		}else{
			cur_frm.set_value("closing_day","")
			frappe.throw("The Month '"+frm.doc.closing_month+"' Has "+months_and_days[String(frm.doc.closing_month)]+" Days")
		}

	}
})


// function called when  the Month Field is Filled under ACCOUNTING AND REPORTING PERIOD
frappe.ui.form.on("International NGO Form", "closing_day", function(frm){
	// check if the user has given a day and month
	if(frm.doc.closing_month && frm.doc.closing_day){
		
		// month is correct
		var months_and_days = {
			"January":31,"February":29,"March":31,"April":30,"May":31,"June":30,"July":31,
			"August":31,"September":30,"October":31,"November":30,"December":31
		}

		// check if given day is correct
		if(frm.doc.closing_day <= months_and_days[String(frm.doc.closing_month)]){
			// allow
		}else{
			cur_frm.set_value("closing_day","")
			frappe.throw("The Month '"+frm.doc.closing_month+"' Has "+months_and_days[String(frm.doc.closing_month)]+" Days")
		}

	}
})

/******************************************************************************************/
// verification section
frappe.ui.form.on("International NGO Form", "view_all_information", function(frm){
	cur_frm.set_value("viewing_information",1)
	cur_frm.save()
})

frappe.ui.form.on("International NGO Form", "edit_information", function(frm){
	// check user is allowed to edit
	if(cur_frm.doc.verified){
		frappe.throw("You cannot edit a form that is already verified \
		,contact the admin for assistance")
	}else{
		// make the forms readble for user
		cur_frm.set_value("viewing_information",0)
		var current_list_of_sections = list_of_sections['International NGO']
		cur_frm.set_value("current_section",current_list_of_sections[current_list_of_sections.length - 1])
		cur_frm.save()
	}
})

frappe.ui.form.on("International NGO Form", "mark_information_as_checked", function(frm){
	check_privillages(frm,"checked")
})

frappe.ui.form.on("International NGO Form", "user_verify", function(frm){
	// check if the user has marked the information as checked
	if(cur_frm.doc.information_checked){
		var required_fields_status = check_required_fields('verification')
		// first check  required fields are given
		if(required_fields_status){
			// confirm if the user want to verify the  information
			frappe.confirm(
				"Please ensure that all the information you have given is correct before verifying <hr> \
				if you are sure you are ready to verify click <b>Yes</b> otherwise click <b>No</b>",
				function(){
					check_privillages(frm,"verify")	
					cur_frm.set_value('viewing_information',1)
				},
				function(){
					// do nothing since the user is not sure
				}
			)
		}else{
			cur_frm.set_value('status','Pending Verification')
			cur_frm.set_value('verified',0)	
		}
	}else{
		frappe.throw("Please mark the information as checked before verification")
	}		
})


frappe.ui.form.on("International NGO Form", "mark_information_as_checked_by_staff", function(frm){
	check_privillages(frm,"checked_by_staff")
})

frappe.ui.form.on("International NGO Form", "user_validate", function(frm){
	check_privillages(frm,"validate")
})


frappe.ui.form.on("International NGO Form", "approve", function(frm){
	check_privillages(frm,"approve")
})

/*****************************************************************************************
Navigation Section */

// function called when  the Previous Button field clicked under Navigation Section
frappe.ui.form.on("International NGO Form", "previous_section_button", function(frm){
	// go to the next section	
	unhide_previous_section(frm)
})

// function called when  the Save and Continue field clicked under Navigation Section
frappe.ui.form.on("International NGO Form", "save_and_continue", function(frm){ 
	// ensure to requirements for the section are complete
	var section_function = validation_function_per_section[frm.doc.current_section] 
	if(section_function()["status"]){
		unhide_next_section(cur_frm,section_function()["steps"])
	}
})

// function called when  the 'Go to screen' button is clicked
frappe.ui.form.on("International NGO Form", "go_to_page", function(frm){
	// call the go to page function
	goToPage()
});
