// Copyright (c) 2019, upande and contributors
// For license information, please see license.txt

frappe.ui.form.on('Test Doctype', {
	refresh: function(frm) {

		if (frm.doc.attach_doc) {
            frm.set_df_property("attach_html", "options", `
                <iframe src="https://docs.google.com/gview?url=${ window.location.origin }/${ frm.doc.attach_doc }&embedded=true" frameborder="0">
</iframe>
            `);
		}
	}
});
