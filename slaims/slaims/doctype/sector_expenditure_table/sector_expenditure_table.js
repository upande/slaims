frappe.ui.form.on("Sales Invoice Item", {
	items_add: function(doc,cdt,cdn){
	    cur_frm.cscript.total(doc.doc,cdt,cdn);
	},
    items_remove: function(doc,cdt,cdn){
	    cur_frm.cscript.total(doc.doc,cdt,cdn);
	}
});

cur_frm.cscript.total = function(doc,cdt,cdn) {
	var total_qty = 0;
    $.each(doc.items || [], function(i, d) {
        total_qty += flt(d.qty);
    });
    cur_frm.set_value(“net_weight”, total_qty);
};
edit:
you can also set event on qty:

qty: function(doc,cdt,cdn){
    cur_frm.cscript.total(doc.doc,cdt,cdn);
}