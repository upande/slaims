# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import datetime


class InternationalNGORenewalForm(Document):
	def validate(self):
		# run the validation function 
		validation_function(self)

		# place correct renewal year
		add_correct_renewal_year(self)

		# function that pulls data from the organiation and autofills
		load_renewal_data_from_organization(self)
	
		# add pillars to the pillars table
		add_pillars_to_table(self)

		# add sectors to the sectors table
		add_sectors_to_table(self)

		# add themes to themes table
		add_themes_to_table(self)

		# add contact persons
		add_contact_spaces(self)

	def on_update(self):
		# update the data to organization form
		map_data_to_organization(self)

def add_correct_renewal_year(self):
	'''
	Function that gets the current organization 
	year and hence the next year i.e renewal year
	using years ranks
	'''
	# check year exist
	if self.year_of_registration: #in this case the year of renewal
		pass
	else:
		try:
			# get the current registration year of the organization
			organization_doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
			# its current year rank
			current_year_doc = frappe.get_doc("Year",str(organization_doc.current_registration_year))
			renewal_year_rank = current_year_doc.rank + 1
			# get a list of years with a specific rank
			list_of_years_matching_rank = frappe.get_list("Year",
				fields=["name"],
				filters = {
					"rank":renewal_year_rank
			})
			# place the renewal year in the form
			self.year_of_registration = list_of_years_matching_rank[0].name
		except:
			# if an error occurs trying placing the global year as the renewal year
			try:
				global_setting_doc = frappe.get_single("Global Settings")
				self.year_of_registration = global_setting_doc.current_financial_year
			except:
				frappe.throw("A global financial year not defined please contact Admin for assistance")

def add_pillars_to_table(self):
	'''
	Function that collects all the pillars and places them 
	in the Pillars table
	'''
	if self.current_section == "pillar_alignment_section":
		# hold all marked pillars
		pillars_holder = []
		for item in self.pillar_alignment_table:
			if item.yes:
				pillars_holder.append(item.pillar)

		# first clear out the table
		self.pillar_alignment_table = []
		# get all the pillars
		list_of_pillars = frappe.get_list("NDP",
			fields=["name"],
			filters = {
				"type":"Pillar"
		})

		for pillar in list_of_pillars:
			# check if that pillar is marked
			if pillar["name"] in pillars_holder:
				self.append("pillar_alignment_table", {
					"pillar":pillar["name"],
					"yes":1
				})
			else:
				self.append("pillar_alignment_table", {
					"pillar":pillar["name"],
					"yes":0
				})


def add_sectors_to_table(self):
	'''
	Function that collects Sectors Related to Pillars Selected
	in the pillars section and places them in the Sectors table
	'''
	if self.current_section == "section_alignment_section":
		# hold all marked sectors
		sectors_holder = []
		for item in self.section_alignment_table:
			if item.yes:
				sectors_holder.append(item.sector)
		# first clear the sector table
		self.section_alignment_table = []
		# get the relavant sectors
		list_of_relevant_sectors = []

		# get sectors for each selected pillar
		for pillar in self.pillar_alignment_table:
			# get all the sectors for each ticked pillar
			if pillar.yes:
				sectors_for_pillar = frappe.get_list("NDP",
					fields=["name","parent_ndp"],
					filters = {
						"type":"Sector",
						"parent_ndp":pillar.pillar
				})

				# append the sectors to the list of pillars
				list_of_relevant_sectors += sectors_for_pillar

		# append the sectors to the sectors table
		for sector in list_of_relevant_sectors:
			# check if that sector is marked
			if sector["name"] in sectors_holder:
				self.append("section_alignment_table", {
					"pillar":sector["parent_ndp"],
					"sector":sector["name"],
					"yes":1
				})
			else:
				self.append("section_alignment_table", {
					"pillar":sector["parent_ndp"],
					"sector":sector["name"]
				})

def add_themes_to_table(self):
	'''
	Function that collects Themes Related to Sectors Selected
	in the Sectors section and places them in the Themes table
	'''
	if self.current_section == "themes_alignment_section":
		themes_holder = []
		for item in self.themes_alignment_table:
			if item.yes:
				themes_holder.append(item.theme)
		# first clear the pillars table
		self.themes_alignment_table = []

		# holds a list of relevant themes
		list_of_relevant_themes = []

		# get sectors for each selected pillar
		for sector in self.section_alignment_table:
			# get all the themes for each ticked sector
			if sector.yes:
				themes_for_sector = frappe.get_list("NDP",
					fields=["name","parent_ndp"],
					filters = {
						"type":"Theme",
						"parent_ndp":sector.sector
				})

				# append the sectors to the list of sectors
				list_of_relevant_themes += themes_for_sector

		# append the themes to the themes table
		for theme in list_of_relevant_themes:
			# check if that sector is marked
			if theme["name"] in themes_holder:
				self.append("themes_alignment_table", {
					"sector":theme["parent_ndp"],
					"theme":theme['name'],
					"yes":1
				})
			else:
				self.append("themes_alignment_table", {
					"sector":theme["parent_ndp"],
					"theme":theme["name"]
				})

def add_contact_spaces(self):
	'''
	Add the Correct Number of Spaces for Connection
	'''
	if len(self.alternative_contact_person) < 3:
		rows_to_add = 3 - len(self.alternative_contact_person)
		for i in range(rows_to_add):
			self.append("alternative_contact_person", {
				# do not add any fields
			})

def check_if_organization_exists(self):
	'''
	Checks if an organization exists else create one
	'''

	# create an organization
	doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
	doc.name = self.full_name_of_the_organization
	doc.status = self.status
	doc.organization_type = "International NGO"
	doc.full_name_of_the_organization = self.full_name_of_the_organization
	doc.acronym = self.acronym
	doc.country_of_origin = self.country_of_origin

	if doc:
		doc.save(ignore_permissions = True)
	else:
		doc.insert(ignore_permissions = True)


def map_data_to_organization(self):
	'''
	Function that Updates the details of the organization
	to the organization form once the form is approved
	'''
	# check if the organization has been verified
	if self.status == "Verified" or  self.status == "Admin Approved":
		check_if_organization_exists(self)

	if self.status == "Validated":
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
		# save the status
		doc.status = "Validated"
		doc.save()

	# now update the other remaining fields
	# if approved update details to linked organization
	if(self.status == "Approved" or self.status == "Imported"):
		# get related organization
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
		
		# ORGANIZATION TYPE SECTION
		doc.status = "Approved"

		# GENERAL ORGANIZATION DETAILS SECTION
		doc.full_name_of_the_organization = self.full_name_of_the_organization
		doc.acronym = self.acronym
		doc.country_of_origin = self.country_of_origin
		doc.organization_year_registered = self.year_of_first_registration
		doc.organization_registration_number = self.organization_registration_number

		# ORGANIZATION ADDRESS SECTION
		doc.address = self.address
		doc.city = self.city
		doc.origin_country = self.origin_country
		doc.postcode = self.postcode

		# ADDRESS OF MAIN OFFICE SECTION
		doc.area_neighborhood = self.area_neighborhood
		doc.town = self.town
		doc.region = self.region
		doc.district = self.district

		# SOMALILAND BRANCH OFFICES
		doc.number_of_branches = self.number_of_branches
		
		# update Somaliland table
		doc.somaliland_branch_offices_table = []
		for branch in self.somaliland_branch_offices_table:
			doc.append("somaliland_branch_offices_table", {
				"region": branch.region,
				"district":branch.district,
				"town":branch.town
			})

		# Organization Director Section
		doc.full_name = self.full_name
		doc.director_country_code = self.director_country_code
		doc.tel_no_1 = self.tel_no_1
		doc.complete_director_tel_no = self.complete_director_tel_no
		doc.director_email = self.director_email

		doc.select_one_director_address = self.select_one_director_address
		doc.directors_address = self.directors_address
		doc.director_city = self.director_city
		doc.director_code = self.director_code
		doc.country = self.country
		doc.region_director_address = self.region_director_address
		doc.region_director_address = self.district_director_address

		# Somaliland Most Senior Executive Section
		doc.if_senior_executive_same_director =  self.if_senior_executive_same_director
		doc.full_name_2 =  self.full_name_2
		doc.country_code_senior_most_executive =  self.country_code_senior_most_executive
		doc.director_tel_no =  self.director_tel_no
		doc.complete_senior_tel_no =  self.complete_senior_tel_no
		doc.email_2 =  self.email_2

		# Three Alternative Contact Persons Section
		doc.alternative_contact_person = []
		for contact in self.alternative_contact_person:
			doc.append("alternative_contact_person", {
				"full_name": contact.full_name,
				"title":contact.title,
				"tel_no":contact.tel_no,
				"email":contact.email
			})

		# Number of Projects Section
		doc.that_are_ongoing = self.that_are_ongoing
		doc.that_are_completed = self.that_are_completed
		doc.confirmed_to_start_during_current_year = self.confirmed_to_start_during_current_year

		# Pillar Alignment Section
		doc.pillar_alignment_table = []
		for pillar in self.pillar_alignment_table:
			if pillar.yes:
				doc.append("pillar_alignment_table", {
					"pillar": pillar.pillar,
					"yes": pillar.yes
				})

		# Sectors Alignment Section
		doc.section_alignment_table = []
		for sector in self.section_alignment_table:
			if sector.yes:
				doc.append("section_alignment_table", {
					"pillar": sector.pillar,
					"sector":sector.sector,
					"yes": sector.yes
				})

		# Themes Alignment Table
		doc.themes_alignment_table = []
		for theme in self.themes_alignment_table:
			if theme.yes:
				doc.append("themes_alignment_table", {
					"sector":theme.sector,
					"theme": theme.theme,
					"yes": theme.yes
				})

		# Geographical area(s) of operation
		doc.marodijeh_check = self.marodijeh_check
		doc.hargeisa = self.hargeisa
		doc.gabiley = self.gabiley
		doc.baligubadle = self.baligubadle
		doc.salahlay = self.salahlay

		doc.sanag_check = self.sanag_check
		doc.erigavo = self.erigavo
		doc.badhan = self.badhan
		doc.las_qoray = self.las_qoray
		doc.ela_fweyn = self.ela_fweyn
		doc.dhahar = self.dhahar
		doc.gar_adag = self.gar_adag
		
		doc.sool_check = self.sool_check
		doc.las_anod = self.las_anod
		doc.hudun = self.hudun
		doc.taleh = self.taleh
		doc.aynabo = self.aynabo

		doc.togdheer_check = self.togdheer_check
		doc.burao = self.burao
		doc.odwayne = self.odwayne
		doc.buhodle = self.buhodle

		doc.awdal_check = self.awdal_check
		doc.borama = self.borama
		doc.zeila = self.zeila
		doc.baki = self.baki
		doc.lughaya = self.lughaya

		doc.sahil_check = self.sahil_check
		doc.berbera = self.berbera
		doc.sheekh = self.sheekh

		doc.hargeisa_region = self.hargeisa_region
		
		# Accounting and reporting period
		doc.closing_month = self.closing_month
		doc.closing_day = self.closing_day

		# Total Number of Staff
		doc.managerial_or_program_staff = self.managerial_or_program_staff
		doc.admin_or_support_staff = self.admin_or_support_staff
		doc.managerial_or_program_staff_2 = self.managerial_or_program_staff_2
		doc.admin_or_support_staff_2 = self.admin_or_support_staff_2

		'''
		# Attachments Section
		doc.attachments_table = []
		all_attachments_table = 'attachments_table'

		# attach renewal request
		for renewal_request in self.attach_renewal_request:
			doc.append(all_attachments_table, {
				'attachment_name': renewal_request.attachment_name,
				'attachment': renewal_request.attachment,
				'field_name':'attach_renewal_request'
			})

		# attach cvs of international staff 
		for int_cv in self.attach_international_cvs:
			doc.append(all_attachments_table, {
				'attachment_name': int_cv.attachment_name,
				'attachment': int_cv.attachment,
				'field_name':'attach_international_cvs'
			})

		# attach financial statements
		for statement in self.attach_financial_statement:
			doc.append(all_attachments_table, {
				'attachment_name': statement.attachment_name,
				'attachment': statement.attachment,
				'field_name':'attach_financial_statement'
			})

		# attach Recommendation letter from the line ministry
		for recommendation in self.attach_recommendation:
			doc.append(all_attachments_table, {
				'attachment_name': recommendation.attachment_name,
				'attachment': recommendation.attachment,
				'field_name':'attach_recommendation'
			})

		# attach agreements
		for agreement in self.attach_agreements:
			doc.append(all_attachments_table, {
				'attachment_name': agreement.attachment_name,
				'attachment': agreement.attachment,
				'field_name':'attach_agreements'
			})

		# attach international employees taxes
		for int_employee_tax in self.attach_int_employees_taxes:
			doc.append(all_attachments_table, {
				'attachment_name': int_employee_tax.attachment_name,
				'attachment': int_employee_tax.attachment,
				'field_name':'attach_int_employees_taxes'
			})


		# attach sector information
		for sector_information in self.attach_sector_information:
			doc.append(all_attachments_table, {
				'attachment_name': sector_information.attachment_name,
				'attachment': sector_information.attachment,
				'field_name':'attach_sector_information'
			})

		# add other attachments
		for other in self.attach_other:
			doc.append(all_attachments_table, {
				'attachment_name': other.attachment_other,
				'attachment': other.attachment,
				'field_name':'attach_other'
			})

		# attach renewal receipt
		for renewal_receipt in self.attach_renewal_receipt:
			doc.append(all_attachments_table, {
				'attachment_name': renewal_receipt.attachment_name,
				'attachment': renewal_receipt.attachment,
				'field_name':'attach_renewal_receipt'
			})
		'''
		
		# add the form as a child table to organization
		doc.append("attached_organization_forms_table", {
			"name_of_form":self.name,
			"year_of_form":self.year_of_registration
		})

		# save the added changes
		doc.save()

def validation_function(self):
	'''
	Function that checks that all the required fields 
	are given
	'''
	# verification fields
	list_of_verification_fields = [
		{"field":self.full_name_of_the_organization,"name":"Name of Organization"},
		{"field":self.acronym,"name":"Acronym"},
		{"field":self.status,"name":"Status"},

		# add more validation required fields below
		
	]

	# validation fields
	list_of_validation_fields = [
		{"field":self.full_name_of_the_organization,"name":"Name of Organization"},
		{"field":self.acronym,"name":"Acronym"},
		{"field":self.status,"name":"Status"}

		# add more validation required fields below
	]

	# check approval fields
	list_of_approval_fields = [
		# add fields required for approval below
	]


	if self.status == "Verified":
		# loop through list of saving fields
		for field in list_of_verification_fields:
			if field["field"]:
				pass
			else:
				# value for the field is not given throw an errror
				frappe.throw("The Field '{}' is Required Before Verification".format(field["name"]))

	if self.status == "Validated":
		# loop through list of saving fields
		for field in list_of_validation_fields:
			if field["field"]:
				pass
			else:
				# value for the field is not given throw an errror
				frappe.throw("The Field '{}' is Required Before Verification".format(field["name"]))

	if self.status == "Approved":
		# loop through list of saving fields
		for field in list_of_approval_fields:
			if field["field"]:
				pass
			else:
				# value for the field is not given throw an errror
				frappe.throw("The Field '{}' is Required Before Verification".format(field["name"]))

	# check if previous financial years was closed
	linked_organization_doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
	if linked_organization_doc.financial_year_not_closed:
		frappe.throw("The previous Financial year for your organization was not closed <hr> \
			Please fill all the required forms for the year {} and contant Admin for assisistance\
			".format(linked_organization_doc.current_registration_year))
	
	
def load_renewal_data_from_organization(self):
	'''
	function that pulls data from the organiation and
	autofills them in this form
	'''
	if self.data_pulled_from_main:
		pass
	else:
		# get related organization
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)

		# GENERAL ORGANIZATION DETAILS SECTION
		self.full_name_of_the_organization = doc.full_name_of_the_organization 
		self.acronym = doc.acronym
		self.country_of_origin = doc.country_of_origin
		self.year_of_first_registration = doc.year_of_first_registration 
		self.organization_registration_number = doc.organization_registration_number

		# ORGANIZATION ADDRESS SECTION
		self.address = doc.address
		self.city = doc.city
		self.origin_country = doc.origin_country
		self.postcode = doc.postcode

		# ADDRESS OF MAIN OFFICE SECTION
		self.area_neighborhood = doc.area_neighborhood
		self.town = doc.town
		self.region = doc.region
		self.district = doc.district
		
		# SOMALILAND BRANCH OFFICES
		self.number_of_branches = doc.number_of_branches
		
		# update Somaliland table
		self.somaliland_branch_offices_table = []

		# pull data from child table of office branches
		
		list_of_branches = frappe.get_list("Somaliland Branch Offices",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"somaliland_branch_offices_table",
						"parenttype":"Organization",
				})

		for branch in list_of_branches:	
			self.append("somaliland_branch_offices_table", {
				"region": branch.region,
				"district":branch.district,
				"town":branch.town
			})

		# Organization Director Section
		self.full_name = doc.full_name
		self.director_country_code = doc.director_country_code
		self.tel_no_1 = doc.tel_no_1
		self.complete_director_tel_no = doc.complete_director_tel_no
		self.director_email = doc.director_email

		self.select_one_director_address = doc.select_one_director_address
		self.directors_address = doc.directors_address
		self.director_city = doc.director_city
		self.director_code = doc.director_code
		self.country = doc.country
		self.region_director_address = doc.region_director_address
		self.region_director_address = doc.district_director_address

		# Somaliland Most Senior Executive Section
		self.if_senior_executive_same_director =  doc.if_senior_executive_same_director
		self.full_name_2 =  doc.full_name_2
		self.country_code_senior_most_executive =  doc.country_code_senior_most_executive
		self.director_tel_no =  doc.director_tel_no
		self.complete_senior_tel_no =  doc.complete_senior_tel_no
		self.email_2 =  doc.email_2

		# clear the fields first
		self.alternative_contact_person = []

		# get the list from the linked table
		list_of_contacts = frappe.get_list("Alternative Contact Persons",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"alternative_contact_person",
						"parenttype":"Organization",
				})

		for contact in list_of_contacts:
					
			self.append("alternative_contact_person", {
				"full_name": contact.full_name,
				"title":contact.title,
				"tel_no":contact.tel_no,
				"email":contact.email
			})

		# Number of Projects Section
		self.that_are_ongoing = doc.that_are_ongoing
		self.that_are_completed = doc.that_are_completed
		self.confirmed_to_start_during_current_year = doc.confirmed_to_start_during_current_year

		# Pillar Alignment Section
		self.pillar_alignment_table = []

		list_of_pillars = frappe.get_list("Pillar Alignment Table",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"pillar_alignment_table",
						"parenttype":"Organization",
				})

		for pillar in list_of_pillars:
			self.append("pillar_alignment_table", {
				"pillar": pillar.pillar,
				"yes": pillar.yes
			})       
		
		# Sectors Alignment Section
		self.section_alignment_table = []

		list_of_sectors = frappe.get_list("Sector Alignment Table",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"section_alignment_table",
						"parenttype":"Organization",
				})

		for sector in list_of_sectors:
			self.append("section_alignment_table", {
				"pillar": sector.pillar,
				"sector":sector.sector,
				"yes": sector.yes
			})

		# Themes Alignment Table
		self.themes_alignment_table = []
		list_of_themes = frappe.get_list("Themes Alignment Table",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"themes_alignment_table",
						"parenttype":"Organization",
				})

		for theme in list_of_themes:
			self.append("themes_alignment_table", {
				"sector":theme.sector,
				"theme": theme.theme,
				"yes": theme.yes
			})

		# Geographical area(s) of operation
		self.marodijeh_check = doc.marodijeh_check
		self.hargeisa = doc.hargeisa
		self.gabiley = doc.gabiley
		self.baligubadle = doc.baligubadle
		self.salahlay = doc.salahlay

		self.sanag_check = doc.sanag_check
		self.erigavo = doc.erigavo
		self.badhan = doc.badhan
		self.las_qoray = doc.las_qoray
		self.ela_fweyn = doc.ela_fweyn
		self.dhahar = doc.dhahar
		self.gar_adag = doc.gar_adag
		
		self.sool_check = doc.sool_check
		self.las_anod = doc.las_anod
		self.hudun = doc.hudun
		self.taleh = doc.taleh
		self.aynabo = doc.aynabo

		self.togdheer_check = doc.togdheer_check
		self.burao = doc.burao
		self.odwayne = doc.odwayne
		self.buhodle = doc.buhodle

		self.awdal_check = doc.awdal_check
		self.borama = doc.borama
		self.zeila = doc.zeila
		self.baki = doc.baki
		self.lughaya = doc.lughaya

		self.sahil_check = doc.sahil_check
		self.berbera = doc.berbera
		self.sheekh = doc.sheekh

		self.hargeisa_region = doc.hargeisa_region
		
		# Accounting and reporting period
		if doc.closing_month and doc.closing_day:
			self.closing_month = doc.closing_month
			self.closing_day = doc.closing_day

		# Total Number of Staff
		self.managerial_or_program_staff = doc.managerial_or_program_staff
		self.admin_or_support_staff = doc.admin_or_support_staff
		self.managerial_or_program_staff_2 = doc.managerial_or_program_staff_2
		self.admin_or_support_staff_2 = doc.admin_or_support_staff_2

		# mark the form that data has been imported
		self.data_pulled_from_main = 1
