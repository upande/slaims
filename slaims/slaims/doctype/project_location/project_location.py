# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class ProjectLocation(Document):
	'''
	This is The Project Location Controller Form
	'''
	def validate(self):
		pass

	def on_update(self):
		pass