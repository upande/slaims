# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import datetime

# list of sections
list_of_sections = [
	"general_organizaiton_details","organization_suggestion_section","address_of_main_office_section_section",
	"director_details_section","three_alternative_contact_persons_section",
	"department_responsible_section","sector_cordination_section",
	"verification","validation","approval"
]

class SomalilandGovernmentInstitutionForm(Document):
	def validate(self):
		# checks for ordinary form filling
		if self.full_name_of_the_organization  and self.acronym:
			# check if the user has an organization
			check_if_user_has_organization(self)
		else:
			frappe.throw("Please give the name and acronym of organization in order to continue")			
		
		# call the execute function
		execute_actions(self)

		# set the next page
		set_next_page(self)

		# check if organization has been created or selected
		if self.link_to_org_name:
			pass
		else:
			# check if organizaion has been created
			list_of_organizations = frappe.get_list("Organization",
				fields=["name"],
				filters = {
					"name":self.full_name_of_the_organization
			})

			if len(list_of_organizations) > 0:
				# get the first organization
				self.link_to_org_name = list_of_organizations[0].name

		# add contact persons
		add_contact_spaces(self)

		# add sectors
		add_sectors(self)

		# upgrade the status for imported documents
		if self.imported:
			upgrade_status_for_imported_documents(self)

	def on_update(self):
		# update information to main doctypes
		map_data_to_organization(self)


def add_contact_spaces(self):
	'''
	Add the Correct Number of Spaces for Connection
	'''
	if len(self.alternative_contact_person) < 2:
		rows_to_add = 2 - len(self.alternative_contact_person)
		for i in range(rows_to_add):
			self.append("alternative_contact_person", {
				# do not add any fields
			})


def add_sectors(self):
	'''
	Add the Correct Number of Sectors
	'''
	if self.current_section == "department_responsible_section":
		# get all the sectors
		list_of_sectors = frappe.get_list("NDP",
			fields=["name"],
			filters = {
				"type":"Sector"
		})

		if len(self.sector__alignment_table) == len(list_of_sectors):
			pass
		else:
			# add the sectors to the table
			for sector in list_of_sectors:
				self.append("sector__alignment_table", {
					"sector":sector["name"]
				})


def check_if_organization_exists(self):
	'''
	Checks if an organization exists else create one
	'''
	list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name = '{}'".format(self.full_name_of_the_organization))
	if len(list_of_organizations) == 1:
		# update the status for the organization
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
		doc.status = "Verified"
		doc.save(ignore_permissions = True)
		
	elif len(list_of_organizations) == 0:
		# create an organization
		doc = frappe.get_doc({"doctype":"Organization"})
		doc.name = self.full_name_of_the_organization
		doc.status = self.status
		doc.organization_type = "Somaliland Government Institution"
		doc.full_name_of_the_organization = self.full_name_of_the_organization
		doc.acronym = self.acronym

		# check if the user is not the admin
		if "Staff" in frappe.get_roles(frappe.session.user):
			pass
		else:
			# assign the current non-staff user to the organization
			doc.user = frappe.session.user
			doc.owner = frappe.session.user
			
		doc.insert(ignore_permissions = True)


def find_simalar_organizations(self):
	'''
	Function That Check if There Are Any Organization with Similar
	Names and Acronyms to those given in the Form
	'''
	# holds all the organization found through this search
	all_organization = [] 

	def process_results(list_of_organizations):
		'''
		Function that will help to make the code Drier within
		this function by reducing repetitive tasks
		'''

		if len(list_of_organizations) > 0:
			for organization in list_of_organizations:
				organization_name = organization[0]
				all_organization.append(organization_name)	
	
	# check if the user gave organizaion name and acronym
	if self.full_name_of_the_organization and self.acronym:
		# set the given name as the search name
		search_organization_name  = self.full_name_of_the_organization
		search_acronymn = self.acronym

		# search based on full name likeness
		list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '%{}%'".format(self.full_name_of_the_organization))
		
		# process the found results
		process_results(list_of_organizations)

		# full name of each word in the name query
		list_of_words_in_name = search_organization_name.split()
		# get rid of conjuctions
		common_conjuctions = ["of","the"]
		# loop through name parts
		for name_part in list_of_words_in_name:
			if name_part.lower() in common_conjuctions:
				pass
			else:
				# process results 
				list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '%{}%'".format(name_part))
				process_results(list_of_organizations)
		
		# starting 2 letters letter query
		first_two_letters = search_organization_name[:2]
		list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '{}%'".format(first_two_letters))
		if len(list_of_organizations) > 0:
			process_results(list_of_organizations)
		else:
			pass

		# Acronym matches
		# full name of acronym matches
		search_acronymn = self.acronym
		list_of_organizations = frappe.db.sql("select name from `tabOrganization` where acronym like '%{}%'".format(search_acronymn))
		if len(list_of_organizations) > 0:
			process_results(list_of_organizations)
		else:
			pass
	
		# loop through unique organizations
		unique_results =  set(all_organization)

		# retun unique results
		return unique_results
	else:
		frappe.throw("Please give the organization name and acronym in order to continue")

def set_next_page(self):
	# skip or show the organization suggestion section
	if self.current_section == "organization_suggestion_section":
		if not self.link_to_org_name:
			# find matches
			found_organizations = find_simalar_organizations(self)
			
			if len(found_organizations) > 0:
				# clear the tables first
				self.organization_suggestion_table = []
				# add each unique result to table
				for organization in found_organizations:
					# append to select table
					self.append("organization_suggestion_table", {
						"organization_name":organization
					})	

				# unselect the yes and no
				self.yes_found_org_match = 0 
				self.did_not_find_org_match	= 0	
			else:
				# create the organization
				new_org_doc  = frappe.get_doc({"doctype":"Organization"})
				new_org_doc.name = self.full_name_of_the_organization
				new_org_doc.full_name_of_the_organization = self.full_name_of_the_organization
				new_org_doc.acronym = self.acronym
				new_org_doc.organization_type = "Somaliland Government Institution"

				# check if the user is not the admin
				if "Staff" in frappe.get_roles(frappe.session.user):
					pass
				else:
					# assign the current non-staff user to the organization
					new_org_doc.user = frappe.session.user
					new_org_doc.owner = frappe.session.user

				new_org_doc.insert(ignore_permissions=True)

				# set the link field to orga name
				self.link_to_org_name = self.full_name_of_the_organization


				# skip this section
				current_section_index = list_of_sections.index("organization_suggestion_section")
				# place new page for user to see depending on direction
				if self.direction == "Forward":
					self.previous_section = list_of_sections[current_section_index]
					self.current_section = list_of_sections[current_section_index+1]
					self.next_section = list_of_sections[current_section_index + 2]
				else:
					self.previous_section = list_of_sections[current_section_index - 2]
					self.current_section = list_of_sections[current_section_index-1]
					self.next_section = list_of_sections[current_section_index]
		else:
			# skip this section
			current_section_index = list_of_sections.index("organization_suggestion_section")
			# place new page for user to see depending on direction
			if self.direction == "Forward":
				self.previous_section = list_of_sections[current_section_index]
				self.current_section = list_of_sections[current_section_index+1]
				self.next_section = list_of_sections[current_section_index + 2]
			else:
				self.previous_section = list_of_sections[current_section_index - 2]
				self.current_section = list_of_sections[current_section_index-1]
				self.next_section = list_of_sections[current_section_index]
	else:
		pass

	# check if a suggestion has been selected and the link field has not been selected
	if not self.imported and not self.link_to_org_name and self.yes_found_org_match:
		# place the correct match in the link field take user to first page
		for organization in self.organization_suggestion_table:
			if organization.yes:
				organization_suggestion_doc = frappe.get_doc("Organization",organization.organization_name)
				# get the organizaion acronym
				# set the selected organization to name field
				self.full_name_of_the_organization = organization.organization_name
				self.acronym = organization_suggestion_doc.acronym 

				# check if the user is not the admin
				if "Staff" in frappe.get_roles(frappe.session.user):
					pass
				else:
					# check if the organization has been assigned to a user
					if organization_suggestion_doc.user:
						pass
					else:
						# assign the current non-staff user to the organization
						organization_suggestion_doc.user = frappe.session.user
						organization_suggestion_doc.owner = frappe.session.user
						organization_suggestion_doc.save(ignore_permissions=True)

				# set the current page to first page
				self.previous_section = ""
				self.current_section = list_of_sections[0]
				self.next_section = list_of_sections[1]

def check_if_user_has_organization(self):
	'''
	Function that ensures that a user is only linked to one 
	organization
	'''
	if "Staff" in frappe.get_roles(frappe.session.user):
		pass
	else:
		# first check if the user is already assigned to an organization
		list_of_organization_user_is_assigned_to = frappe.get_list("Organization",
			fields=["*"],
			filters = {
				"user":frappe.session.user
		})
		
		if len(list_of_organization_user_is_assigned_to) > 0:
			# check if the organization is the same as the that on the form
			if list_of_organization_user_is_assigned_to[0]['name'] == self.full_name_of_the_organization:
				pass
			else:
				frappe.throw("You username '{}' is already assigned to the organization '{}'\
					<hr> Contact the administrator for assistance if this is wrong  \
						".format(frappe.session.user,list_of_organization_user_is_assigned_to[0]['name']))
		
		# check if the data is not imported and the link name does not exist
		if not self.imported and not self.link_to_org_name:
			# check that organization name and acronym exists
			if self.full_name_of_the_organization and self.acronym:
				# check if the organization already exists
				list_of_organizations_registered = frappe.get_list("Organization",
						fields=["*"],
						filters = {
							"name":self.full_name_of_the_organization
					})
			
				if len(list_of_organizations_registered) == 1:
					# check if the organization is linked to a user
					if list_of_organizations_registered[0].user:
						# check if that user is the current user
						if list_of_organizations_registered[0].user == frappe.session.user:
							# assign any form if any exists
							assign_registration_forms_to_correct_users(self,list_of_organizations_registered)
							# set the correct organization in the link field
							self.link_to_org_name = list_of_organizations_registered[0].full_name_of_the_organization
						else:
							frappe.msgprint("The organization {} is linked to a different user,contact administrator for assistance".format(self.full_name_of_the_organization))
							# cancell the document
							self.status = "Cancelled"
							# continue from here 
							
					else:
						# update the doctype
						document_to_update = frappe.get_doc("Organization",self.full_name_of_the_organization)
						document_to_update.user = frappe.session.user
						document_to_update.owner = frappe.session.user
						document_to_update.save(ignore_permissions=True)

						# assign any form if any exists
						assign_registration_forms_to_correct_users(self,list_of_organizations_registered)
						# set the correct organization in the link field
						self.link_to_org_name = list_of_organizations_registered[0].full_name_of_the_organization
				else:
					# allow the user to continue normally
					pass
			else:
				frappe.msgprint("Please provide the organization name and acronym in screen one in order to continue")
				# take the users to screen one
				self.previous_section = ""
				self.current_section = list_of_sections[0]
				self.next_section = list_of_sections[1]
		else:
			pass

def upgrade_status_for_imported_documents(self):
	if self.status == "Admin Verified":
		# progress the form to Verified for imported data
		self.status = "Verified"

	elif self.status == "Verified":
		# progress the form to Validated for imported data
		self.status = "Validated"

	elif self.status == "Validated":
		# progress the form to Approved for imported data
		self.status = "Approved"

def assign_registration_forms_to_correct_users(self,list_of_organizations_registered):
	'''
	Function that assigns registration forms to the correct user 
	in that owns the organization
	'''
	# check if the organization has linked registration forms							
	if list_of_organizations_registered[0].organization_type != "Local NGO":
		type_of_registration_form = list_of_organizations_registered[0].organization_type + " Form"

		# get registration forms of given type
		list_of_registration_forms = frappe.db.sql("select name from `tab{}` where full_name_of_the_organization = '{}' && status !='Cancelled'".format(type_of_registration_form,self.full_name_of_the_organization))

		if len(list_of_registration_forms) > 0:
			# check if the form is the same as the first form
			if self.name == list_of_registration_forms[0][0]:	
				pass
			else:
				# change the ownership of the form
				# continue from here
				form_doc = frappe.get_doc(type_of_registration_form,list_of_registration_forms[0][0])
				form_doc.owner = frappe.session.user
				form_doc.save(ignore_permissions=True)
				frappe.msgprint("A registration for this organization form has been assigned to you find it under list of '{}' Forms".format(list_of_organizations_registered[0].organization_type + " Form"))
				self.status = "Cancelled"
		else:
			# allow the user to continue with the process but set correct values
			self.full_name_of_the_organization = list_of_organizations_registered[0].name
			self.acronym = list_of_organizations_registered[0].acronym
			self.link_to_org_name = list_of_organizations_registered[0].name

			# take the user to page 1
			self.previous_section = ""
			self.current_section = list_of_sections[0]
			self.next_section = list_of_sections[1]

	elif list_of_organizations_registered[0].organization_type == "Local NGO":
		# check somali forms
		type_of_registration_form = "Local NGO Form Somali"
		# get registration forms of given type
		list_of_registration_forms_somali = frappe.db.sql("select name,full_name_of_the_organization from `tabLocal NGO Form Somali` where full_name_of_the_organization = '{}' && status !='Cancelled'".format(self.full_name_of_the_organization))

		if len(list_of_registration_forms_somali) > 0:
			# check if the form is the same as the first form
			if self.name == list_of_registration_forms_somali[0][0]:
				pass
			else:
				# change the ownership of the form
				# continue from here
				form_doc = frappe.get_doc("Local NGO Form Somali",list_of_registration_forms_somali[0][0])
				form_doc.owner = frappe.session.user
				form_doc.save(ignore_permissions=True)
				frappe.msgprint("A registration for this organization form has been assigned to you find it under list of 'Local NGO Form Somali' List")
				self.status = "Cancelled"

		# check somali forms
		type_of_registration_form = "Local NGO Form English"
		# get registration forms of given type
		list_of_registration_forms_english = frappe.db.sql("select name,full_name_of_the_organization from `tabLocal NGO Form English` where full_name_of_the_organization = '{}' && status !='Cancelled'".format(self.full_name_of_the_organization))

		if len(list_of_registration_forms_english) > 0:
			# check if the form is the same as the first form
			if self.name == list_of_registration_forms_english[0][0]:
				pass
			else:
				# change the ownership of the form
				# continue from here
				form_doc = frappe.get_doc("Local NGO Form Somali",list_of_registration_forms_somali[0][0])
				form_doc.owner = frappe.session.user
				form_doc.save(ignore_permissions=True)
				frappe.msgprint("A registration for this organization form has been assigned to you find it under list of 'Local NGO Form Somali' List")
				self.status = "Cancelled"


		if len(list_of_registration_forms_english) > 0 and len(list_of_registration_forms_somali) > 0:
			pass
		else:
			# allow the user to continue with the process but set correct values
			self.full_name_of_the_organization = list_of_organizations_registered[0].name
			self.acronym = list_of_organizations_registered[0].acronym
			self.link_to_org_name = list_of_organizations_registered[0].name
			
			# take the user to page 1
			self.previous_section = ""
			self.current_section = list_of_sections[0]
			self.next_section = list_of_sections[1]	

def map_data_to_organization(self):
	'''
	Function that Updates the details of the organization
	to the organization form once the form is approved
	'''
	# check if the organization has been verified
	if self.status == "Verified" or  self.status == "Admin Approved":
		check_if_organization_exists(self)

	if self.status == "Validated":
		# get related organization
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
		doc.status = "Validated"
		doc.save()

	# if approved update details to linked organization
	if(self.status == "Approved" or self.status == "Imported"):
		# get related organization
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
		
		# ORGANIZATION TYPE SECTION
		doc.status = "Approved"

		# GENERAL ORGANIZATION DETAILS SECTION
		doc.full_name_of_the_organization = self.full_name_of_the_organization
		doc.acronym = self.acronym

		# ADDRESS OF MAIN OFFICE SECTION
		doc.area_neighborhood = self.area_neighborhood
		doc.town = self.town
		doc.region = self.region
		doc.district = self.district

		# Director Address Section
		doc.full_name =  self.director_full_name
		doc.director_country_code =  self.country_code
		doc.tel_no_1 =  self.tel_no
		doc.complete_director_tel_no =  self.complete_tel_no

		# Three Alternative Contact Persons Section
		doc.alternative_contact_person = []
		for contact in self.alternative_contact_person:
			doc.append("alternative_contact_person", {
				"full_name": contact.full_name,
				"title":contact.title,
				"tel_no":contact.tel_no,
				"email":contact.email
			})
		
		# Sector  alignment table section
		doc.sector__alignment_table = []
		for sector in self.sector__alignment_table:
			doc.append("sector__alignment_table", {
				"sector": sector.sector,
				"yes":sector.yes,
			})
		
		# Department responsible for coordination issues
		doc.coordinating_sectors_table = []
		for fora in self.coordinating_sectors_table:
			doc.append("coordinating_sectors_table", {
				"fora_attended": fora.fora_attended,
				"your_organization_the_chair_of_this_scf":fora.your_organization_the_chair_of_this_scf,
			})

		# add the form as a child table to organization
		doc.append("attached_organization_forms_table", {
			"name_of_form":self.name,
			"year_of_form":self.year_of_registration
		})

		# save the added changes
		doc.save()

def execute_actions(self):
	'''
	Function that executes some functions once the documents
	is updated
	'''
	# check if the organization has been created
	if self.link_to_org_name:
		pass
	else:
		# check if the user already checked that the organization was
		# not found in the list of suggestions
		if self.did_not_find_org_match:
			found_organizations = frappe.get_list("Organization",
				fields=["name"],
				filters = {
					"name":self.full_name_of_the_organization
			})
			# check the length of found organizations
			if len(found_organizations) > 0:
				# set the link organ name as orgs full name since it been saved to database
				self.link_to_org_name = self.full_name_of_the_organization
			else:
				# create this new organization
				create_new_organization(self,"Somaliland Government Institution")
				# set the link organ name as orgs full name since it been saved to database
				self.link_to_org_name = self.full_name_of_the_organization
		
		# check if the user selected and organization from the suggestion table
		for organization in self.organization_suggestion_table:
			if organization.yes:
				organization_suggestion_doc = frappe.get_doc("Organization",organization.organization_name)
				# get the organizaion acronym
				# set the selected organization to name field
				self.full_name_of_the_organization = organization.organization_name
				self.acronym = organization_suggestion_doc.acronym 
				self.link_to_org_name =  organization.organization_name
			

def create_new_organization(self,organization_type):
	'''
	Function that creates a new organization
	based on a given name and type
	'''
	# create the organization
	new_org_doc  = frappe.get_doc({"doctype":"Organization"})
	new_org_doc.name = self.full_name_of_the_organization
	new_org_doc.full_name_of_the_organization = self.full_name_of_the_organization
	new_org_doc.acronym = self.acronym
	new_org_doc.organization_type = organization_type

	# check if the user is not the admin
	if "Staff" in frappe.get_roles(frappe.session.user):
		pass
	else:
		# assign the current non-staff user to the organization
		new_org_doc.user = frappe.session.user
		new_org_doc.owner = frappe.session.user
	# now save the document
	new_org_doc.insert(ignore_permissions=True)