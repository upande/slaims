// Copyright (c) 2019, Frappe Technologies and contributors
// For license information, please see license.txt


// global varibles
// ==========================================================================================================


// general functions section
// ==========================================================================================================

// list of sections listed in rder of filling/ occurance
var list_of_sections = [
	"project_section_section","implementing_partners_section_section",
	"pillar_selection_section_section","relevant_sectors_section","priority_programs_section",
	"relevant_mda_per_sector_section","relevant_ndp_outcome_by_selected_priority_program_section"
]


// function that filter the route to be selected from territory
function filter_organization(){
	if(frappe.user.has_role("Alignment Interface Admin")){
		// allow user to see all organization
	}else{
		cur_frm.set_query("name_of_organization", function() {
			return {
				"filters": {
					"owner": frappe.session.user
				}
			}
		});
	}
}


// function that filter the projects visible to users
function filter_project(){
	if(frappe.user.has_role("Alignment Interface Admin")){
		// allow users to see all projects
	}else{
		cur_frm.set_query("project", function() {
			return {
				"filters": {
					"owner": frappe.session.user
				}
			}
		});
	}

}

// function that determines which section the user should see
function navigation_function(frm,list_of_sections){
	// hide all the fields
	hide_unhide_fields(frm, list_of_sections, false)
	// check the fields to unhide
	if(frm.doc.current_section){
		// unhide the current section
		frm.toggle_display(frm.doc.current_section, true)
	}else{
		// set the current sectio as the first section
		frm.doc.current_section = list_of_sections[0]
		frm.doc.next_section = list_of_sections[1]
		cur_frm.save()
	}
}


/*function that toogles field to hide or unhide*/
function hide_unhide_fields(frm, list_of_fields, hide_or_unhide) {
	for (var i = 0; i < list_of_fields.length; i++) {
		frm.toggle_display(list_of_fields[i], hide_or_unhide)
	}
}

// function that determines the next section the user need to fill and unhides it
function unhide_next_section(frm,steps){
	// unhide_next_field(list_of_fields)
	var current_section_index = list_of_sections.indexOf(frm.doc.current_section)
	var next_user_section = list_of_sections[current_section_index + steps]
	var future_user_section = list_of_sections[current_section_index + steps+1]

	// set new previous,current and next sections
	if(future_user_section){
		cur_frm.set_value("previous_section",frm.doc.current_section)
		cur_frm.set_value("current_section",next_user_section)
		cur_frm.set_value("next_section",future_user_section)
		// unhide the save and continue button
		frm.toggle_display("save_and_continue", true)
	}else{
		// check already on the last page
		if(list_of_sections.indexOf(frm.doc.current_section) == list_of_sections.length - 1){
			msgprint("You Have Finished Filling in the NDP Alignment, You Can Now Continue Filling in the Project Form")
			frappe.route_options = {
				"previous_section":"ndp_aligment_interface_section",
				"current_section":"attachments_section_section",
				"next_section":"verification_section_section"
				// "ndp_alignment_interface_link":frm.doc.name
			}
			frappe.set_route("Form", "Project Form",frm.doc.project_form)
		}else{
			cur_frm.set_value("previous_section",frm.doc.current_section)
			cur_frm.set_value("current_section",next_user_section)
			cur_frm.set_value("next_section","")
		}
		
	}
	
	// save the form
	cur_frm.save()
}

// function that determines the previous section the user filled and unhides it
function unhide_previous_section(frm){
	// unhide_next_field(list_of_fields)
	var current_section_index = list_of_sections.indexOf(frm.doc.current_section)
	var current_user_section = list_of_sections[current_section_index - 1]
	var previous_user_section = list_of_sections[current_section_index - 2]

	// set new previous,current and next sections
	if(previous_user_section){
		cur_frm.set_value("previous_section",previous_user_section)
		cur_frm.set_value("current_section",current_user_section)
		cur_frm.set_value("next_section",frm.doc.current_section)
	}else{

		// check already on the last page
		if(list_of_sections.indexOf(frm.doc.current_section) == 0){
			msgprint("You Reached the Beggining of the Form")
		}else{
			cur_frm.set_value("previous_section","")
			cur_frm.set_value("next_section",frm.doc.current_section)
			cur_frm.set_value("current_section",current_user_section)
		}
	}
	
	// save the form
	cur_frm.save()
}

var check_required_project_section_section = function(frm){
	if(frm.doc.correct_information){
		return {status:true,steps:1}
	}else{
		frappe.throw("You Need to Confirm The InFormation In This Section is Correct In Order To Continue"+"<hr>"+"Please Tick the Field 'Do You Confirm That The Above Information is Correct'")
	}
}

var check_required_implementing_partners_section_section = function(frm){
	if(frm.doc.correct_implementing_partners){
		return {status:true,steps:1}
	}else{
		frappe.throw("You Need to Confirm All Implementing Patners are Correct In Order To Continue"+"<hr>"+"Please Tick the Field 'Confirm, All the Implementing Partners in the Table Above are Correct'")
	}
	
}

var check_required_pillar_selection_section_section = function(frm){
	if(frm.doc.confirm_pillars_check){
		return {status:true,steps:1}
	}else{
		frappe.throw("You Need to Confirm Selection of Pillars in Order to Continue"+"<hr>"+"Please Tick the Field 'Confirm, I have Selected All Pillars The Project Contributes to'")
	}
}

var check_required_relevant_sectors_section = function(frm){
	if(frm.doc.relevant_sectors_check){
		return {status:true,steps:1}
	}else{
		frappe.throw("You Need to Confirm Selection of Sectors in Order to Continue"+"<hr>"+"Please Tick the Field 'Confirm , I Have Selected All The Sectors the Project Contributes to'")
	}
}

var check_required_priority_programs_section = function(frm){
	if(frm.doc.relevant_programs_check){
		return {status:true,steps:1}
	}else{
		frappe.throw("You Need to Confirm Selection of Priority Programs in Order to Continue"+"<hr>"+"Please Tick the Field 'Confirm, I Have Selected  All The Relevant Programs the Project Contributes to'")
	}
}

var check_required_relevant_mda_per_sector_section = function(frm){
	if(frm.doc.relevant_mda_check){
		return {status:true,steps:1}
	}else{
		frappe.throw("You Need to Confirm Selection of MDA's Overseeing the Project in Order to Continue"+"<hr>"+"Please Tick the Field 'Confirm, I Have Selected All The MDAs Relevant MDAs Overseeing this Project'")
	}
}

var check_required_relevant_ndp_outcome_by_selected_priority_program_section = function(frm){
	if(frm.doc.relevant_outcomes_check){
		return {status:true,steps:1}
	}else{
		frappe.throw("You Need to Confirm Selection of Relvant Outcome of The Project in Order to Continue"+"<hr>"+"Please Tick the Field 'Confirm, I Have Selected  All The Relevant Outcomes  That the Project Contributes to'")
	}
}

var validation_function_per_section ={
	"project_section_section":check_required_project_section_section,
	"implementing_partners_section_section":check_required_implementing_partners_section_section,
	"pillar_selection_section_section":check_required_pillar_selection_section_section,
	"relevant_sectors_section":check_required_relevant_sectors_section,
	"priority_programs_section":check_required_priority_programs_section,
	"relevant_mda_per_sector_section":check_required_relevant_mda_per_sector_section,
	"relevant_ndp_outcome_by_selected_priority_program_section":check_required_relevant_ndp_outcome_by_selected_priority_program_section,
}

// function that checks if fields in an imput list have value
function check_fields_exists(list_of_fields){
	$.each(list_of_fields, function(i,v){
		if(v["value"]){
			// do nothing because field value exists
		}else{
			cur_frm.set_value("correct_information","")
			frappe.throw("You Need To Enter Values for Field '"+v["field"]+"' First")
		}
	});
}


var process_project_section_section = function(frm){
	// get implemening partners and place them in the implemnting parterners table
	cur_frm.clear_table("implementing_partners");
	cur_frm.set_value("correct_implementing_partners","")

	// get implementing patners
	frappe.call({
		method: "frappe.client.get_list",
		args: 	{
				parent:"Project Form",
				doctype: "Destination Implementing Reporting",
				filters: {
					parent:cur_frm.doc.project_form
				},
		fields:["*"]
		},
		callback: function(response) {
			if(response.message.length > 0){
				$.each(response.message, function(i,v){
					var implementing_patners = cur_frm.add_child("implementing_partners")
					implementing_patners.implementing_partner = v["implementing_organization"]
					cur_frm.refresh_fields();
				})
			}
		}
	})
}

var process_implementing_partners_section_section = function(frm){
	// clears the pillars table
	cur_frm.clear_table("pillar_selection_table");
	cur_frm.set_value("confirm_pillars_check","")
	// get all the pillars
	frappe.call({
		method: "frappe.client.get_list",
		args: 	{
				doctype: "NDP",
				filters: {
					type:"Pillar"
				},
		fields:["name"]
		},
		callback: function(response) {
			if(response.message.length > 0){
				$.each(response.message, function(i,v){
					var row_pillar_table = cur_frm.add_child("pillar_selection_table")
					row_pillar_table.pillar = v["name"]
					cur_frm.refresh_fields();
				})
			}
		}
	})
}


var process_pillar_selection_section_section = function(frm){
	// clears the sectors table
	cur_frm.clear_table("relevant_sectors");
	cur_frm.set_value("relevant_sectors_check","")
	
	// loop through list of selected pillars
	$.each(cur_frm.doc.pillar_selection_table, function(i,v){
		if(v["yes"]){
			// get relavant sectors and place the in the sectors table
			frappe.call({
				method: "frappe.client.get_list",
				args: 	{
						doctype: "NDP",
						filters: {
							type:"Sector",
							parent_ndp:v["pillar"]
						},
				fields:["name"]
				},
				callback: function(response) {
					if(response.message.length > 0){
						$.each(response.message, function(j,k){
							var row_sector_table = cur_frm.add_child("relevant_sectors")
							row_sector_table.pillar = v["pillar"]
							row_sector_table.sector = k["name"]
							cur_frm.refresh_fields();
						})
					}
				}
			})
		}else{
			// do nothing
		}
	});
}

var process_relevant_sectors_section = function(frm){
	// clears the priority programs table
	cur_frm.clear_table("priority_program");
	cur_frm.set_value("relevant_programs_check","")
	
	// loop through list of selected sectors
	$.each(cur_frm.doc.relevant_sectors, function(i,v){
		if(v["yes"]){
			// get relavant programs and place the in the programs table
			frappe.call({
				method: "frappe.client.get_list",
				args: 	{
						doctype: "Program",
						filters: {
							sector:v["sector"]
						},
				fields:["*"]
				},
				callback: function(response) {
					if(response.message.length > 0){
						$.each(response.message, function(j,k){
							var row_program_table = cur_frm.add_child("priority_program")
							row_program_table.priority_program = k["name"]
							row_program_table.priority_program_description = k["program"]
							row_program_table.sector = k["sector"]
							cur_frm.refresh_fields();
						})
					}
				}
			})
		}else{
			// do nothing
		}
	});
}

var process_priority_programs_section = function(frm){
	// return {status:true,steps:1}
}

var process_relevant_mda_per_sector_section = function(frm){
	// clears the outcomes table
	// cur_frm.clear_table("relevant_outcomes");
	// cur_frm.set_value("relevant_outcomes_check","")
	
	// // loop through list of selected priority programs
	// $.each(cur_frm.doc.priority_program, function(i,v){
	// 	if(v["yes"]){
	// 		// get relavant outomes and place them in the outcomes table
	// 		frappe.call({
	// 			method: "frappe.client.get_list",
	// 			args: 	{
	// 					doctype: "Outcome",
	// 					filters: {
	// 						linked_to_priority_program:1,
	// 						linked_priority_program:v["priority_program"]
	// 					},
	// 			fields:["*"]
	// 			},
	// 			callback: function(response) {
	// 				if(response.message.length > 0){
	// 					$.each(response.message, function(j,k){
	// 						var row_outcome_table = cur_frm.add_child("relevant_outcomes")
	// 						row_outcome_table.outcome = k["truncated_outcome_title"]
	// 						row_outcome_table.outcome_description = k["outcome"]
	// 						row_outcome_table.attached_to_priority_program = k["linked_to_priority_program"]
	// 						row_outcome_table.priority_program = k["linked_priority_program"]
	// 						row_outcome_table.sector = k["sector"]
	// 					// 	cur_frm.refresh_fields();
	// 					})
	// 				}
	// 			}
	// 		})
	// 	}else{
	// 		// do nothing
	// 	}
	// });
}

var process_relevant_ndp_outcome_by_selected_priority_program_section = function(frm){

}


var process_function_per_section ={
	"project_section_section":process_project_section_section,
	"implementing_partners_section_section":process_implementing_partners_section_section,
	"pillar_selection_section_section":process_pillar_selection_section_section,
	"relevant_sectors_section":process_relevant_sectors_section,
	"priority_programs_section":process_priority_programs_section,
	"relevant_mda_per_sector_section":process_relevant_mda_per_sector_section,
	"relevant_ndp_outcome_by_selected_priority_program_section":process_relevant_ndp_outcome_by_selected_priority_program_section,
}

/* end of the general functions section
// =================================================================================================
/* This section  contains functions that are triggered by the form action refresh or
reload to perform various action*/

frappe.ui.form.on('NDP Alignment With Programs MDAs Outcomes', {
	refresh: function(frm) {
		//Determines current section user should see
		navigation_function(frm,list_of_sections)
	}
});


/*******************************************************************************************
PROJECT SECTION SECTION */

 // function that called when the project_form field in filled
frappe.ui.form.on("NDP Alignment With Programs MDAs Outcomes", "project_form", function(frm){ 
	// check if user selected a project form
	if(frm.doc.project_form){
		// get sectors from project
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Project Form",
					filters: {
						name:frm.doc.project_form
					},
			fields:["*"]
			},
			callback: function(response) {
				// check if the project exist
				var list_of_projects = response.message
				if(list_of_projects.length == 0){
					frappe.throw("No Such Project Found")
				}else if(list_of_projects.length > 0){
					var current_project = response.message[0]
					// place the correct organization and the master project
					cur_frm.set_value("name_of_organization",current_project["your_organization"])
					cur_frm.set_value("project",current_project["project_name"])	
				}
			}
		})
	}else{
		// if the project form is removed removed all the related fields
		cur_frm.set_value("name_of_organization","")
		cur_frm.set_value("project","")
	}
})


 // function that called when the project_form field in filled
 frappe.ui.form.on("NDP Alignment With Programs MDAs Outcomes","correct_information", function(frm){ 
	if(frm.doc.correct_information){
		// check if the project form is given
		var project_section_fields = [
			{field:"* Project Form (Select One)",value:frm.doc.project_form},
			{field:"* Project",value:frm.doc.project},
			{field:"* Name of Organization",value:frm.doc.name_of_organization}
		]
		check_fields_exists(project_section_fields)
	}
});


/*******************************************************************************************
IMPLEMENTING PARTNERS SECTION */

// function that called when the correct_implementing_partners field in filled
frappe.ui.form.on("NDP Alignment With Programs MDAs Outcomes","correct_implementing_partners", function(frm){ 
	
});


/*******************************************************************************************
PILLAR SELECTION SECTION */

// function that called when the confirm_pillars_check field in filled
frappe.ui.form.on("NDP Alignment With Programs MDAs Outcomes","confirm_pillars_check", function(frm){ 
	if(frm.doc.confirm_pillars_check){
		// check if the user ticked any pillar
		var pillar_selected = false

		$.each(frm.doc.pillar_selection_table, function(i,v){
			if(v["yes"]){
				pillar_selected = true
			}else{
				// do nothing
			}
		});
	
		if(pillar_selected){
			// allow user to continue
		}else{
			cur_frm.set_value("confirm_pillars_check","")
			frappe.throw("You Have Not Selected Any Pillar"+"<hr>"+"Please Select Pillars That The Project Contributes to In Order to Continue")
		}
	}
});

/*******************************************************************************************
RELEVANT SECTORS SECTION */

// function that called when the relevant_sectors_check field is checked
frappe.ui.form.on("NDP Alignment With Programs MDAs Outcomes","relevant_sectors_check", function(frm){ 
	if(frm.doc.relevant_sectors_check){
		// check if the user ticked any sector
		var sector_selected = false

		$.each(frm.doc.relevant_sectors, function(i,v){
			if(v["yes"]){
				sector_selected = true
			}else{
				// do nothing
			}
		});
	
		if(sector_selected){
			// allow user to continues
		}else{
			cur_frm.set_value("relevant_sectors_check","")
			frappe.throw("You Have Not Selected Any Program"+"<hr>"+"Please Select Sectots That The Project Contributes to In Order to Continue")
		}
	}
});


/*******************************************************************************************
PRIORITY PROGRAMS SECTION */

// function that called when the confirm_pillars_check field in filled
frappe.ui.form.on("NDP Alignment With Programs MDAs Outcomes","relevant_programs_check", function(frm){ 
	if(frm.doc.relevant_programs_check){
		// check if the user ticked any sector
		var programs_selected = false

		$.each(frm.doc.priority_program, function(i,v){
			if(v["yes"]){
				programs_selected = true
			}else{
				// do nothing
			}
		});
	
		if(programs_selected){
			// allow user to continues
		}else{
			cur_frm.set_value("relevant_programs_check","")
			frappe.throw("You Have Not Selected Any Priority Program In The Table"+"<hr>"+"Please Select Priority Programs That The Project Contributes to In Order to Continue")
		}
	}
});


/*******************************************************************************************
RELEVANT MDA PER SECTOR */

// function that called when the relevant_mda_check field in filled
frappe.ui.form.on("NDP Alignment With Programs MDAs Outcomes","relevant_mda_check", function(frm){ 
	if(frm.doc.relevant_mda_check){
	// 	// check if the user ticked any mda
	// 	var programs_selected = false

	// 	$.each(frm.doc.priority_program, function(i,v){
	// 		if(v["yes"]){
	// 			programs_selected = true
	// 		}else{
	// 			// do nothing
	// 		}
	// 	});
	
	// 	if(programs_selected){
	// 		// allow user to continues
	// 	}else{
	// 		cur_frm.set_value("relevant_programs_check","")
	// 		frappe.throw("You Have Not Selected Any Priority Program In The Table"+"<hr>"+"Please Select Sectors That The Project Contributes to In Order to Continue")
	// 	}
	}
});


/*******************************************************************************************
RELEVANT NDP OUTCOME BY SELECTED PRIORITY PROGRAM */

// function that called when the relevant_outcomes_check field in checked
frappe.ui.form.on("NDP Alignment With Programs MDAs Outcomes","relevant_outcomes_check", function(frm){ 
	if(frm.doc.relevant_outcomes_check){
		// check the form as complete
		cur_frm.set_value("form_complete",1)
	}
});















// // function that fetches relevant priority Programs for Relevant Priority Programs 
// frappe.ui.form.on("NDP Alignment With Programs MDAs Outcomes", "project_form", function(frm,cdt,cdn){ 
// 	if(frm.doc.project_form){
// 		// clear all the related child tables
// 		frm.clear_table("implementing_partners");
// 		frm.clear_table("relevant_sectors");
// 		frm.clear_table("priority_program");
// 		frm.clear_table("relevant_mda");
// 		frm.clear_table("relevant_outcomes");

// 		// get implementing patners
// 		frappe.call({
// 			method: "frappe.client.get_list",
// 			args: 	{
// 					parent:"Project Form",
// 					doctype: "Destination Implementing Reporting",
// 					filters: {
// 						parent:frm.doc.project_form
// 					},
// 			fields:["*"]
// 			},
// 			callback: function(response) {
// 				if(response.message.length > 0){
// 					$.each(response.message, function(i,v){
// 						var implementing_patners = frm.add_child("implementing_partners")
// 						implementing_patners.implementing_partner = v["implementing_organization"]
// 						cur_frm.refresh_fields();
// 					})
// 				}
// 			}
// 		})

// 		// get sectors from project
// 		frappe.call({
// 			method: "frappe.client.get_list",
// 			args: 	{
// 					doctype: "Project Form",
// 					filters: {
// 						name:frm.doc.project_form
// 					},
// 			fields:["*"]
// 			},
// 			callback: function(response) {
// 				// check if the project exist
// 				var list_of_projects = response.message
// 				if(list_of_projects.length == 0){
// 					frappe.throw("No Such Project Found")
// 				}else if(list_of_projects.length > 0){
// 					var current_project = response.message[0]
// 					// place the correct organization in the Organization Field
// 					cur_frm.set_value("name_of_organization",current_project["your_organization"])
					
// 					var list_of_sectors = [
// 						{"field_name":"economy","field_value":"Economy"},
// 						{"field_name":"education","field_value":"Education"},
// 						{"field_name":"energy_and_extractives","field_value":"Energy and Extractives"},
// 						{"field_name":"environment","field_value":"Environment"},
// 						{"field_name":"production","field_value":"Production"},
// 						{"field_name":"governance","field_value":"Governance"},
// 						{"field_name":"wash_sector","field_value":"WASH"},
// 						{"field_name":"infrastructure","field_value":"Infrastructure"},
// 						{"field_name":"cross_cutting_youth","field_value":"Cross Cutting Youth"},
// 						{"field_name":"cross_cutting_social_protection","field_value":"Cross Cutting Social Protection"},
// 						{"field_name":"cross_cutting_employment_and_labor","field_value":"Cross Cutting Employment and Labour"},
// 						{"field_name":"health","field_value":"Health"}
// 					]
			
// 					$.each(list_of_sectors, function(i,v){
// 						if(current_project[v["field_name"]]){
// 							var relavant_sectors = frm.add_child("relevant_sectors")
// 							relavant_sectors.relevant_sector = v["field_value"]
// 							cur_frm.refresh_fields();

// 							// get relevant programs
// 							frappe.call({
// 								method: "frappe.client.get_list",
// 								args: 	{
// 										doctype: "Program",
// 										filters: {
// 											sector:v["field_value"]
// 										},
// 								fields:["*"]
// 								},
// 								callback: function(response) {
// 									if(response.message.length > 0){
// 										$.each(response.message, function(i,v){
// 											if(v["priority_program"] == 1){
// 												var priority_programs = frm.add_child("priority_program")
// 												priority_programs.priority_program = v["name"]
// 												priority_programs.priority_program_description = v["program"]
// 												priority_programs.sector = v["sector"]
// 												cur_frm.refresh_fields();
// 											}else{
// 												// for none priority programs
// 											}
// 										})
// 									}
// 								}
// 							})

// 							// get relevant mda
// 							frappe.call({
// 								method: "frappe.client.get_list",
// 								args: 	{
// 										parent:"MDA",
// 										doctype: "MDA NDP Alignment",
// 										filters: {
// 											sector:v["field_value"]
// 										},
// 								fields:["*"]
// 								},
// 								callback: function(response) {
// 									if(response.message.length > 0){
// 										$.each(response.message, function(i,v){
// 											var relevant_mda = frm.add_child("relevant_mda")
// 											relevant_mda.relevant_mda_per_sector = v["parent"]
// 											relevant_mda.sector = v["sector"]
// 											cur_frm.refresh_fields();
// 										});
// 									}
// 								}
// 							});

// 							// get relevant outcomes
// 							frappe.call({
// 								method: "frappe.client.get_list",
// 								args: 	{
// 										doctype: "Outcome",
// 										filters: {
// 											sector:v["field_value"]
// 										},
// 								fields:["*"]
// 								},
// 								callback: function(response) {
// 									if(response.message.length > 0){
// 										$.each(response.message, function(i,v){
// 											var relevant_outcome = frm.add_child("relevant_outcomes")
// 											relevant_outcome.outcome = v["outcome_code"]
// 											relevant_outcome.outcome_description = v['outcome']
// 											relevant_outcome.attached_to_priority_program = v['linked_to_priority_program']
// 											relevant_outcome.priority_program = v['linked_priority_program']
// 											relevant_outcome.sector = v['sector']
// 											cur_frm.refresh_fields();
// 										});
// 									}
// 								}
// 							});
// 						}
// 					})
// 				}
// 			}
// 		})
// 	}

// });



/*****************************************************************************************
Navigation Section */

// function called when  the Previous Button field clicked under Navigation Section
frappe.ui.form.on("NDP Alignment With Programs MDAs Outcomes", "previous_button", function(frm){
	// go to the next section
	unhide_previous_section(frm)
})

// function called when  the Save and Continue field clicked under Navigation Section
frappe.ui.form.on("NDP Alignment With Programs MDAs Outcomes", "save_and_continue", function(frm){ 
	// go to the next section
	// ensure to requirements for the section are complete
	var section_function = validation_function_per_section[frm.doc.current_section] 
	if(section_function(frm)["status"]){		

		// perform processs required by the next section
		var section_process = process_function_per_section[frm.doc.current_section] 
		// run the process
		section_process()

		// then unhide the next section
		unhide_next_section(frm,section_function(frm)["steps"])
	}	
	
})


















// function that checks if priority programs have been given
frappe.ui.form.on("Priority Programs Table", "contributes_to", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn];
	if(child.contributes_to == 1){
		// check if the other fields have been filled
		if(child.priority_program){
			// do nothing 
		}else{
			child.contributes_to = 0
			cur_frm.refresh_fields();
			frappe.throw("No Priority Program Found")
		}
	}
});

// check if relevant MDA has been provided
frappe.ui.form.on("Relevant MDAs Table", "lead_mda", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn];
	if(child.lead_mda == 1){
		// check if the other fields have been filled
		if(child.relevant_mda_per_sector && child.sector ){
			// do nothing 
		}else{
			child.lead_mda = 0
			cur_frm.refresh_fields();
			frappe.throw("No Priority Program or Sector Found")
		}
	}
});


// check if relevant MDA has been provided
frappe.ui.form.on("Outcome by Selected Priority Program Table", "attached_to_priority_program", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn];
	if(child.attached_to_priority_program == 1){
		// check if the other fields have been filled
		if(child.outcome && child.priority_program && child.sector ){
			// do nothing 
		}else{
			child.attached_to_priority_program = 0
			cur_frm.refresh_fields();
			frappe.throw("No Outcome,Priority Program or Sector Found")
		}
	}
});

// function that fetches relevant priority Programs for Relevant Priority Programs 
frappe.ui.form.on("Project", "fetch_ndp_ii_alignment__details", function(frm,cdt,cdn){ 
	var list_of_sector_fields = ["health","education","WASH","economy","energy_and_extractives",
	"production","infrastructure","governance","environment","cross_cutting_employment_and_labor",
	"cross_cutting_social_protection","cross_cutting_youth"]
		
});




