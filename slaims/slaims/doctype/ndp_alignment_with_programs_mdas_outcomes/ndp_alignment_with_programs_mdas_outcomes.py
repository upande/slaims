# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document


class NDPAlignmentWithProgramsMDAsOutcomes(Document):
	'''
	This is the NDP Alignment With Programs MDAs Outcomes Controller Class
	'''
	
	def validate(self):
		# check_required_fields(self)
		# check required tables
		# check_required_tables(self)
		pass

	def on_update(self):
		# check if Project for form exist else create
		# check_if_link_exists_to_form_and_project(self)
		#check if form is appproved in then create li
		# check_if_form_is_approved(self)

		# check if ndp is complete
		check_if_complete(self)


def check_required_fields(self):
	'''
	Funtion that checks that all teh required field are
	Given 
	'''
	pass

def check_required_tables(self):
	'''
	Function that checks to ensure that all the required 
	tables are provided
	'''
	pass	

def check_if_link_exists_to_form_and_project(self):
	'''
	Function that checks if the Project linked to 
	project form exists else creates Project
	'''
	# check if the project link exists in form else place 1
	doc = frappe.get_doc("Project Form",self.name)
	if doc.ndp_aligment_sector_program_outcome:
		# check if they are equal else replace
		
		if doc.ndp_aligment_sector_program_outcome == self.name:
			# the linked NDP Aligment Interface is the Same hence pass
			pass
		else:
			# update the NDP Aligment Interface
			doc.ndp_aligment_sector_program_outcome = self.name
			doc.save()
	else:
		doc.ndp_aligment_sector_program_outcome = self.name
		doc.save()
	
	

def check_if_form_is_approved(self):
	'''
	Function that checks if the form is approved in then 
	updates project details
	'''
	pass
	
def check_if_complete(self):
	'''
	Function that checks if the NDP alignment form is complete and
	adds the NDP interface to the Project Form
	'''
	#print "*"*80
	self.form_complete
	if self.form_complete == 1:
		# add the ndp interface to the form
		doc = frappe.get_doc("Project Form",self.project_form)
		doc.ndp_alignment_interface_link = self.name
		doc.save()

	else:
		# the ndp interface is not complete hence pass
		pass

		
