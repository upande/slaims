// Copyright (c) 2019, upande and contributors
// For license information, please see license.txt

// list of organization form sections in organization
var list_of_sections = {
	"Private Sector Company": [
		"general_organizaiton_details","organization_address",
		"address_of_main_office_section_section",
		"somaliland_most_senior_executive_section",
		"country_director_address_section","three_alternative_contact_persons_section",
		"part_two","pillar_alignment_section","section_alignment_section","themes_alignment_section",
		"geographical_area",
		"attachments_section_section","verification",
	],
	"All": [
		"organization_type_section_section",
		"general_organizaiton_details","organization_address",
		"address_of_main_office_section_section",
		"somaliland_most_senior_executive_section",
		"country_director_address_section","three_alternative_contact_persons_section",
		"part_two","pillar_alignment_section","section_alignment_section","themes_alignment_section",
		"geographical_area",
		"attachments_section_section",
		"validation","verification","approval"
	]
}

var list_of_all_fields = [
	// organization details section
	"full_name_of_the_organization",
	"year_of_first_registration",
	"organization_registration_number",

	// DATA PULLED FROM MAIN
	"data_pulled_from_main",

	// organization address section
	"address", "city", "origin_country", "postcode", 
	
	// Addrerss of main office in Somaliland section
	"area_neighborhood", "town", "region", "district", 
	
	// Somaliland most senior executive section
	"full_name_2", "country_code_senior_most_executive", "director_tel_no", 
	"complete_senior_tel_no", "email_2", 
	
	// Country director address section
	"select_one_director_address", "directors_address", "area_neighborhood_most_senior", 
	"director_city", "director_code", "region_most_senior", "district_most_senior", "country",

	// Three alternative contact person section
	"alternative_contact_person", "number_of_projects_confirmed_during_the_current_year", "that_are_ongoing",
	"that_are_completed", "confirmed_to_start_during_current_year",

	// Pillar alignment section
	"pillar_alignment_table", "confirm_pillars_check", 
	
	// Sector alignment section
	"section_alignment_table", "confirm_sectors_check", 
	
	// Themes alignment section
	"themes_alignment_table", "confirm_themes_check", 
	
	// Geographical areas section
	"marodijeh_check", "hargeisa", "gabiley", "baligubadle", "salahlay", "sanag_check",
	"erigavo", "badhan", "las_qoray", "ela_fweyn", "dhahar", "gar_adag", "hargeisa_region",
	"sool_check", "las_anod", "hudun", "taleh", "aynabo", "togdheer_check", "burao",
	"odwayne", "buhodle", "awdal_check", "borama", "zeila", "baki", "lughaya", "sahil_check",
	"berbera", "sheekh",

	// Attachments section
	"valid_registrationlicence_from_the_mot", "certificate_of_tax_clearance", "a_copy_of_articles_of_incorporation",
	"standard_letter_of_agreement", 
	
	// Verification and approval section
	"verifier_name","verifier_designation","verification_date",	
]

// function that sets custom buttons
function add_custom_buttons(button_name,action){
	cur_frm.add_custom_button(__(button_name), function(){

		// only show this buttons to users with administrative privillages
		if(action=="Unverify"){
			unverify_button()
		}else if (action == "Unvalidate"){
			unvalidate_button()
		}else if(action=="Edit"){
			// check user is allowed to edit
			if(cur_frm.doc.verified){
				frappe.throw("You cannot edit a form that is already verified \
				,contact the admin for assistance")
			}else{
				// make the forms readble for user
				cur_frm.set_value("viewing_information",0)
				cur_frm.set_value("current_section",list_of_sections[0])
				cur_frm.save()
			}
		}else if(action = "Go To Page"){
			goToPage()
		}

	},__("Form Menu"));
}

// function that unverifies and unvalidates the form
function unverify_button(){
	cur_frm.set_value("information_checked",0)
	cur_frm.set_value("verifier_name","")
	cur_frm.set_value("verifier_designation","")
	cur_frm.set_value("verification_date","")
	cur_frm.set_value("verifier_confirmation_of_inst_1","")
	cur_frm.set_value("verified","")
	cur_frm.set_value("validated","")
	cur_frm.set_value("validated_by","")
	cur_frm.set_value("verified","")
	cur_frm.set_value("information_checked_by_staff",0)
	cur_frm.set_value("information_checked_by","")
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Pending Verification")
	cur_frm.save()
}

// function that unvalidates the form
function unvalidate_button(){
	cur_frm.set_value("information_checked_by_staff",0)
	cur_frm.set_value("information_checked_by","")
	cur_frm.set_value("validated","")
	cur_frm.set_value("validated_by","")
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Pending Validation")
	cur_frm.save()
}


/*function that toogles field to hide or unhide*/
function hide_unhide_fields(frm, list_of_fields, hide_or_unhide) {
	for (var i = 0; i < list_of_fields.length; i++) {
		frm.toggle_display(list_of_fields[i], hide_or_unhide)
	}
}

// function that redirect the user to a given url
function redirect_url(new_url){
	// allow the user to view page
	window.location = new_url
}

// function that determines which section the user should see
function navigation_function(frm,list_of_sections){
	
	if(cur_frm.doc.viewing_information){
		// allow user to see all section
		hide_unhide_fields(frm, list_of_sections["All"], true)
	}else{
		// hide all the fields
		hide_unhide_fields(frm, list_of_sections["All"], false)

		// if form has a current section
		if(frm.doc.current_section){
			// unhide the current section
			frm.toggle_display(frm.doc.current_section,true)
		}else{
			frm.doc.current_section = list_of_sections["Private Sector Company"][0]
			frm.doc.next_section = list_of_sections["Private Sector Company"][1]

			// refresh form
			cur_frm.refresh()
		}

		// fill in the section page numbers
		if(frm.doc.current_section){
			// do nothing for now
			var number_of_sections = list_of_sections["Private Sector Company"].length
			var current_section_index = list_of_sections["Private Sector Company"].indexOf(cur_frm.doc.current_section)
			// fill in the section page numbers
			cur_frm.set_value("pagesection","Screen: "+String(parseInt(current_section_index)+1)+" of "+String(number_of_sections))
		}else{
			cur_frm.set_value("pagesection","Screen: 1")
		}
	}
}


// function that determines the next section the user need to fill and unhides it
function unhide_next_section(frm,steps){
	var current_section_index = list_of_sections["Private Sector Company"].indexOf(cur_frm.doc.current_section)

	var current_user_section = list_of_sections["Private Sector Company"][current_section_index]
	var next_user_section = list_of_sections["Private Sector Company"][current_section_index + steps]
	var future_user_section = list_of_sections["Private Sector Company"][current_section_index + steps+1]

	// set next sections
	if(next_user_section){
		cur_frm.set_value("previous_section",current_user_section)
		cur_frm.set_value("current_section",next_user_section)
		cur_frm.set_value("next_section",future_user_section)
	}else{
		msgprint("You Reached the End of the Form")
	}

	// set screen numbers
	
	// save the form
	cur_frm.save()
}

// function that determines the previous section the user filled and unhides it
function unhide_previous_section(frm){
	// unhide_next_field(list_of_fields)	
	var current_section_index = list_of_sections["Private Sector Company"].indexOf(frm.doc.current_section)

	var current_user_section = list_of_sections["Private Sector Company"][current_section_index - 1]
	var previous_user_section = list_of_sections["Private Sector Company"][current_section_index - 2]
	var future_user_section = list_of_sections["Private Sector Company"][current_section_index]

	// set next sections
	if(current_user_section){
		cur_frm.set_value("previous_section",previous_user_section)
		cur_frm.set_value("current_section",current_user_section)
		cur_frm.set_value("next_section",future_user_section)
	}else{
		msgprint("You Reached the Beginning of the Form")
	}

	// save the form
	cur_frm.save()
}


var check_required_organization_type_section_section = function(){
	return {status:true,steps:1}
}

var check_required_general_organizaiton_details = function(){
	// check has given all the required fields
	if(cur_frm.doc.full_name_of_the_organization && cur_frm.doc.acronym && cur_frm.doc.country_of_origin ){
		return {status:true,steps:1}	
	}else{
		frappe.throw("Please Give Values for The Fields '* Name','* Acronym','* Country of Origin (Select One)' In Order to Continue")
	}
}

var check_required_organization_address = function(){
	return {status:true,steps:1}
}

var check_required_address_of_main_office_section_section = function(){
	return {status:true,steps:1}
}

var check_required_country_director_address_section = function(){
	return {status:true,steps:1}
}

var check_required_somaliland_most_senior_executive_section = function(){
	return {status:true,steps:1}
}

var check_required_three_alternative_contact_persons_section = function(){
	return {status:true,steps:1}
}


var check_required_part_two = function(){
	return {status:true,steps:1}
}

var check_required_pillar_alignment_section = function(){
	return {status:true,steps:1}
}

var check_required_section_alignment_section = function(){
	return {status:true,steps:1}
}

var check_required_themes_alignment_section = function(){
	return {status:true,steps:1}
}

var check_required_themes_alignment_section = function(){
	return {status:true,steps:1}
}

var check_required_section_break_47 = function(){
	return {status:true,steps:1}
}

var check_required_geographical_area = function(){
	return {status:true,steps:1}
}

var check_required_attachments_section_section = function(){
	return {status:true,steps:1}
}

var check_required_verification = function(){
	return {status:true,steps:1}
}


var validation_function_per_section ={
	"organization_type_section_section":check_required_organization_type_section_section,
	"general_organizaiton_details":check_required_general_organizaiton_details,
	"organization_address":check_required_organization_address,
	"address_of_main_office_section_section":check_required_address_of_main_office_section_section,
	"country_director_address_section":check_required_country_director_address_section,
	"somaliland_most_senior_executive_section":check_required_somaliland_most_senior_executive_section,
	"three_alternative_contact_persons_section":check_required_three_alternative_contact_persons_section,
	"part_two":check_required_part_two,
	"pillar_alignment_section": check_required_pillar_alignment_section,
	"section_alignment_section": check_required_section_alignment_section,
	"themes_alignment_section": check_required_themes_alignment_section,
	"section_alignment_section":check_required_section_alignment_section,
	"section_break_47":check_required_section_break_47,
	"geographical_area":check_required_geographical_area,
	"attachments_section_section":check_required_attachments_section_section,
	"verification":check_required_verification
}

// function filter relevant fields
function filter_fields(){

	// filter organization for specific user and organization type
	if(frappe.user.has_role("Staff")){
		// filter organization based on status only as pending renewal
		cur_frm.set_query("full_name_of_the_organization", function() {
			return {
				"filters": {
					"organization_type": "Private Sector Company",
					"status":"Pending Renewal"
				}
			}
		});
	}else{
		// filter organization based on those assigned to user
		cur_frm.set_query("full_name_of_the_organization", function() {
			return {
				"filters": {
					"organization_type": "Private Sector Company",
					"user":frappe.session.user,
					"status":"Pending Renewal"
				}
			}
		});
	}

	// filter regions
	cur_frm.set_query("region", function() {
		return {
			"filters": {
				"type": "Region",
				name:["!=","Central/Hargeisa"]
			}
		}
	});

	// filter districts
	cur_frm.set_query("district", function() {
		return {
			"filters": {
				"type": "District"
			}
		}
	});

}

function exitButton(){
	
	cur_frm.add_custom_button('Exit', function(){
		
		if(frappe.user.has_role('Staff') || frappe.user.has_role('Super Admin')){
			window.location = '/desk#List/Private%20Sector%20Company%20Renewal%20Form/List'
		} else{
			window.location = '/profile_organization'
		}
	})
	
}

// function that hides or unhides certain fields based on what the user selects while filling the form
function options_based_hide_unhide(){
	if(frappe.user.has_role("Staff")){
		add_custom_buttons("Unverify Form","Unverify")
	}

	if(frappe.user.has_role("Documents Approver")){
		add_custom_buttons("Unvalidate Form","Unvalidate")
	}

	// allow all users to seee this menu items
	add_custom_buttons("Edit Form","Edit")
	add_custom_buttons("Go to screen","Go To Page")

	// COUNTRY/DEPARTMENT DIRECTOR ADDRESS SECTION
	if(cur_frm.doc.select_one_director_address){
		// unhide the address fields in the section
		cur_frm.toggle_display("directors_address", true);
		cur_frm.toggle_display("director_code", true);
		cur_frm.toggle_display("country", true);
		cur_frm.toggle_display("director_city", true);
	}else{
		// hide the address fields in the section
		cur_frm.toggle_display("directors_address", false);
		cur_frm.toggle_display("director_code", false);
		cur_frm.toggle_display("country", false);
		cur_frm.toggle_display("director_city", false);
	}

	// REGISTRATION YEARS SECTION
	if(cur_frm.doc.were_there_years_unregistered == "Yes"){
		// show the unregistered years table
		cur_frm.toggle_display("inst_unregistered_2", true);
		cur_frm.toggle_display("unregistered_years", true);
	}else{
		// hide the unregistered years table
		cur_frm.toggle_display("inst_unregistered_2", false);
		cur_frm.toggle_display("unregistered_years", false);
		// clear the unregistered  years table
		cur_frm.clear_table("unregistered_years"); 
	}

	// Hide some unnecessary fields on organization details section
	// for directors address
	if (cur_frm.doc.select_one_director_address == "Same address as Somaliland main office") {
		cur_frm.toggle_display("director_code", false)
		cur_frm.toggle_display("country", false)
		cur_frm.toggle_display("directors_address", false)
		cur_frm.toggle_display("region_most_senior", true)
		cur_frm.toggle_display("district_most_senior", true)
		cur_frm.toggle_display("area_neighborhood_most_senior", true)
		
	} else if (cur_frm.doc.select_one_director_address == "Same address as origin country main office"){
		cur_frm.toggle_display("director_code", true)
		cur_frm.toggle_display("country", true)
		cur_frm.toggle_display("directors_address", true)
		cur_frm.toggle_display("region_most_senior", false)
		cur_frm.toggle_display("district_most_senior", false)
		cur_frm.toggle_display("area_neighborhood_most_senior", false)
	} else if (cur_frm.doc.select_one_director_address == "Other") {
		cur_frm.toggle_display("directors_address", true)
		cur_frm.toggle_display("director_city", true)
		cur_frm.toggle_display("director_code", false)
		cur_frm.toggle_display("country", true)
		cur_frm.toggle_display("region_most_senior", false)
		cur_frm.toggle_display("district_most_senior", false)
		cur_frm.toggle_display("area_neighborhood_most_senior", false)
	
	}

	// approval sections
	if(frappe.user.has_role("Staff")){
		// check if form is already verified
		if(cur_frm.doc.verified == 1){
			// show all fields
			cur_frm.toggle_display("validation", true);
			// check if user has the role of documents approver
			if(frappe.user.has_role("Documents Approver")){
				cur_frm.toggle_display("approval", true);
			}else{
				cur_frm.toggle_display("approval", false);
			}
		}else{
			// hide the validation ,approval section
			cur_frm.toggle_display("validation", false);
			cur_frm.toggle_display("approval", false);
		}
	}else{
		// hide the validation ,approval section
		cur_frm.toggle_display("validation", false);
		cur_frm.toggle_display("approval", false);
	}

	if (frappe.user_roles.includes('Staff') || frappe.user_roles.includes('Super Admin')) {
		// Dont add exit button
		// exitButton()
	} else {
		exitButton()
	} 
}

// Validate email
// Email validation function

function ValidateEmail(mail){
	
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
	 {
	   return true
	 }
	   return false

  }

// Function to navigate through sections
function goToPage(){
	frappe.prompt([
		{'fieldname': 'page', 'fieldtype': 'Int', 'label': 'Go to page', 'reqd': 1}  
	],
	function(values){
		if(values.page <= list_of_sections['Private Sector Company'].length &&  values.page > 0 ){
			// set the current page
			cur_frm.set_value("previous_section",list_of_sections['Private Sector Company'][values.page -2])
			cur_frm.set_value("current_section",list_of_sections['Private Sector Company'][values.page-1])
			cur_frm.set_value("next_section",list_of_sections['Private Sector Company'][values.page ])
	
			// save the form in order to go to that page
			cur_frm.save()
	
		}else{
			msgprint("Please provide a page betweeen 1 and "+String(list_of_sections['Private Sector Company'].length))
		}
	},
	'Go to page',
	'Go'
	)
}

function make_fields_read_only(list_of_fields){
	if(frappe.user.has_role("Super Admin")){
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",0)	
		})
	}
	else if(frappe.user.has_role("Staff")){
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",1)	
		})
	}else{
		if(cur_frm.doc.verified == 1 || cur_frm.doc.viewing_information){
			// check if user has validation rights
			if(frappe.user.has_role("Super Admin")){
				list_of_fields.forEach(function(v,i){
					cur_frm.set_df_property(v,"read_only",0)	
				})
			}else{
				list_of_fields.forEach(function(v,i){
					cur_frm.set_df_property(v,"read_only",1)	
				})
			}
		}else{
			list_of_fields.forEach(function(v,i){
				cur_frm.set_df_property(v,"read_only",0)	
			})
		}
	}	
}


// function that determines if  user has privillages to Validite , approve,verify info
function check_privillages(frm,action){
	// check if doc is saved
	if(frm.doc.__islocal ? 0 : 1){
		// check privillages absed on action
		if(action == "checked"){
			// check if the user is the owner of the doc
			if(frappe.user.has_role("Staff")){
				// allow administrator to mark as checked
				cur_frm.set_value("information_checked",1)
				cur_frm.save()
			}
			else if(frm.doc.owner){
				// check if the current user is also the owner
				if(frappe.session.user == frm.doc.owner){
					// allow administrator to validate
					cur_frm.set_value("information_checked",1)
					cur_frm.save()
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}else{
				// do not do anything for now
			}

		}else if(action == "verify"){
			// check if the user is the owner of the doc
			if(frappe.user.has_role("Staff")){
				if (frm.doc.verifier_name && frm.doc.verifier_designation && frm.doc.verification_date && frm.doc.verifier_confirmation_of_inst_1) {
					// allow administrator to validate
					cur_frm.set_value("verified",1)
					cur_frm.set_value("status","Verified")
					cur_frm.save()

				} else if(frm.verifier_confirmation_of_inst_1 != 1){
					frappe.throw('You need to <b>confirm</b> that you have read and understood the instructions')
				}

				else {
					cur_frm.set_value('verified', 0);
					frappe.throw('Fill in <b>Name</b>, <b>Designation</b>, <b>Date</b> and <b>Confirm</b> fields before verifying');
				}
				
			}
			else if(frm.doc.owner){
				// check if the current user is also the owner
				if(frappe.session.user == frm.doc.owner){
					if (frm.doc.verifier_name && frm.doc.verifier_designation && frm.doc.verification_date && frm.doc.verifier_confirmation_of_inst) {
						// allow user to validate their organization
						cur_frm.set_value("verified",1);
						cur_frm.set_value("status","Verified");
						cur_frm.save();
					} else {
						cur_frm.set_value('verified', 0);
						frappe.throw('Fill in <b>Name</b>, <b>Designation</b>, <b>Date</b> and <b>Confirm</b> fields before verifying');
					}
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}else{
				if(frappe.session.user == frm.doc.created_by){
					if (frm.doc.verifier_name && frm.doc.verifier_designation && frm.doc.verification_date && frm.doc.verifier_confirmation_of_inst) {
						// allow user to validate their organization
						cur_frm.set_value("verified",1)
						cur_frm.set_value("status","Verified")
						cur_frm.save()
					} else {
						cur_frm.set_value('verified', 0)
						frappe.throw('Fill in <b>Name</b>, <b>Designation</b>, <b>Date</b> and <b>Confirm</b> fields before verifying');

					}
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}

		}else if(action == "checked_by_staff"){
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Staff")){
				// the user has privillages allow
				// check if the if its validated
				if(frm.doc.verified == 1){
					// information_checked_by_mopnd_staff
					cur_frm.set_value("information_checked_by_staff",1)
					cur_frm.set_value("information_checked_by",frappe.session.user)
					cur_frm.save()
				}else{
					cur_frm.set_value("information_checked_by_staff",0)
					cur_frm.set_value("information_checked_by","")
					frappe.throw("Form need to be Verified before Staff can Mark it as Checked")
				}	
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
		}else if(action == "validate"){
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Staff")){
				// check if staff has marked the form as checked
				if(frm.doc.information_checked_by_staff == 1){
					cur_frm.set_value("validated",1)
					cur_frm.set_value("status","Validated")
					cur_frm.set_value("validated_by",frappe.session.user)
					cur_frm.save()
				}else{
					frappe.throw("Form need to be Checked by Staff before Validation")
				}
				
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
	
		}else if(action == "approve"){
			// check if the user has the Role "Documents Approver"
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Documents Approver")){
				// the user has privillages allow
				// check if the if its validated
				if(frm.doc.validated == 1){
					cur_frm.set_value("approved",1)
					cur_frm.set_value("status","Approved")
					cur_frm.set_value("approved_by",frappe.session.user)
					cur_frm.save()
				}else{
					frappe.throw("Form need to be validated before approval")
				}
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
		}
	}else{
		frappe.throw("Please save the document first")
	}
}


// function that checks to ensure that all the required fields are
// given before an action is undertaken e.g verification, validation etc.
function check_required_fields(action){
	var return_value = true
	if(action == "verification"){
		// fields required before verification
		var verification_required_fields = [
			{"field_name":"* Country of origin","field_value":cur_frm.doc.country_of_origin}
		]

		// loop through checking if all the required fields are given
		verification_required_fields.forEach(function(required_field){
			if(required_field["field_value"]){
				// do nothing
			}else{
				return_value = false
				frappe.msgprint(`The field ${required_field['field_name']} is required before verification`)
			}
		})
	}else if (action == 'validation'){

	}else if (action == 'approval'){

	}
	
	// return the return value
	return return_value
}

/*****************************************************************************************/
// Trigger based function

// Autofill acronym field
frappe.ui.form.on("Private Sector Company Renewal Form",{
	full_name_of_the_organization:function(){
	if(cur_frm.doc.full_name_of_the_organization){
		// call the organization
		frappe.call({
			method: "frappe.client.get_list",
			args:   {
					doctype: "Organization",
					filters: {
						name:cur_frm.doc.full_name_of_the_organization
					},
			fields:["*"]
			},
			callback: function(response) {
				cur_frm.set_value("acronym",response.message[0]["acronym"])
				cur_frm.set_value("country_of_origin",response.message[0]["country_of_origin"])
				// save form so that it pulls all data from existing form
				cur_frm.save()
			} 
		})
	} else{
		cur_frm.set_value("acronym","")
		cur_frm.set_value("country_of_origin", "")
	}
  }
});

frappe.ui.form.on('Private Sector Company Renewal Form', {
	refresh: function(frm) {
		navigation_function(frm,list_of_sections)
		filter_fields()
		options_based_hide_unhide()
		
		// make fields read only based on role and status
		make_fields_read_only(list_of_all_fields)
	}
});

frappe.ui.form.on('Private Sector Company Renewal Form', {
	after_save: function(frm) {
		// check if user is not part of the admin
		if(frappe.user.has_role("Staff") || frappe.user.has_role("Super Admin") || frappe.user.has_role("Documents Approver")){
			// do no do anything
		}else{
			// check if the user is a basic user and the status is verified
			if(frappe.user.has_role("Organization Login")){
				if(cur_frm.doc.status == "Verified"){
					// redirect to profile page
					setTimeout(function(){redirect_url('/profile_organization');}, 950);
				}
			}
		}
	}
});

cur_frm.cscript.custom_refresh = function() {
    if(frappe.user.has_role("Staff")) {
		// show button
    }else{
		cur_frm.appframe.buttons.Menu.remove();
	}
}

/*****************************************************************************************/
// Address of main office in Somaliland section

// function called when  the region field clicked under Navigation Section
frappe.ui.form.on("Private Sector Company Renewal Form", "region", function(frm){
	if(cur_frm.doc.region){
		// check if a district is already given
		if(cur_frm.doc.district){
			// check if district is within region
			frappe.call({
				method: "frappe.client.get_list",
				args: 	{
						doctype: "Administrative Unit",
						filters: {
							name:frm.doc.district
						},
				fields:["*"]
				},
				callback: function(response) {	
					if(response.message[0]["parent_administrative_unit"] == cur_frm.doc.region){
						// this is correct
					}else{
						// clear the field of district
						cur_frm.set_value("district","")
					}
				}
			})

		}else{
			// set query
			cur_frm.set_query("district", function() {
				return {
					"filters": {
						"type": "District",
						"parent_administrative_unit":cur_frm.doc.region
					}
				}
			});

		}
	}
})

// function called when  the region field clicked under Navigation Section
frappe.ui.form.on("Private Sector Company Renewal Form", "district", function(frm){
	// check if a district is already given
	if(cur_frm.doc.region){
		// check if district is within region
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Administrative Unit",
					filters: {
						name:frm.doc.district
					},
			fields:["*"]
			},
			callback: function(response) {	
				if(response.message[0]["parent_administrative_unit"] == cur_frm.doc.region){
					// this is correct
				}else{
					var district_name_holder = cur_frm.doc.district
					// clear the field of district
					cur_frm.set_value("district","")
					frappe.throw("'"+district_name_holder+"' is not a '"+cur_frm.doc.region +"' District")
					
				}
			}
		})

	}else{
		// pass
	}
})


/*****************************************************************************************
SOMALILAND MOST SENIOR EXECUTIVE */
// function called when  the '* Country Code' is filled in the SOMALILAND MOST SENIOR EXECUTIVE 
frappe.ui.form.on("Private Sector Company Renewal Form", "country_code_senior_most_executive", function(frm){
	if(frm.doc.country_code_senior_most_executive && frm.doc.director_tel_no){
		// combine the code and telephone into complete telephone number
		var full_telephone_number_director = frm.doc.country_code_senior_most_executive+String(frm.doc.director_tel_no)
		cur_frm.set_value("complete_senior_tel_no",full_telephone_number_director)
	}
})

// function called when  the '* Tel No' is filled in the SOMALILAND MOST SENIOR EXECUTIVE 
frappe.ui.form.on("Private Sector Company Renewal Form", "director_tel_no", function(frm){
	if(frm.doc.country_code_senior_most_executive && frm.doc.director_tel_no){
		// combine the code and telephone into complete telephone number
		var full_telephone_number_director = frm.doc.country_code_senior_most_executive+String(frm.doc.director_tel_no)
		cur_frm.set_value("complete_senior_tel_no",full_telephone_number_director)
	}
})

frappe.ui.form.on("Private Sector Company Renewal Form", "email_2", function(frm){
	//check if email_2 has been filled
	if(cur_frm.doc.email_2){
		//validte the email
		if(ValidateEmail(cur_frm.doc.email_2)){
			//allow the user to contineu
		}else{
			cur_frm.set_value("email_2","")
			frappe.throw("Please enter a valid email")
		}
	}else{
		//do not anything
	}
})



/*****************************************************************************************
COUNTRY/DEPARTMENT DIRECTOR ADDRESS SECTION */

// function called when  the '* Office Address (Select One)' is filled in the COUNTRY/DEPARTMENT DIRECTOR ADDRESS SECTION 
frappe.ui.form.on("Private Sector Company Renewal Form", "select_one_director_address", function(frm){
	if(cur_frm.doc.select_one_director_address == "Same address as origin country main office"){
		//clear fields
		cur_frm.set_value("directors_address","")
		cur_frm.set_value("director_city","")
		cur_frm.set_value('director_code', "")
		cur_frm.set_value("directors_address","")
		cur_frm.set_value("director_city","")
		cur_frm.set_value("region_most_senior", "")
		cur_frm.set_value("district_most_senior", "")
		cur_frm.set_value("area_neighborhood_most_senior", "")
		cur_frm.set_value("country", "")

		// autofill fields with address of origin country
		cur_frm.set_value("directors_address",cur_frm.doc.address)
		cur_frm.set_value("director_city",cur_frm.doc.city)
		cur_frm.set_value("director_code", cur_frm.doc.postcode)
		cur_frm.set_value("country", cur_frm.doc.origin_country)


	}else if(cur_frm.doc.select_one_director_address == "Same address as Somaliland main office"){
		// autofill fields with address of main office
		//clear fields
		cur_frm.set_value("directors_address","")
		cur_frm.set_value("director_city","")
		cur_frm.set_value('director_code', "")
		cur_frm.set_value("directors_address","")
		cur_frm.set_value("director_city","")
		cur_frm.set_value("region_most_senior", "")
		cur_frm.set_value("district_most_senior", "")
		cur_frm.set_value("area_neighborhood_most_senior", "")
		cur_frm.set_value("country", "")


		// autofill fields with address of origin country
		cur_frm.set_value("area_neighborhood_most_senior", cur_frm.doc.area_neighborhood)
		cur_frm.set_value("director_city",cur_frm.doc.town)
		cur_frm.set_value("region_most_senior", cur_frm.doc.region)
		cur_frm.set_value("district_most_senior", cur_frm.doc.district )

	} else if(cur_frm.doc.select_one_director_address == "Other"){
		cur_frm.set_value("directors_address","")
		cur_frm.set_value("director_city","")
		cur_frm.set_value('director_code', "")
		cur_frm.set_value("directors_address","")
		cur_frm.set_value("director_city","")
		cur_frm.set_value("region_most_senior", "")
		cur_frm.set_value("district_most_senior", "")
		cur_frm.set_value("area_neighborhood_most_senior", "")
		cur_frm.set_value("country", "")

	}
	
	
	// refresh the form so as to hide/unhide fields based on selected option
	cur_frm.refresh()

})

/*****************************************************************************************
PILLAR ALIGNMENT SECTION */

// function called when  the confirm_pillars_check field clicked under PILLAR ALIGNMENT SECTION
frappe.ui.form.on("Private Sector Company Renewal Form", "confirm_pillars_check", function(frm){
	// check if the user has selected any pillar
	var pillar_selected = false

	if(frm.doc.confirm_pillars_check){
		$.each(frm.doc.pillar_alignment_table, function(i,v){
			if(v["yes"]){
				pillar_selected = true
			}else{
				// do nothing
			}
		});
	
		if(pillar_selected){
			// allow user to continues
		}else{
			// check if there is any pillar in the table
			if(frm.doc.pillar_alignment_table.length == 0){
				// do nothing
			}else{
				cur_frm.set_value("confirm_pillars_check","")
				frappe.throw("You Have Not Ticked Any Pillar In The Table"+"<hr>"+"Please Tick 'Yes' Against Pillars That The Organization Contributes to In Order to Continue")
			}	
		}
	}
})


/*****************************************************************************************
SECTION ALIGNMENT SECTION */

// function called when  the confirm_sectors_check field clicked under SECTION ALIGNMENT SECTION
frappe.ui.form.on("Private Sector Company Renewal Form", "confirm_sectors_check", function(frm){
	// check if the user has selected any pillar
	var selectors_selected = false

	if(frm.doc.confirm_sectors_check){
		$.each(frm.doc.section_alignment_table, function(i,v){
			if(v["yes"]){
				selectors_selected = true
			}else{
				// do nothing
			}
		});
	
		if(selectors_selected){
			// allow user to continues
		}else{
			// check if there is any sector in the table
			if(frm.doc.section_alignment_table.length == 0){
				// do nothing
			}else{
				cur_frm.set_value("confirm_sectors_check","")
				frappe.throw("You Have Not Ticked Any Sector In The Table"+"<hr>"+"Please Tick 'Yes' Against Sectors That The Organization Contributes to In Order to Continue")
			}
		}
	}
})

/*****************************************************************************************
THEMES ALIGNMENT SECTION */

// function called when  the confirm_themes_check field clicked under THEMES ALIGNMENT SECTION
frappe.ui.form.on("Private Sector Company Renewal Form", "confirm_themes_check", function(frm){
	// check if the user has selected any pillar
	var themes_selected = false

	if(frm.doc.confirm_themes_check){
		$.each(frm.doc.themes_alignment_table, function(i,v){
			if(v["yes"]){
				themes_selected = true
			}else{
				// do nothing
			}
		});
	
		if(themes_selected){
			// allow user to continues
		}else{
			// check if there is any themes in the table
			if(frm.doc.themes_alignment_table.length == 0){
				// do nothing
			}else{
				cur_frm.set_value("confirm_themes_check","")
				frappe.throw("You Have Not Ticked Any Themes In The Table"+"<hr>"+"Please Tick 'Yes' Against Themes That The Organization Contributes to In Order to Continue")
			}
		}
	}
})

/******************************************************************************************/
frappe.ui.form.on("Private Sector Company Renewal Form", "view_all_information", function(frm){
	cur_frm.set_value("viewing_information",1)
	cur_frm.save()
})

frappe.ui.form.on("Private Sector Company Renewal Form", "edit_information", function(frm){
	// check if the form is verified
	if(cur_frm.doc.verified){
		frappe.throw("You cannot edit a document that is already verified,contact admin for assistance")
	}else{
		cur_frm.set_value("viewing_information",0)
		var current_list_of_sections = list_of_sections['Private Sector Company']
		cur_frm.set_value("current_section",current_list_of_sections[current_list_of_sections.length - 1])
		cur_frm.save()
	}
})

frappe.ui.form.on("Private Sector Company Renewal Form", "mark_information_as_checked", function(frm){
	check_privillages(frm,"checked")
})

frappe.ui.form.on("Private Sector Company Renewal Form", "user_verify", function(frm){
	// check if infomation is marked as checked
	if(cur_frm.doc.information_checked){
		var required_fields_status = check_required_fields('verification')
		// first check  required fields are given
		if(required_fields_status){
			frappe.confirm(
				"Please ensure that all the information you have given is correct before verifying <hr> \
				if you are sure you are ready to verify click <b>Yes</b> otherwise click <b>No</b>",
				function(){
					check_privillages(frm,"verify")	
					cur_frm.set_value('viewing_information',1)
				},
				function(){
					// do nothing since the user is not sure
				}
			)	
		}else{
			cur_frm.set_value('status','Pending Verification')
			cur_frm.set_value('verified',0)	
		}	
	}else{
		frappe.throw("Please mark the information as checked first")
	}
})

frappe.ui.form.on("Private Sector Company Renewal Form", "mark_information_as_checked_by_staff", function(frm){
	check_privillages(frm,"checked_by_staff")
})


frappe.ui.form.on("Private Sector Company Renewal Form", "user_validate", function(frm){
	check_privillages(frm,"validate")
})

frappe.ui.form.on("Private Sector Company Renewal Form", "approve", function(frm){
	check_privillages(frm,"approve")
})



/*****************************************************************************************
Navigation Section */

// function called when  the Previous Button field clicked under Navigation Section
frappe.ui.form.on("Private Sector Company Renewal Form", "previous_section_button", function(frm){
	// go to the next section	
	unhide_previous_section(frm)
})

// function called when  the Save and Continue field clicked under Navigation Section
frappe.ui.form.on("Private Sector Company Renewal Form", "save_and_continue", function(frm){ 

	// ensure to requirements for the section are complete
	var section_function = validation_function_per_section[frm.doc.current_section] 
	if(section_function()["status"]){
		unhide_next_section(cur_frm,section_function()["steps"])
	}

})

// Function called on clicking go to screen button
frappe.ui.form.on("Private Sector Company Renewal Form", "go_to_page", function(frm){
	// call the go to page function
	goToPage()
});