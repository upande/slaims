# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import datetime

class AnnualPlanForm(Document):
	'''
	This is the Annual Plan Form Controller Class
	'''
	def validate(self):
		# check if required fields are given
		check_required_fields(self)

		# check if the project title is selected
		if self.project_title:
			pass
		else:
			frappe.throw("You have not selected a Project")
		
		# check validation requirements
		check_year_validation(self)

		# upgrade the status for imported documents
		if self.imported:
			upgrade_status_for_imported_documents(self)

	def on_update(self):
		# map data to main doctype
		map_data_to_organization(self)

def check_required_fields(self):
	'''
	Funtion that checks that all the required field are
	Given 
	'''
	# fields required before saving
	required_fields_before_saving = [
		{"field_name":"Project","field_value":self.project_title,"section":"General Information"},
		{"field_name":"Full Name of the Organization ","field_value":self.full_name_of_the_organization,"section":"General Information"},
		{"field_name":"Organization Acronym","field_value":self.organization_acronym,"section":"General Information"},
	]
	# loop through the list
	loop_through_required_list(self,required_fields_before_saving)

def loop_through_required_list(self,list_of_required_fields):
	'''
	Function that loops through a list of given 
	fields and determine if any of them is missing
	'''
	# looping throught fields
	for field in list_of_required_fields:
		if field["field_value"]:
			# pass because field is available
			pass
		else:
			frappe.throw("You have not filled required field '{}' in {}".format(field["field_name"],field['section']))

def check_if_amounts_add_up(self):
	'''
	Function that checks that the total in the regional
	distribution and regional distribution is equal to the 
	annul plan amount
	'''
	# check totals for sectors
	if self.total:
		# check if amount is equal to the annual plan amount
		if self.total == self.annual_budget_amount:
			pass
		else:
			frappe.throw("The amount given in the sectors ditribution table is not equal to the \
				total plan amount")
	else:
		pass

	# check totals for regions
	if self.total_regions:
		# check if amount is equal to the annual plan amount
		if self.total_regions == self.annual_budget_amount:
			pass
		else:
			frappe.throw("The amount given in the regions ditribution table is not equal to the \
				total plan amount")
	else:
		pass


def check_year_validation(self):
	'''
	Function that checks that all the validation requirements 
	before saving
	'''
	# if the data is imported ignore this check
	if self.imported == 1:
		pass
	else:
		# check correctness of the year of registration
		selected_project_doc = frappe.get_doc("Project",self.project_title)
		start_year = selected_project_doc.project_start_date.year
		end_year = selected_project_doc.project_end_date.year
		plan_year = int(self.annual_plan_year)

		# check if selected year is within the project's duration
		if plan_year >= start_year and  plan_year <= end_year:
			# selected years is within the project's duration
			pass				
		else:
			frappe.throw("The selected annual plan year is not within the projects duration")

def upgrade_status_for_imported_documents(self):
	if self.status == "Admin Verified":
		# progress the form to Verified for imported data
		self.status = "Verified"

	elif self.status == "Verified":
		# progress the form to Validated for imported data
		self.status = "Validated"

	elif self.status == "Validated":
		# progress the form to Approved for imported data
		self.status = "Approved"


def check_if_plan_exists(self):
	'''
	Checks if an plan exists else create one
	'''
	# contruct the name
	plan_form_name = self.project_title+"-"+self.organization_acronym+"-("+str(self.annual_plan_year)+")-AP"
	list_of_plan = frappe.get_list("Annual Plan",
		fields=["*"],
		filters = {
			"name":plan_form_name
		})
	

	# frappe.throw("pause")
	if len(list_of_plan) == 1:
		# create an organization
		doc = frappe.get_doc("Annual Plan",plan_form_name)
		doc.status = self.status
		doc.save(ignore_permissions = True)

	elif len(list_of_plan) == 0:
		# create an organization
		doc = frappe.get_doc({"doctype":"Annual Plan"})
		doc.name = plan_form_name
		doc.project_title = self.project_title
		doc.full_name_of_the_organization = self.full_name_of_the_organization
		doc.organization_acronym = self.organization_acronym
		doc.status = self.status
		doc.starting_on = self.starting_on
		doc.finishing_on = self.finishing_on
		doc.annual_plan_year = self.annual_plan_year
		doc.annual_budget_amount = self.annual_budget_amount
		
		# save the document
		doc.insert(ignore_permissions=True)

def map_data_to_organization(self):
	'''
	Function that updates the details of the plan form
	to the annual plan form once the form is approved
	'''
	if self.status == "Verified":
		check_if_plan_exists(self)

	if self.status == "Validated":
		# update the status in  the main doctype on validate
		plan_form_name = self.project_title+"-"+self.organization_acronym+"-("+str(self.annual_plan_year)+")-AP"
		plan_doc = frappe.get_doc("Annual Plan",plan_form_name)
		plan_doc.status = "Validated"
		# save the updated document
		plan_doc.save()

	# now update the other remaining fields
	# if approved update details to linked organization
	if(self.status == "Approved"):
		plan_form_name = self.project_title+"-"+self.organization_acronym+"-("+str(self.annual_plan_year)+")-AP"
		# get related report
		doc = frappe.get_doc("Annual Plan",plan_form_name)
		
		# # ORGANIZATION TYPE SECTION
		doc.status = self.status
		doc.annual_budget_amount = self.annual_budget_amount

		# general_information_section
		doc.starting_on = self.starting_on
		doc.finishing_on = self.finishing_on
		doc.annual_plan_year = self.annual_plan_year

		# project_annual_budget_section
		doc.annual_budget_amount = self.annual_budget_amount
		doc.direct_implementation_costs = self.direct_implementation_costs
		doc.operational_overhead = self.operational_overhead


		# government_institution_overseeing_the_project_section
		doc.government_institution_overseeing_the_project = []
		for institution in self.government_institution_overseeing_the_project:
			doc.append("government_institution_overseeing_the_project", {
				"mda_name": institution.mda_name,
			})
		
		# ndp_pillars_budget_distribution_section
		doc.ndp_pillar_alignment = []
		for sector in self.ndp_pillar_alignment:
			doc.append("ndp_pillar_alignment", {
				"sector": sector.sector,
				"amount":sector.amount
			})

		doc.total = self.total

		# regional_budget_distribution_section
		doc.regional_distribution_table = []
		for region in self.regional_distribution_table:
			doc.append("regional_distribution_table", {
				"region": region.region,
				"amount":region.amount
			})

		doc.total_regions = self.total_regions

		# attachements section
		doc.detailed_annual_plan = self.detailed_annual_plan

		# now save the document
		doc.save(ignore_permissions=True)

