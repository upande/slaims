// Copyright (c) 2019, upande and contributors
// For license information, please see license.txt


var list_of_fields = [
	"annual_budget_amount","annual_plan_year",
	"direct_implementation_costs","operational_overhead",
	"government_institution_overseeing_the_project","ndp_pillar_alignment",
	"regional_distribution_table",

	// Attachment Section
	"detailed_annual_plan",

	// Verification and approval section
	"verifier_name","verifier_designation",
]

// function that filter the route to be selected from territory
function filter_organization(frm){
	if(frappe.user.has_role("Staff")){
		// allow user to see all projects
		filter_project(frm,null,"Staff")
	}else{
		// find the basic users organization
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Organization",
					filters: {
						user:frappe.session.user
					},
			fields:["*"]
			},
			callback: function(response) {
				if(response.message.length > 0){
					// user has an organization set organization name
					filter_project(frm,response.message[0].name,"Organization Login")
				}else{
					// user has not registered an organization
					frappe.msgprint("You have not registered an organization")
					location.replace("/profile_organization")
				}
			}
		})
	}
}

// trying to filter using child table
// function that filter the projects visible to users
function filter_project(frm,organization_name,user_role){
	frappe.call({
		method: "slaims.custom_methods.check_if_project_is_active",
		args: 	{
			organization_name:organization_name,
			user_role:user_role
		},
		callback: function(response) {
			if(response.message.length > 0){
				// set the options on the select field
				frm.set_df_property('select_project', 'options', response.message);
			}else{
				frappe.msgprint("You do not have any active Project Reporting Obligations")
			}	
		}
	})
}

// function that asks the user is they are creating their own organization
function check_if_creater_is_owner(frm){

	frappe.confirm(
		"If you are creating a plan for your organization; click'Yes', If Creating A Plan for Another Organization Click 'No'",
		function(){
			// user chose yes check the created by Owner Field
			frm.set_value("created_by_owner","Yes")
			frm.set_value("plan_owner",frappe.session.user)
		},
		function(){
			// user chose no
			frm.set_value("created_by_owner","No")
			frm.set_value("created_by",frappe.session.user)
		}
	)
}


// function that determines if  user has privillages to Validite , approve,verify info
function check_privillages(frm,action){
	// check if doc is saved
	if(frm.doc.__islocal ? 0 : 1){
		// check privillages absed on action
		if(action == "checked"){
			// check if the user is the owner of the doc
			if(frappe.user.has_role("Staff")){
				// allow administrator to mark as checked
				cur_frm.set_value("checked",1)
				cur_frm.save()
			}
			else if(frm.doc.owner){
				// check if the current user is also the owner
				if(frappe.session.user == frm.doc.owner){
					// allow administrator to validate
					cur_frm.set_value("checked",1)
					cur_frm.save()
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}else{
				// do not do anything for now
			}

		}else if(action == "verify"){
			// check if the user is the owner of the doc
			if(frappe.user.has_role("Staff")){
				// allow administrator to validate
				if (frm.doc.verifier_name && frm.doc.verifier_designation && cur_frm.doc.verification_date) {
					// allow administrator to validate
					cur_frm.set_value("verified",1)
					cur_frm.set_value("status","Verified")
					cur_frm.set_value("verified_by",frappe.session.user)
					cur_frm.save()
				} else {
					cur_frm.set_value('verified', 0);
					frappe.throw('Fill in <b>Name</b>,<b>Designation</b> and <b>Date</b> fields before verifying');
				}
			}
			else if(frm.doc.owner){
				// check if the current user is also the owner
				if(frappe.session.user == frm.doc.owner){
					// allow user to validate their organization
					if (frm.doc.verifier_name && frm.doc.verifier_designation && cur_frm.doc.verification_date) {
						// allow administrator to validate
						cur_frm.set_value("verified",1)
						cur_frm.set_value("status","Verified")
						cur_frm.set_value("verified_by",frappe.session.user)
						cur_frm.save()
					} else {
						cur_frm.set_value('verified', 0);
						frappe.throw('Fill in <b>Name</b>,<b>Designation</b> and <b>Date</b> fields before verifying');
					}
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}else{
				if(frappe.session.user == frm.doc.created_by){
					// allow user to validate their organization
					if (frm.doc.verifier_name && frm.doc.verifier_designation && cur_frm.doc.verification_date ) {
						// allow administrator to validate
						cur_frm.set_value("verified",1)
						cur_frm.set_value("status","Verified")
						cur_frm.set_value("verified_by",frappe.session.user)
						cur_frm.save()
					} else {
						cur_frm.set_value('verified', 0);
						frappe.throw('Fill in <b>Name</b>,<b>Designation</b> and <b>Date</b> fields before verifying');
					}
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}

		}else if(action == "checked_by_staff"){
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Staff")){
				// the user has privillages allow
				// check if the if its validated
				if(frm.doc.verified == 1){
					// information_checked_by_mopnd_staff
					cur_frm.set_value("information_checked_by_staff",1)
					cur_frm.set_value("information_checked_by",frappe.session.user)
					cur_frm.save()
				}else{
					cur_frm.set_value("information_checked_by_staff",0)
					cur_frm.set_value("information_checked_by","")
					frappe.throw("Form need to be Verified before Staff can Mark it as Checked")
				}	
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
		}else if(action == "validate"){
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Staff")){
				// check if staff has marked the form as checked
				if(frm.doc.information_checked_by_staff == 1){
					cur_frm.set_value("validated",1)
					cur_frm.set_value("status","Validated")
					cur_frm.set_value("validated_by",frappe.session.user)
					cur_frm.save()
				}else{
					frappe.throw("Form need to be Checked by Staff before Validation")
				}
				
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
	
		}else if(action == "approve"){
			// check if the user has the Role "Documents Approver"
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Documents Approver")){
				// the user has privillages allow
				// check if the if its validated
				if(frm.doc.validated == 1){
					cur_frm.set_value("approved",1)
					cur_frm.set_value("status","Approved")
					cur_frm.set_value("approved_by",frappe.session.user)
					cur_frm.save()
				}else{
					frappe.throw("Form need to be validated before approval")
				}
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
		}
	}else{
		frappe.throw("Please save the document first")
	}
}

// function that sets custom buttons
function add_custom_buttons(button_name,action){
	cur_frm.add_custom_button(__(button_name), function(){

		// only show this buttons to users with administrative privillages
		if(action=="Unverify"){
			unverify_button()
		}else if (action == "Unvalidate"){
			unvalidate_button()
		}else if(action=="Edit"){
			// check user is allowed to edit
			if(cur_frm.doc.verified){
				frappe.throw("You cannot edit a form that is already verified \
				,contact the admin for assistance")
			}else{
				// make the forms readble for user
				cur_frm.set_value("viewing_information",0)
				cur_frm.set_value("current_section",list_of_sections[0])
				cur_frm.save()
			}
		}else if(action = "Go To Page"){
			goToPage()
		}

	},__("Form Menu"));
}

// function that unverifies and unvalidates the form
function unverify_button(){
	cur_frm.set_value("checked",0)
	cur_frm.set_value("verifier_name","")
	cur_frm.set_value("verifier_designation","")
	cur_frm.set_value("verification_date","")
	cur_frm.set_value("verified","")
	cur_frm.set_value("validated","")
	cur_frm.set_value("validated_by","")
	cur_frm.set_value("verified","")
	cur_frm.set_value("information_checked_by_staff",0)
	cur_frm.set_value("information_checked_by","")
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Pending Verification")
	cur_frm.save()
}

// function that unvalidates the form
function unvalidate_button(){
	cur_frm.set_value("information_checked_by_staff",0)
	cur_frm.set_value("information_checked_by","")
	cur_frm.set_value("validated","")
	cur_frm.set_value("validated_by","")
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Pending Validation")
	cur_frm.save()
}


function exitButton(){
	
	cur_frm.add_custom_button('Exit', function(){
		
		if(frappe.user.has_role('Staff') || frappe.user.has_role('Super Admin')){
			window.location = '/desk#List/Annual%20Plan%20Form/List'
		} else{
			window.location = '/profile_plan'
		}
	})
}

function option_based_hide_unhide(){
	// adding buttons
	if(frappe.user.has_role("Staff")){
		add_custom_buttons("Unverify Form","Unverify")
	}

	if(frappe.user.has_role("Documents Approver")){
		add_custom_buttons("Unvalidate Form","Unvalidate")
	}

	if(frappe.user.has_role("Staff")){
		// check if form is already verified
		if(cur_frm.doc.verified == 1){
			// hide the select project and organizaiton fields
			cur_frm.toggle_display("select_project", false);
			cur_frm.toggle_display("select_organization", false);
			// show all fields
			cur_frm.toggle_display("plan_validation_approval_section", true);
			// check if user has the role of documents approver
			if(frappe.user.has_role("Documents Approver")){
				cur_frm.toggle_display("plan_approval_section", true);
			}else{
				cur_frm.toggle_display("plan_approval_section", false);
			}
		}else{
			// hide the select project and organizaiton fields
			cur_frm.toggle_display("select_project", true);
			cur_frm.toggle_display("select_organization", true);
			// hide the validation ,approval section
			cur_frm.toggle_display("plan_validation_approval_section", false);
			cur_frm.toggle_display("plan_approval_section", false);
		}
	}else{
		// check if form is already verified
		if(cur_frm.doc.verified == 1){
			// hide the select project and organizaiton fields
			cur_frm.toggle_display("select_project", false);
			cur_frm.toggle_display("select_organization", false);
			
		}else{
			// hide the select project and organizaiton fields
			cur_frm.toggle_display("select_project", true);
			cur_frm.toggle_display("select_organization", true);
		}
		// hide the validation ,approval section
		cur_frm.toggle_display("plan_validation_approval_section", false);
		cur_frm.toggle_display("plan_approval_section", false);
	}

	// Add exit button to forms of basic users.
	if (frappe.user_roles.includes('Staff') || frappe.user_roles.includes('Super Admin')) {
		// Dont add exit button
		// exitButton()
	} else {
		exitButton()
	}
}

function make_fields_read_only(list_of_fields){
	if(frappe.user.has_role("Super Admin")){
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",0)	
		})

	}
	else if(frappe.user.has_role("Staff")){
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",1)	
		})

	}else{
		// make the other fieldds read only as well
		if(cur_frm.doc.verified == 1){
			// check if user has validation rights
			if(frappe.user.has_role("Staff")){
				list_of_fields.forEach(function(v,i){
					cur_frm.set_df_property(v,"read_only",0)	
				})
			}else{
				list_of_fields.forEach(function(v,i){
					cur_frm.set_df_property(v,"read_only",1)	
				})
			}
		}else{
			list_of_fields.forEach(function(v,i){
				cur_frm.set_df_property(v,"read_only",0)	
			})
		}
	}	
}

// function that redirect the user to a given url
function redirect_url(new_url){
	// allow the user to view page
	window.location = new_url
}


/* end of the general functions section
// =================================================================================================
/* This section  contains functions that are triggered by the form action refresh or
reload to perform various action*/

frappe.ui.form.on('Annual Plan Form', {
	onload: function(frm) {
		// deny users ability to add rows
		cur_frm.get_field("government_institution_overseeing_the_project").grid.cannot_add_rows = true;
		cur_frm.get_field("ndp_pillar_alignment").grid.cannot_add_rows = true;
		cur_frm.get_field("regional_distribution_table").grid.cannot_add_rows = true;
	}
})

frappe.ui.form.on('Annual Plan Form', {
	refresh: function(frm) {
		// filter the organizations
		filter_organization(frm)

		// make fields readonly based on role and status
		make_fields_read_only(list_of_fields)

		// hide / unhide fields based on selected options
		option_based_hide_unhide()

		// deny user ability to add rows in some tables
		cur_frm.get_field("government_institution_overseeing_the_project").grid.cannot_add_rows = true;
		cur_frm.get_field("ndp_pillar_alignment").grid.cannot_add_rows = true;
		cur_frm.get_field("regional_distribution_table").grid.cannot_add_rows = true;
	}
});


frappe.ui.form.on('Annual Plan Form', {
	after_save: function(frm) {
		// check if user is not part of the admin
		if(frappe.user.has_role("Staff") || frappe.user.has_role("Super Admin") || frappe.user.has_role("Documents Approver")){
			// do no do anything
		}else{
			// check if the user is a basic user and the status is verified
			if(frappe.user.has_role("Organization Login")){
				if(cur_frm.doc.status == "Verified"){
					// redirect to profile page
					// redirect_url('/profile_project')
					setTimeout(function(){redirect_url('/profile_plan');}, 950);
				}
			}
		}
	}
});


/*******************************************************************************************
Top Section */

/*******************************************************************************************
General Details Section */
// set the project title from the selected project
frappe.ui.form.on("Annual Plan Form", "select_project", function(frm){ 
	/*everytime a project is selected in clear-out the crucial fields so they can be 
	filled again*/
	cur_frm.set_value("select_organization","")
	cur_frm.set_value("project_title","")
	
	// get a list of implementing organizations linked to the the project
	if(cur_frm.doc.select_project){
		if(frappe.user.has_role("Staff")){
			// get linked organizations
			frappe.call({
				method: "slaims.custom_methods.get_project_implementors",
				args: 	{
					project_name:cur_frm.doc.select_project,
					organization_name:null
				},
				callback: function(response) {
					if(response.message.length > 0){
						// set the options on the select field
						frm.set_df_property('select_organization', 'options', response.message);
					}else{
						cur_frm.set_value('select_organization','')
						cur_frm.set_value('select_organization','')
						frappe.msgprint("No Reporting Organization are assigned to chosen project")
					}	
				}
			})

		}else{
			// get organization_name and acronym
			frappe.call({
				method: "frappe.client.get_value",
				args: 	{
						doctype: "Organization",
						filters: {
							user:frappe.session.user
						},
				fieldname:["name"]
				},
				callback: function(response) {
					var current_user_organization = response.message.name
					// get linked organizations
					frappe.call({
						method: "slaims.custom_methods.get_project_implementors",
						args: 	{
							project_name:cur_frm.doc.select_project,
							organization_name:current_user_organization
						},
						callback: function(response) {
							if(response.message.length > 0){
								// set the options on the select field
								frm.set_df_property('select_organization', 'options', response.message);
							}else{
								cur_frm.set_value('select_organization','')
								cur_frm.set_value('select_organization','')
								frappe.msgprint("No Reporting Organization are assigned to chosen project")
							}	
						}
					})
				}
			})
		}
	}else{
		// clear fields
		cur_frm.set_value("select_organization","")
	}
})

// set the project title from the selected project
frappe.ui.form.on("Annual Plan Form", "select_organization", function(frm){ 
	// first clear the project field
	cur_frm.set_value("project_title","")
	// now set some values
	if(cur_frm.doc.select_organization && cur_frm.doc.select_project){
		// get organization_name and acronym
		frappe.call({
			method: "frappe.client.get_value",
			args: 	{
					doctype: "Organization",
					filters: {
						name:cur_frm.doc.select_organization
					},
			fieldname:["name","acronym"]
			},
			callback: function(response) {
				// set project title name
				cur_frm.set_value("project_title",cur_frm.doc.select_project)
				cur_frm.set_value("full_name_of_the_organization",response.message.name)
				cur_frm.set_value("organization_acronym",response.message.acronym)
				cur_frm.refresh_fields();
			}
		})		
	}
})

// function that checks the project's end date
frappe.ui.form.on("Annual Plan Form", "project_title", function(frm){
	// if a project has been selected
	if(frm.doc.project_title){
		// check the project document
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Project",
					filters: {
						name:frm.doc.project_title
					},
			fields:["*"]
			},
			callback: function(response) {
				if(response.message.length < 1){
					frappe.throw("An Error Occured While Fetching the Selected Project")
				}else{
					// get the project
					var current_project = response.message[0]
					// check if th project is complete
					if(current_project['status'] == 'Complete'){
						// clear the project title field
						cur_frm.set_value("select_project","")
						frappe.throw("The selected project is Completed/Finished")
					}
					
					// get the project amount the organization should report for
					frappe.call({
						method: "frappe.client.get_value",
						args: 	{
								parent:"Project",
								doctype: "Destination Implementing Reporting",
								fieldname:['implementing_organization','amount'],
								filters: {
									parent:cur_frm.doc.select_project,
									implementing_organization:cur_frm.doc.select_organization
								},
						fields:["*"]
						},
						callback: function(response){
							cur_frm.set_value("total_project_amount",response.message.amount)
						}
					})
					
				
					// clear the mdas table
					cur_frm.clear_table("government_institution_overseeing_the_project");
					cur_frm.refresh_fields();
					// get the mdas from the project
					frappe.call({
						method: "frappe.client.get_list",
						args: 	{
								parent:"Project Registration Form",
								doctype: "Relevant MDAs Table",
								filters: {
									parent:current_project["name"],
								},
						fields:["*"]
						},
						callback: function(response) {
							// append the results to the mdas table
							$.each(response.message, function(i,v){
								var mda_row = cur_frm.add_child("government_institution_overseeing_the_project")
								mda_row.mda_name = v.relevant_mda_per_sector
								cur_frm.refresh_fields();
							})
						}
					})
					
				
					// clear the sectors table
					cur_frm.clear_table("ndp_pillar_alignment");
					cur_frm.refresh_fields();
					// get the selected sectors
					frappe.call({
						method: "frappe.client.get_list",
						args: 	{
								parent:"Project",
								doctype: "Sector Alignment Table",
								filters: {
									parent:current_project["name"]
								},
						fields:["*"]
						},
						callback: function(response) {
							$.each(response.message,function(i,v){
								var sector_row = frm.add_child("ndp_pillar_alignment")
								sector_row.sector = v.sector
								cur_frm.refresh_fields();
							})
						}
					})

					// clear the regions distributation 
					cur_frm.clear_table("regional_distribution_table");
					cur_frm.refresh_fields();
					// get the selected region
					frappe.call({
						method: "frappe.client.get_list",
						args: 	{
								parent:"Project",
								doctype: "Project Location",
								filters: {
									parent:current_project["name"]
								},
						fields:["*"]
						},
						callback: function(response) {
							var regions_holder = []	
							$.each(response.message,function(i,v){
								if(regions_holder.includes(v.region)){
									// pass
								}else{
									// add to array
									regions_holder.push(v.region)
								}
							})

							// now add the regions to the table
							$.each(regions_holder,function(i,v){
								var region_row = frm.add_child("regional_distribution_table")
								region_row.region = v
								cur_frm.refresh_fields();
							});
						}
					})

					// get end and start dates
					cur_frm.set_value("starting_on",current_project["project_start_date"])
					cur_frm.set_value("finishing_on",current_project["project_end_date"])

				}
			}
		})
	}else{
		// clear fields
		cur_frm.set_value("full_name_of_the_organization","")
		cur_frm.set_value("organization_acronym","")
		cur_frm.set_value("total_project_amount","")
		cur_frm.set_value("starting_on","")
		cur_frm.set_value("finishing_on","")

		// clear the tables as well
		cur_frm.clear_table("government_institution_overseeing_the_project");
		cur_frm.clear_table("ndp_pillar_alignment");
		cur_frm.clear_table("regional_distribution_table");
		cur_frm.clear_table("regional_distribution_table");
		
		// refresh fields
		cur_frm.refresh_fields();
	}
});

/*******************************************************************************************
PROJECT ANNUAL BUDGET */
// function that is called when the annual_budget_amount is clicked
frappe.ui.form.on("Annual Plan Form", "annual_budget_amount", function(frm){ 

	// first check that the project title is given
	if(frm.doc.project_title){
		// check that the project amount is not more than the project amount
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Project",
					filters: {
						name:frm.doc.project_title
					},
			fields:["*"]
			},
			callback: function(response) {
				if(response.message.length < 1){
					frappe.throw("An Error Occured While Fetching Details of the Selected Project")
				}else{
					// get the project
					var current_project = response.message[0]
					
					// undo the restriction annual plan amount
					// if(current_project.total_project_amount_in_usd >= frm.doc.annual_budget_amount ){
					// 	// allow the user to set amount
					// }else{
					// 	// clear the amount fields and the distributions
					// 	cur_frm.set_value("annual_budget_amount","")
					// 	cur_frm.set_value("direct_implementation_costs","")
					// 	cur_frm.set_value("operational_overhead","")
					// 	frappe.throw("The annual plan amount cannot be greater than project amount")
					// }
				}
			}
		});
		
	}else{
		cur_frm.set_value("annual_budget_amount","")
		frappe.throw("Please Select a Project First")
	}
});

// function that is triggered when the Direct Implementation Costs fieled is filled
frappe.ui.form.on("Annual Plan Form", "direct_implementation_costs", function(frm){ 
	var operational_costs = frm.doc.annual_budget_amount - frm.doc.direct_implementation_costs
	
	if(frm.doc.annual_budget_amount >= frm.doc.direct_implementation_costs){
		// check if the correct value is already set
		if(operational_costs == frm.doc.operational_costs ){
			// do nothing
		}else{
			// set the correct value
			cur_frm.set_value("operational_overhead",operational_costs)
		}
	}else{
		cur_frm.set_value("direct_implementation_costs","")
		frappe.throw("Direct implementation costs in USD ($) cannot be greater than `Annual Budget Amount (USD)`")
	}
})

// function that is triggered when the Operational Overheads field is filled
frappe.ui.form.on("Annual Plan Form", "operational_overhead", function(frm){
	// check amount is not greater than budget amount
	if(frm.doc.annual_budget_amount >= frm.doc.operational_overhead){
		var direct_costs = frm.doc.annual_budget_amount - frm.doc.operational_overhead
		// check if the correct value is already set
		if(direct_costs == frm.doc.direct_implementation_costs ){
			// do nothing
		}else{
			// set the correct value
			cur_frm.set_value("direct_implementation_costs",direct_costs)
		}
	}else{
		cur_frm.set_value("operational_overhead","")
		frappe.throw("Operational overhead in USD ($) cannot be greater than `Annual Budget Amount (USD)`")
	}
})

/*******************************************************************************************
SECTOR AND REGIONAL BUDGET DISTRIBUTION SECTION */

// function tha calculates the total for sector distribution
frappe.ui.form.on("Pillar Budget Table", "amount", function(frm,cdt,cdn){ 
	var row = locals[cdt][cdn]

	// calculate total
	var region_details = frm.doc.ndp_pillar_alignment;
	var total_amount = 0

	$.each(region_details, function(i,v){
		if(v["amount"]){
			total_amount += v["amount"]
		}
	});
 
	// check if the total is greater than the annual budget amount
	if(total_amount > cur_frm.doc.annual_budget_amount){
		// clear out the addes amount and the total amount
		total_amount = 0
		row.amount = 0
		frappe.throw("The total amount given in each sector has exceded the total annual plan amount \
		Please add the correct amounts for each sector")
	}else{
		// allow the user to continue
	}

	// set the different values
	frm.set_value("total",total_amount)
});

// function tha calculates the total for regional distribution
frappe.ui.form.on("Regional Distribution Table", "amount", function(frm,cdt,cdn){ 
	var row = locals[cdt][cdn]

	// calculate total
	var region_details = frm.doc.regional_distribution_table;
	var total_amount = 0

	$.each(region_details, function(i,v){
		if(v["amount"]){
			total_amount += v["amount"]
		}
	});

	// check if the total is greater than the annual budget amount
	if(total_amount > cur_frm.doc.annual_budget_amount){
		// clear out the addes amount and the total amount
		total_amount = 0
		row.amount = 0
		frappe.throw("The total amount given in each region has exceded the total annual plan amount \
		Please add the correct amounts for each region")
	}else{
		// allow the user to continue
	}
 
	// set the different values
	frm.set_value("total_regions",total_amount)
});

/*******************************************************************************************
VERIFICATION/VALIDATION/APPROVAL SECTION */
// validating functions
frappe.ui.form.on("Annual Plan Form", "mark_as_checked", function(frm){ 
	check_privillages(frm,"checked")
});

frappe.ui.form.on("Annual Plan Form", "mark_information_as_checked_by_staff", function(frm){
	check_privillages(frm,"checked_by_staff")
})

// function that validates the form when the validate button is clicked
frappe.ui.form.on("Annual Plan Form", "validate_plan", function(frm){ 
	check_privillages(frm,"validate")
});

// function that validates the form when the validate button is clicked
frappe.ui.form.on("Annual Plan Form", "verify_plan", function(frm){ 
	if(cur_frm.doc.checked){
		// confirm if the user want to validate information
		frappe.confirm(
			"Please ensure that all the information you have given is correct before verifying <hr> \
			if you are sure you are ready to verify click 'Yes' otherwise click 'No'",
			function(){
				// validate the form
				check_privillages(frm,"verify")
			},
			function(){
				// do nothing since the user is not sure
			}
		)	
	}else{
		frappe.throw("Please mark the information as checked first in order to continue")
	}
});

// function that validates the form when the validate button is clicked
frappe.ui.form.on("Annual Plan Form", "approve_plan", function(frm){ 
	check_privillages(frm,"approve")
});