# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

from frappe.utils import add_days, cint, cstr, flt, getdate, rounded, date_diff, money_in_words
from frappe.model.naming import make_autoname
from frappe import msgprint, _
from frappe.utils.background_jobs import enqueue


# std lib imports
import datetime

class Inquiry(Document):
	def validate(self):
		pass
	
	def on_update(self):
		'''
		Function that runs when an inquiry is saved
		'''

		if self.status == "Submitted":
			# get day of the week
			day_of_the_week = datetime.datetime.today().weekday()

			# keys and day of the week
			list_of_days = ["monday","tuesday","wednesday","thursday","friday","saturday","sunday"]
			
			# alter filters based on the current day
			filters_defined = {}
			filters_defined[list_of_days[day_of_the_week]] = 1
				
			# get recipients from the current staff on call
			list_of_staff_on_call = frappe.get_list("Help",
				fields=["email"],
				filters = filters_defined,
				ignore_permissions = True,
			)

			# create a notification for user
			if self.saved:
				pass
			else:
				# get a list of recipients
				for staff in list_of_staff_on_call:
					# send email to these addresses
					recipient = str(staff['email'])
					message = "User {} has submitted a new inquiry".format(frappe.session.user)
					
					# send email
					send_email(self,recipient,message)

				# throw a message to the user
				frappe.msgprint("You inquiry has been submitted sucessfully <hr>\
					wait for email communication from the Ministry of Planning")
				
				# make the document as saved i.e submitted
				self.saved = 1

		elif self.status == "Resolved":
			# send email informing the user that their inquiry has been resolved
	
			# send email to these addresses
			recipient = str(self.owner)
			message = "Thank you for gettting  in touch with us ,we would like \
				to inform you that your inquiry has been resolved"

			# send email
			send_email(self,recipient,message)
		
def send_email(self,receiver,message):
	'''
	Function that sends email to given recipients
	'''
	if receiver:
		email_args = {
			"recipients": [receiver],
			"message":message,
			"subject": 'Inquiry',
			"reference_doctype": self.doctype,
			"reference_name": self.name
			}
		enqueue(method=frappe.sendmail, queue='short', timeout=300, async=True, **email_args)
	else:
		msgprint(_("Email not found"))


@frappe.whitelist(allow_guest = True)
def read_organization(user_name):
	'''
	get a list of all the organizations
	'''
	organizations = frappe.db.sql("select name from `tabOrganization` where user = '{}'".format(user_name))
	# return  {'key':'test'}
	if len(organizations) > 0:
		return organizations[0][0]
	else:
		return False

