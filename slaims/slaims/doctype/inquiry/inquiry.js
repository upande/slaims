// Copyright (c) 2019, upande and contributors
// For license information, please see license.txt

frappe.ui.form.on('Inquiry', {
	refresh: function(frm) {
		// hide the resolve button if the user does not have admin prilliages
		if(frappe.user.has_role('Staff') || frappe.user.has_role('Super Admin')) {
			cur_frm.toggle_display("resolve", true)
		}else{
			// make the fields readonly if the inquiry is submitted
			if(cur_frm.doc.status == "Submitted" || cur_frm.doc.status == "Resolved"){
				cur_frm.fields.forEach(function(l){ cur_frm.set_df_property(l.df.fieldname, "read_only", 1); })
			}
			cur_frm.toggle_display("resolve", false)
			// add user details automatically
			var user = frappe.session.user
			cur_frm.set_value('email',user)

			// get user name
			frappe.call({
				method: "frappe.client.get_list",
				args: 	{
						doctype: "User",
						filters: {
							email:user
						},
				fields:["*"]
				},
				callback: function(response) {
					cur_frm.set_value('user_name',response.message[0].full_name)
				}
			})

			// get the users organization
			frappe.call({
				method: "slaims.slaims.doctype.inquiry.inquiry.read_organization",
				args: 	{
					'user_name':frappe.session.user
				},
				callback: function(response) {
					if(response.message){
						cur_frm.set_value('organization',response.message)
					}
				}
			})
		}
	}
});


// function called when  the submit button is clicked 
frappe.ui.form.on("Inquiry", "submit", function(frm){ 
	cur_frm.set_value("status","Submitted")
	cur_frm.save()
})

// function called when  the resolve button is clicked 
frappe.ui.form.on("Inquiry", "resolve", function(frm){ 
	cur_frm.set_value("status","Resolved")
	cur_frm.save()
})