# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Organization(Document):
	
	def validate(self):
		check_registation_year(self)
		check_number_of_linked_users(self)
		pass

	def on_update(self):
		pass	

def check_registation_year(self):
	'''
	Function that checks if the organizaiton's registration is given
	in the form else add the current global year
	input:
		self - Document
	output:
		None
	'''	
	if self.current_registration_year:
		pass
	else:
		# get the current global year
		current_global_year = frappe.db.get_single_value('Global Settings','current_financial_year')
		self.current_registration_year = current_global_year
		
def check_number_of_linked_users(self):
	'''
	Ensures that an organization has only one linked user
	'''
	if self.user:
		# ensure user is not already associated with another organization
		list_of_organizations_users = frappe.get_list("Organization",
				fields=["*"],
				filters = {
					"user": self.user
			})

		if len(list_of_organizations_users) == 0:
			pass
		elif len(list_of_organizations_users) == 1:
			# check if the organization is same as self.organization
			if list_of_organizations_users[0].name == self.full_name_of_the_organization:
				pass
			else:
				frappe.throw("The user '{}' is already linked with organization '{}'".format(self.user,list_of_organizations_users[0].name))
		else:
			frappe.throw("You have more than one user linked to this organization")

		# check if the owner of the organization is the same as the selected user
		if self.owner != self.user:
			self.owner =  self.user
		