// Copyright (c) 2019, Frappe Technologies and contributors
// For license information, please see license.txt

// list of organization form sections in organization
var list_of_sections = {
	"International NGO": ["organization_type_section_section",
		"general_organizaiton_details","organization_uniqueness_section_section","organization_address",
		"address_of_main_office_section_section","somaliland_branch_offices_section",
		"country_director","somaliland_most_senior_executive_section",
		"country_director_address_section","three_alternative_contact_persons_section",
		"registration_years_section",
		"part_two","pillar_alignment_section","section_alignment_section","themes_alignment_section",
		"geographical_area",
		"accounting_and_reporting_period_section","total_number_of_staff_section",
		"declaration","attachments_section_section","verification_section_section",
	],
	"Local NGO": [
		"organization_type_section_section","general_organizaiton_details",
		"organization_address","registering_persons_details_section","operation_level_section",
		"organization_description_section_section","pillar_alignment_section",
		"section_alignment_section","themes_alignment_section","geographical_area",
		"chapter_2","declaration","attachments_section_section", 'staff_details_section',
	],

	"Somaliland Government Institution":[
		"organization_type_section_section","general_organizaiton_details",
		"address_of_main_office_section_section","director_details_section",
		"three_alternative_contact_persons_section","department_responsible_section",
		"sector_coordination_fora_attended_section"
	],
	"Private Sector Company":[
		"organization_type_section_section","general_organizaiton_details",
		"organization_address","address_of_main_office_section_section",
		"somaliland_most_senior_executive_section",
		"country_director_address_section","three_alternative_contact_persons_section",
		"part_two","pillar_alignment_section","section_alignment_section","themes_alignment_section",
		"geographical_area","declaration","attachments_section_section","verification_section_section"
	],
	"Multi-Lateral Organizations":[
		"organization_type_section_section","general_organizaiton_details",
		"organization_address","address_of_main_office_section_section",
		"somaliland_most_senior_executive_section",
		"country_director_address_section","three_alternative_contact_persons_section",
		"part_two","pillar_alignment_section","section_alignment_section","themes_alignment_section",
		"geographical_area",
		"declaration","attachments_section_section"
	],
	"UN System Organization":[
		"organization_type_section_section","general_organizaiton_details",
		"organization_address","address_of_main_office_section_section",
		"somaliland_branch_offices_section",
		"somaliland_most_senior_executive_section",
		"country_director_address_section","three_alternative_contact_persons_section",
		"part_two","pillar_alignment_section","section_alignment_section","themes_alignment_section",
		"geographical_area",
		"declaration","attachments_section_section"
	],
	"Bi-lateral Organization":[
		"organization_type_section_section","general_organizaiton_details",
		"organization_address","address_of_main_office_section_section",
		"somaliland_most_senior_executive_section",
		"country_director_address_section","three_alternative_contact_persons_section",
		"part_two","pillar_alignment_section","section_alignment_section","themes_alignment_section",
		"geographical_area",
		"declaration","attachments_section_section"
	],
	"Umbrella or Consortium Organization":[
		"organization_type_section_section","general_organizaiton_details",
		"address_of_main_office_section_section","registering_persons_details_section",
		"laws_section","operation_level_section","organization_description_section_section",
		"pillar_alignment_section","section_alignment_table","themes_alignment_table",
		"geographical_area","members_information","international_organization",
		"more_information_section","declaration","attachments_section_section",
		"registration_office_section"
	],
	"All": [

		// INGO fields
		"organization_type_section_section","general_organizaiton_details","organization_address",
		"address_of_main_office_section_section","somaliland_branch_offices_section",
		"country_director","somaliland_most_senior_executive_section","country_director_address_section",
		"three_alternative_contact_persons_section",
		"sector_coordination_fora_attended_section","registration_years_section",
		"part_two","pillar_alignment_section","section_alignment_section","themes_alignment_section",
		// the two sector below should always be hidden should be finally removed
		"ndp_ii_pillars_section","section_break_47"

		,"geographical_area",
		"accounting_and_reporting_period_section","total_number_of_staff_section",
		"declaration","attachments_section_section",
		"verification_section_section",

		// LNGO Fields
		"registering_persons_details_section","operation_level_section",
		"organization_description_section_section",
		"chapter_2",

		// umbrella or consortium
		"laws_section","members_information","international_organization",
		"more_information_section","registration_office_section",

		// Somaliland government institution
		"country_director","department_responsible_section",

		// multilateral organization & Bilteral & UN System & Private Sector Company
		// no unique fields
		
	]
}

var all_fields = [
	// organization_type_section_section
	"registration_number", "organization_type",
	"organization_details_fetched", 

	// general_organizaiton_details
	"full_name_of_the_organization","acronym","country_of_origin",
	
	// organization_address
	"address","city","origin_country","postcode", "state_local_1",

	// address_of_main_office_section_section
	"area_neighborhood", "construction", "town", "street_local_1", "region",
	"district", "country_code_umbrealla", "telephone_umbrella", "complete_organization_address",
	"email_umbrella",

	// Somaliland branch offices
	"number_of_branches", "somaliland_branch_offices_table", "full_name", "director_country_code",
	 "tel_no_1", "complete_director_tel_no", "director_email", "select_one_director_address",
	 "directors_address", "area_neighborhood_senior_most", "director_city", "director_code", "country",
	 "region_director_address", "district_director_address", "if_senior_executive_same_director",
	 "full_name_2", "country_code_senior_most_executive", "director_tel_no", "complete_senior_tel_no",
	 "email_2",

	 // Alternative contact person section

	 "alternative_contact_person",

	 //Sector coordination Fora attended
	 "coordinating_sectors_table", "organization_year_registered", "were_there_years_unregistered",
	 "unregistered_years",

	 // Number of projects section
	 "number_of_projects_confirmed_during_the_current_year", "that_are_ongoing", "that_are_completed",
	 "confirmed_to_start_during_current_year",

	 // Community counsel
	 "name_of_person_registering", "code_person_registering", "tel_person_registering", "complete_tel_registering",
	 "email_person_registering",

	 //Q 1aad : XOGTA Dallada / Laws Section
	 "yes_laws", "no_laws", "national_tax", "regional_tax", "district_level",
	 
	 //CUTUBKA 1: GUDDOOMADA GUUD

	 "orgnization_description", 

	 // Pillar alignment section

	 "pillar_alignment_table", "section_alignment_table",
	 
	 // themes alignment section
	 
	 "themes_alignment_table",
	 
	 //Q 2aad: XOGTA XUBNAHA DALLADA (URURADA KU BOHOOBAY DALLADA) section
	 "consortium_members",
	 
	 //Geographical areas
	 
	 "marodijeh_check", "hargeisa", "gabiley", "baligubadle", "salahlay", "sanag_check",
	 
	 //Districts
	 "erigavo", "badhan", "las_qoray", "ela_fweyn", "dhahar", "gar_adag", "hargeisa_region",
	 "sool_check", "las_anod", "hudun", "taleh", "aynabo", "togdheer_check", "burao", "odwayne", "buhodle",
	 "awdal_check", "borama", "zeila", "baki", "lughaya", "sahil_check", "berbera", "sheekh",

	 // Accounting and reporting period section
	 "closing_month", "closing_day",

	 // Total number of staff
	 "managerial_or_program_staff", "admin_or_support_staff",
	 
	 // International number of staff
	 "managerial_or_program_staff_2", "admin_or_support_staff_2", "communicate_members", 
	 //Q 3 aad :XOGTA XUBNAHA GUDIDA DALADA EE LAGU DOORTAY SHIRWAYNIHII DALADA section
	 "international_communication", 
	 //Q 4aad: XOG DHEERAAD AH section
	 "foundation_date", "foundation_venue", "yes_conved_by_mopnd", "convening_members_of_mopnd", 
	 "no_conved_by_mopnd", "yes_consultative_committee", "consultative_committee", "not_consultative_committee",

	 // Declaration section
	 "full_name_of_declarator", "title", "declaration_date", "read_instructions", "applicant_name",
	 "history",

	 //Operational details section
	 "sector__alignment_table", 

	 //International NGO attachments section
	 "cert_from_origin_country", "an_attestation_of_your_existence", "detailed_organization_info",
	 "evidence_of_similar_work", "founders_cv_certs", "cvs_of_international", "activity_plan", 
	 "intent_letter", "country_office_evidence", "reciept_of_payment",

	 //UN system organization attachment section
	 "agreement_with_government",

	//Local ngo organization attachments
	"attach_logo_local_1", 

	//Umbrella or consortium organization
	"association_rules", "board_of_directors", "proof_of_members", "members_cv", "written_request", "property_list",
	 "next_year_plan", "last_year_performance", "organization_1000",

	 //Private sector company attachments section 
	 "valid_registrationlicence_from_the_mot", "certificate_of_tax_clearance", "a_copy_of_articles_of_incorporation",

	 //Go’aanka Madaxa Diwaan galinta section
	 "consent", "consent_information", "refund", "refund_reason", "disagree", "disagree_reason", "registered_name",
	 "history_office_use", "consent_by_director", "agree_director", "rejected_director", "superitendant_name",
	 "history_superitendant", 
	 
	 //Validation verification section
	 "validated", "validated_by", "user_validate",
	 
	 //Staff verification section
	 "verified", "verified_by", "verify",
	 
	 // Approval dg section
	 "approved_by", "approve",
	 
	 //History of attached forms section
	 "attached_organization_forms_table",

	 // Attachments
	 "standard_letter_of_agreement", "attach_logo_local_1",
	 
	 // Organization users section
	 "user"

	]


var fields_based_on_type = {
	// local NGO
	"Local NGO":[
		// organization type section
		"status","registration_number","organization_type",
		"organization_details_fetched",

		// general organization details section
		"enter_full_name_of_organization","full_name_of_the_organization",
		"acronym","country_of_origin",

		// organization_address
		"state_local_1","city","street_local_1",

		// registering_persons_details_section
		"name_of_person_registering","code_person_registering","tel_person_registering",
		"email_person_registering",

		// operation_level_section
		"organization_activity","national_tax","regional_tax",

		// organization_description_section_section
		"description_local_2","description_english_1","brief_organization_description",

		// pillar_alignment_section
		"inst_pillar_section","pillar_alignment_table",

		// section_alignment_section
		"inst_sectors_section","section_alignment_table",

		// themes_alignment_section
		"inst_themes_section","themes_alignment_table",

		// chapter_2
		"founding_members_inst","communicate_members",

		// declaration
		"inst_declaration","full_name_of_declarator","declaration_date",

		// attachments_section_section
		"local_ngo_attachments","attach_logo_local_1"
	],

	// All
	"All":[

		// organization type section
		"status","registration_number","organization_type",
		"organization_details_fetched",

		// general organization details section
		"enter_full_name_of_organization","full_name_of_the_organization",
		"acronym","country_of_origin",

		// organization_address
		"state_local_1","city","street_local_1",

		// registering_persons_details_section
		"name_of_person_registering","code_person_registering","tel_person_registering",
		"email_person_registering",

		// operation_level_section
		"organization_activity","national_tax","regional_tax",

		// organization_description_section_section
		"description_english_1","brief_organization_description",

		// pillar_alignment_section
		"inst_pillar_section","pillar_alignment_table",

		// section_alignment_section
		"inst_sectors_section","section_alignment_table",

		// themes_alignment_section
		"inst_themes_section","themes_alignment_table",

		// chapter_2
		"founding_members_inst","communicate_members",

		// declaration
		"inst_declaration","full_name_of_declarator","declaration_date",

		// attachments_section_section
		"local_ngo_attachments","attach_logo_local_1"
	]
}

var fields_visible_to_admin =  ["change_owner_of_the_organization", "new_owner"]
var list_of_headings = {
	"International NGO":["enter_full_name_of_mda", "consortium_address", "address_in_origin_country_local", "department_director_details",
						 "inst_country_director_2", "director_of_department_responsible_for_coordination_issues",
						 "enter_two_alternative", "un_system_organization_attachments", "local_ngo_attachments",
						 "attachments_for_umbrealla", "private_sector_company_attachements", "bilateral_organization_attachment",
						 "multilateral_organization_attachment", "inst_declaration_somali", "consortium_heading"
						],
						
	"Local NGO": ["enter_full_name_of_mda", "consortium_address", "address_in_origin_country", "description_local_1",
				  "inst_attachment_ingo", "un_system_organization_attachments", "attachments_for_umbrealla",
				  "private_sector_company_attachements", "bilateral_organization_attachment",
				  "multilateral_organization_attachment", "inst_declaration_somali", "consortium_heading"
          ],

	"Multi-Lateral Organizations": ["consortium_address", "enter_full_name_of_mda", "enter_three_alternative",
								  "address_in_origin_country_local", "inst_country_director_2",
								  "instruction_senior_executive_same_director", "director_of_department_responsible_for_coordination_issues",
								  "attachments_for_umbrealla", "local_ngo_attachments", "un_system_organization_attachments",
								  "private_sector_company_attachements", "inst_attachment_ingo", "bilateral_organization_attachment",
								  "inst_declaration_somali", "consortium_heading"

								 ],
								 
	"Bi-lateral Organization": ["inst_org1_1", "consortium_address", "enter_full_name_of_mda", "address_in_origin_country_local", "inst_country_director_2",
							   "director_of_department_responsible_for_coordination_issues", "instruction_senior_executive_same_director",
							   "enter_three_alternative", "attachments_for_umbrealla", "local_ngo_attachments", "un_system_organization_attachments",
	                           "private_sector_company_attachements",  "inst_attachment_ingo", "consortium_heading",
							   "multilateral_organization_attachment", "inst_declaration_somali"],

	"Somaliland Government Institution": ["consortium_heading", "consortium_address", "enter_full_name_of_organization", "enter_three_alternative", "inst_org1_1",
										  "bilateral_organization_attachment", "inst_declaration_somali", "multilateral_organization_attachment"
										],

	"Private Sector Company": ["consortium_address", "enter_full_name_of_mda", "address_in_origin_country_local",
							   "enter_three_alternative", "inst_country_director_2", "instruction_senior_executive_same_director",
							    "director_of_department_responsible_for_coordination_issues",
							   "attachments_for_umbrealla", "local_ngo_attachments", "un_system_organization_attachments",
							   "inst_attachment_ingo", "local_ngo_attachments", "bilateral_organization_attachment",
							   "multilateral_organization_attachment", "inst_declaration_somali", "consortium_heading"
							  ],
							  
	"UN System Organization": ["consortium_address", "enter_full_name_of_mda", "enter_three_alternative",
							   "address_in_origin_country_local", "inst_country_director_2",
							   "instruction_senior_executive_same_director", "director_of_department_responsible_for_coordination_issues",
							   "attachments_for_umbrealla", "local_ngo_attachments", "multilateral_organization_attachments",
							   "inst_attachment_ingo", "bilateral_organization_attachment", "multilateral_organization_attachment",
							   "private_sector_company_attachements", "inst_declaration_somali", "consortium_heading"
							  ],

	"Umbrella or Consortium Organization": ['enter_full_name_of_mda', 'enter_full_name_of_organization', 'description_local_2',
										   'un_system_organization_attachments', 'inst_attachment_ingo',
										   'private_sector_company_attachements',  "local_ngo_attachments", "multilateral_organization_attachments",
										   "inst_attachment_ingo", "bilateral_organization_attachment", "multilateral_organization_attachment",
										   "private_sector_company_attachements", "inst_declaration", "address_in_somaliland_and_main_office_column",],
	
	"All":["inst_org1_1", "enter_full_name_of_organization",
		   "enter_full_name_of_mda", "address_in_origin_country", "address_in_origin_country_local",
		   "address_in_somaliland_and_main_office_column", "inst_no_branches", "instruction_branch_details",
		   "enter_organization_country_director_details_below", "department_director_details",
		   "inst_country_director_1", "inst_country_director_2", "instruction_senior_executive_same_director",
		   "director_of_department_responsible_for_coordination_issues", "instruction_senior_executive_details",
		   "enter_three_alternative", "enter_two_alternative", "department_responsible_for_coordination_issues",
		   "inst_what_year_registered", "inst_years_unregistered", "inst_unregistered_2", "laws_instruction",
		   "laws_instruction_1", "laws_instruction_2", "organization_activity", "description_local_2",
		   "description_local_1", "inst_pillar_section", "inst_sectors_section", "inst_themes_section",
		   "members_information_1", "marodijeh", "districts_marodijeh", "sanag", "districts_sanag", "central",
		   "sool", "districts_sool", "togdheer", "districts_togdheer", "awdal", "districts_awdal", "sahil",
		   "districts_sahil", "inst_books_closing", "inst_local_staff", "inst_international_staff", "founding_members_inst",
		   "international_organization_inst", "more_inf_instru", "convention_instruction", "instruction_listing_members",
		   "consultative_commitee_instr", "instruction_consultative_commitee", "inst_declaration", "department_responsible",
		   "inst_attachment_ingo", "un_system_organization_attachments", "local_ngo_attachments", "attachments_for_umbrealla",
		   "private_sector_company_attachements", "registration_decision", "decision_by_director",
          ]
		}
// /* This section contains code from the general functions section
// which are called is the form triggered functions section*/
// // ================================================================================================


/*function that toogles field to hide or unhide*/
function hide_unhide_fields(frm, list_of_fields, hide_or_unhide) {

	for (var i = 0; i < list_of_fields.length; i++) {
		frm.toggle_display(list_of_fields[i], hide_or_unhide)
	}
}

// function that hides or unhides certain fields based on what the user selects while filling the form
function options_based_hide_unhide(){
	if(cur_frm.doc.organization_type == "International NGO"){
		hide_unhide_fields(cur_frm, list_of_headings['All'], true)
		hide_unhide_fields(cur_frm, list_of_headings['International NGO'], false)

	}else if(cur_frm.doc.organization_type == "Local NGO"){
		hide_unhide_fields(cur_frm, list_of_headings['All'], true)
		hide_unhide_fields(cur_frm, list_of_headings['Local NGO'], false)

	}else if(cur_frm.doc.organization_type == "Somaliland Government Institution"){
		hide_unhide_fields(cur_frm, list_of_headings['All'], true)
		hide_unhide_fields(cur_frm, list_of_headings['Somaliland Government Institution'], false)

	}else if(cur_frm.doc.organization_type == "Private Sector Company"){
		hide_unhide_fields(cur_frm, list_of_headings['All'], true)
		hide_unhide_fields(cur_frm, list_of_headings['Private Sector Company'], false)

	}else if(cur_frm.doc.organization_type == "Bi-lateral Organization"){
		hide_unhide_fields(cur_frm, list_of_headings['All'], true)
		hide_unhide_fields(cur_frm, list_of_headings['Bi-lateral Organization'], false)}		
	

	else if(cur_frm.doc.organization_type == "Multi-Lateral Organizations"){
		hide_unhide_fields(cur_frm, list_of_headings['All'], true)
		hide_unhide_fields(cur_frm, list_of_headings['Multi-Lateral Organizations'], false)

	}else if(cur_frm.doc.organization_type == "UN System Organization"){
		hide_unhide_fields(cur_frm, list_of_headings['All'], true)
		hide_unhide_fields(cur_frm, list_of_headings['UN System Organization'], false)

	}else if(cur_frm.doc.organization_type == "Umbrella or Consortium Organization"){
		hide_unhide_fields(cur_frm, list_of_headings['All'], true)
		hide_unhide_fields(cur_frm, list_of_headings['Umbrella or Consortium Organization'], false)
	}else{
		// do not hide any fields for now
	}
}


// function that determines which section the user should see
function navigation_function(frm,list_of_sections){
	// check if the form is complete
	// hide all the fields
	hide_unhide_fields(frm, list_of_sections["All"], false)
	// unhide all fields based on organization type
	$.each(list_of_sections[frm.doc.organization_type],function(i,v){
		frm.toggle_display(v, true)
	})	
}


// function hide_unhide_on_refresh(frm) {
// 	if (frm.doc.organization_type && frm.doc.organization_type == "International NGO") {
// 		hide_function(frm, field_to_hide_unhide, "international_ngo")
// 	}
// 	else if (frm.doc.organization_type && frm.doc.organization_type == "Local NGO") {
// 		hide_function(frm, field_to_hide_unhide, "local_ngo")
// 	}
// 	else {
// 		hide_function(frm, field_to_hide_unhide, "none")
// 		if(frm.doc.organization_type =="Select One"){
// 			// do nothing for now
// 		}
// 		else{
// 			var non_existant_form ="Registration Form for "+frm.doc.organization_type+" is Currently Unavailable"
// 			frappe.msgprint(non_existant_form)
// 		}
// 	}

// 	function hide_function(frm, field_to_hide_unhide, language) {
// 		var hide_fields = field_to_hide_unhide["all"]
// 		var unhide_fields = field_to_hide_unhide[language]
// 		if (language == "none") {
// 			hide_unhide_fields(frm, hide_fields, false)
// 		}
// 		else {
// 			hide_unhide_fields(frm, hide_fields, false)
// 			hide_unhide_fields(frm, unhide_fields, true)
// 		}
// 	}
// }

// // function that checks a user has a pre-existing organization and 
// // redirects them to it
// function redirect_if_existing_organization(frm){
// 	// get current username
// 	var current_user = frappe.session.user
// 	frappe.call({
// 		method: "frappe.client.get_list",
// 		args: 	{
// 				doctype: "Organization",
// 				filters: {
// 					owner:current_user
// 				},
// 		fields:["*"]
// 		},
// 		callback: function(response) {	
// 			if(response.message.length == 0){
// 				// do not do anything let user create an organization
// 			}
// 			else if(response.message.length > 0){
// 				// check if the form is  new
// 				if(frm.doc.__islocal ? 0 : 1){
// 					// check if the current form belongs to the user
// 					if(cur_frm.doc.name == response.message[0].name ){
// 						// belongs to user hence do nothing
// 					}else{
// 						// does not belong to user hence redirect
// 						msgprint("You Do Not Have Permisions to View Selected Organization, You Have been Redirected To Your Organization: "+response.message[0].name)
// 						frappe.set_route("Form", "Organization",response.message[0].name)
// 					}
// 				}else{
// 					msgprint("You Have Already Registered Your Organiziation: "+response.message[0].name+" You Have be Redirected to It ")
// 					frappe.set_route("Form", "Organization",response.message[0].name)
// 				}
// 			}
// 		}	
// 	});
// }

// // function that sets custom buttons
// function add_custom_buttons(button_name,action){
// 	cur_frm.add_custom_button(__(button_name), function(){
// 		if(action=="Renew"){
// 			// untick the validated fiedl and remove the name of validating user
// 			msgprint("Functionality Under Development")
// 		}
		
// 	},__("Renew"));
// }

// // function that sets custom buttons
// function add_custom_buttons1(button_name,action){
// 	cur_frm.add_custom_button(__(button_name), function(){
// 		if(action=="Edit"){
// 			// untick the validated fiedl and remove the name of validating user
// 			msgprint("Functionality Under Development")
// 		}
		
// 	},__("Edit"));
// }

function make_fields_read_only(list_of_fields){
	if(frappe.user.has_role("Super Admin")){
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",0)
		})
	}else{
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",1)	
		})
	}
}

// function that makes fields of a child table read only
function make_child_fields_read_only(frm, child_table){
	var all_keys = Object.keys(child_table)
	var invisible_fields = ["modified_by", "name", "parent", "creation", "modified", "doctype", "idx", "parenttype","owner", "docstatus", "parentfield"]

	child_table.forEach(function(row) {
		var all_keys = Object.keys(row)
		all_keys.forEach(function(field){
			if(invisible_fields.includes(field)){
				// pass beacuse field is not visible on grid
			}else{
				var df = frappe.meta.get_docfield(row["doctype"], field, cur_frm.doc.name);
				df.read_only = 1;
			}
		})		
	});	
}

/* end of the general functions section
// =================================================================================================
/* This section  contains functions that are triggered by the form action refresh or
reload to perform various action*/
frappe.ui.form.on('Organization', {
	refresh: function(frm) {
		// make fields read only
		// make_fields_read_only(all_fields)

		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(cur_frm, cur_frm.doc.alternative_contact_person)
		}
		if(cur_frm.doc.coordinating_sectors_table){
			make_child_fields_read_only(frm, cur_frm.doc.coordinating_sectors_table)
		}
		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(frm, cur_frm.doc.unregistered_years)
		}
		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(cur_frm, cur_frm.doc.alternative_contact_person)
		}
		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(frm, cur_frm.doc.coordinating_sectors_table)
		}
		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(frm, cur_frm.doc.pillar_alignment_table)
		}
		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(frm, cur_frm.doc.section_alignment_table)
		}
		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(frm, cur_frm.doc.themes_alignment_table)
		}
		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(frm, cur_frm.doc.consortium_members)
		}
		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(frm, cur_frm.doc.communicate_members)
		}
		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(cur_frm, cur_frm.doc.alternative_contact_person)
		}
		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(frm, cur_frm.doc.convening_members_of_mopnd)
		}
		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(frm, cur_frm.doc.consultative_committee)
		}
		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(frm, cur_frm.doc.sector__alignment_table)
		}	
		if(cur_frm.doc.alternative_contact_person){
			make_child_fields_read_only(frm, cur_frm.doc.attached_organization_forms_table)
		}	
	
	
		if(cur_frm.doc.organization_type){
			
			// hide unhide section based on type
			hide_unhide_fields(frm,list_of_sections["All"], false)
			hide_unhide_fields(frm,list_of_sections[cur_frm.doc.organization_type], true)

		// 	// hide unhide fields based on organization type
		// 	hide_unhide_fields(frm,fields_based_on_type["All"], false)
		// 	// unhide the required fields
		// 	hide_unhide_fields(frm,fields_based_on_type[cur_frm.doc.organization_type], true)
		}else{
			// unhide all the fields
			hide_unhide_fields(frm,list_of_sections["All"], true)
		}
		

		if(frappe.user.has_role("Staff")){
			// allow user to see all the fields
		    make_fields_read_only(all_fields)
		}else{
			// make fields read only
			// cur_frm.fields.forEach(function(l){ cur_frm.set_df_property(l.df.fieldname, "read_only", 1); })
			make_fields_read_only(all_fields)
		
		}
		
	
		// Determines current section user should see
		// navigation_function(frm,list_of_sections)
		// determine if user can see special section/fields
		// hide or unhide fiels based on various option
		options_based_hide_unhide()
		

		// hide fields visible only to admin
		// if(frappe.user.has_role("Organization Admin")){
		// 	// unhide fields
		// 	hide_unhide_fields(frm, fields_visible_to_admin, true)
		// 	// add custom buttons for admins
		// 	add_custom_buttons("Unverify Form","Unverify")
		// 	add_custom_buttons("Unapprove Form","Unapprove")
		// 	add_custom_buttons("Mark Organization as Donor","Mark Donor")
		// 	add_custom_buttons("UnMark Organization as Donor","Unmark Donor")
		// 	add_custom_buttons("Admin Approve Form","Admin Approve")
		// }else{
		// 	// hide fields
		// 	hide_unhide_fields(frm, fields_visible_to_admin, false)
		// }
		
		// // check if form is new else redirect to organization form
		// if(frm.doc.__islocal ? 0 : 1){
		// 	cur_frm.set_df_property("registration_type","read_only",1)
		// 	cur_frm.set_df_property("renew_organization","read_only",1)
		// 	cur_frm.set_df_property("organization_type","read_only",1)

		// 	// make fields of child tables read_only
		// 	make_child_fields_read_only(frm,frm.doc.alternative_contact_person)
		// 	make_child_fields_read_only(frm,frm.doc.somaliland_branch_offices_table)
		// 	make_child_fields_read_only(frm,frm.doc.unregistered_years)
		// }else{
		// 	// redirect to project form
		// 	frappe.set_route("Form", "Organization Form","New Organization Form 1")

		// 	frm.toggle_display("organization_type", true)
		// 	// do not make the selection of user functionality compulsory for now
		// 	// if(frm.doc.organization_owner || frm.doc.created_by_other_user){
		// 	// 	// unhide the organization type field
		// 	// 	frm.toggle_display("organization_type", true)
		// 	// }else{
		// 	// 	// check if creater is owner
		// 	// 	if(frappe.user.has_role("Organization Form Admin")){
		// 	// 		// allow user to create and set administrator as cretor
		// 	// 		frm.set_value("created_by_owner","No")
		// 	// 		frm.set_value("created_by_other_user",frappe.session.user)
		// 	// 	}else{
		// 	// 		check_if_creater_is_owner(frm)
		// 	// 	}
		// 	// }
		// }
		
		
		
		
		
		// make child table fields read only
		// var df = frappe.meta.get_docfield("Alternative Contact Persons", "full_name", cur_frm.doc.name);
		// df.read_only = 1;
		
		// var newrow = cur_frm.grids[0].grid.grid_rows[cur_frm.grids[0].grid.grid_rows.length - 1].doc;
		// hide / unhide fields based on selected options
		// hide_unhide_on_refresh(frm)
		// // check if user has role Organization Admin Role
		// if(frappe.user.has_role("Organization Admin")){
		// 	// let the user create a new organization 
		// }else{
		// 	// redirect if user already has a organization
		// 	redirect_if_existing_organization(frm)
		// }

		// // add dashboard buttons
		// add_custom_buttons("Renew Registration","Renew")
		// add_custom_buttons1("Edit","Edit Details")
	
	}
});

// function that hide/unhide section based on selected language
frappe.ui.form.on("Organization", "organization_type", function(frm){ 
	// refresh to hide/unhide fields
	// console.log("refreshing")
	frm.refresh()
});


// // function that hide/unhide section based on selected language
// frappe.ui.form.on("Organization", "select_one", function(frm){ 
// 	// refresh to hide/unhide fields
// 	if(frm.doc.select_one =="Select One"){
// 		hide_unhide_fields(frm,['directors_address','director_city','director_code','country'], false)
// 	}
// 	else if(frm.doc.select_one =="Other"){
// 		hide_unhide_fields(frm,['directors_address','director_city','director_code','country'], true)
// 	}
// 	else{
// 		hide_unhide_fields(frm,['directors_address','director_city','director_code','country'], false)
// 	}
// });

// // function that checks the year of first registration
// frappe.ui.form.on("Organization", "what_year_registered", function(frm){ 
// 	// check that input is a int
// 	var registration_year = parseInt(frm.doc.what_year_registered)
// 	if(registration_year){
// 		// check if years are correct
// 		var dt = new Date();
// 		var current_year = dt.getFullYear()
// 		if(registration_year >= 1991 && registration_year <current_year){
// 			// correct range
// 		}else{
// 			frappe.msgprint("Invalid Registration Year, YearShould be Between 1991 and "+String(current_year))
// 			frm.set_value("what_year_registered","")
// 		}
// 	}else{
// 		if(registration_year.length == 0){
// 			// do nothing else
// 		}else{
// 			frappe.msgprint("Year Entered Should be a Number e.g 2019")
// 		}
		
// 	}
// });


// function that checks the validity of years when the organization was not registered
frappe.ui.form.on("Unregistered Years Table", "unregistered", function(frm,cdt,cdn){ 
	// check that input is a int
	var child = locals[cdt][cdn];
	var registration_year = parseInt(child.unregistered)
	if(registration_year){
		// check if years are correct
		var dt = new Date();
		var current_year = dt.getFullYear()
		if(registration_year >= 1991 && registration_year <current_year){
			// correct range
		}else{
			frappe.msgprint("Invalid Un-Registered Year, Years Should be Between 1991 and "+String(current_year))
			frm.set_value("what_year_registered","")
		}
	}else{
		if(registration_year.length == 0){
			// do nothing else
		}else{
			frappe.msgprint("Year Entered Should be a Number e.g 2019")
		}
		
	}
});

// ****************************************************************************************************
// Organization users section */
frappe.ui.form.on("Organization Users Table", "user", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn]

	// check if child exists
	if(child.user){
		if(cur_frm.doc.full_name_of_the_organization && cur_frm.doc.acronym && cur_frm.doc.organization_type ){
			child.organization = cur_frm.doc.full_name_of_the_organization
			child.organization_type = cur_frm.doc.organization_type
		}else{
			child.user = ""
			child.organization = ""
			cur_frm.refresh_fields("organization_users_table")
			frappe.throw("Please add the organization name,acronym  and type in order to continue")
		}
	}else{
		// dont do anything
	}

	cur_frm.refresh_fields("organization_users_table")
})
