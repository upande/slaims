// Copyright (c) 2020, upande and contributors
// For license information, please see license.txt

// ****************************************************************************************************
// General functions section
function filter_fields(frm){
	// users can only select years whose status is open
	cur_frm.set_query("year_to_close", function() {
		return {
			"filters": {
				"status": "Open"
			}
		}
	});
	// user can only select years that are not closed
	cur_frm.set_query("year_to_open", function() {
		return {
			"filters": {
				"status": ""
			}
		}
	});
}

// function that clears a given set of fields based on the provided option
function clear_all_related_incase_of_error(type_of_fields){
	if(type_of_fields == "Single Organization"){
		cur_frm.set_value("select_organization","");
		cur_frm.set_value("financial_year_to_close","");
		cur_frm.set_value("financial_year_to_open","");
		cur_frm.set_value("close_selected_org_fins_year",0);
	}else if(type_of_fields == "All Organizations"){
		cur_frm.set_value("year_to_close","");
		cur_frm.set_value("year_to_open","");
		cur_frm.set_value("new_year_start_date","");
		cur_frm.set_value("new_year_end_date","");
		cur_frm.set_value("new_year_rank",0);
		cur_frm.set_value("close_year",0);
	}	
}

// ****************************************************************************************************
// Trigger based functions section
frappe.ui.form.on('Global Settings', {
	refresh: function(frm) {
		filter_fields(frm)
	}
});

// function that runs when the user click on close_financial_year_for_all__organizations
frappe.ui.form.on("Global Settings", "close_financial_year_for_all__organizations", function(frm){ 
	if(cur_frm.doc.close_financial_year_for_all__organizations){
		// unclick the close_financial_year_for_all__organizations
		cur_frm.set_value("close_year_of_single_organization",0)
		// get the current finacial year to close
	}else{
		// clear out the close single organization fields
		clear_all_related_incase_of_error("All Organizations")
	}
})

// function that runs when the year to year_to_close is selected
frappe.ui.form.on("Global Settings", "year_to_close", function(frm){ 
	if(cur_frm.doc.year_to_close){
		// get the year to open
		frappe.call({
			method: "frappe.client.get_value",
			args:{
				doctype: "Year",
				filters: {
					name:cur_frm.doc.year_to_close
				},
				fieldname:["name","rank"]
			},
			callback: function(year_response) {
				// check if year exist
				if(year_response.message){
				// the following year exist
					var org_year_rank = year_response.message.rank + 1
					// get a year with the above rank
					// now check if the financial year exist
					frappe.call({
						method: "frappe.client.get_value",
						args:{
							doctype: "Year",
							filters: {
								rank:org_year_rank
							},
							fieldname:["name","rank"]
						},
						callback: function(year_to_open_response) {
							// check if a year was found
							if(year_to_open_response.message){
								// now set values for year to open and year to close
								cur_frm.set_value("year_to_open",year_to_open_response.message.name)
							}else{
								// clear the year to close & year to open field
								cur_frm.set_value("year_to_close","")
								cur_frm.set_value("year_to_open","")
								// throw an error
								frappe.throw(`A financial year after ${year_response.message.name} \
								does not exist please create it under list of \
								<a href='/desk#List/Year/List' style='color:blue;'>Years</a>`)
							}
						}
					})
				}else{
					/// clear the year to close & year to open field
					cur_frm.set_value("year_to_close","")
					// throw an error
					frappe.throw(`An error occured while trying to get the next financial year to open`)
				}
			}
		})
	}else{
		// clear the year to open field
		cur_frm.set_value("year_to_open","")
	}
})

// function that runs when a year_to_open is selected
frappe.ui.form.on("Global Settings", "year_to_open", function(frm){ 
	if(cur_frm.doc.year_to_open){
		// check the years start and enddates
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Year",
					filters: {
						name:cur_frm.doc.year_to_open
					},
			fields:["*"]
			},
			callback: function(response) {
				cur_frm.set_value('new_year_start_date',response.message[0]['start_date'])
				cur_frm.set_value('new_year_end_date',response.message[0]['end_date'])
				cur_frm.set_value('new_year_rank',response.message[0]['rank'])
			}
		})
	}
})


// function that runs when the user click on manually close button
frappe.ui.form.on("Global Settings", "manually_close_financial_year", function(frm){ 
	cur_frm.set_value('close_year',1)
	// now save the document
	cur_frm.save()	
})

// function that runs when the user click on manually close button
frappe.ui.form.on("Global Settings", "update_details_of_current_financial_year", function(frm){ 
	cur_frm.set_value('update_current_year',1)
	// now save the document
	cur_frm.save()
})

// function that runs when the user click on close_year_of_single_organization
frappe.ui.form.on("Global Settings", "close_year_of_single_organization", function(frm){ 
	if(cur_frm.doc.close_year_of_single_organization){
		// unclick the close_financial_year_for_all__organizations
		cur_frm.set_value("close_financial_year_for_all__organizations",0)
	}else{
		// clear out the close single organization fields
		clear_all_related_incase_of_error("Single Organization")
	}
})


// function that runs when the user click on select_organization
frappe.ui.form.on("Global Settings", "select_organization", function(frm){ 
	if(cur_frm.doc.select_organization){
		// get the doc of the current organization
		frappe.call({
			method: "frappe.client.get_value",
			args:{
				doctype: "Organization",
				filters: {
					name:cur_frm.doc.select_organization
				},
				fieldname:["name","current_registration_year"]
			},
			callback: function(response) {
				// place the organization current registration year field
				if(response.message.current_registration_year){
					// get the next financial year the organization will be registered to 
					frappe.call({
						method: "frappe.client.get_value",
						args:{
							doctype: "Year",
							filters: {
								name:response.message.current_registration_year
							},
							fieldname:["name","rank"]
						},
						callback: function(year_response) {
							// check if year exist
							if(year_response.message){
							// the following year exist
								var org_year_rank = year_response.message.rank + 1
								// get a year with the above rank
								// now check if the financial year exist
								frappe.call({
									method: "frappe.client.get_value",
									args:{
										doctype: "Year",
										filters: {
											rank:org_year_rank
										},
										fieldname:["name","rank"]
									},
									callback: function(year_to_open_response) {
										// check if a year was found
										if(year_to_open_response.message){
											// now set values for year to open and year to close
											cur_frm.set_value("financial_year_to_close",response.message.current_registration_year)
											cur_frm.set_value("financial_year_to_open",year_to_open_response.message.name)
										}else{
											clear_all_related_incase_of_error("Single Organization")
											frappe.throw(`A financial year after ${response.message.current_registration_year} \
											does not exist please create it under list of \
											<a href='/desk#List/Year/List' style='color:blue;'>Years</a>`)
										}
									}
								})
							}else{
								clear_all_related_incase_of_error("Single Organization")
								frappe.throw("An Error occured while getting the <b>Organization's Financial year</b> \
								<hr>Please check if the year is saved under list of years")
							}
						}
					})
				}else{
					// clear out the selected organization field
					clear_all_related_incase_of_error("Single Organization")
					frappe.throw("A registation year is not yet defined for selected organization \
						please define it under Approved Forms")
				}
			} 
		})
	}	
})

// function that runs when the user click on close_single_org_fin_year_button
frappe.ui.form.on("Global Settings", "close_single_org_fin_year_button", function(frm){ 
	// check the close finacial year of selected organization
	cur_frm.set_value("close_selected_org_fins_year",1)
	// needs to clear out the fields for closing year for all organizations
	clear_all_related_incase_of_error("All Organizations")
	// now save the form
	cur_frm.save()
})


// function that runs when the user click on view_org_fin_year_did_not_close
frappe.ui.form.on("Global Settings", "view_org_fin_year_did_not_close", function(frm){ 
	frappe.set_route('List', 'Failed Closing of Financial Year');
})
