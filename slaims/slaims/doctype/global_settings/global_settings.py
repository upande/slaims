# -*- coding: utf-8 -*-
# Copyright (c) 2020, upande and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

# application imports
from slaims.custom_methods import close_open_year,close_open_year_for_all_organizations,\
close_open_year_for_an_organization

class GlobalSettings(Document):
	def validate(self):
		'''
		function that checks that all the requirements
		are okay before saving
		'''	
		# check required fields
		check_required_fields(self)
		# check to ensure field rquirements are met
		check_field_requirements(self)
		#open new financial year
		open_close_year(self)
		# close years based on options
		open_close_years_based_on_option(self)
		# clear some unrequired fields
		clear_unrequired_fields(self)

	def on_update(self):
		'''
		Function that run when the document is saved
		'''
		pass
		
	def on_trash(self):
		'''
		Function that runs when the document is 
		deleted
		'''
		# frappe.throw("You are not allowed to delete global settings")
		pass


def check_required_fields(self):
	'''
	Function that checks that all the required 
	fields are given 
	Input:
		self i.e the document
	Output:
		No Error - pass
		Error  - throws an alert message to the user
	'''
	list_of_required_fields_update = [
		{"field_name":"Year","field_value":self.current_financial_year},	
	]

	list_of_required_fields_closing = [
		{"field_name":"Year to Close","field_value":self.year_to_close},
		{"field_name":"Year to Open","field_value":self.year_to_open},
		{"field_name":"New Year Start Date ","field_value":self.new_year_start_date},
		{"field_name":"New Year End Date","field_value":self.new_year_end_date},	
	]

	list_of_required_fields_closing_single_orgs_year = [
		{"field_name":"Select Organization","field_value":self.select_organization},
		{"field_name":"Financial Year to Close","field_value":self.financial_year_to_close},
		{"field_name":"Financial Year to Open","field_value":self.financial_year_to_open},
	]

	if self.update_current_year:
		# check if an open year exist
		for field in list_of_required_fields_update:	
			if field["field_value"]:
				pass
			else:
				frappe.throw("You have not given any value for the field <strong>{}</strong>".format(field["field_name"]))

	elif self.close_year:
		for field in list_of_required_fields_closing:	
			if field["field_value"]:
				pass
			else:
				frappe.throw("You have not given any value for the field <strong>{}</strong>".format(field["field_name"]))
	elif self.close_year_of_single_organization:
		for field in list_of_required_fields_closing_single_orgs_year:	
			if field["field_value"]:
				pass
			else:
				frappe.throw("You have not given any value for the field <strong>{}</strong>".format(field["field_name"]))

def check_field_requirements(self):
	'''
	Checks that all the requirements of the fields
	are met before the the settings are saved
	'''
	# the new year should have a higher rank than the current year
	if self.close_year:
		new_year_doc = frappe.get_doc("Year",self.year_to_open)
		current_year_doc = frappe.get_doc("Year",self.year_to_close)
		if new_year_doc.rank >= current_year_doc.rank:
			pass
		else:
			frappe.throw("The new year has to be later than the current year")	

		# The year  to close should be the current year
		if self.current_financial_year:
			if self.year_to_close != self.current_financial_year:
				frappe.throw("The year to close should be the same as the \
					<strong>Current Financial Year</strong>")
	
	if self.update_current_year:
		# if user is trying to update the current year details
		# the current year should already be present
		if not self.current_financial_year:
			frappe.throw("Current financial year is yet to be defined (use mysql)")

def open_close_year(self):
	'''
	Function that runs to either open or 
	close a financial year
	'''
	if self.update_current_year:
		# update details
		current_year_doc = frappe.get_doc("Year",self.current_financial_year)
		self.current_financial_start_date = current_year_doc.start_date
		self.current_financial_end_date = current_year_doc.end_date
		self.submission_deadline = current_year_doc.submission_deadline
		self.current_financial_closing_date = current_year_doc.year_closing_date
		self.current_year_rank = current_year_doc.rank
	
	elif self.close_year:
		# close current year
		year_to_close_doc = frappe.get_doc("Year",self.year_to_close)
		# now close the year
		if year_to_close_doc.status == "Open":
			year_to_close_doc.status = "Closed"
			year_to_close_doc.save()

		year_to_open_doc = frappe.get_doc("Year",self.year_to_open)
		if year_to_open_doc.status == "Closed":
			frappe.msgprint("The new year you selected is already closed")
		else:
			year_to_open_doc.status = "Open"
			self.current_financial_year = self.year_to_open
			self.current_financial_start_date = self.new_year_start_date
			self.current_financial_end_date = self.new_year_end_date
			self.submission_deadline = year_to_open_doc.submission_deadline
			self.current_financial_closing_date = year_to_open_doc.year_closing_date
			self.current_year_rank = self.new_year_rank

			# determine whether or not to assign a new rank
			if year_to_open_doc.rank:
				pass
			else:
				# assign a new rank
				current_finacial_year_rank = year_to_close_doc.rank
				year_to_open_doc.rank = current_finacial_year_rank + 1

			# now save the changes
			year_to_open_doc.save()
	elif self.close_year_of_single_organization:
		# close the year of single organization
		pass
	else:
		pass

def clear_unrequired_fields(self):
	'''
	Function that clears some unrequired fields 
	in preparation for the next round of settings
	'''
	self.update_current_year = 0
	self.close_financial_year_for_all__organizations = 0
	self.year_to_close = ""
	self.year_to_open = ""
	self.close_year = 0
	self.new_year_start_date = ""
	self.new_year_end_date = ""
	self.new_year_rank = ""
	self.close_year_of_single_organization = 0
	self.select_organization = ""
	self.financial_year_to_close = ""
	self.financial_year_to_open = ""
	self.close_selected_org_fins_year = 0


def open_close_years_based_on_option(self):
	'''
	Function that opens or closes a financial year based
	on a given a given option
	'''
	if self.close_selected_org_fins_year:
		# close the financial year of a given organization
		try:
			# get the organization name,fin year to close and fin year to open
			close_open_year_for_an_organization(self.select_organization)
		except:
			frappe.throw("An error occured while closing financial year please try again")
	elif self.close_year:
		# close current financial year
		close_open_year_for_all_organizations(self.year_to_close)

def close_financial_year_for_single_organization(organization_name):
	'''
	Function that closes the financial year of a specific organization 
	input:
		organization_name -str
	output:
		dict - {'status':True/False,'message':error message if a failure occurs'}
	'''
	pass
	
