// Copyright (c) 2019, upande and contributors
// For license information, please see license.txt

function get_pillars_into_pillar_aligment_table(frm){
	frappe.call({
		method: "frappe.client.get_list",
		args: 	{
				doctype: "NDP",
				filters: {
					type:"Pillar"
				},
		fields:["name"]
		},
		callback: function(response) {
			$.each(response.message, function(i,v){
				var pillar_row = frm.add_child("pillar_alignment_table")
				pillar_row.pillar = v["name"]
				cur_frm.refresh_fields();
			});
		}
	});
}


function get_sectors_into_sector_aligment_table(frm,parent_ndp){
	frappe.call({
		method: "frappe.client.get_list",
		args: 	{
				doctype: "NDP",
				filters: {
					parent_ndp:parent_ndp,
					type:"Sector"
				},
		fields:["name","parent_ndp"]
		},
		callback: function(response) {
			$.each(response.message, function(i,v){
				var sector_row = frm.add_child("section_alignment_table")
				sector_row.pillar = v["parent_ndp"]
				sector_row.sector = v["name"]
				cur_frm.refresh_fields();
			});
		}
	});	
}

// function that removes sectors and themes under a theme that a user has unselected
function remove_child_table_fields_with_pillar(frm,pillar){
	// hold the rows of both themes and sectors before clearing the fields
	var list_of_sector_rows = frm.doc.section_alignment_table
	var list_of_themes_rows = frm.doc.themes_alignment_table

	// clear the themes and section aligment tables
	cur_frm.clear_table("section_alignment_table"); 
	cur_frm.clear_table("themes_alignment_table");

	// loop through the list of sectors adding back only the applicable themes and sectors 
	$.each(list_of_sector_rows, function(i,v){
		if(v["pillar"] == pillar){
			cur_frm.refresh_fields();
		}else{
			// add sector
			var sector_row = frm.add_child("section_alignment_table")
			sector_row.pillar = v["pillar"]
			sector_row.sector = v["sector"]
			sector_row.yes = v["yes"]
			cur_frm.refresh_fields();

			// loop through the list of themes
			$.each(list_of_themes_rows, function(j,k){
				// add back themes belonging to the setor
				if(k["sector"] == v["sector"]){
					var theme_row = frm.add_child("themes_alignment_table")
					theme_row.sector = v["sector"]
					theme_row.theme = k["name"]
					theme_row.yes = v["yes"]
					cur_frm.refresh_fields();
				}
			});
		}
	});
}

// function that add themes to themes alignment table when sector is selected by the user
function get_themes_into_themes_aligment_table(frm,parent_ndp){
	frappe.call({
		method: "frappe.client.get_list",
		args: 	{
				doctype: "NDP",
				filters: {
					parent_ndp:parent_ndp,
					type:"Theme"
				},
		fields:["name","parent_ndp"]
		},
		callback: function(response) {
			$.each(response.message, function(i,v){
				var theme_row = frm.add_child("themes_alignment_table")
				theme_row.sector = v["parent_ndp"]
				theme_row.theme = v["name"]
				cur_frm.refresh_fields();
			});
			cur_frm.refresh_fields();
		}
	});	
}
		
// function that removes themes for a sector when a user unticks it
function remove_child_table_fields_with_sector(frm,sector){
	var list_of_themes_rows = frm.doc.themes_alignment_table
	cur_frm.clear_table("themes_alignment_table"); 
	$.each(list_of_themes_rows, function(i,v){
		if(v["sector"] == sector){
			cur_frm.refresh_fields();
		}else{
			var theme_row = frm.add_child("themes_alignment_table")
			theme_row.sector = v["sector"]
			theme_row.theme = v["theme"]
			theme_row.yes = v["yes"]
			cur_frm.refresh_fields();
		}
	});
}


// =========================================================================================================
frappe.ui.form.on('Test Organization Form', {
	refresh: function(frm) {
		// get all the pillars and place them in the table
		get_pillars_into_pillar_aligment_table(frm)
		
	}
});


// function that is triggered when a user ticks a pillar
frappe.ui.form.on("Pillar Alignment Table", "yes", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn];
	if(child.yes == 1){
		// user selected pillar hence add all the pillars sector in below table
		get_sectors_into_sector_aligment_table(frm,child.pillar)
		
	}else{
		// user unselected pillar hence remove all child table field with pillar
		remove_child_table_fields_with_pillar(frm,child.pillar)
	}
});


// function that is triggered when a user ticks a sector
frappe.ui.form.on("Sector Alignment Table", "yes", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn];
	if(child.yes == 1){
		// user selected pillar hence add all the pillars sector in below table
		get_themes_into_themes_aligment_table(frm,child.sector)
		
	}else{
		// user unselected pillar hence remove all child table field with pillar
		remove_child_table_fields_with_sector(frm,child.sector)
	}
});