# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt
from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

import requests
import pymysql.cursors
import json

# list of sections listed in order of filling/ occurance
list_of_sections = ["section_break_1","section_a_section",
	# "source_suggestion",
	"project_amount_section","section_c_section",
	"section_b_section","destination_implementing_and_reporting_organization_section",
	"implementing__partners_section_section","project_details_section",
	"pillar_alignment_section","sector_alignment_section_section","priority_programs_section_section",
	"relevant_mdas_section_section","relevant_ndp_outcome_by_selected_priority_program_section",
	"project_location_section","project_description_section",
	"attachments_section_section","verification_section_section"
]

class ProjectRegistrationForm(Document):
	
	def validate(self):
		# check that required fields are given
		check_required_fields(self)

		# ensure that the given name is not too long
		truncate_name(self)

		# ensure user does not have duplicate form for project
		check_that_no_pending_form_already_exist(self)

		# functionality for imported forms
		status_based_action_for_imported(self)

		# takes the user to the correct next page
		# set_next_page_option(self)

		# function that is called every time the document is saved
		execute_action_on_save(self)

	def on_update(self):
		# takes the user to the correct next page
		set_next_page_option(self)
		# add the just created organizations to the table
		add_created_organizations_to_form(self)
		# update the fields to the main doctype
		map_data_to_project(self)

	def on_trash(self):
		pass

def check_that_no_pending_form_already_exist(self):
	'''
	Check to ensure the user does not have any duplicate
	forms for the same project
	'''
	# generate the subproject_name
	sub_project_name = ""
	sub_project_name += self.new_project_title
	sub_project_name +="-"
	sub_project_name += self.organization_acronym

	# get list of project reg forms
	list_of_project_reg_forms = frappe.get_list("Project Registration Form",
			fields=["name"],
			filters = {
				"master_project_title":self.new_project_title,
				"organization_acronym":self.organization_acronym,
				"status":["!=","Cancelled"]
			})

	if len(list_of_project_reg_forms) > 0:
		# check if the forms are more that 1
		if len(list_of_project_reg_forms) == 1:
			# check if the form is the same as the current form
			if self.name == list_of_project_reg_forms[0].name:
				pass
			else:
				# another form already exists
				frappe.throw("You already have a pending Project Registration Form for \
					project <b>{}</b> please <b>Complete </b> it or <b>Cancel</b> it if you want to fill a new form".format(self.new_project_title))
	else:
		# no form already exist hence allow the user to create the form
		pass

def truncate_name(self):
	'''
	Function that truncates the project name if it is 
	too long for a data field
	'''
	if self.new_project_title and len(self.new_project_title)>100:
		# truncate the name
		self.full_project_title = self.new_project_title
		self.new_project_title = self.new_project_title[0:100]
	else:
		# just place the full project name in the field
		self.full_project_title = self.new_project_title

def status_based_action_for_imported(self):
	'''
	Function that determines the action to be executed based on 
	the current status
	'''
	if self.status == "Admin Verified" and self.imported == 1:
		# check user gave the project name , organization and organization acronym
		if self.new_project_title and self.organization_acronym and self.select_your_organization:
			# create a project title name
			list_of_projects = frappe.get_list("Project",
				fields=["*"],
				filters = {
					"name":self.new_project_title
				})

			if len(list_of_projects)> 0:
				self.no_did_not_find_proj = 0
				self.yes_found_project = 1
				self.project_title_selection_field = self.new_project_title
				# move user to the next step which is verified status
				self.status = "Verified"
			else:
				admin_master_project = frappe.get_doc({"doctype":"Project"})
				admin_master_project.name = self.new_project_title
				admin_master_project.project_name = self.new_project_title
				admin_master_project.master_project = 1
				admin_master_project.status = self.status
				
				# add master project
				admin_master_project.insert(ignore_permissions=True)

	elif self.status == "Verified" and self.imported == 1:
		# move the user to validation
		self.status = "Validated"

	elif self.status == "Validated" and self.imported == 1:
		# move the user to validation
		self.status = "Approved"

def execute_action_on_save(self):
	'''
	Function that runs whenever the document is saved
	'''
	#check or create implementing organization
	check_or_create_implementing_orgs(self)
	# check or create source organization
	check_or_create_source_orgs(self)

	if self.status == "Pending Verification":
		if self.master_project_title:
			pass
		else:
			# check or create master project
			list_of_projects = frappe.get_list("Project",
				fields=["*"],
				filters = {
					"name":self.new_project_title
				})

			if len(list_of_projects)> 0:
				# set the master project name 
				self.master_project_title = self.new_project_title
			else:
				master_project = frappe.get_doc({"doctype":"Project"})
				master_project.name = self.new_project_title
				master_project.project_name = self.new_project_title
				master_project.master_project = 1
				master_project.project_code = self.new_project_code
				
				# add master project
				master_project.insert(ignore_permissions=True)

				# set the master project name 
				self.master_project_title = self.new_project_title

	
	elif self.status == "Verified":
		# check or create subproject
		if self.sub_project_title:
			# pass the subproject_name has been created
			project_doc = frappe.get_doc("Project",self.sub_project_title)
			project_doc.status = self.status
			
			# add the implementing n reporting organizations
			project_doc.destination_organization_table = []
			for implementor in self.destination_organization_table:
				if implementor.implementing_organization:
					project_doc.append("destination_organization_table", {
						"implementing_organization":implementor.implementing_organization,
						"implementing_organization_is_not_in_the_list_above":implementor.implementing_organization_is_not_in_the_list_above,
						"enter_name_of_implementing_organization":implementor.enter_name_of_implementing_organization,
						"enter_acronym":implementor.enter_acronym,
						"organization_type":implementor.organization_type,
						"amount":implementor.amount,
						"reporting_obligation":implementor.reporting_obligation,
						"reporting_organization":implementor.reporting_organization,
						
			
					})
			# save the changes
			project_doc.save(ignore_permissions = True)
		else:
			# generate the subproject_name
			sub_project_name = ""
			sub_project_name += self.master_project_title
			sub_project_name +="-"
			sub_project_name += self.organization_acronym

			# check if the sub project is already created
			list_of_found_projects = frappe.get_list("Project",
				fields=["*"],
				filters = {
					"name":sub_project_name
			})

			if len(list_of_found_projects) > 0:
				# set the just created sub project name to the field
				self.sub_project_title = sub_project_name
				self.status = self.status
			else:
				# create a new subproject
				new_sub_project_doc = frappe.get_doc({"doctype":"Project"})
				new_sub_project_doc.name = sub_project_name
				new_sub_project_doc.project_name = sub_project_name
				new_sub_project_doc.project_code = self.new_project_code
				new_sub_project_doc.sub_project = 1
				new_sub_project_doc.link_to_master_project = self.project_title_selection_field
				new_sub_project_doc.status = self.status
				new_sub_project_doc.project_start_date = self.project_start_date
				new_sub_project_doc.project_end_date = self.project_end_date
				new_sub_project_doc.your_organization = self.select_your_organization
				new_sub_project_doc.organization_acronym = self.organization_acronym
				new_sub_project_doc.total_project_amount_in_usd = self.total_project_amount_in_usd

				# add pillars 
				new_sub_project_doc.pillar_alignment_table = []
				for pillar in self.pillar_alignment_table:
					if pillar.pillar and pillar.yes:
						new_sub_project_doc.append("pillar_alignment_table", {
							"pillar":pillar.pillar,
							"yes":pillar.yes
						})

				# add the implementing n reporting organizations
				new_sub_project_doc.destination_organization_table = []
				for implementor in self.destination_organization_table:
					if implementor.implementing_organization:
						new_sub_project_doc.append("destination_organization_table", {
							"implementing_organization":implementor.implementing_organization,
							"implementing_organization_is_not_in_the_list_above":implementor.implementing_organization_is_not_in_the_list_above,
							"enter_name_of_implementing_organization":implementor.enter_name_of_implementing_organization,
							"enter_acronym":implementor.enter_acronym,
							"organization_type":implementor.organization_type,
							"amount":implementor.amount,
							"reporting_obligation":implementor.reporting_obligation,
							"reporting_organization":implementor.reporting_organization,
						})

				# first clear  table
				new_sub_project_doc.sector_alignment_table = []
				for sector in self.sector_alignment_table:
					if sector.yes:
						new_sub_project_doc.append("sector_alignment_table", {
							"pillar":sector.pillar,
							"sector":sector.sector,
							"yes":sector.yes
						})

				# relevant_mdas_section_section
				# first clear  table
				new_sub_project_doc.relevant_mda_table = []

				for mda in self.relevant_mda_table:
					if mda.lead_mda:
						new_sub_project_doc.append("relevant_mda_table", {
							"relevant_mda_per_sector":mda.relevant_mda_per_sector,
							"sector":mda.sector,
							"lead_mda":mda.lead_mda,
						})

				list_of_regions = [{self.marodijeh_check: "Marodijeh/Maroodijeex",
		                    self.sanag_check: "Sanag/Sanaag",
							self.hargeisa_region: "Central/Hargeisa",
							self.sool_check: "Sool/Sool",
							self.togdheer_check: "Togdheer/Togdheer",
							self.awdal_check: "Awdal/Awdal",
							self.sahil_check: "Sahil/Saaxil",
						  }]

				for region in list_of_regions:
					new_sub_project_doc.project_location_table = []

					if region:
						# check morodijeh
						marodijeh_check = False

						if self.hargeisa:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Marodijeh/Maroodijeex',
								'district': 'Hargeisa/Hargeysa'
							})
							marodijeh_check = True

						if self.gabiley:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Marodijeh/Maroodijeex',
								'district': 'Gabiley/Gabilay'
							})
							marodijeh_check = True

						if self.baligubadle:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Marodijeh/Maroodijeex',
								'district': 'Baligubadle/Baligubadle'
							})
							marodijeh_check = True

						if self.salahlay:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Marodijeh/Maroodijeex',
								'district': 'Salahlay/Salaxlay'
							})
							marodijeh_check = True

						if self.marodijeh_check and marodijeh_check == False:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Marodijeh/Maroodijeex',
							})

						# check region Sanaag
						sanag_check = False

						if self.erigavo:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
								'district': 'Erigavo/Ceerigaabo'
							})
							sanag_check = True

						if self.badhan:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
								'district': 'Badhan/Badhan'
							})
							sanag_check = True

						if self.las_qoray:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
								'district': 'Las Qoray /Laasqoray'
							})
							sanag_check = True

						if self.ela_fweyn:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
								'district': 'El Afweyn/Ceelafweyn'
							})
							sanag_check = True

						if self.dhahar:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
								'district': 'Dhahar /Dhahar'
							})
							sanag_check = True

						if self.gar_adag:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
								'district': 'Gar Adag /Garadag'
							})
							sanag_check = True

						if self.sanag_check and sanag_check == False:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
							})

						# Check for Central or Hargeisa	
						hargeisa_check = False

						if self.hargeisa_region and hargeisa_check == False :
							new_sub_project_doc.append('project_location_table', {
								'region': 'Central/Hargeisa',
							})

						# check for sool
						sool_check = False
		
						if self.las_anod:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sool/Sool',
								'district': 'Las Anod /Laascaanood'
							})
							sool_check = True

						if self.hudun:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sool/Sool',
								'district': 'Hudun /Xuddun'
							})
							sool_check = True

						if self.taleh:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sool/Sool',
								'district': 'Taleh/Taleex'
							})
							sool_check = True

						if self.aynabo:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sool/Sool',
								'district': 'Aynabo/Caynabo'
							})
							sool_check = True

						if self.sool_check and sool_check == False:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sool/Sool',
							})

						# check togder region
						togdheer_check = False

						if self.burao:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Togdheer/Togdheer',
								'district': 'Burao/Burco'
							})
							togdheer_check = True

						if self.odwayne:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Togdheer/Togdheer',
								'district': 'Odwayne/Oodwayne'
							})
							togdheer_check = True

						if self.buhodle:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Togdheer/Togdheer',
								'district': 'Buhodle/Buuhoodle'
							})
							togdheer_check = True

						if self.togdheer_check and togdheer_check == False:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Togdheer/Togdheer',
							})

						# check awadal
						awdal_check = False

						if self.borama:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Awdal/Awdal',
								'district': 'Borama/Boorama '
							})
							awdal_check = True

						if self.zeila:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Awdal/Awdal',
								'district': 'Zeila/Saylac '
							})
							awdal_check = True

						if self.baki:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Awdal/Awdal',
								'district': 'Baki/Baki'
							})
							awdal_check = True

						if self.lughaya:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Awdal/Awdal',
								'district': 'Lughaya/Lughaya'
							})
							awdal_check = True

						if self.awdal_check and awdal_check == False:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Awdal/Awdal',
							})

						# check sahil
						sahil_check = False

						if self.berbera:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sahil/Saaxil',
								'district': 'Berbera/Berbera'
							})
							sahil_check == True

						if self.sheekh:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sahil/Saaxil',
								'district': 'Sheikh/Sheekh'
							})
							sahil_check == True
						
						if self.sahil_check and sahil_check == False:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sahil/Saaxil',
							})

				# Add the destiantion and implementing organization
				new_sub_project_doc.destination_organization_table = []
				for destination in self.destination_organization_table:
					if destination.implementing_organization:
						new_sub_project_doc.append("destination_organization_table", {
								"implementing_organization": destination.implementing_organization,
								"amount":destination.amount,
								"reporting_obligation":destination.reporting_obligation,
								"reporting_organization":destination.reporting_organization
							})
					else:
						try:
							new_sub_project_doc.append("destination_organization_table", {
									"implementing_organization": destination.enter_name_of_implementing_organization,
									"amount":destination.amount,
									"reporting_obligation":destination.reporting_obligation,
									"reporting_organization":destination.reporting_organization
								})
						except:
							pass	

				new_sub_project_doc.insert(ignore_permissions=True)
				# set the just created sub project name to the field
				self.sub_project_title = sub_project_name

		# check if reporting organization has been selected in implementing table
		for implementor in self.destination_organization_table:
			if implementor.reporting_obligation == 1 or implementor.reporting_organization:
				pass
			else:
				# reporting organization not defined hence make destination organization reporting
				implementor.reporting_organization = self.select_your_organization

		# add just created organizations to form
		append_organizations_created_on_verification(self)

	elif self.status == "Validated":
		# pass the subproject_name has been created
		project_doc = frappe.get_doc("Project",self.sub_project_title)
		project_doc.status = 	self.status
		project_doc.save(ignore_permissions = True)
		
		# check if single fields are given
		single_fields_required_from_group(self)
		# add just created organization
		append_organizations_created_on_verification(self)

	elif self.status == "Approved":
		pass

	# none status based actions
	calulated_committed_amount = 0
	calulated_pipeline_amount = 0
	calculated_funding_gap = 0
	# loop through sources tables to generate total
	for source_organization_entry in self.source_organization_table:
		#calculate different amounts
		if source_organization_entry.funding_status_select_one == "Pipeline":
			calulated_pipeline_amount += source_organization_entry.amount
		elif source_organization_entry.funding_status_select_one == "Committed":
			calulated_committed_amount += source_organization_entry.amount

	#set the different 
	if self.total_project_amount_in_usd:
		calculated_funding_gap = self.total_project_amount_in_usd - calulated_committed_amount
	self.amount_commited = calulated_committed_amount
	self.funding_gap_in_usd = calculated_funding_gap
	self.amount_in_pipeline = calulated_pipeline_amount


def unverify_n_redirect(self,message,current_section):
	'''
	Function that unverifies the form and
	redirect the user to the page the need
	to complete
	'''
	frappe.msgprint(message)
	# Unverify the form
	unverify_form(self)
	# redirect the user to that page
	self.current_section = current_section

def check_items_for_required_tables(self):
	'''
	Function that checks all the required tables to ensure 
	that all the required tables are filled correctly
	'''
	if self.status == "Verified" or self.status == "Validated" \
		or self.status == "Approved":

		continue_executing = True
		# check sources table
		if continue_executing:
			screen_number = list_of_sections.index('section_a_section') + 1
			if len(self.source_organization_table) > 0:
				# check if user confirmed the sources
				if self.all_sources_filled_check:
					# check if all the required fields are filled correctly
					for source in self.source_organization_table:
						if source.amount and source.funding_status_select_one  and source.source_organization_type:
							if source.source_organization_name:
								pass
							elif source.enter_name_of_source_organization and source.enter_acronym_of_source_organization_eg_un_for_united_nations:
								pass
							elif source.source_organization_does_not_exist_in_the_list_above and source.enter_funder_type != 'OTHER' and source.enter_name_of_source_organization:
								pass
							elif source.source_organization_does_not_exist_in_the_list_above and source.enter_funder_type == 'OTHER' and source.other_specify_name:
								pass
							else:
								continue_executing = False
								unverify_n_redirect(self,"Source details are missing in the source table in screen {}".format(screen_number),'section_a_section')
						elif source.amount and source.source_organization_does_not_exist_in_the_list_above:
							pass
						else:
							continue_executing = False
							unverify_n_redirect(self,"Source details are missing in the source table in screen {}".format(screen_number),'section_a_section')
				else:
					continue_executing = False
					unverify_n_redirect(self,"You have not ticked the checkbox to confirm the sources in screen {}".format(screen_number),'section_a_section')
			else:
				continue_executing = False
				unverify_n_redirect(self,"You have not added any sources organization in screen {}".format(screen_number),'section_a_section')

		# check the implementing organization table
		if continue_executing:
			screen_number = list_of_sections.index('destination_implementing_and_reporting_organization_section') + 1
			# check len of destionation and implementing org table
			if len(self.destination_organization_table) > 0:
				# check if  destionation and implementing orgs
				if self.all_implementors_added_check:
					# check if all the required fields are filled correctly
					for implementor in self.destination_organization_table:
						if implementor.amount:
							if implementor.implementing_organization:
								pass
							elif implementor.enter_name_of_implementing_organization and implementor.enter_acronym and implementor.organization_type:
								# check if this implementing organization got a match
								found_suggestions = find_simalar_organization(implementor.enter_name_of_implementing_organization,implementor.enter_acronym)

								if len(found_suggestions) > 0:
									# check if the user marked either yes or no in the suggestions
									if self.found_implementors_match or self.did_not_find_implementors_match:
										pass
									else:
										section_to_redirect = list_of_sections.index('implementing__partners_section_section') + 1
										message = "You have not filled required field in screen {}".format(section_to_redirect)
										unverify_n_redirect(self,message,'implementing__partners_section_section')
								else:
									# no suggestion was found hence pass
									pass
							else:
								unverify_n_redirect(self,"Some details are missing in the implementing organizations table in screen {}".format(screen_number),'destination_implementing_and_reporting_organization_section')
						else:
							unverify_n_redirect(self,"Amount missing in the implementing organizations table in screen {}".format(screen_number),'destination_implementing_and_reporting_organization_section')
				else:
					unverify_n_redirect(self,"You have not ticked the checkbox to confirm implementing organizations in screen {}".format(screen_number),'destination_implementing_and_reporting_organization_section')
			else:
				unverify_n_redirect(self,"You have not added any implemting organization in screen {}".format(screen_number),'destination_implementing_and_reporting_organization_section')

def loop_through_required_list(self,list_of_required_fields,status_to_revert):
	'''
	Function that loops through a list of given 
	fields and determine if any of them is missing
	'''
	# looping throught fields
	for field in list_of_required_fields:
		if field["field_value"]:
			# pass because field is available
			pass
		else:
			screen_number = list_of_sections.index(field['section']) + 1
			frappe.msgprint("You have not filled required field '{}' in screen {}".format(field["field_name"],screen_number))
			# unverify form
			unverify_form(self)
			# redirect the user to that page
			self.current_section = field['section']
			
def check_required_fields(self):
	'''
	Function for checking required fields
	before any dependant action is performed
	'''
	# fields required before saving
	required_fields_before_saving = [
		{"field_name":"Organization","field_value":self.select_your_organization,"section":"section_break_1"},
		{"field_name":"Organization Acronym","field_value":self.organization_acronym,"section":"section_break_1"},
		{"field_name":"New Project Title","field_value":self.new_project_title,"section":"section_break_1"},
		{"field_name":"Project Code","field_value":self.new_project_code,"section":"section_break_1"},
	]

	required_fields_before_verification = [
		{"field_name":"Organization Acronym","field_value":self.organization_acronym,"section":"section_break_1"},
		{"field_name":"Master Project Title","field_value":self.master_project_title,"section":"section_break_1"},
		{"field_name":"Project Start Date","field_value":self.project_start_date,"section":"project_details_section"},
		{"field_name":"Project End Date","field_value":self.project_end_date,"section":"project_details_section"},
	]

	required_fields_before_validation = [

	]

	required_fields_before_approval = [
		
	]
	
	# check if all the required fields before saving document in various statuses
	if self.status == "Pending Verification":
		# check if all the required fields are given
		loop_through_required_list(self,required_fields_before_saving,"Pending Verification")

	elif self.status == "Verified":
		# first check project amounts
		check_project_amounts(self)
		# loop through other required fields
		loop_through_required_list(self,required_fields_before_saving,"Pending Verification")
		loop_through_required_list(self,required_fields_before_verification,"Pending Verification")
		# check to ensure all the required tables are filled_correctly
		check_items_for_required_tables(self)
		# check if single fields are given
		single_fields_required_from_group(self)

	elif self.status == "Validated":
		# first check project amounts
		check_project_amounts(self)
		# loop through other required fields
		loop_through_required_list(self,required_fields_before_saving,"Verified")
		loop_through_required_list(self,required_fields_before_verification,"Verified")
		loop_through_required_list(self,required_fields_before_validation,"Verified")

	elif self.status == "Approved":
		# first check project amounts
		check_project_amounts(self)
		# loop through other required fields
		loop_through_required_list(self,required_fields_before_saving,"Validated")
		loop_through_required_list(self,required_fields_before_verification,"Validated")
		loop_through_required_list(self,required_fields_before_validation,"Validated")
		loop_through_required_list(self,required_fields_before_approval,"Validated")

def check_project_amounts(self):
	'''
	Function that checks the project amounts given to ensure they
	are correct or correctly stated
	'''
	# ensure the project amount are given before saving
	if self.total_project_amount_in_usd:
		# ensure the amount is not zero
		if self.total_project_amount_in_usd > 0:
			pass
		else:
			frappe.msgprint("You have not filled required field 'Total Project Amount' in the Project Amount Section")
			# unverify form
			unverify_form(self)
	else:
		frappe.msgprint("You have not filled required field 'Total Project Amount' in the Project Amount Section")
		# unverify form
		unverify_form(self)

def unverify_form(self):
	'''
	Function that make unverifies the form
	'''
	self.status = "Pending Verification" 
	self.viewing_information = 0
	self.verified = 0
	self.verified = 0
	self.verified_by = ""
	self.validated = 0
	self.validated_by = ""
	self.approved = 0
	self.approved_by = ""

def check_or_create_source_orgs(self):
	'''
	check if the source organization the user
	gave have been created
	'''
	# check if the organization has been created 
	list_of_source = frappe.get_list("Source Organization",
		fields=["*"],
		filters = {
			"parent":self.name,
			"parenttype":"Project Registration Form",
			"parentfield":"source_organization_table",
			"source_organization_does_not_exist_in_the_list_above":1
		})

	if len(list_of_source) == 0:
		pass
	else:
		# check if all the organization have been created
		for source in list_of_source:
			if source.source_organization_does_not_exist_in_the_list_above and source.enter_name_of_source_organization:
				list_of_organizations = frappe.get_list("Organization",
					fields=["*"],
					filters = {
						"name":source["enter_name_of_source_organization"]
					})
			elif source.source_organization_does_not_exist_in_the_list_above and source.enter_funder_type == 'OTHER':
				list_of_organizations = frappe.get_list("Organization",
					fields=["*"],
					filters = {
						"name":source["other_specify_name"]
					})
			
			if len(list_of_organizations)== 0 and source.enter_funder_type != 'OTHER' and source.source_organization_does_not_exist_in_the_list_above == 1:
				# create the organization 
				org_doc = frappe.get_doc({"doctype":"Organization"})
				org_doc.name = source["enter_name_of_source_organization"]
				org_doc.organization_type = source["enter_funder_type"]
				org_doc.full_name_of_the_organization = source["enter_name_of_source_organization"]
				org_doc.acronym = source["enter_acronym_of_source_organization_eg_un_for_united_nations"]
				org_doc.insert(ignore_permissions=True)
				# insert the document to the database
				org_doc.insert(ignore_permissions=True)

			elif len(list_of_organizations) == 0 and source.enter_funder_type == 'OTHER' and source.source_organization_does_not_exist_in_the_list_above == 1:
				org_doc = frappe.get_doc({"doctype":"Organization"})
				org_doc.name = source["other_specify_name"]
				org_doc.full_name_of_the_organization = source["other_specify_name"]
				org_doc.organization_type = 'OTHER'
				org_doc.insert(ignore_permissions=True)

			else:
				pass

def check_or_create_implementing_orgs(self):
	'''
	check if the source organization the user
	gave have been created
	'''
	# check if the user is currently on either either the destination -implementors 
	# section or the suggestion section
	
	if self.current_section == "destination_implementing_and_reporting_organization_section" \
		or self.current_section == "implementing__partners_section_section":
		pass
	else:
		# check if the organization has been created 
		list_of_implementors = frappe.get_list("Destination Implementing Reporting",
			fields=["*"],
			filters = {
				"parent":self.name,
				"parenttype":"Project Registration Form",
				"parentfield":"destination_organization_table",
				"implementing_organization_is_not_in_the_list_above":1
			})
		
		if len(list_of_implementors) == 0:
			pass
		else:
			# check if all the organization have been created
			for implementor in list_of_implementors:
				list_of_organizations = frappe.get_list("Organization",
					fields=["*"],
					filters = {
						"name":implementor["enter_name_of_implementing_organization"]
					})

				if len(list_of_organizations)== 0:
					# create the organization 
					org_doc = frappe.get_doc({"doctype":"Organization"})
					org_doc.name = implementor["enter_name_of_implementing_organization"]
					org_doc.full_name_of_the_organization = implementor["enter_name_of_implementing_organization"]
					org_doc.acronym = implementor["enter_acronym"]
					org_doc.organization_type = implementor["organization_type"]
					
					# now insert the document in the database
					org_doc.insert(ignore_permissions=True)

def single_fields_required_from_group(self):
	'''
	Function that checks that atleast one field from the list of 
	given fields is filled or true
	'''
	# return_value = False # return value is initialized to False

	# required_sectors_before_saving = [
	# 	{"field_name":"Health","field_value":self.health},
	# 	{"field_name":"Education","field_value":self.education},
	# 	{"field_name":"Wash Sector","field_value":self.wash_sector},
	# 	{"field_name":"Economy","field_value":self.economy},
	# 	{"field_name":"Energy and Extractives","field_value":self.energy_and_extractives},
	# 	{"field_name":"Production","field_value":self.production},
	# 	{"field_name":"Infrastructure","field_value":self.infrastructure},
	# 	{"field_name":"Governance ","field_value":self.governance},
	# 	{"field_name":"Environment","field_value":self.environment},
	# 	{"field_name":"Cross Cutting Employment and Labor","field_value":self.cross_cutting_employment_and_labor},
	# 	{"field_name":"Cross Cutting Social Protection","field_value":self.cross_cutting_social_protection},
	# 	{"field_name":"Cross Cutting Youth","field_value":self.cross_cutting_youth},
	# ]

	# # looping through list of required fields
	# for field in required_sectors_before_saving:
	# 	if field["field_value"]:
	# 		# pass because field is available
	# 		return_value = True
	# 	else:
	# 		pass

	# if(return_value):
	# 	pass
	# else:
	# 	frappe.throw("You Need to Specify Atleast One Sector The Project Conributes To")
	return_value = True


def set_next_page_option(self):
	'''
	Function that is used to set the next page the user should 
	see for special cases
	'''
	if self.current_section == "section_a_section":
		# source organizations section
		pass

	'''
	# check if the user needs to see this section in the first place
	if self.current_section == "source_suggestion":
		# check if any of the sources in the source is not already in the database
		list_of_sources = frappe.get_list("Source Organization",
			fields=["source_organization_name","source_organization_does_not_exist_in_the_list_above","enter_name_of_source_organization"],
			filters = {
				"parent":self.name,
				"parenttype":"Project Registration Form",
				"parentfield":"source_organization_table",
				"source_organization_does_not_exist_in_the_list_above":1
			})

		# first clear the table of suggestions
		self.source_suggestion_table = []
		self.yes_found_source_match = ""
		self.no_no_source_match = ""

		# for source in list_of_sources:
		# 	# find similar sources
		# 	list_of_source_suggetions = find_simalar_organization(source.enter_name_of_source_organization,source.enter_acronym_of_source_organization_eg_un_for_united_nations)
			
		# 	if len(list_of_sources) > 0:
		# 		for source_suggestion in list_of_source_suggetions:
		# 			self.append("source_suggestion_table", {
		# 				"given__source_name":source.enter_name_of_source_organization,
		# 				"did_you_mean":source_suggestion,
		# 			})
		
		if len(self.source_suggestion_table) > 0:
			# allow user to see page
			pass
		else:
			source_suggestion_index = list_of_sections.index("source_suggestion")
			# place new page for user to see depending on direction
			if self.direction == "Forward":
				self.previous_section = list_of_sections[source_suggestion_index]
				self.current_section = list_of_sections[source_suggestion_index+1]
				self.next_section = list_of_sections[source_suggestion_index + 2]
			else:
				# set the next section
				self.previous_section = list_of_sections[source_suggestion_index - 2]
				self.current_section = list_of_sections[source_suggestion_index-1]
				self.next_section = list_of_sections[source_suggestion_index]
	'''

	if self.current_section == "section_b_section":
		pass

	if self.current_section == "destination_implementing_and_reporting_organization_section":
	
		# confirmation field first
		self.all_implementors_added_check = 0
		
		if self.total_implementor_check:
			# check if the organization already on the table
			current_organization_in_table = False
			for implementor in self.destination_organization_table:
				if implementor.implementing_organization == self.select_your_organization:
					current_organization_in_table = True
					if implementor.amount == self.total_project_amount_in_usd:
						pass
					else:
						implementor.amount = self.total_project_amount_in_usd

			# determine whether or not to add the implementor in the table
			if current_organization_in_table:
				pass
			else:
				# add organization as implementor
				self.append("destination_organization_table", {
					"implementing_organization": self.select_your_organization,
					"reporting_obligation":1,
					"reporting_organization":self.select_your_organization,
					"amount":self.total_project_amount_in_usd
				})

		elif self.partial_implementor_check:
			# check if the organization already on the table
			current_organization_in_table = False
			for implementor in self.destination_organization_table:
				if implementor.implementing_organization == self.select_your_organization:
					current_organization_in_table = True
					if(implementor.amount) < self.total_project_amount_in_usd:
						pass
					else:
						implementor.amount = 0
			
			# determine whether or not not add the amount in the table
			if current_organization_in_table:
				pass
			else:
				# add organization as implementor
				self.append("destination_organization_table", {
					"implementing_organization": self.select_your_organization,
					"reporting_obligation":1,
					"reporting_organization":self.select_your_organization
				})
		elif self.not_implementor_check:
			# allow the user to continue
			pass
			
	if self.current_section == "implementing__partners_section_section":		
		# check if any of the implementors is not already in the database
		list_of_implementors = frappe.get_list("Destination Implementing Reporting",
			fields=["*"],
			filters = {
				"parent":self.name,
				"parenttype":"Project Registration Form",
				"parentfield":"destination_organization_table",
				"implementing_organization_is_not_in_the_list_above":1
			})
		
		# fist clear the table of suggestions
		self.implementing_partners_suggestion_table = []
		self.found_implementors_match = 0
		self.did_not_find_implementors_match = 0

		for implementor in list_of_implementors:
			# find similar implementors
			list_of_implementor_suggetions = find_simalar_organization(implementor.enter_name_of_implementing_organization,implementor.enter_acronym)

			if len(list_of_implementor_suggetions) > 0:
				for implementor_suggestion in list_of_implementor_suggetions:
					self.append("implementing_partners_suggestion_table", {
						"given_name":implementor.enter_name_of_implementing_organization,
						"did_you_mean":implementor_suggestion
					})

			else:
				# if the entered name of the organization does not match any suggestion
				# from the database go right ahead and create the organization
				new_implementor_doc = frappe.get_doc({"doctype":"Organization"})
				new_implementor_doc.name = implementor["enter_name_of_implementing_organization"],
				new_implementor_doc.full_name_of_the_organization = implementor["enter_name_of_implementing_organization"]
				new_implementor_doc.acronym = implementor["enter_acronym"]

				# # save the document
				new_implementor_doc.insert(ignore_permissions=True)
				

		# detrmine if the user should see this page
		if len(self.implementing_partners_suggestion_table) > 0:
			# allow user to see page
			pass
		else:
			implementor_suggestion_index = list_of_sections.index("implementing__partners_section_section")
			# place new page for user to see depending on direction
			if self.direction == "Forward":
				self.previous_section = list_of_sections[implementor_suggestion_index]
				self.current_section = list_of_sections[implementor_suggestion_index+1]
				self.next_section = list_of_sections[implementor_suggestion_index + 2]
			else:
				# set the next section section
				self.previous_section = list_of_sections[implementor_suggestion_index - 2]
				self.current_section = list_of_sections[implementor_suggestion_index-1]
				self.next_section = list_of_sections[implementor_suggestion_index]

	if self.current_section == "project_details_section":
		# check if the project name has been given
		if self.master_project_title:
			# set the project title name 
			self.project_name = self.master_project_title
			# get the correct code
			project_doc = frappe.get_doc("Project",self.master_project_title)
			self.project_code = project_doc.project_code
		else:
			pass
	
	if self.current_section == "pillar_alignment_section":
		# hold all marked pillars
		pillars_holder = []
		for item in self.pillar_alignment_table:
			if item.yes:
				pillars_holder.append(item.pillar)

		# first clear the pillars table
		self.pillar_alignment_table = []
		self.confirm_pillar_selection = 0

		# get all the pillars
		list_of_pillars = frappe.get_list("NDP",
			fields = ["name"],
			filters={
				"type": "Pillar"
		})

		# if pillars
		if len(list_of_pillars) > 0:
			for pillar in list_of_pillars:
				# check if that pillar is marked
				if pillar["name"] in pillars_holder:
					self.append("pillar_alignment_table", {
						"pillar":pillar["name"],
						"yes":1
					})
				else:
					self.append("pillar_alignment_table", {
						"pillar":pillar["name"],
						"yes":0
					})	
		else:
			frappe.throw("No Pillars Were Found Please Select Add NDP Pillars In Order to Continue")
		

	if self.current_section == "sector_alignment_section_section":
		# hold all marked sectors
		sectors_holder = []
		for item in self.sector_alignment_table:
			if item.yes:
				sectors_holder.append(item.sector)

		# first clear the sector table
		self.sector_alignment_table = []
		self.confirm_sector_selection = 0
		# get the relavant sectors
		list_of_relevant_sectors = []

		# get sectors for each selected pillar
		for pillar in self.pillar_alignment_table:
			# get all the sectors for each ticked pillar
			if pillar.yes:
				sectors_for_pillar = frappe.get_list("NDP",
					fields=["name","parent_ndp"],
					filters = {
						"type":"Sector",
						"parent_ndp":pillar.pillar
				})

				# append the sectors to the list of pillars
				list_of_relevant_sectors += sectors_for_pillar

		# append the sectors to the sectors table
		for sector in list_of_relevant_sectors:
			# check if that sector is marked
			if sector["name"] in sectors_holder:
				self.append("sector_alignment_table", {
					"pillar":sector["parent_ndp"],
					"sector":sector["name"],
					"yes":1
				})
			else:
				self.append("sector_alignment_table", {
					"pillar":sector["parent_ndp"],
					"sector":sector["name"]
				})

	if self.current_section == "priority_programs_section_section":
		# hold all marked programs
		programs_holder = []
		for item in self.priority_programs_table:
			if item.yes:
				programs_holder.append(item.priority_program)

		# first clear the priority programs table
		self.priority_programs_table = []
		self.confirm_priority_programs = 0
		self.no_ndp_check = 0
		# get sectors for each selected pillar
		for sector in self.sector_alignment_table:
			# get all the programs for each ticked sector
			if sector.yes:
				program_for_sector = frappe.get_list("Program",
					fields=["name","program_title","program","sector"],
					filters = {
						"sector":sector.sector,
				})

				for program in program_for_sector:
					# check if the program is marked
					if program["name"] in programs_holder:
						self.append("priority_programs_table", {
							"priority_program":program["name"],
							"priority_program_description":program["program"],
							"sector":program["sector"],
							"yes":1
						})
					else:
						self.append("priority_programs_table", {
							"priority_program":program["name"],
							"priority_program_description":program["program"],
							"sector":program["sector"]
						})

	if self.current_section == "relevant_mdas_section_section":
		# hold all marked programs
		mdas_holder = []
		for item in self.relevant_mda_table:
			if item.lead_mda:
				mdas_holder.append(item.relevant_mda_per_sector)

		# # first clear the relevant mdas table
		self.relevant_mda_table = []
		self.confirm_mdas = 0
		self.no_gi_check = 0
		
		# loop through all the selected sectors
		for sector in self.sector_alignment_table:
			if sector.yes:
				# get all the mdas for each ticked sector
				mdas_for_sector_list = frappe.get_list("MDA NDP Alignment",
					fields=["sector","parent"],
					filters = {
						"sector":sector.sector,
						"parenttype":"MDA"
				})

				# loop through the found ndp 
				for mda in mdas_for_sector_list:
					# check if it exits
					if mda['parent'] in mdas_holder:
						self.append("relevant_mda_table", {
							"relevant_mda_per_sector":mda["parent"],
							"sector":mda["sector"],
							"lead_mda":1
						})
					else:
						self.append("relevant_mda_table", {
							"relevant_mda_per_sector":mda["parent"],
							"sector":mda["sector"]
						})

	if self.current_section == "relevant_ndp_outcome_by_selected_priority_program_section":
		# hold all marked outcomes
		outcomes_holder = []
		for item in self.relevant_outcomes__table:
			if item.yes:
				outcomes_holder.append(item.outcome)

		# first clear the relevatn table
		self.relevant_outcomes__table = []
		self.confirm_outcomes = 0
		self.no_outcome_check = 0
		
		# loop through all the selected priority programs
		for program in self.priority_programs_table:
			# get all the outcomes linked to these outcomes
			if program.yes:
				outomes_for_program_list = frappe.get_list("Outcome",
					fields=["name","sector","outcome","linked_priority_program"],
					filters = {
						"linked_priority_program":program.priority_program
				})

				for outcome in outomes_for_program_list:
					# check if outcome is checked
					if outcome['name'] in outcomes_holder:
						self.append("relevant_outcomes__table", {
							"outcome":outcome["name"],
							"outcome_description":outcome["outcome"],
							"priority_program":outcome["linked_priority_program"],
							"sector":outcome["sector"],
							"yes":1
						})
					else:
						self.append("relevant_outcomes__table", {
							"outcome":outcome["name"],
							"outcome_description":outcome["outcome"],
							"priority_program":outcome["linked_priority_program"],
							"sector":outcome["sector"]
						})

def find_simalar_organization(given_name,given_acronym):
	'''
	Function That Check if There Are Any Organization with Similar
	Names and Acronyms to those given in the Form
	'''
	# holds all the organization found through this search
	all_found_organization = []

	def process_results(list_of_organizations):
		'''
		Function that will help to make the code Drier within
		this function by reducing repetitive tasks
		'''

		if len(list_of_organizations) > 0:
			for organization in list_of_organizations:
				organization_name = organization[0]
				all_found_organization.append(organization_name)

	# now check for similarities in name
	list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '%{}%'".format(given_name))
	process_results(list_of_organizations)

	# full name of each word in the name query
	list_of_words_in_name = given_name.split()
	# get rid of conjuctions
	common_conjuctions = ["of","the"]
	# loop through name parts
	for name_part in list_of_words_in_name:
		if name_part.lower() in common_conjuctions:
			pass
		else:
			# process results 
			list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '%{}%'".format(name_part))
			process_results(list_of_organizations)

	# starting 2 letters letter query
	first_two_letters = given_name[:2]
	list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '{}%'".format(first_two_letters))
	if len(list_of_organizations) > 0:
		process_results(list_of_organizations)
	else:
		pass

	# Acronym matches
	# full name of acronym matches
	if given_acronym:
		search_acronymn = given_acronym
		list_of_organizations = frappe.db.sql("select name from `tabOrganization` where acronym like '%{}%'".format(search_acronymn))
		if len(list_of_organizations) > 0:
			process_results(list_of_organizations)
		else:
			pass
	
	# loop through unique organizations
	unique_organizations =  set(all_found_organization)
	
	# return unique organizations
	return unique_organizations


def find_simalar_projects(self):
	'''
	Function That Checks if There Are Any Master Project With Similar Name
	if the user does not find their projects listed in the list of Projects
	'''
	# holds all the organization found through this search
	all_found_projects = [] 

	def process_results(list_of_projects):
		'''
		Function that will help to make the code Drier within
		this function by reducing repetitive tasks
		'''
		if len(list_of_projects) > 0:
			for project in list_of_projects:
				project_name = project[0]
				all_found_projects.append(project_name)
		
	# now check for similarities in name
	list_of_projects = frappe.db.sql("select name from `tabProject` where name like '%{}%' && master_project = 1".format(self.new_project_title))
	process_results(list_of_projects)

	# full name of each word in the name query
	list_of_words_in_name = self.new_project_title.split()
	# get rid of conjuctions
	common_conjuctions = ["of","the"]
	# loop through name parts
	for name_part in list_of_words_in_name:
		if name_part.lower() in common_conjuctions:
			pass
		else:
			# process results 
			list_of_projects = frappe.db.sql("select name from `tabProject` where name like '%{}%' && master_project = 1".format(name_part))
			process_results(list_of_projects)

	# starting 2 letters letter query
	first_two_letters = self.new_project_title[:2]
	list_of_projects = frappe.db.sql("select name from `tabProject` where name like '{}%' && master_project = 1".format(first_two_letters))
	if len(list_of_projects) > 0:
		process_results(list_of_projects)
		pass

	# Acronym matches
	# full name of acronym matches
	search_project_code = self.new_project_code
	list_of_projects = frappe.db.sql("select name from `tabProject` where project_code like '%{}%' && master_project = 1".format(search_project_code))
	if len(list_of_projects) > 0:
		process_results(list_of_projects)
		pass

	# sources based matches
	for source in self.source_organization_table:
		# check if user gave a registered source
		if source.source_organization_does_not_exist_in_the_list_above == 0:
			# get relevant projects
			list_of_projects = frappe.db.sql("select parent from `tabSource Organization` where  parenttype = 'Project' && parentfield = 'source_organization_table' && source_organization_name = '{}'".format(source.source_organization_name))
			if len(list_of_projects) > 0:
				# check if project are master
				for project in list_of_projects:
					doc = frappe.get_doc('Project',project[0])
					# if the project is a master project
					if doc.master_project == 1:
						all_found_projects.append(project[0])

	# co-implemtors based matches
	for co_implementor in self.co_implementing_table:
		# get relevant projects
			list_of_projects = frappe.db.sql("select parent from `tabCoImplementing Partners Table` where  parenttype = 'Project' && parentfield = 'co_implementing_table' && organization_name = '{}'".format(co_implementor.organization_name))
			if len(list_of_projects) > 0:
				# check if project are master
				for project in list_of_projects:
					doc = frappe.get_doc('Project',project[0])
					# if the project is a master project
					if doc.master_project == 1:
						all_found_projects.append(project[0])	
	
	unique_results =  set(all_found_projects)
	return unique_results

def append_organizations_created_on_verification(self):
	'''
	Function that appends the correct creates source 
	to form on verification
	This function is called under validation i.e after
	verification has already taken place
	'''
	
	# append the just created source organizations
	for source in self.source_organization_table:
		if source.source_organization_does_not_exist_in_the_list_above and source.enter_funder_type != 'OTHER':
			# confirm if the organization has been created
			try:
				frappe.get_doc("Organization",source.enter_name_of_source_organization)
				source.source_organization_name = source.enter_name_of_source_organization
				source.source_organization_does_not_exist_in_the_list_above = 0 
				# source.enter_name_of_source_organization = ""
				# source.enter_acronym_of_source_organization_eg_un_for_united_nations = ""
			except:
				
				# unverify form and ask the user to verify again
				screen_number = list_of_sections.index('section_a_section') + 1
				message = "Please confirm that all the details of the funding \
					organization table are given and verify the form again \
						in screen {}".format(screen_number)
				# call the unverify and redirect function here
				unverify_n_redirect(self,message,'section_a_section')
		elif source.source_organization_does_not_exist_in_the_list_above and source.enter_funder_type == 'OTHER':
			try:
				# frappe.get_doc("Organization",source.other_specify_name)
				source.source_organization_name = source.other_specify_name
				source.source_organization_does_not_exist_in_the_list_above = 0 
				source.enter_name_of_source_organization = ""
				# source.enter_acronym_of_source_organization_eg_un_for_united_nations = ""
				source.source_organization_type = 'OTHER'
				
			except:
				
				# unverify form and ask the user to verify again
				screen_number = list_of_sections.index('section_a_section') + 1
				message = "Please confirm that all the details of the funding \
					organization table are given and verify the form again \
						in screen {}".format(screen_number)
				# call the unverify and redirect function here
				unverify_n_redirect(self,message,'section_a_section')
		else:
			pass


	# append the just created implementing organizations
	for implementor in self.destination_organization_table:
		if not implementor.implementing_organization:
			# confirm if the organization has been created
			try:
				frappe.get_doc("Organization",implementor.enter_name_of_implementing_organization)
				implementor.implementing_organization = implementor.enter_name_of_implementing_organization
				# clear the entered fields
				implementor.implementing_organization_is_not_in_the_list_above = ""
				implementor.enter_name_of_implementing_organization = ""
				implementor.enter_acronym = ""
				implementor.organization_type = ""
			except:
				# unverify form and ask the user to verify again
				screen_number = list_of_sections.index('destination_implementing_and_reporting_organization_section') + 1
				message = "Please confirm that all the details of the implementing \
					organization table are given and verify the form again \
						in screen {}".format(screen_number)
				# call the unverify and redirect function here
				unverify_n_redirect(self,message,'destination_implementing_and_reporting_organization_section')

def add_created_organizations_to_form(self):
	'''
	Function that add all the newly created organizations 
	to the form
	'''
	implementing_orgs_holder = []
	for implementor in self.destination_organization_table:
		implemetor_dict = {"implementing_organization":implementor.implementing_organization,
		"implementing_organization_is_not_in_the_list_above":implementor.implementing_organization_is_not_in_the_list_above,
		"enter_name_of_implementing_organization":implementor.enter_name_of_implementing_organization,
		"enter_acronym":implementor.enter_acronym,
		"organization_type":implementor.organization_type,
		"amount":implementor.amount,
		"reporting_obligation":implementor.reporting_obligation,
		"reporting_organization":implementor.reporting_organization,
		}
		# append the dict to the holder
		implementing_orgs_holder.append(implemetor_dict)

	def add_organizations_recursive(self,implementing_orgs_holder):
		'''
		Recursive function that is called until all organizations
		are added
		'''
		run_rest_of_function = True
		if self.current_section == "destination_implementing_and_reporting_organization_section" \
			or self.current_section == "implementing__partners_section_section":
			run_rest_of_function = False
		else:
			# check if there any suggestion in the suggestions section
			if len(self.implementing_partners_suggestion_table)>0:
				if self.found_implementors_match or self.did_not_find_implementors_match:
					# check if all the organizations have been created
					for implementor in implementing_orgs_holder:
						if implementor['implementing_organization_is_not_in_the_list_above']:
							# check if the given name is already saved
							list_of_found_imps = frappe.get_list("Organization",
								fields=["name"],
								filters = {
									"name":implementor['enter_name_of_implementing_organization']
								})

							if(len(list_of_found_imps)>0):
								pass
							else:
								run_rest_of_function = False
								self.save()
				else:
					self.current_section = "implementing__partners_section_section"
					run_rest_of_function = False
					frappe.msgprint("You have not selected any option under suggestion")
			else:
				# check if all the organizations have been created
				for implementor in implementing_orgs_holder:
					if implementor['implementing_organization_is_not_in_the_list_above']:
						# check if the given name is already saved
						list_of_found_imps = frappe.get_list("Organization",
							fields=["name"],
							filters = {
								"name":implementor['enter_name_of_implementing_organization']
							})

						if(len(list_of_found_imps)>0):
							pass
						else:
							run_rest_of_function = False
							self.save()
		
		if run_rest_of_function:
			self.destination_organization_table = []
			# add the removed organizations
			for implementor in implementing_orgs_holder:
				# add the newly created organization to the child table
				if implementor['implementing_organization_is_not_in_the_list_above']:
					organization_doc_name = implementor['enter_name_of_implementing_organization']
				else:
					organization_doc_name = implementor['implementing_organization']
				self.append("destination_organization_table", {
						"implementing_organization":organization_doc_name,
						"amount":implementor['amount'],
						"reporting_obligation":implementor['reporting_obligation'],
						"reporting_organization":implementor['reporting_organization'],
					})

	# call the recursive function
	add_organizations_recursive(self,implementing_orgs_holder)

def map_data_to_project(self):
	'''
	Function that Updates the details of the organization
	to the organization form once the form is approved
	'''
	# check if the project has been verified
	if self.status == "Verified":
		pass

	if self.status == "Admin Verified":
		pass
	
	if self.status == "Approved":
		sub_project_doc = frappe.get_doc("Project",self.sub_project_title)
		# top section
		sub_project_doc.status = self.status
		sub_project_doc.sub_project = 0
		sub_project_doc.sub_project = 1
		sub_project_doc.link_to_master_project = self.master_project_title
		sub_project_doc.full_project_title = self.full_project_title

		# general_project_details_section
		# sub_project_doc.project_name = self.sub_project_name
		sub_project_doc.project_code = self.new_project_code
		sub_project_doc.phase = self.phase
		# sub_project_doc.full_project_title = self.
		sub_project_doc.sub_project_name = self.sub_project_name
		# sub_project_doc.project_renewed_from = self.sub_project_name
		sub_project_doc.your_organization = self.select_your_organization
		sub_project_doc.organization_acronym = self.organization_acronym

		# Source Organization Section
		sub_project_doc.source_organization_table = []

		for source in self.source_organization_table:
			
			# check if the source type and source are given correctly
			if source.source_organization_type and source.source_organization_name :
				new_source_doc = frappe.get_doc('Organization', source.source_organization_name)
				sub_project_doc.append("source_organization_table", {
						"funds_outside_sl_yes":source.funds_outside_sl_yes,
						"funds_outside_sl_no":source.funds_outside_sl_no,
						"source_organization_type": source.source_organization_type,
						"source_organization_name":source.source_organization_name,
						"funding_status_select_one":source.funding_status_select_one,
						"amount":source.amount,
					})

			elif source.source_organization_does_not_exist_in_the_list_above == 1 and source.enter_name_of_source_organization:
				# get the organization type for created
				new_source_doc = frappe.get_doc("Organization",source.enter_name_of_source_organization)
				# find the correct types
				
				sub_project_doc.append("source_organization_table", {
						"funds_outside_sl_yes":source.funds_outside_sl_yes,
						"funds_outside_sl_no":source.funds_outside_sl_no,
						"source_organization_does_not_exist_in_the_list_above": source.source_organization_does_not_exist_in_the_list_above,
						"source_organization_type": source.enter_funder_type,
						"enter_name_of_source_organization":source.enter_name_of_source_organization,
						"funding_status_select_one":source.funding_status_select_one,
						"amount":source.amount,
					})
					
			elif source.source_organization_does_not_exist_in_the_list_above == 1 and source.enter_funder_type == 'OTHER':
				new_source_doc = frappe.get_doc('Organization', source.enter_name_of_source_organization)

				sub_project_doc.append('source_organization_table', {
					"funds_outside_sl_yes":source.funds_outside_sl_yes,
					"funds_outside_sl_no":source.funds_outside_sl_no,
					'source_organization_does_not_exist_in_the_list_above': source.source_organization_does_not_exist_in_the_list_above,
					'source_organization_type': 'OTHER',
					'source_organization_name': source.other_specify_name,
					'funding_status_select_one':source.funding_status_select_one,
					'amount':source.amount,
				})

			else:
				pass

		sub_project_doc.all_sources_filled_check = self.all_sources_filled_check		
		
		sub_project_doc.phase = self.phase
		# project_amount_section
		sub_project_doc.total_project_amount_in_usd = self.total_project_amount_in_usd
		sub_project_doc.funding_gap_in_usd = self.funding_gap_in_usd
		sub_project_doc.amount_commited = self.amount_commited
		sub_project_doc.amount_in_pipeline = self.amount_in_pipeline

		# first clear the co-implementors table
		sub_project_doc.co_implementing_table = []
		for coimplementor in self.co_implementing_table:
			if coimplementor.organization_name:
				sub_project_doc.append("co_implementing_table", {
						"organization_name": coimplementor.organization_name
					})

		# map type of implementation
		sub_project_doc.total_implementor_check = self.total_implementor_check
		sub_project_doc.partial_implementor_check = self.partial_implementor_check
		sub_project_doc.not_implementor_check = self.not_implementor_check

		# first clear the implementors table
		sub_project_doc.destination_organization_table = []
		for imple_rep_obligation in self.destination_organization_table:
			sub_project_doc.append("destination_organization_table", {
				"implementing_organization":imple_rep_obligation.implementing_organization,
				"implementing_organization_is_not_in_the_list_above":imple_rep_obligation.implementing_organization_is_not_in_the_list_above,
				"enter_name_of_implementing_organization":imple_rep_obligation.enter_name_of_implementing_organization,
				"enter_acronym":imple_rep_obligation.enter_acronym,
				"organization_type":imple_rep_obligation.organization_type,
				"amount":imple_rep_obligation.amount,
				"reporting_obligation":imple_rep_obligation.reporting_obligation,
				"reporting_organization":imple_rep_obligation.reporting_organization,
			})

		# General Project Details Section
		sub_project_doc.operational_amount = self.project_name
		# sub_project_doc.project_code = self.project_code
		sub_project_doc.operational_amount = self.sub_project_name
		# sub_project_doc.operational_amount = self.phase
		# sub_project_doc.operational_amount = self.project_renewed_from
		# sub_project_doc.operational_amount = self.project_phase
		sub_project_doc.project_start_date = self.project_start_date
		sub_project_doc.project_end_date = self.project_end_date

		# pillar_alignment_section
		# first clear  table
		sub_project_doc.pillar_alignment_table = []
		for pillar in self.pillar_alignment_table:
			if pillar.pillar and pillar.yes:
				sub_project_doc.append("pillar_alignment_table", {
					"pillar":pillar.pillar,
					"yes":pillar.yes
				})

		# sector_alignment_section_section
		# first clear  table
		sub_project_doc.sector_alignment_table = []
		for sector in self.sector_alignment_table:
			if sector.yes:
				sub_project_doc.append("sector_alignment_table", {
					"pillar":sector.pillar,
					"sector":sector.sector,
					"yes":sector.yes
				})

		# priority_programs_section_section
		# first clear  table
		sub_project_doc.priority_programs_table = []
		for program in self.priority_programs_table:
			if program.priority_program and program.yes:
				sub_project_doc.append("priority_programs_table", {
					"priority_program":program.priority_program,
					"priority_program_description":program.priority_program_description,
					"sector":program.sector,
					"yes":program.yes
				})

		# relevant_mdas_section_section
		# first clear  table
		sub_project_doc.relevant_mda_table = []

		for mda in self.relevant_mda_table:
			if mda.lead_mda:
				sub_project_doc.append("relevant_mda_table", {
					"relevant_mda_per_sector":mda.relevant_mda_per_sector,
					"sector":mda.sector,
					"lead_mda":mda.lead_mda,
				})

		# add relevant outcomes
		sub_project_doc.relevant_outcomes__table = []
		for outcome in self.relevant_outcomes__table:
			if outcome.outcome and outcome.yes:
				sub_project_doc.append("relevant_outcomes__table", {
					"outcome":outcome.outcome,
					"outcome_description":outcome.outcome_description,
					"attached_to_priority_program":outcome.attached_to_priority_program,
					"priority_program":outcome.priority_program,
					"sector":outcome.sector,
					"yes":outcome.yes,
				})

		list_of_regions = [{self.marodijeh_check: "Marodijeh/Maroodijeex",
		                    self.sanag_check: "Sanag/Sanaag",
							self.hargeisa_region: "Central/Hargeisa",
							self.sool_check: "Sool/Sool",
							self.togdheer_check: "Togdheer/Togdheer",
							self.awdal_check: "Awdal/Awdal",
							self.sahil_check: "Sahil/Saaxil",
						  }]

		
		for region in list_of_regions:
			sub_project_doc.project_location_table = []

			if region:
				# check morodijeh
				marodijeh_check = False

				if self.hargeisa:
					sub_project_doc.append('project_location_table', {
						'region': 'Marodijeh/Maroodijeex',
						'district': 'Hargeisa/Hargeysa'
					})
					marodijeh_check = True

				if self.gabiley:
					sub_project_doc.append('project_location_table', {
						'region': 'Marodijeh/Maroodijeex',
						'district': 'Gabiley/Gabilay'
					})
					marodijeh_check = True

				if self.baligubadle:
					sub_project_doc.append('project_location_table', {
						'region': 'Marodijeh/Maroodijeex',
						'district': 'Baligubadle/Baligubadle'
					})
					marodijeh_check = True

				if self.salahlay:
					sub_project_doc.append('project_location_table', {
						'region': 'Marodijeh/Maroodijeex',
						'district': 'Salahlay/Salaxlay'
					})
					marodijeh_check = True

				if self.marodijeh_check and marodijeh_check == False:
					sub_project_doc.append('project_location_table', {
						'region': 'Marodijeh/Maroodijeex',
					})

				# check region Sanaag
				sanag_check = False

				if self.erigavo:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
						'district': 'Erigavo/Ceerigaabo'
					})
					sanag_check = True

				if self.badhan:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
						'district': 'Badhan/Badhan'
					})
					sanag_check = True

				if self.las_qoray:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
						'district': 'Las Qoray /Laasqoray'
					})
					sanag_check = True

				if self.ela_fweyn:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
						'district': 'El Afweyn/Ceelafweyn'
					})
					sanag_check = True

				if self.dhahar:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
						'district': 'Dhahar /Dhahar'
					})
					sanag_check = True

				if self.gar_adag:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
						'district': 'Gar Adag /Garadag'
					})
					sanag_check = True

				if self.sanag_check and sanag_check == False:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
					})


				# Check for Central or Hargeisa	
				hargeisa_check = False

				# if self.hargeisa_district:
				# 	sub_project_doc.append('project_location_table', {
				# 		'region': 'Central/Hargeisa',
				# 		'district': 'Hargeisa/Hargeysa'
				# 	})
				# 	hargeisa_check = True

				if self.hargeisa_region and hargeisa_check == False :
					sub_project_doc.append('project_location_table', {
						'region': 'Central/Hargeisa',
					})

				# check for sool
				sool_check = False
 
				if self.las_anod:
					sub_project_doc.append('project_location_table', {
						'region': 'Sool/Sool',
						'district': 'Las Anod /Laascaanood'
					})
					sool_check = True

				if self.hudun:
					sub_project_doc.append('project_location_table', {
						'region': 'Sool/Sool',
						'district': 'Hudun /Xuddun'
					})
					sool_check = True

				if self.taleh:
					sub_project_doc.append('project_location_table', {
						'region': 'Sool/Sool',
						'district': 'Taleh/Taleex'
					})
					sool_check = True

				if self.aynabo:
					sub_project_doc.append('project_location_table', {
						'region': 'Sool/Sool',
						'district': 'Aynabo/Caynabo'
					})
					sool_check = True

				if self.sool_check and sool_check == False:
					sub_project_doc.append('project_location_table', {
						'region': 'Sool/Sool',
					})

				# check togder region
				togdheer_check = False

				if self.burao:
					sub_project_doc.append('project_location_table', {
						'region': 'Togdheer/Togdheer',
						'district': 'Burao/Burco'
					})
					togdheer_check = True

				if self.odwayne:
					sub_project_doc.append('project_location_table', {
						'region': 'Togdheer/Togdheer',
						'district': 'Odwayne/Oodwayne'
					})
					togdheer_check = True

				if self.buhodle:
					sub_project_doc.append('project_location_table', {
						'region': 'Togdheer/Togdheer',
						'district': 'Buhodle/Buuhoodle'
					})
					togdheer_check = True

				if self.togdheer_check and togdheer_check == False:
					sub_project_doc.append('project_location_table', {
						'region': 'Togdheer/Togdheer',
					})

				# check awadal
				awdal_check = False

				if self.borama:
					sub_project_doc.append('project_location_table', {
						'region': 'Awdal/Awdal',
						'district': 'Borama/Boorama '
					})
					awdal_check = True

				if self.zeila:
					sub_project_doc.append('project_location_table', {
						'region': 'Awdal/Awdal',
						'district': 'Zeila/Saylac '
					})
					awdal_check = True

				if self.baki:
					sub_project_doc.append('project_location_table', {
						'region': 'Awdal/Awdal',
						'district': 'Baki/Baki'
					})
					awdal_check = True

				if self.lughaya:
					sub_project_doc.append('project_location_table', {
						'region': 'Awdal/Awdal',
						'district': 'Lughaya/Lughaya'
					})
					awdal_check = True

				if self.awdal_check and awdal_check == False:
					sub_project_doc.append('project_location_table', {
						'region': 'Awdal/Awdal',
					})

				# check sahil
				sahil_check = False

				if self.berbera:
					sub_project_doc.append('project_location_table', {
						'region': 'Sahil/Saaxil',
						'district': 'Berbera/Berbera'
					})
					sahil_check == True

				if self.sheekh:
					sub_project_doc.append('project_location_table', {
						'region': 'Sahil/Saaxil',
						'district': 'Sheikh/Sheekh'
					})
					sahil_check == True
				
				if self.sahil_check and sahil_check == False:
					sub_project_doc.append('project_location_table', {
						'region': 'Sahil/Saaxil',
					})		
		
		# list_of_districts = [{self.hargeisa: "Hargeisa",}]

		# for district in list_of_districts:

		# 	if district:

		# 		if self.hargeisa:
		# 			sub_project_doc.append("project_location_table", {
		# 				"district": "Hargeisa",
		# 			})


		
		# list_of_regions = [{self.marodijeh_check:"Marodijeh/Maroodijeex"}]
		# for region in list_of_regions:
		# 	# print "*"*80
		# 	if region:
		# 		print region
		# 		sub_project_doc.append("project_location_table", {
		# 			"region":region[1],
		# 			# "district":location.district,
		# 		})
        #     # print list_of_regions[region]
			


        # sub_project_doc.project_location_table = []
        # for location in self.project_location_table:
        #   if location.region:
        #       sub_project_doc.append("project_location_table", {
        #           "region":location.region,
        #           "district":location.district,
        #       })
		
		
		# sub_project_doc.project_location_table = []
		# for location in self.project_location_table:
		# 	if location.region:
		# 		sub_project_doc.append("project_location_table", {
		# 			"region":location.region,
		# 			"district":location.district,
		# 		})

		# add project description
		sub_project_doc.description = self.description

		# update the project doc
		sub_project_doc.attach_the_projects_log_frame =  self.attach_the_projects_log_frame

		# save the changes to the form
		sub_project_doc.save(ignore_permissions=True)
			