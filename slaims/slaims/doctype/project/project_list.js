// // Copyright (c) 2015, Frappe Technologies Pvt. Ltd. and Contributors
// // License: GNU General Public License v3. See license.txt

frappe.listview_settings['Project'] = {
	filters: [
        ['master_project', '!=', 1]
    ],

	onload: function (doc) {
		// console.log("frappe.listview_settings[Foo] ")
	},
	
	refresh:function(doc){
        
	}
}