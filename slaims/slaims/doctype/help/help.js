// Copyright (c) 2019, upande and contributors
// For license information, please see license.txt

// Email validation function
function ValidateEmail(mail){
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
	 {
		 return true
	 }
		 return false
}

// Validate phone number
  
function validatePhone(phone)
{
  if(/^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/){
	  return true
  }
  	  return false
}



frappe.ui.form.on('Help','email',function(frm,cdt,cdn){
	var row = locals[cdt][cdn]

	if(row.email){
		if(ValidateEmail(row.email)){
			// allow the user to continue
		}else{
			row.email = ''
			frappe.throw('Please enter a valid email')
		}
	}
})

frappe.ui.form.on('Help','telephone_no',function(frm,cdt,cdn){
	var row = locals[cdt][cdn]

	if(row.telephone_no){
		if(validatePhone(row.telephone_no)){
			// allow the user to continue
		}else{
			row.telephone_no = ''
			frappe.throw("Please enter a valid telephone number")
		}
	}
})
