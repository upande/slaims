# -*- coding: utf-8 -*-
# Copyright (c) 2019, Frappe Technologies and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Outcome(Document):
	'''
	This is the Controller Class for the Outcome Doctype
	'''

	def validate(self):
		'''
		Function that validates a  record before s
		saving
		'''
		# check if the user ticked 'linked to priority Program'
		if self.linked_to_priority_program == 1:
			# then the linked program field should be given
			if(self.linked_priority_program):
				# check if the linked program is a priority program
				priority_programs = frappe.db.sql("""SELECT name,priority_program from `tabProgram` WHERE name = '{}'""".format(self.linked_priority_program))
				if len(priority_programs) < 1:
					frappe.throw("An Error Has Occured While Trying to Fetch Priority Program")
				else:
					pass
			else:
				frappe.throw("You Have Marked this Outcome as 'Linked to Priority Program', Please Provide The Priority Program It is Linked To")
