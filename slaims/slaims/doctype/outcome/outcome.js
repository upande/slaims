// Copyright (c) 2019, Frappe Technologies and contributors
// For license information, please see license.txt



// global variables


// ================================================================================================
/* This section contains code from the general functions section
which are called is the form triggered functions section*/





/* end of the general functions section
// =================================================================================================
/* This section  contains functions that are triggered by the form action refresh or
reload to perform various action*/

frappe.ui.form.on('Outcome', {
	refresh: function(frm) {
		
	}
});


// function called when  outcome code field is filled
frappe.ui.form.on("Outcome", "outcome_code", function(frm){
	if(frm.doc.outcome_code){
		// clear the visible_outcome_code field and add new trancated value
		cur_frm.set_value("visible_outcome_code","")

		// add the outcome_code value
		cur_frm.set_value("visible_outcome_code",frm.doc.outcome_code)
	}else{
		// clear the visible_outcome_code field 
		cur_frm.set_value("visible_outcome_code","")
	}
})


// function called when  outcomes field is filled
frappe.ui.form.on("Outcome", "outcome", function(frm){
	if(frm.doc.outcome){
		// clear the trancate field and add new trancated value
		cur_frm.set_value("truncated_outcome_title","")

		// get the trancated value
		var trancated_value = frm.doc.outcome.slice(0,135)
		cur_frm.set_value("truncated_outcome_title",trancated_value+"...")
	}else{
		// clear the trancate field 
		cur_frm.set_value("truncated_outcome_title","")
	}
})
