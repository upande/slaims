# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

import requests
import pymysql.cursors
import json

# list of sections listed in order of filling/ occurance
list_of_sections = ["section_break_1","section_a_section",
	"source_suggestion",
	"project_amount_section","section_c_section",
	"section_b_section","destination_implementing_and_reporting_organization_section",
	"implementing__partners_section_section","project_details_section",
	"pillar_alignment_section","sector_alignment_section_section","priority_programs_section_section",
	"relevant_mdas_section_section","relevant_ndp_outcome_by_selected_priority_program_section",
	"project_location_section","project_description_section",
	"attachments_section_section",
	"verification_section_section"
]

class ProjectRenewalForm(Document):
	
	def validate(self):
		# check that required fields are given
		check_required_fields(self)

		# takes the user to the correct next page
		set_next_page_option(self)

		# map_data from the subproject
		map_data_to_form(self)

		# function that is called every time the document is saved
		execute_action_on_save(self)

	def on_update(self):
		# update the fields to the main doctype
		map_data_to_project(self)

	def on_trash(self):
		pass

def check_required_fields(self):
	'''
	Function for checking required fields
	before any dependant action is performed
	'''
	# ensure that the no other project with same phase exist
	list_of_forms = frappe.get_list("Project Renewal Form",
		fields=["*"],
		filters = {
			"phase_title":self.phase_title
		})
	
	if len(list_of_forms) > 0:
		# check if it is the same doc
		if list_of_forms[0]['name'] == self.name:
			pass
		else:
			frappe.throw("You have already filled a renewal form for phase {} for this project </br> \
				Please check the list of <b>Project Renewal Forms</b> under <b>SLAIMS</b> \
					".format(list_of_forms[0]['phase']))


	# fields required before saving
	required_fields_before_saving = [
		{"field_name":"Organization Name","field_value":self.select_your_organization,"section":"section_break_1"},
		{"field_name":"Organization Acronym","field_value":self.organization_acronym,"section":"section_break_1"},
		{"field_name":"Project Title","field_value":self.sub_project_title_1,"section":"section_break_1"},
		{"field_name":"Project-Phase-Title","field_value":self.phase_title,"section":"section_break_1"},
		{"field_name":"Phase","field_value":self.phase,"section":"section_break_1"},
	]

	required_fields_before_verification = [
		{"field_name":"Project Start Date","field_value":self.project_start_date,"section":"project_details_section"},
		{"field_name":"Project End Date","field_value":self.project_end_date,"section":"project_details_section"},
	]

	required_fields_before_validation = [

	]

	required_fields_before_approval = [
		# section_break_1
		# {"field_name":"Status","field_value":self.status,"section":"project_details_section"},
		# {"field_name":"Master Project","field_value":self.master_project,"section":"project_details_section"},

		# project_details_section
		# {"field_name":"Project Title","field_value":self.project_title_selection_field,"section":"project_details_section"},
		# {"field_name":"Project Code","field_value":self.new_project_code,"section":"project_details_section"},
		# {"field_name":"Full Project Title","field_value":self.},
		# {"field_name":"Sub Project Title","field_value":self.sub_project_title_1,"section":"project_details_section"},
		# {"field_name":"Project Renewed From","field_value":self.},
		# {"field_name":"Phase","field_value":self.phase},
		# {"field_name":"Organization (Select One)","field_value":self.select_your_organization,"section":"project_details_section"},
		# {"field_name":"Organization Acronym","field_value":self.organization_acronym,"section":"project_details_section"},
		# {"field_name":"Project Start Date","field_value":self.project_start_date,"section":"project_details_section"},
		# {"field_name":"Project End Date","field_value":self.project_end_date,"section":"project_details_section"},

		# project_amount_section
		# {"field_name":"Total Project Amount  in USD ($) in USD ($)","field_value":self.total_project_amount_in_usd,"section":"project_details_section"},
		# {"field_name":"Total Project Amount","field_value":self.total_project_amount,"section":"project_details_section"},
		# {"field_name":"Balance Amount in USD($)","field_value":self.balance_amount_in_usd,"section":"project_details_section"},
		# {"field_name":"Amount Commited in USD ($)","field_value":self.amount_commited,"section":"project_details_section"},
		# {"field_name":"Amount in Pipeline in USD ($)","field_value":self.amount_in_pipeline,"section":"project_details_section"},
		# {"field_name":"Operational Amount in USD ($)","field_value":self.operational_amount,"section":"project_details_section"},

		# project_description_section
		# {"field_name":"Description","field_value":self.description,"section":"project_details_section"},
		# {"field_name":"Project's Log Frame","field_value":self.attach_the_projects_log_frame,"section":"project_details_section"}
		
	]
	
	# check if all the required fields before saving document in various statuses
	if self.status == "Pending Verification":
		# check if all the required fields are given
		loop_through_required_list(self,required_fields_before_saving,"Pending Verification")

	elif self.status == "Verified":
		loop_through_required_list(self,required_fields_before_saving,"Pending Verification")
		loop_through_required_list(self,required_fields_before_verification,"Pending Verification")
		
		# check to ensure all the required tables are filled_correctly
		check_items_for_required_tables(self)
		# check if single fields are given
		single_fields_required_from_group(self)
		
	elif self.status == "Validated":
		loop_through_required_list(self,required_fields_before_saving,"Verified")
		loop_through_required_list(self,required_fields_before_verification,"Verified")
		loop_through_required_list(self,required_fields_before_validation,"Verified")

	elif self.status == "Approved":
		loop_through_required_list(self,required_fields_before_saving,"Validated")
		loop_through_required_list(self,required_fields_before_verification,"Validated")
		loop_through_required_list(self,required_fields_before_validation,"Validated")
		loop_through_required_list(self,required_fields_before_approval,"Validated")

def unverify_n_redirect(self,message,current_section):
	'''
	Function that unverifies the form and
	redirect the user to the page the need
	to complete
	'''
	frappe.msgprint(message)
	# Unverify the form
	unverify_form(self)
	# redirect the user to that page
	self.current_section = current_section


def check_items_for_required_tables(self):
	'''
	Function that checks all the required tables to ensure 
	that all the required tables are filled correctly
	'''
	if self.status == "Verified" or self.status == "Validated" \
		or self.status == "Approved":

		continue_executing = True
		# check sources table
		if continue_executing:
			screen_number = list_of_sections.index('section_a_section') + 1
			if len(self.source_organization_table) > 0:
				# check if user confirmed the sources
				if self.all_sources_filled_check:
					# check if all the required fields are filled correctly
					for source in self.source_organization_table:
						if source.amount and source.funding_status_select_one  and source.source_organization_type:
							if source.source_organization_name:
								pass
							elif source.enter_name_of_source_organization and source.enter_acronym_of_source_organization_eg_un_for_united_nations:
								# check if this source got a match
								found_suggestions = find_simalar_organization(source.enter_name_of_source_organization,source.enter_acronym_of_source_organization_eg_un_for_united_nations)

								if len(found_suggestions) > 0:
									# check if the user marked either yes or no in the suggestions
									if self.yes_found_source_match or self.no_no_source_match:
										pass
									else:
										continue_executing = False
										section_to_redirect = list_of_sections.index('source_suggestion') + 1
										message = "You have not filled required field in screen {}".format(section_to_redirect)
										unverify_n_redirect(self,message,'source_suggestion')	
								else:
									# no suggestion was found hence pass
									pass
							else:
								continue_executing = False
								unverify_n_redirect(self,"Source details are missing in the source table in screen {}".format(screen_number),'section_a_section')
						else:
							continue_executing = False
							unverify_n_redirect(self,"Source details are missing in the source table in screen {}".format(screen_number),'section_a_section')
				else:
					continue_executing = False
					unverify_n_redirect(self,"You have not ticked the checkbox to confirm the sources in screen {}".format(screen_number),'section_a_section')
			else:
				continue_executing = False
				unverify_n_redirect(self,"You have not added any sources organization in screen {}".format(screen_number),'section_a_section')

		# check the implementing organization table
		if continue_executing:
			screen_number = list_of_sections.index('destination_implementing_and_reporting_organization_section') + 1
			# check len of destionation and implementing org table
			if len(self.destination_organization_table) > 0:
				# check if  destionation and implementing orgs
				if self.all_implementors_added_check:
					# check if all the required fields are filled correctly
					for implementor in self.destination_organization_table:
						if implementor.amount:
							if implementor.implementing_organization:
								pass
							elif implementor.enter_name_of_implementing_organization and implementor.enter_acronym and implementor.organization_type:
								# check if this implementing organization got a match
								found_suggestions = find_simalar_organization(implementor.enter_name_of_implementing_organization,implementor.enter_acronym)

								if len(found_suggestions) > 0:
									# check if the user marked either yes or no in the suggestions
									if self.found_implementors_match or self.did_not_find_implementors_match:
										pass
									else:
										section_to_redirect = list_of_sections.index('implementing__partners_section_section') + 1
										message = "You have not filled required field in screen {}".format(section_to_redirect)
										unverify_n_redirect(self,message,'implementing__partners_section_section')
								else:
									# no suggestion was found hence pass
									pass
							else:
								unverify_n_redirect(self,"Some details are missing in the implementing organizations table in screen {}".format(screen_number),'destination_implementing_and_reporting_organization_section')
						else:
							unverify_n_redirect(self,"Amount missing in the implementing organizations table in screen {}".format(screen_number),'destination_implementing_and_reporting_organization_section')
				else:
					unverify_n_redirect(self,"You have not ticked the checkbox to confirm implementing organizations in screen {}".format(screen_number),'destination_implementing_and_reporting_organization_section')
			else:
				unverify_n_redirect(self,"You have not added any implemting organization in screen {}".format(screen_number),'destination_implementing_and_reporting_organization_section')

def loop_through_required_list(self,list_of_required_fields,status_to_revert):
	'''
	Function that loops through a list of given 
	fields and determine if any of them is missing
	'''
	# looping throught fields
	for field in list_of_required_fields:
		if field["field_value"]:
			# pass because field is available
			pass
		else:
			screen_number = list_of_sections.index(field['section']) + 1
			frappe.msgprint("You have not filled required field '{}' in screen {}".format(field["field_name"],screen_number))
			# unverify form
			unverify_form(self)
			# redirect the user to that page
			self.current_section = field['section']

def unverify_form(self):
	'''
	Function that make unverifies the form
	'''
	self.status = "Pending Verification" 
	self.viewing_information = 0
	self.verified = 0
	self.verified = 0
	self.verified_by = ""
	self.validated = 0
	self.validated_by = ""
	self.approved = 0
	self.approved_by = ""

def check_or_create_source_orgs(self):
	'''
	check if the source organization the user
	gave have been created
	'''
	# check if the organization has been created 
	list_of_source = frappe.get_list("Source Organization",
		fields=["*"],
		filters = {
			"parent":self.name,
			"parenttype":"Project Registration Form",
			"parentfield":"source_organization_table",
			"source_organization_does_not_exist_in_the_list_above":1
		})

	if len(list_of_source) == 0:
		pass
	else:
		# check if all the organization have been created
		for source in list_of_source:
			if source.source_organization_does_not_exist_in_the_list_above and source.enter_name_of_source_organization:
				list_of_organizations = frappe.get_list("Organization",
					fields=["*"],
					filters = {
						"name":source["enter_name_of_source_organization"]
					})
			elif source.source_organization_does_not_exist_in_the_list_above and source.enter_funder_type == 'OTHER':
				list_of_organizations = frappe.get_list("Organization",
					fields=["*"],
					filters = {
						"name":source["other_specify_name"]
					})
			
			if len(list_of_organizations)== 0 and source.enter_funder_type != 'OTHER' and source.source_organization_does_not_exist_in_the_list_above == 1:
				# create the organization 
				org_doc = frappe.get_doc({"doctype":"Organization"})
				org_doc.name = source["enter_name_of_source_organization"]
				org_doc.organization_type = source["enter_funder_type"]
				org_doc.full_name_of_the_organization = source["enter_name_of_source_organization"]
				org_doc.acronym = source["enter_acronym_of_source_organization_eg_un_for_united_nations"]
				org_doc.insert(ignore_permissions=True)

				org_doc.insert(ignore_permissions=True)

			elif len(list_of_organizations) == 0 and source.enter_funder_type == 'OTHER' and source.source_organization_does_not_exist_in_the_list_above == 1:
			
				org_doc = frappe.get_doc({"doctype":"Organization"})
				org_doc.name = source["other_specify_name"]
				org_doc.full_name_of_the_organization = source["other_specify_name"]
				org_doc.organization_type = 'OTHER'
				org_doc.insert(ignore_permissions=True)

			else:
				pass

def check_or_create_implementing_orgs(self):
	'''
	check if the source organization the user
	gave have been created
	'''
	# check if the organization has been created 
	list_of_implementors = frappe.get_list("Destination Implementing Reporting",
		fields=["*"],
		filters = {
			"parent":self.name,
			"parenttype":"Project Renewal Form",
			"parentfield":"destination_organization_table",
			"implementing_organization_is_not_in_the_list_above":1
		})
	
	if len(list_of_implementors) == 0:
		pass
	else:
		# check if all the organization have been created
		for implementor in list_of_implementors:
			list_of_organizations = frappe.get_list("Organization",
				fields=["*"],
				filters = {
					"name":implementor["enter_name_of_implementing_organization"]
				})

			if len(list_of_organizations)== 0:
				# create the organization 
				org_doc = frappe.get_doc({"doctype":"Organization"})
				org_doc.name = implementor["enter_name_of_implementing_organization"]
				org_doc.full_name_of_the_organization = implementor["enter_name_of_implementing_organization"]
				org_doc.acronym = implementor["enter_acronym"]
				org_doc.organization_type = implementor["organization_type"]
				
				# now insert the document in the database
				org_doc.insert(ignore_permissions=True)

def single_fields_required_from_group(self):
	'''
	Function that checks that atleast one field from the list of 
	given fields is filled or true
	'''
	# return_value = False # return value is initialized to False

	# required_sectors_before_saving = [
	# 	{"field_name":"Health","field_value":self.health},
	# 	{"field_name":"Education","field_value":self.education},
	# 	{"field_name":"Wash Sector","field_value":self.wash_sector},
	# 	{"field_name":"Economy","field_value":self.economy},
	# 	{"field_name":"Energy and Extractives","field_value":self.energy_and_extractives},
	# 	{"field_name":"Production","field_value":self.production},
	# 	{"field_name":"Infrastructure","field_value":self.infrastructure},
	# 	{"field_name":"Governance ","field_value":self.governance},
	# 	{"field_name":"Environment","field_value":self.environment},
	# 	{"field_name":"Cross Cutting Employment and Labor","field_value":self.cross_cutting_employment_and_labor},
	# 	{"field_name":"Cross Cutting Social Protection","field_value":self.cross_cutting_social_protection},
	# 	{"field_name":"Cross Cutting Youth","field_value":self.cross_cutting_youth},
	# ]

	# # looping through list of required fields
	# for field in required_sectors_before_saving:
	# 	if field["field_value"]:
	# 		# pass because field is available
	# 		return_value = True
	# 	else:
	# 		pass

	# if(return_value):
	# 	pass
	# else:
	# 	frappe.throw("You Need to Specify Atleast One Sector The Project Conributes To")
	return_value = True

def execute_action_on_save(self):
	'''
	Function that runs whenever the document is saved
	'''

	if self.status == "Pending Verification":
		pass
	
	elif self.status == "Verified":
		print('*'*80)
		print(self.sub_project_title)
		# check if the sub project has been created
		if self.sub_project_title:
			# pass the subproject_name has been created
			pass
		else:
			project_names = frappe.db.sql('SELECT name from `tabProject`')
			project_names = [name for name_tuple in project_names for name in name_tuple]
	
			list_of_found_projects = frappe.get_list("Project",
					fields=["*"],
					filters = {
						"name":self.phase_title
				})

			if len(list_of_found_projects) > 0:
				# set the just created sub project name to the field
				self.sub_project_title  = self.phase_title
			else:
				# create a new subproject
				new_sub_project_doc = frappe.get_doc({"doctype":"Project"})
				new_sub_project_doc.name = self.phase_title
				new_sub_project_doc.project_name = self.phase_title
				new_sub_project_doc.project_code = self.new_project_code
				new_sub_project_doc.sub_project = 1
				new_sub_project_doc.link_to_master_project = self.project_title_selection_field
				new_sub_project_doc.status = self.status
				new_sub_project_doc.your_organization = self.select_your_organization
				new_sub_project_doc.organization_acronym = self.organization_acronym 
				new_sub_project_doc.project_start_date = self.project_start_date 
				new_sub_project_doc.project_end_date = self.project_end_date 
				new_sub_project_doc.your_organization = self.select_your_organization
				new_sub_project_doc.organization_acronym = self.organization_acronym
				new_sub_project_doc.total_project_amount_in_usd = self.total_project_amount_in_usd

				# add pillars 
				new_sub_project_doc.pillar_alignment_table = []
				for pillar in self.pillar_alignment_table:
					if pillar.pillar and pillar.yes:
						new_sub_project_doc.append("pillar_alignment_table", {
							"pillar":pillar.pillar,
							"yes":pillar.yes
						})

				# first clear  table
				new_sub_project_doc.sector_alignment_table = []
				for sector in self.sector_alignment_table:
					if sector.yes:
						new_sub_project_doc.append("sector_alignment_table", {
							"pillar":sector.pillar,
							"sector":sector.sector,
							"yes":sector.yes
						})

				# relevant_mdas_section_section
				# first clear  table
				new_sub_project_doc.relevant_mda_table = []

				for mda in self.relevant_mda_table:
					if mda.lead_mda:
						new_sub_project_doc.append("relevant_mda_table", {
							"relevant_mda_per_sector":mda.relevant_mda_per_sector,
							"sector":mda.sector,
							"lead_mda":mda.lead_mda,
						})

				list_of_regions = [{self.marodijeh_check: "Marodijeh/Maroodijeex",
		                    self.sanag_check: "Sanag/Sanaag",
							self.hargeisa_region: "Central/Hargeisa",
							self.sool_check: "Sool/Sool",
							self.togdheer_check: "Togdheer/Togdheer",
							self.awdal_check: "Awdal/Awdal",
							self.sahil_check: "Sahil/Saaxil",
						  }]

				for region in list_of_regions:
					new_sub_project_doc.project_location_table = []

					if region:
						# check morodijeh
						marodijeh_check = False

						if self.hargeisa:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Marodijeh/Maroodijeex',
								'district': 'Hargeisa/Hargeysa'
							})
							marodijeh_check = True

						if self.gabiley:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Marodijeh/Maroodijeex',
								'district': 'Gabiley/Gabilay'
							})
							marodijeh_check = True

						if self.baligubadle:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Marodijeh/Maroodijeex',
								'district': 'Baligubadle/Baligubadle'
							})
							marodijeh_check = True

						if self.salahlay:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Marodijeh/Maroodijeex',
								'district': 'Salahlay/Salaxlay'
							})
							marodijeh_check = True

						if self.marodijeh_check and marodijeh_check == False:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Marodijeh/Maroodijeex',
							})

						# check region Sanaag
						sanag_check = False

						if self.erigavo:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
								'district': 'Erigavo/Ceerigaabo'
							})
							sanag_check = True

						if self.badhan:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
								'district': 'Badhan/Badhan'
							})
							sanag_check = True

						if self.las_qoray:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
								'district': 'Las Qoray /Laasqoray'
							})
							sanag_check = True

						if self.ela_fweyn:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
								'district': 'El Afweyn/Ceelafweyn'
							})
							sanag_check = True

						if self.dhahar:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
								'district': 'Dhahar /Dhahar'
							})
							sanag_check = True

						if self.gar_adag:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
								'district': 'Gar Adag /Garadag'
							})
							sanag_check = True

						if self.sanag_check and sanag_check == False:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sanag/Sanaag',
							})


						# Check for Central or Hargeisa	
						hargeisa_check = False

						# if self.hargeisa_district:
						# 	new_sub_project_doc.append('project_location_table', {
						# 		'region': 'Central/Hargeisa',
						# 		'district': 'Hargeisa/Hargeysa'
						# 	})
						# 	hargeisa_check = True

						if self.hargeisa_region and hargeisa_check == False :
							new_sub_project_doc.append('project_location_table', {
								'region': 'Central/Hargeisa',
							})

						# check for sool
						sool_check = False
		
						if self.las_anod:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sool/Sool',
								'district': 'Las Anod /Laascaanood'
							})
							sool_check = True

						if self.hudun:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sool/Sool',
								'district': 'Hudun /Xuddun'
							})
							sool_check = True

						if self.taleh:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sool/Sool',
								'district': 'Taleh/Taleex'
							})
							sool_check = True

						if self.aynabo:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sool/Sool',
								'district': 'Aynabo/Caynabo'
							})
							sool_check = True

						if self.sool_check and sool_check == False:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sool/Sool',
							})

						# check togder region
						togdheer_check = False

						if self.burao:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Togdheer/Togdheer',
								'district': 'Burao/Burco'
							})
							togdheer_check = True

						if self.odwayne:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Togdheer/Togdheer',
								'district': 'Odwayne/Oodwayne'
							})
							togdheer_check = True

						if self.buhodle:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Togdheer/Togdheer',
								'district': 'Buhodle/Buuhoodle'
							})
							togdheer_check = True

						if self.togdheer_check and togdheer_check == False:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Togdheer/Togdheer',
							})

						# check awadal
						awdal_check = False

						if self.borama:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Awdal/Awdal',
								'district': 'Borama/Boorama '
							})
							awdal_check = True

						if self.zeila:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Awdal/Awdal',
								'district': 'Zeila/Saylac '
							})
							awdal_check = True

						if self.baki:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Awdal/Awdal',
								'district': 'Baki/Baki'
							})
							awdal_check = True

						if self.lughaya:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Awdal/Awdal',
								'district': 'Lughaya/Lughaya'
							})
							awdal_check = True

						if self.awdal_check and awdal_check == False:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Awdal/Awdal',
							})

						# check sahil
						sahil_check = False

						if self.berbera:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sahil/Saaxil',
								'district': 'Berbera/Berbera'
							})
							sahil_check == True

						if self.sheekh:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sahil/Saaxil',
								'district': 'Sheikh/Sheekh'
							})
							sahil_check == True
						
						if self.sahil_check and sahil_check == False:
							new_sub_project_doc.append('project_location_table', {
								'region': 'Sahil/Saaxil',
							})

				new_sub_project_doc.insert(ignore_permissions=True)
				# set the just created sub project name to the field
				self.sub_project_title = self.phase_title

		# check or create source organization
		check_or_create_source_orgs(self)
		#check or create implementing organization
		check_or_create_implementing_orgs(self)

		# check if reporting organization has been selected in implementing table
		for implementor in self.destination_organization_table:
			if implementor.reporting_obligation == 1 or implementor.reporting_organization:
				pass
			else:
				# reporting organization not defined hence make destination organization reporting
				implementor.reporting_organization = self.select_your_organization

	elif self.status == "Validated":
		# append the created sources
		append_organizations_created_on_verification(self)

		# get the approved document
		doc = frappe.get_doc("Project",self.phase_title)
		doc.status == "Validated"

	elif self.status == "Approved":
		# get the approved document
		doc = frappe.get_doc("Project",self.phase_title)
		doc.status == "Approved"


def set_marked_pillars(self,sub_project_doc):
	'''
	Function that sets all the pillars that have been marked
	'''
	# get all the pillars in the table
	sub_project_pillars = frappe.get_list("Pillar Alignment Table",
			fields=["*"],
			filters = {
				"parent":sub_project_doc.name,
				"parenttype":"Project",
				"parentfield":"pillar_alignment_table"
		})

	pillars_marked = False
	# loop through to check if any is marked
	for pillar in self.pillar_alignment_table:
		if pillar.yes:
			pillars_marked = True
	
	if pillars_marked:
		pass
	else:
		# clear out the current table first
		self.pillar_alignment_table = []
		marked_pillars_holder = []

		# add all the marked pillars in the main doctype
		for main_marked_pillar in sub_project_pillars:
			marked_pillars_holder.append(main_marked_pillar.pillar)
			
		# get all the related pillars
		all_pillars = frappe.get_list("NDP",
				fields=["*"],
				filters = {
					"type":"Pillar"
			})

		# if pillars
		if len(all_pillars) > 0:
			# Pillar Alignment Section
			if len(self.pillar_alignment_table) > 0:
				pass
			else:
				self.confirm_pillar_selection = 0
				for pillar in all_pillars:
					if pillar.name in marked_pillars_holder:
						self.append("pillar_alignment_table", {
							"pillar": pillar.name,
							"yes":1
						})
					else:
						self.append("pillar_alignment_table", {
							"pillar": pillar.name
						})
		else:
			frappe.throw("No pillars were found please select add NDP pillars in the pillar \
				alignment section in order to continue")		


def set_marked_sectors(self,sub_project_doc):
	'''
	Function that sets all the sectors that have been marked
	'''
	# first clear the sectors table
	self.sector_alignment_table = []
	self.confirm_sector_selection = 0

	# get the relavant sectors
	list_of_relevant_sectors = []

	# get sectors for each selected pillar
	for pillar in self.pillar_alignment_table:
		# get all the sectors for each ticked pillar
		if pillar.yes:
			sectors_for_pillar = frappe.get_list("NDP",
				fields=["name","parent_ndp"],
				filters = {
					"type":"Sector",
					"parent_ndp":pillar.pillar
			})

			# append the sectors to the list of sectors
			list_of_relevant_sectors += sectors_for_pillar

	# get all the pillars in the table
	sub_project_sectors = frappe.get_list("Sector Alignment Table",
		fields=["*"],
		filters = {
			"parent":sub_project_doc.name,
			"parenttype":"Project",
			"parentfield":"sector_alignment_table"
	})

	sectors_marked = False
	# loop through to check if any is marked
	for sector in self.sector_alignment_table:
		if sector.yes:
			sector_marked = True
	
	if sectors_marked:
		pass
	else:
		# clear out the current table first
		self.sector_alignment_table = []
		marked_sectors_holder = []

		# add all the marked pillars in the main doctype
		for main_marked_sector in sub_project_sectors:
			marked_sectors_holder.append(main_marked_sector.sector)

		for relevant_sector in list_of_relevant_sectors:
			if relevant_sector['name'] in marked_sectors_holder:
				self.append("sector_alignment_table", {
						"pillar":relevant_sector['parent_ndp'],
						"sector": relevant_sector['name'],
						"yes":1
					})
			else:
				self.append("sector_alignment_table", {
						"pillar":relevant_sector['parent_ndp'],
						"sector": relevant_sector['name'],
					})

def set_marked_programs(self,sub_project_doc):
	'''
	Function that sets all the programs that have been marked
	'''
	# first clear the priority table
	self.priority_programs_table = []
	self.confirm_priority_programs = 0
	self.no_ndp_check = 0

	# get the relavant programs
	list_of_relevant_programs = []

	# get program for each selected sector
	for sector in self.sector_alignment_table:
		# get all the program for each ticked sector
		if sector.yes:
			program_for_sector = frappe.get_list("Program",
				fields=["*"],
				filters = {
					"sector":sector.sector
			})

			# append the sectors to the list of sectors
			list_of_relevant_programs += program_for_sector

	# place all the relavant programs in the table
	for relevant_program in list_of_relevant_programs:
		self.append("priority_programs_table", {
				"priority_program":relevant_program['program_title'],
				"priority_program_description": relevant_program['program'],
				"sector":relevant_program['sector']
			})

	# get all the marked programs from the approved form
	sub_project_programs = frappe.get_list("Priority Programs Table",
		fields=["priority_program"],
		filters = {
			"parent":sub_project_doc.name,
			"parenttype":"Project",
			"parentfield":"priority_programs_table"
	})

	# mark all the programs from the query above in the table
	for program in sub_project_programs:
		# loop through priority program table
		for relevant_program in self.priority_programs_table:
			if relevant_program.priority_program == program['priority_program']:
				# mark the program
				relevant_program.yes = 1
	
def set_marked_mdas(self,sub_project_doc):
	'''
	Function that sets all the mdas that have been marked
	'''
	# first clear the priority table
	self.relevant_mda_table = []
	self.confirm_mdas = 0
	self.no_gi_check = 0

	# get the relavant mdas
	list_of_relevant_mdas = []

	list_of_relavant_sectors = []
	# get mdas for each selected sector
	for sector in self.sector_alignment_table:
		# get all the mda for each ticked sector
		if sector.yes:
			mdas_for_sector = frappe.get_list("MDA NDP Alignment",
				fields=["parent","sector"],
				filters = {
					"sector":sector.sector
				})
			list_of_relevant_mdas += mdas_for_sector

	# place all the relavant mdas in the table
	for relevant_mda in list_of_relevant_mdas:
		self.append("relevant_mda_table", {
				"relevant_mda_per_sector":relevant_mda['parent'],
				"sector": relevant_mda['sector'],
			})


	# get all the marked mdas from the approved form
	sub_project_mdas = frappe.get_list("Relevant MDAs Table",
		fields=["relevant_mda_per_sector"],
		filters = {
			"parent":sub_project_doc.name,
			"parenttype":"Project",
			"parentfield":"relevant_mda_table"
	})

	# mark all the mdas from the query above in the table
	for mda in sub_project_mdas:
		# loop through mdas program table
		for relevant_mda in self.relevant_mda_table:
			if relevant_mda.relevant_mda_per_sector == mda['relevant_mda_per_sector']:
				relevant_mda.lead_mda = 1

def set_marked_outcomes(self,sub_project_doc):
	'''
	Function that sets all the outcomes that have been marked
	'''
	# first clear the outcomes table
	self.relevant_outcomes__table = []
	self.confirm_outcomes = 0
	self.no_outcome_check = 0

	# get the relavant outcomes
	list_of_relevant_outcomes = []

	# get outcome for each priority program
	for program in self.priority_programs_table:
		# get all the outcomes linked with each ticked program
		if program.yes:
			outcomes_for_sector = frappe.get_list("Outcome",
				fields=["*"],
				filters = {
					"linked_priority_program":program.priority_program
			})

			# append the outcome to the list of outcome
			list_of_relevant_outcomes += outcomes_for_sector

	# place all the relavant outcomes in the table
	for relevant_outcome in list_of_relevant_outcomes:
		self.append("relevant_outcomes__table", {
				"outcome":relevant_outcome['truncated_outcome_title'],
				"outcome_description": relevant_outcome['outcome'],
				"attached_to_priority_program":1,
				"priority_program":relevant_outcome['linked_priority_program'],
				"sector":relevant_outcome['sector'],
			})

	# get all the marked outcomes from the approved form
	sub_project_outcomes = frappe.get_list("Outcome by Selected Priority Program Table",
		fields=["outcome"],
		filters = {
			"parent":sub_project_doc.name,
			"parenttype":"Project",
			"parentfield":"relevant_outcomes__table"
	})

	# mark all the outcomes from the query above in the table
	for outcome in sub_project_outcomes:
		# loop through outcome
		for relevant_outcome in self.relevant_outcomes__table:
			if relevant_outcome.outcome == outcome['outcome']:
				# mark the program
				relevant_outcome.yes = 1

def append_organizations_created_on_verification(self):
	'''
	Function that appends the correct creates source 
	to form on verification
	This function is called under validation i.e after
	verification has already taken place
	'''
	# append the just created source organizations
	for source in self.source_organization_table:
		if source.source_organization_does_not_exist_in_the_list_above and source.enter_funder_type != 'OTHER':
			# confirm if the organization has been created
			try:
				frappe.get_doc("Organization",source.enter_name_of_source_organization)
				source.source_organization_name = source.enter_name_of_source_organization
				source.source_organization_does_not_exist_in_the_list_above = 0 
				# source.enter_name_of_source_organization = ""
				# source.enter_acronym_of_source_organization_eg_un_for_united_nations = ""
			except:
				
				# unverify form and ask the user to verify again
				screen_number = list_of_sections.index('section_a_section') + 1
				message = "Please confirm that all the details of the funding \
					organization table are given and verify the form again \
						in screen {}".format(screen_number)
				# call the unverify and redirect function here
				unverify_n_redirect(self,message,'section_a_section')
		elif source.source_organization_does_not_exist_in_the_list_above and source.enter_funder_type == 'OTHER':
			try:
				# frappe.get_doc("Organization",source.other_specify_name)
				source.source_organization_name = source.other_specify_name
				source.source_organization_does_not_exist_in_the_list_above = 0 
				source.enter_name_of_source_organization = ""
				# source.enter_acronym_of_source_organization_eg_un_for_united_nations = ""
				source.source_organization_type = 'OTHER'
				
			except:
				
				# unverify form and ask the user to verify again
				screen_number = list_of_sections.index('section_a_section') + 1
				message = "Please confirm that all the details of the funding \
					organization table are given and verify the form again \
						in screen {}".format(screen_number)
				# call the unverify and redirect function here
				unverify_n_redirect(self,message,'section_a_section')
		else:
			pass

	# append the just created implementing organizations
	for implementor in self.destination_organization_table:
		if not implementor.implementing_organization:
			# confirm if the organization has been created
			try:
				frappe.get_doc("Organization",implementor.enter_name_of_implementing_organization)
				implementor.implementing_organization = implementor.enter_name_of_implementing_organization
				# clear the entered fields
				implementor.implementing_organization_is_not_in_the_list_above = ""
				implementor.enter_name_of_implementing_organization = ""
				implementor.enter_acronym = ""
				implementor.organization_type = ""
			except:
				# unverify form and ask the user to verify again
				screen_number = list_of_sections.index('destination_implementing_and_reporting_organization_section') + 1
				message = "Please confirm that all the details of the implementing \
					organization table are given and verify the form again \
						in screen {}".format(screen_number)
				# call the unverify and redirect function here
				unverify_n_redirect(self,message,'destination_implementing_and_reporting_organization_section')

def set_next_page_option(self):
	'''
	Function that is used to set the next page the user should 
	see for special cases
	'''

	if self.current_section == "section_a_section":
		pass
	
	# check if the user needs to seee this section in the first place
	if self.current_section == "source_suggestion":
		# check if any of the sources in the source is not already in the database
		list_of_sources = frappe.get_list("Source Organization",
			fields=["*"],
			filters = {
				"parent":self.name,
				"parenttype":"Project Renewal Form",
				"parentfield":"source_organization_table"
			})

		
		# fist clear the table of suggestions
		self.source_suggestion_table = []
		self.yes_found_source_match = ""
		self.no_no_source_match = ""

		for source in list_of_sources:

			if source.source_organization_does_not_exist_in_the_list_above:

				# find similar sources
				list_of_source_suggetions = find_simalar_organization(source.enter_name_of_source_organization,source.enter_acronym_of_source_organization_eg_un_for_united_nations)
				
				if len(list_of_sources) > 0:
					for source_suggestion in list_of_source_suggetions:
						self.append("source_suggestion_table", {
							"given__source_name":source.enter_name_of_source_organization,
							"did_you_mean":source_suggestion,
						})
			else:
				pass

		if len(self.source_suggestion_table) > 0:
			# allow user to see page
			pass
		else:
			sources_suggestion_index = list_of_sections.index("source_suggestion")
			# place new page for user to see depending on direction
			if self.direction == "Forward":
				self.previous_section = list_of_sections[sources_suggestion_index]
				self.current_section = list_of_sections[sources_suggestion_index+1]
				self.next_section = list_of_sections[sources_suggestion_index + 2]
			else:
				self.previous_section = list_of_sections[sources_suggestion_index - 2]
				self.current_section = list_of_sections[sources_suggestion_index-1]
				self.next_section = list_of_sections[sources_suggestion_index]

	if self.current_section == "project_amount_section":
		list_of_project_sources = frappe.get_list("Source Organization",
			fields=["*"],
			filters = {
				"parent":self.name,
				"parenttype":"Project Renewal Form",
				"parentfield":"source_organization_table"
		})

		if len(list_of_project_sources)>0:
			total_amount = 0
			committed_amount = 0
			pipeline_amount = 0
			operational_amount = 0
			for source in list_of_project_sources: 

				total_amount += source["amount"]
				if source["funding_status_select_one"] == "Pipeline":
					pipeline_amount += source["amount"]
				elif source["funding_status_select_one"] == "Committed":
					committed_amount += source["amount"]
				# elif source["funding_status_select_one"] == "Operational":
				# 	operational_amount += source["amount"]

			self.total_project_amount_in_usd = total_amount	
			self.total_project_amount = total_amount
			self.amount_commited = committed_amount
			self.amount_in_pipeline = pipeline_amount
			self.funding_gap_in_usd = total_amount - committed_amount
			# self.operational_amount = operational_amount

		else:
			pass
	
	if self.current_section == "section_b_section":
		pass

	if self.current_section == "destination_implementing_and_reporting_organization_section":
	
		# confirmation field first
		self.all_implementors_added_check = 0

		if self.total_implementor_check:

			# check if the organization already on the table
			list_of_already_existing = frappe.get_list("Destination Implementing Reporting",
				fields=["*"],
				filters = {
					"parent":self.name,
					"parenttype":"Project Renewal Form",
					"parentfield":"destination_organization_table",
					"implementing_organization":self.select_your_organization
				}
			)

			if len(list_of_already_existing) > 0:
				pass
			else:
				# add organization as implementor
				self.append("destination_organization_table", {
					"implementing_organization": self.select_your_organization,
					"reporting_obligation":1,
					"reporting_organization":self.select_your_organization,
					"amount":self.total_project_amount_in_usd
				})

		elif self.partial_implementor_check:
			# check if the organization already on the table
			list_of_already_existing = frappe.get_list("Destination Implementing Reporting",
				fields=["*"],
				filters = {
					"parent":self.name,
					"parenttype":"Project Renewal Form",
					"parentfield":"destination_organization_table",
					"implementing_organization":self.select_your_organization
				}
			)

			if len(list_of_already_existing) > 0:
				pass
			else:
				# add organization as implementor
				self.append("destination_organization_table", {
					"implementing_organization": self.select_your_organization,
					"reporting_obligation":1,
					"reporting_organization":self.select_your_organization
				})

		elif self.not_implementor_check:
			# allow user to add only other implementors
			pass

	if self.current_section == "implementing__partners_section_section":
		
		# check if any of the implementors is not already in the database
		list_of_implementors = frappe.get_list("Destination Implementing Reporting",
			fields=["*"],
			filters = {
				"parent":self.name,
				"parenttype":"Project Renewal Form",
				"parentfield":"destination_organization_table",
				"implementing_organization_is_not_in_the_list_above":1
			})
		
		# fist clear the table of suggestions
		self.implementing_partners_suggestion_table = []
		self.found_implementors_match = ""
		self.did_not_find_implementors_match = ""

		for implementor in list_of_implementors:
			# find similar implementors
			list_of_implementor_suggetions = find_simalar_organization(implementor.enter_name_of_implementing_organization,implementor.enter_acronym)

			if len(list_of_implementor_suggetions) > 0:
				for implementor_suggestion in list_of_implementor_suggetions:
					self.append("implementing_partners_suggestion_table", {
						"given_name":implementor.enter_name_of_implementing_organization,
						"did_you_mean":implementor_suggestion
					})

			else:
				# no match was found hence create it
				new_implementor_doc = frappe.get_doc({"doctype":"Organization"})
				new_implementor_doc.name = implementor["enter_name_of_implementing_organization"],
				new_implementor_doc.full_name_of_the_organization = implementor["enter_name_of_implementing_organization"]
				new_implementor_doc.acronym = implementor["enter_acronym"]

				# # save the document
				new_implementor_doc.insert(ignore_permissions=True)


		if len(self.implementing_partners_suggestion_table) > 0:
			# allow user to see page
			pass
		else:
			implementor_suggestion_index = list_of_sections.index("implementing__partners_section_section")
			# place new page for user to see depending on direction
			if self.direction == "Forward":
				self.previous_section = list_of_sections[implementor_suggestion_index]
				self.current_section = list_of_sections[implementor_suggestion_index+1]
				self.next_section = list_of_sections[implementor_suggestion_index + 2]
			else:
				self.previous_section = list_of_sections[implementor_suggestion_index - 2]
				self.current_section = list_of_sections[implementor_suggestion_index-1]
				self.next_section = list_of_sections[implementor_suggestion_index]


	if self.current_section == "project_details_section":
		# check if the project name has been given
		if self.project_title_selection_field:
			# set the project title name 
			self.project_name = self.project_title_selection_field
			# get the correct code
			project_doc = frappe.get_doc("Project",self.project_title_selection_field)
			self.project_code = project_doc.project_code
		else:
			pass
	
	if self.current_section == "pillar_alignment_section":
		# get all the related pillars
		all_pillars = frappe.get_list("NDP",
				fields=["*"],
				filters = {
					"type":"Pillar"
			})

		# if pillars
		if len(all_pillars) > 0:
			# Pillar Alignment Section
			if len(self.pillar_alignment_table) > 0:
				pass
			else:
				self.confirm_pillar_selection = ""
				for pillar in all_pillars:
					self.append("pillar_alignment_table", {
						"pillar": pillar.name,
					})
		else:
			frappe.throw("No pillars were found please select add NDP pillars in the pillar \
				alignment section in order to continue")		

	if self.current_section == "sector_alignment_section_section":
		# first clear the sectors table
		self.sector_alignment_table = []
		self.confirm_sector_selection = ""

		# get the relavant sectors
		list_of_relevant_sectors = []

		# get sectors for each selected pillar
		for pillar in self.pillar_alignment_table:
			# get all the sectors for each ticked pillar
			if pillar.yes:
				sectors_for_pillar = frappe.get_list("NDP",
					fields=["name","parent_ndp"],
					filters = {
						"type":"Sector",
						"parent_ndp":pillar.pillar
				})

				# append the sectors to the list of pillars
				list_of_relevant_sectors += sectors_for_pillar

		# append the sectors to the sectors table
		for sector in list_of_relevant_sectors:
			self.append("sector_alignment_table", {
				"pillar":sector["parent_ndp"],
				"sector":sector["name"]
			})

	if self.current_section == "priority_programs_section_section":
		# # first clear the priority programs table
		self.priority_programs_table = []
		self.confirm_priority_programs = ""

		# get sectors for each selected pillar
		for sector in self.sector_alignment_table:
			
			# get all the programs for each ticked sector
			if sector.yes:
				program_for_sector = frappe.get_list("Program",
					fields=["name","program_title","program","sector"],
					filters = {
						"sector":sector.sector,
				})

				for program in program_for_sector:
					self.append("priority_programs_table", {
						"priority_program":program["name"],
						"priority_program_description":program["program"],
						"sector":program["sector"]
					})


	if self.current_section == "relevant_mdas_section_section":
		# # first clear the relevant mdas table
		self.relevant_mda_table = []
		self.confirm_priority_programs = ""
		
		# loop through all the selected sectors
		for sector in self.sector_alignment_table:
			# get all the mdas for each ticked sector
			if sector.yes:
				mdas_for_sector_list = frappe.get_list("Department Responsible Table",
					fields=["sector","parent"],
					filters = {
						"sector":sector.sector,
						"parenttype":"Organization"
				})

				# loop through the found ndp 
				for mda in mdas_for_sector_list:
					self.append("relevant_mda_table", {
						"relevant_mda_per_sector":mda["parent"],
						"sector":mda["sector"]
					})

	if self.current_section == "relevant_ndp_outcome_by_selected_priority_program_section":
		# # first clear the relevatn table
		self.relevant_outcomes__table = []
		self.confirm_outcomes = ""
		
		# loop through all the selected priority programs
		for program in self.priority_programs_table:
			# get all the outcomes linked to these outcomes
			if program.yes:
				outomes_for_program_list = frappe.get_list("Outcome",
					fields=["name","sector","outcome","linked_priority_program"],
					filters = {
						"linked_priority_program":program.priority_program
				})

				for outcome in outomes_for_program_list:
					self.append("relevant_outcomes__table", {
						"outcome":outcome["name"],
						"outcome_description":outcome["outcome"],
						"priority_program":outcome["linked_priority_program"],
						"sector":outcome["sector"]
					})

def find_simalar_organization(given_name,given_acronym):
	'''
	Function That Check if There Are Any Organization with Similar
	Names and Acronyms to those given in the Form
	'''
	# holds all the organization found through this search
	all_found_organization = []

	def process_results(list_of_organizations):
		'''
		Function that will help to make the code Drier within
		this function by reducing repetitive tasks
		'''

		if len(list_of_organizations) > 0:
			for organization in list_of_organizations:
				organization_name = organization[0]
				all_found_organization.append(organization_name)

	# now check for similarities in name
	list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '%{}%'".format(given_name))
	process_results(list_of_organizations)



	# full name of each word in the name query
	list_of_words_in_name = given_name.split()
	# get rid of conjuctions
	common_conjuctions = ["of","the"]
	# loop through name parts
	for name_part in list_of_words_in_name:
		if name_part.lower() in common_conjuctions:
			pass
		else:
			# process results 
			list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '%{}%'".format(name_part))
			process_results(list_of_organizations)

	# starting 2 letters letter query
	first_two_letters = given_name[:2]
	list_of_organizations = frappe.db.sql("select name from `tabOrganization` where name like '{}%'".format(first_two_letters))
	if len(list_of_organizations) > 0:
		process_results(list_of_organizations)
	else:
		pass

	# Acronym matches
	# full name of acronym matches
	if given_acronym:
		search_acronymn = given_acronym
		list_of_organizations = frappe.db.sql("select name from `tabOrganization` where acronym like '%{}%'".format(search_acronymn))
		if len(list_of_organizations) > 0:
			process_results(list_of_organizations)
		else:
			pass
	
	# loop through unique organizations
	unique_organizations =  set(all_found_organization)
	
	# return unique organizations
	return unique_organizations


def find_simalar_projects(self):
	'''
	Function That Checks if There Are Any Master Project With Similar Name
	if the user does not find their projects listed in the list of Projects
	'''
	# holds all the organization found through this search
	all_found_projects = [] 

	def process_results(list_of_projects):
		'''
		Function that will help to make the code Drier within
		this function by reducing repetitive tasks
		'''
		if len(list_of_projects) > 0:
			for project in list_of_projects:
				project_name = project[0]
				all_found_projects.append(project_name)
		
	# now check for similarities in name
	list_of_projects = frappe.db.sql("select name from `tabProject` where name like '%{}%' && master_project = 1".format(self.new_project_title))
	process_results(list_of_projects)

	# full name of each word in the name query
	list_of_words_in_name = self.new_project_title.split()
	# get rid of conjuctions
	common_conjuctions = ["of","the"]
	# loop through name parts
	for name_part in list_of_words_in_name:
		if name_part.lower() in common_conjuctions:
			pass
		else:
			# process results 
			list_of_projects = frappe.db.sql("select name from `tabProject` where name like '%{}%' && master_project = 1".format(name_part))
			process_results(list_of_projects)

	# starting 2 letters letter query
	first_two_letters = self.new_project_title[:2]
	list_of_projects = frappe.db.sql("select name from `tabProject` where name like '{}%' && master_project = 1".format(first_two_letters))
	if len(list_of_projects) > 0:
		process_results(list_of_projects)
		pass

	# Acronym matches
	# full name of acronym matches
	search_project_code = self.new_project_code
	list_of_projects = frappe.db.sql("select name from `tabProject` where project_code like '%{}%' && master_project = 1".format(search_project_code))
	if len(list_of_projects) > 0:
		process_results(list_of_projects)
		pass

	# sources based matches
	for source in self.source_organization_table:
		# check if user gave a registered source
		if source.source_organization_does_not_exist_in_the_list_above == 0:
			# get relevant projects
			list_of_projects = frappe.db.sql("select parent from `tabSource Organization` where  parenttype = 'Project' && parentfield = 'source_organization_table' && source_organization_name = '{}'".format(source.source_organization_name))
			if len(list_of_projects) > 0:
				# check if project are master
				for project in list_of_projects:
					doc = frappe.get_doc('Project',project[0])
					# if the project is a master project
					if doc.master_project == 1:
						all_found_projects.append(project[0])

	# co-implemtors based matches
	for co_implementor in self.co_implementing_table:
		# get relevant projects
			list_of_projects = frappe.db.sql("select parent from `tabCoImplementing Partners Table` where  parenttype = 'Project' && parentfield = 'co_implementing_table' && organization_name = '{}'".format(co_implementor.organization_name))
			if len(list_of_projects) > 0:
				# check if project are master
				for project in list_of_projects:
					doc = frappe.get_doc('Project',project[0])
					# if the project is a master project
					if doc.master_project == 1:
						all_found_projects.append(project[0])	
	
	unique_results =  set(all_found_projects)
	return unique_results

def check_if_project_title_exists(self,project_name,project_code):
	'''
	Checks project titles exists else create them
	'''

	list_of_projects = frappe.get_list("Project",
		fields=["*"],
		filters = {
			"name":project_name
	})
	
	
	if len(list_of_projects) == 1:
		pass
	elif len(list_of_projects) == 0:
		# create a project
		doc = frappe.get_doc({"doctype":"Project"})
		doc.name = project_name
		doc.project_name  = project_name
		doc.project_code = project_code
		doc.insert()

def map_data_to_project(self):
	'''
	Function that Updates the details of the organization
	to the organization form once the form is approved
	'''
	# check if the project has been verified
	if self.status == "Verified":
		pass

	if self.status == "Admin Approved":
		pass

	if self.status == "Validated":
		pass
	
	if self.status == "Approved":
		# create instance of subproject
		sub_project_doc = frappe.get_doc("Project", self.phase_title)

		# top section
		sub_project_doc.status = self.status
		sub_project_doc.master_project = 0
		# top section
		sub_project_doc.sub_project = 0
		sub_project_doc.sub_project = 1
		sub_project_doc.link_to_master_project = self.project_title_selection_field

		# general_project_details_section
		# sub_project_doc.project_name = self.sub_project_name
		sub_project_doc.project_code = self.new_project_code
		# sub_project_doc.full_project_title = self.
		sub_project_doc.sub_project_name = self.sub_project_name
		# sub_project_doc.project_renewed_from = self.sub_project_name
		sub_project_doc.your_organization = self.select_your_organization
		sub_project_doc.organization_acronym = self.organization_acronym

		# Source Organization Section
		# first clear the fields

		# Screen 2
		# sub_project_doc.yes_destination_organization = self.yes_destination_organization
		# sub_project_doc.no_destination_organization = self.no_destination_organization

		sub_project_doc.source_organization_table = []

		# project_details_section
		# sub_project_doc.project_name = self.sub_project_name
		# sub_project_doc.project_code = self.
		# sub_project_doc.full_project_title = self.
		# sub_project_doc.sub_project_name = self.
		# sub_project_doc.project_renewed_from = self.
		# sub_project_doc.phase = self.

		# save the changes to the form
		for source in self.source_organization_table:
			
			# check if the source type and source are given correctly
			if source.source_organization_type and source.source_organization_name :
				new_source_doc = frappe.get_doc('Organization', source.source_organization_name)
				sub_project_doc.append("source_organization_table", {
						"source_organization_type": source.source_organization_type,
						"source_organization_name":source.source_organization_name,
						"funding_status_select_one":source.funding_status_select_one,
						"amount":source.amount,
					})

			elif source.source_organization_does_not_exist_in_the_list_above == 1 and source.enter_name_of_source_organization:
				# get the organization type for created
				new_source_doc = frappe.get_doc("Organization",source.enter_name_of_source_organization)
				# find the correct types
				
				sub_project_doc.append("source_organization_table", {
						'source_organization_does_not_exist_in_the_list_above': source.source_organization_does_not_exist_in_the_list_above,
						"source_organization_type": source.enter_funder_type,
						"enter_name_of_source_organization":source.enter_name_of_source_organization,
						"funding_status_select_one":source.funding_status_select_one,
						"amount":source.amount,
					})
					
			elif source.source_organization_does_not_exist_in_the_list_above == 1 and source.enter_funder_type == 'OTHER':
				new_source_doc = frappe.get_doc('Organization', source.enter_name_of_source_organization)

				sub_project_doc.append('source_organization_table', {
					'source_organization_does_not_exist_in_the_list_above': source.source_organization_does_not_exist_in_the_list_above,
					'source_organization_type': 'OTHER',
					'source_organization_name': source.other_specify_name,
					'funding_status_select_one':source.funding_status_select_one,
					'amount':source.amount,
				})

			else:
				pass

		sub_project_doc.all_sources_filled_check = self.all_sources_filled_check		

		# project_amount_section
		sub_project_doc.total_project_amount_in_usd = self.total_project_amount_in_usd
		sub_project_doc.total_project_amount = self.total_project_amount
		# sub_project_doc.balance_amount_in_usd = self.balance_amount_in_usd
		sub_project_doc.amount_commited = self.amount_commited
		sub_project_doc.amount_in_pipeline = self.amount_in_pipeline
		# sub_project_doc.operational_amount = self.operational_amount
		sub_project_doc.funding_gap_in_usd = self.funding_gap_in_usd
		sub_project_doc.confirm_total_amounts = self.confirm_total_amounts

		# co-implementing partners section
		sub_project_doc.co_implementors_yes = self.co_implementors_yes
		sub_project_doc.co_implementors_no = self.co_implementors_no
		# first clear the co-implementors table
		sub_project_doc.co_implementing_table = []
		for coimplementor in self.co_implementing_table:
			if coimplementor.organization_name:
				sub_project_doc.append("co_implementing_table", {
						"organization_name": coimplementor.organization_name
					})
		sub_project_doc.all_implementors_added_check = self.confirm_coimplementors

		sub_project_doc.total_implementor_check = self.total_implementor_check
		sub_project_doc.partial_implementor_check = self.partial_implementor_check
		sub_project_doc.not_implementor_check = self.not_implementor_check

		# first clear the destiantion table
		sub_project_doc.destination_organization_table = []
		for destination in self.destination_organization_table:
			if destination.implementing_organization:
				sub_project_doc.append("destination_organization_table", {
						"implementing_organization": destination.implementing_organization,
						"amount":destination.amount,
						"reporting_obligation":destination.reporting_obligation,
						"reporting_organization":destination.reporting_organization
					})
			else:
				try:
					sub_project_doc.append("destination_organization_table", {
							"implementing_organization": destination.enter_name_of_implementing_organization,
							"amount":destination.amount,
							"reporting_obligation":destination.reporting_obligation,
							"reporting_organization":destination.reporting_organization
						})
				except:
					pass
		sub_project_doc.all_implementors_added_check = self.all_implementors_added_check
		
		# General Project Details Section
		sub_project_doc.project_name = self.sub_project_name
		sub_project_doc.operational_amount = self.project_name
		sub_project_doc.project_code = self.project_code
		sub_project_doc.operational_amount = self.sub_project_name
		# sub_project_doc.operational_amount = self.phase
		# sub_project_doc.operational_amount = self.project_renewed_from
		# sub_project_doc.operational_amount = self.project_phase
		sub_project_doc.project_start_date = self.project_start_date
		sub_project_doc.project_end_date = self.project_end_date

		# pillar_alignment_section
		# first clear  table
		sub_project_doc.pillar_alignment_table = []
		for pillar in self.pillar_alignment_table:
			if pillar.pillar and pillar.yes:
				sub_project_doc.append("pillar_alignment_table", {
					"pillar":pillar.pillar,
					"yes":pillar.yes
				})

		sub_project_doc.confirm_pillar_selection = self.confirm_pillar_selection
		# sector_alignment_section_section
		sub_project_doc.sector_alignment_table = []
		for sector in self.sector_alignment_table:
			if sector.pillar and sector.sector and  sector.yes:
				sub_project_doc.append("sector_alignment_table", {
					"pillar":sector.pillar,
					"sector":sector.sector,
					"yes":sector.yes
				})

		sub_project_doc.confirm_sector_selection = self.confirm_sector_selection
		
		# priority_programs_section_section
		# first clear  table
		sub_project_doc.priority_programs_table = []
		for program in self.priority_programs_table:
			if program.priority_program and program.yes:
				sub_project_doc.append("priority_programs_table", {
					"priority_program":program.priority_program,
					"priority_program_description":program.priority_program_description,
					"sector":program.sector,
					"yes":program.yes
				})

		# confirm priority programs
		sub_project_doc.confirm_priority_programs = self.confirm_priority_programs
		sub_project_doc.no_ndp_check = self.no_ndp_check

		# relevant_mdas_section_section
		# first clear  table
		sub_project_doc.relevant_mda_table = []
		for mda in self.relevant_mda_table:
			if mda.relevant_mda_per_sector and mda.lead_mda:
				sub_project_doc.append("relevant_mda_table", {
					"relevant_mda_per_sector":mda.relevant_mda_per_sector,
					"sector":mda.sector,
					"lead_mda":mda.lead_mda,
				})

		sub_project_doc.confirm_mdas = self.confirm_mdas
		sub_project_doc.no_gi_check = self.no_gi_check

		# add relevant outcomes
		sub_project_doc.relevant_outcomes__table = []
		for outcome in self.relevant_outcomes__table:
			if outcome.outcome and outcome.yes:
				sub_project_doc.append("relevant_outcomes__table", {
					"outcome":outcome.outcome,
					"outcome_description":outcome.outcome_description,
					"attached_to_priority_program":outcome.attached_to_priority_program,
					"priority_program":outcome.priority_program,
					"sector":outcome.sector,
					"yes":outcome.yes,
				})

		sub_project_doc.confirm_outcomes = self.confirm_outcomes
		sub_project_doc.no_outcome_check = self.no_outcome_check

		# save checked data to approved project
		list_of_regions = [{self.marodijeh_check: "Marodijeh/Maroodijeex",
								self.sanag_check: "Sanag/Sanaag",
								self.hargeisa_region: "Central/Hargeisa",
								self.sool_check: "Sool/Sool",
								self.togdheer_check: "Togdheer/Togdheer",
								self.awdal_check: "Awdal/Awdal",
								self.sahil_check: "Sahil/Saaxil",
							}]


		for region in list_of_regions:
			sub_project_doc.project_location_table = []
			if region:
				# check morodijeh
				marodijeh_check = False

				if self.hargeisa:
					sub_project_doc.append('project_location_table', {
						'region': 'Marodijeh/Maroodijeex',
						'district': 'Hargeisa/Hargeysa'
					})
					marodijeh_check = True

				if self.gabiley:
					sub_project_doc.append('project_location_table', {
						'region': 'Marodijeh/Maroodijeex',
						'district': 'Gabiley/Gabilay'
					})
					marodijeh_check = True

				if self.baligubadle:
					sub_project_doc.append('project_location_table', {
						'region': 'Marodijeh/Maroodijeex',
						'district': 'Baligubadle/Baligubadle'
					})
					marodijeh_check = True

				if self.salahlay:
					sub_project_doc.append('project_location_table', {
						'region': 'Marodijeh/Maroodijeex',
						'district': 'Salahlay/Salaxlay'
					})
					marodijeh_check = True

				if self.marodijeh_check and marodijeh_check == False:
					sub_project_doc.append('project_location_table', {
						'region': 'Marodijeh/Maroodijeex',
					})

				# check region Sanaag
				sanag_check = False

				if self.erigavo:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
						'district': 'Erigavo/Ceerigaabo'
					})
					sanag_check = True

				if self.badhan:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
						'district': 'Badhan/Badhan'
					})
					sanag_check = True

				if self.las_qoray:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
						'district': 'Las Qoray /Laasqoray'
					})
					sanag_check = True

				if self.ela_fweyn:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
						'district': 'El Afweyn/Ceelafweyn'
					})
					sanag_check = True

				if self.dhahar:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
						'district': 'Dhahar /Dhahar'
					})
					sanag_check = True

				if self.gar_adag:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
						'district': 'Gar Adag /Garadag'
					})
					sanag_check = True

				if self.sanag_check and sanag_check == False:
					sub_project_doc.append('project_location_table', {
						'region': 'Sanag/Sanaag',
					})


				# Check for Central or Hargeisa	
				hargeisa_check = False

				if self.hargeisa_region and hargeisa_check == False :
					sub_project_doc.append('project_location_table', {
						'region': 'Central/Hargeisa',
					})

				# check for sool
				sool_check = False
 
				if self.las_anod:
					sub_project_doc.append('project_location_table', {
						'region': 'Sool/Sool',
						'district': 'Las Anod /Laascaanood'
					})
					sool_check = True

				if self.hudun:
					sub_project_doc.append('project_location_table', {
						'region': 'Sool/Sool',
						'district': 'Hudun /Xuddun'
					})
					sool_check = True

				if self.taleh:
					sub_project_doc.append('project_location_table', {
						'region': 'Sool/Sool',
						'district': 'Taleh/Taleex'
					})
					sool_check = True

				if self.aynabo:
					sub_project_doc.append('project_location_table', {
						'region': 'Sool/Sool',
						'district': 'Aynabo/Caynabo'
					})
					sool_check = True

				if self.sool_check and sool_check == False:
					sub_project_doc.append('project_location_table', {
						'region': 'Sool/Sool',
					})

				# check togder region
				togdheer_check = False

				if self.burao:
					sub_project_doc.append('project_location_table', {
						'region': 'Togdheer/Togdheer',
						'district': 'Burao/Burco'
					})
					togdheer_check = True

				if self.odwayne:
					sub_project_doc.append('project_location_table', {
						'region': 'Togdheer/Togdheer',
						'district': 'Odwayne/Oodwayne'
					})
					togdheer_check = True

				if self.buhodle:
					sub_project_doc.append('project_location_table', {
						'region': 'Togdheer/Togdheer',
						'district': 'Buhodle/Buuhoodle'
					})
					togdheer_check = True

				if self.togdheer_check and togdheer_check == False:
					sub_project_doc.append('project_location_table', {
						'region': 'Togdheer/Togdheer',
					})

				# check awadal
				awdal_check = False

				if self.borama:
					sub_project_doc.append('project_location_table', {
						'region': 'Awdal/Awdal',
						'district': 'Borama/Boorama '
					})
					awdal_check = True

				if self.zeila:
					sub_project_doc.append('project_location_table', {
						'region': 'Awdal/Awdal',
						'district': 'Zeila/Saylac '
					})
					awdal_check = True

				if self.baki:
					sub_project_doc.append('project_location_table', {
						'region': 'Awdal/Awdal',
						'district': 'Baki/Baki'
					})
					awdal_check = True

				if self.lughaya:
					sub_project_doc.append('project_location_table', {
						'region': 'Awdal/Awdal',
						'district': 'Lughaya/Lughaya'
					})
					awdal_check = True

				if self.awdal_check and awdal_check == False:
					sub_project_doc.append('project_location_table', {
						'region': 'Awdal/Awdal',
					})

				# check sahil
				sahil_check = False
				if self.berbera:
					sub_project_doc.append('project_location_table', {
						'region': 'Sahil/Saaxil',
						'district': 'Berbera/Berbera'
					})
					sahil_check == True

				if self.sheekh:
					sub_project_doc.append('project_location_table', {
						'region': 'Sahil/Saaxil',
						'district': 'Sheikh/Sheekh'
					})
					sahil_check == True
				
				if self.sahil_check and sahil_check == False:
					sub_project_doc.append('project_location_table', {
						'region': 'Sahil/Saaxil',
					})		
		
		# map description
		sub_project_doc.description = self.description

		# Attach log frame
		sub_project_doc.attach_the_projects_log_frame = self.attach_the_projects_log_frame

		sub_project_doc.save()


def map_data_to_form(self):
	'''
	Function that maps data from the project to the form
	'''
	if self.pulled_from_main:
		pass
	else:
		# get the subproject doc
		sub_project_doc = frappe.get_doc("Project",self.sub_project_title_1)

		# section_break_1
		self.select_your_organization = sub_project_doc.your_organization
		self.organization_acronym =  sub_project_doc.organization_acronym
		self.project_title_selection_field =  sub_project_doc.link_to_master_project
		self.new_project_code = sub_project_doc.project_code

		# sources section
		self.yes_destination_organization = 1
		# get all the sources in the table
		sub_project_sources = frappe.get_list("Source Organization",
				fields=["*"],
				filters = {
					"parent":sub_project_doc.name,
					"parenttype":"Project",
					"parentfield":"source_organization_table"
			})

		# excluding amounts in order to let the user enter new funding amount
		if len(self.source_organization_table) > 0:
				pass
		else:
			for source in sub_project_sources:
				self.append("source_organization_table", {
					"source_organization_type": source["source_organization_type"],
					"source_organization_name":source["source_organization_name"],
					"source_organization_type":source["source_organization_type"],
					"funding_status_select_one":source["funding_status_select_one"],
					"amount":source["amount"],
				})

		self.all_sources_filled_check = sub_project_doc.all_sources_filled_check		

		# pull all the project totals
		self.total_project_amount_in_usd = sub_project_doc.total_project_amount_in_usd
		self.total_project_amount =  sub_project_doc.total_project_amount
		self.funding_gap_in_usd =  sub_project_doc.funding_gap_in_usd
		self.amount_in_pipeline = sub_project_doc.amount_in_pipeline
		self.amount_commited =  sub_project_doc.amount_commited
		self.total_project_budget = sub_project_doc.total_project_budget
		
		# get all the co-implementors in the table
		sub_project_co_imps = frappe.get_list("CoImplementing Partners Table",
				fields=["*"],
				filters = {
					"parent":sub_project_doc.name,
					"parenttype":"Project",
					"parentfield":"co_implementing_table"
			})

		if len(self.co_implementing_table) > 0:
				pass
		else:
			if len(sub_project_co_imps) > 0:
				self.co_implementors_yes = 1

			# loop through found co-implementing partners
			for co_imp_org in sub_project_co_imps:
				self.append("co_implementing_table", {
					"organization_name": co_imp_org["organization_name"],
					"yes":1,
				})

		# map type of implementation
		self.total_implementor_check = sub_project_doc.total_implementor_check
		self.partial_implementor_check = sub_project_doc.partial_implementor_check
		self.not_implementor_check = sub_project_doc.not_implementor_check

		# destination implementating organizations destination_implementing_and_reporting_organization_section
		# get all the destination,implenting organizations in the table
		sub_project_dest_imps = frappe.get_list("Destination Implementing Reporting",
				fields=["*"],
				filters = {
					"parent":sub_project_doc.name,
					"parenttype":"Project",
					"parentfield":"destination_organization_table"
			})

		if len(self.destination_organization_table) > 0:
				pass
		else:
			# loop through found co-implementing partners
			for dest_imp_org in sub_project_dest_imps:
				self.append("destination_organization_table", {
					"implementing_organization": dest_imp_org["implementing_organization"],
					"amount":dest_imp_org["amount"],
					"reporting_obligation":dest_imp_org["reporting_obligation"],
					"reporting_organization":dest_imp_org["reporting_organization"],
				})

		# General project details section
		self.project_name = sub_project_doc.project_name
		self.project_code = sub_project_doc.project_code
		self.sub_project_name = sub_project_doc.sub_project_name
		self.project_renewed_from = sub_project_doc.project_renewed_from
		self.project_start_date = sub_project_doc.project_start_date
		self.project_end_date = sub_project_doc.project_end_date

		# pillar alignment section
		set_marked_pillars(self,sub_project_doc)

		# sector alignemnt Section
		set_marked_sectors(self,sub_project_doc)

		# Priority programs section
		set_marked_programs(self,sub_project_doc)

		# map mdas 
		set_marked_mdas(self,sub_project_doc)

		# map outcomes
		set_marked_outcomes(self,sub_project_doc)

		# map locations 
		set_selected_locations(self,sub_project_doc)

		# project description
		self.description = sub_project_doc.description
		self.attach_the_projects_log_frame = sub_project_doc.attach_the_projects_log_frame

		# mark form as having pulled from main
		# self.pulled_from_main = 1



def set_selected_locations(self,sub_project_doc):
	'''
	Map all the locations from the approved project to the form
	'''
	# get all the locations
	sub_doc_locations = frappe.get_list("Project Location",
		fields=["region","district"],
		filters = {
			"parent":sub_project_doc.name,
			"parenttype":"Project",
			"parentfield":"project_location_table"
	})

	# loop through each location doc
	for location_doc in sub_doc_locations:
		# add the region
		if location_doc['region'] == "Marodijeh/Maroodijeex":
			self.marodijeh_check = 1
		elif location_doc['region'] == "Sanag/Sanaag":
			self.sanag_check = 1
		elif location_doc['region'] == "Central/Hargeisa":
			self.hargeisa_region = 1
		elif location_doc['region'] == "Sool/Sool":
			self.sool_check = 1
		elif location_doc['region'] == "Togdheer/Togdheer":
			self.togdheer_check = 1
		elif location_doc['region'] == "Awdal/Awdal":
			self.awdal_check = 1
		elif location_doc['region'] == "Sahil/Saaxil":
			self.sahil_check = 1

		# add the district under Maroodijeex
		if location_doc['district'] == "Hargeisa/Hargeysa":
			self.hargeisa = 1
		elif location_doc['district'] == "Gabiley/Gabilay":
			self.gabiley = 1
		elif location_doc['district'] == "Baligubadle/Baligubadle":
			self.baligubadle = 1
		elif location_doc['district'] == "Salahlay/Salaxlay":
			self.salahlay = 1

		# add the district under Sanaag
		if location_doc['district'] == "Erigavo/Ceerigaabo":
			self.erigavo = 1
		elif location_doc['district'] == "Badhan/Badhan":
			self.badhan = 1
		elif location_doc['district'] == "Las Qoray /Laasqoray":
			self.las_qoray = 1
		elif location_doc['district'] == "El Afweyn/Ceelafweyn":
			self.ela_fweyn = 1
		elif location_doc['district'] == "Dhahar /Dhahar":
			self.dhahar = 1
		elif location_doc['district'] == "Gar Adag /Garadag":
			self.gar_adag = 1

		# add the district under Sool
		if location_doc['district'] == "Las Anod /Laascaanood":
			self.las_anod = 1
		elif location_doc['district'] == "Hudun /Xuddun":
			self.hudun = 1
		elif location_doc['district'] == "Taleh/Taleex":
			self.taleh = 1
		elif location_doc['district'] == "Aynabo/Caynabo":
			self.aynabo = 1

		# add the district under Togdheer
		if location_doc['district'] == "Burao/Burco":
			self.burao = 1
		elif location_doc['district'] == "Odwayne/Oodwayne":
			self.odwayne = 1
		elif location_doc['district'] == "Buhodle/Buuhoodle":
			self.buhodle = 1

		# add the district under Awdal
		if location_doc['district'] == "Borama/Boorama":
			self.borama = 1
		elif location_doc['district'] == "Zeila/Saylac":
			self.zeila = 1
		elif location_doc['district'] == "Baki/Baki":
			self.baki = 1
		elif location_doc['district'] == "Lughaya/Lughaya":
			self.lughaya = 1

		# add the district under Sahil
		if location_doc['district'] == "Berbera/Berbera":
			self.berbera = 1
		elif location_doc['district'] == "Sheikh/Sheekh":
			self.sheekh = 1
		

