// Copyright (c) 2019, Frappe Technologies and contributors
// For license information, please see license.txt


// ================================================================================================
/* This section contains code from the general functions section
which are called is the form triggered functions section*/

// add comment in order push

// global variables
var list_of_project_fields = [
	// general organizaiton details
	"sub_project_title_1",

	// organization recieved funds
	"yes_destination_organization","no_destination_organization","source_organization_table",
	"all_sources_filled_check",

	// soure suggestion section
	"source_suggestion_table","yes_found_source_match","no_no_source_match",

	// project_amount_section
	"confirm_total_amounts",

	// section_c_section co-implementing organization section
	"co_implementors_yes","co_implementors_no",
	"co_implementing_table","confirm_coimplementors",

	// section_b_section type of implementation section
	"total_implementor_check","partial_implementor_check","not_implementor_check",

	// destination_implementing_and_reporting_organization_section
	"all_implementors_added_check",

	// project_details_section
	"project_start_date","project_end_date",

	// pillar_alignment_section
	"confirm_pillar_selection",

	// sector_alignment_section_section
	"confirm_sector_selection",

	// priority_programs_section_section
	"confirm_priority_programs","no_ndp_check",

	// relevant_mdas_section_section
	"confirm_mdas","no_gi_check",

	// relevant_ndp_outcome_by_selected_priority_program_section
	"confirm_outcomes","no_outcome_check",

	// project_location_section
	"marodijeh_check","hargeisa","gabiley","baligubadle","salahlay",
	"sanag_check","erigavo","badhan","las_qoray","ela_fweyn","dhahar",
	"gar_adag","hargeisa_region","hargeisa_district",
	"sool_check","las_anod","hudun","taleh","aynabo",
	"togdheer_check","burao","odwayne","buhodle",
	"awdal_check","borama","zeila","baki","lughaya",
	"sahil_check","berbera","sheekh",

	// project_description_section
	"description",
	
	// attachments_section_section
	"attach_the_projects_log_frame",

	// Verification and approval section
	"verifier_name","verifier_designation",
]

// Array of sections listed in order of filling/ occurance
var list_of_sections = [
	"section_break_1","section_a_section",
	"source_suggestion",
	"project_amount_section","section_c_section",
	"section_b_section","destination_implementing_and_reporting_organization_section",
	"implementing__partners_section_section","project_details_section",
	"pillar_alignment_section","sector_alignment_section_section","priority_programs_section_section",
	"relevant_mdas_section_section","relevant_ndp_outcome_by_selected_priority_program_section",
	"project_location_section","project_description_section",
	"attachments_section_section",
	"verification_section_section",
]

var list_of_all_view_form_sections = [
	"section_break_1","section_a_section",
	"project_amount_section","section_c_section",
	"section_b_section","destination_implementing_and_reporting_organization_section",
	"project_details_section","pillar_alignment_section",
	"sector_alignment_section_section","priority_programs_section_section",
	"relevant_mdas_section_section","relevant_ndp_outcome_by_selected_priority_program_section",
	"project_location_section","project_description_section",
	"attachments_section_section",
	"verification_section_section","validation","project_approval_section"
]

var list_of_all_sections = [
	"section_break_1","section_a_section",
	"source_suggestion",
	"project_amount_section","section_c_section",
	"section_b_section","destination_implementing_and_reporting_organization_section",
	"implementing__partners_section_section","project_details_section",
	"pillar_alignment_section","sector_alignment_section_section","priority_programs_section_section",
	"relevant_mdas_section_section","relevant_ndp_outcome_by_selected_priority_program_section",
	"project_location_section","project_description_section",
	"attachments_section_section",
	"verification_section_section","validation","validation","approval"
]

var field_to_hide_unhide = {
	new_project_name: ["enter_new_project_name","enter_new_project_code"],
	all: ["enter_new_project_name","enter_new_project_code"],
}

var project_title_suggestions_instruction = "Based on the Source Organizations and Co-Implementing\
	Organizations Given in Previous Sections the Following are Possible Project Titles \
	You are Trying to Create. Please Go Through The Table Below to Determine \
	if You Project Matches Any of them"

// /* This section contains code from the general functions section
// which are called is the form triggered functions section*/
// // ================================================================================================

// function that redirect the user to a given url
function redirect_url(new_url){
	// allow the user to view page
	window.location = new_url
}

/*function that toogles field to hide or unhide*/
function hide_unhide_fields(frm, list_of_fields, hide_or_unhide) {
	for (var i = 0; i < list_of_fields.length; i++) {
		frm.toggle_display(list_of_fields[i], hide_or_unhide)
	}
}

function hide_unhide_on_refresh(frm) {
	if (frm.doc.project_name == "OTHER") {
		hide_function(frm, field_to_hide_unhide, "new_project_name")
	}
	
	function hide_function(frm, field_to_hide_unhide, language) {
		var hide_fields = field_to_hide_unhide["all"]
		var unhide_fields = field_to_hide_unhide[language]
		if (language == "none") {
			hide_unhide_fields(frm, hide_fields, false)
		}
		else {
			hide_unhide_fields(frm, hide_fields, false)
			hide_unhide_fields(frm, unhide_fields, true)
		}
	}
}


// function that determines if  user has privillages to Validite , approve,verify info
function check_privillages(frm,action){
	// check if doc is saved
	if(frm.doc.__islocal ? 0 : 1){
		// check privillages absed on action
		if(action == "checked"){
			// check if the user is the owner of the doc
			if(frappe.user.has_role("Staff")){
				// allow administrator to mark as checked
				cur_frm.set_value("information_checked",1)
				cur_frm.save()
			}
			else if(frm.doc.owner){
				// check if the current user is also the owner
				if(frappe.session.user == frm.doc.owner){
					// allow administrator to validate
					cur_frm.set_value("information_checked",1)
					cur_frm.save()
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}else{
				// do not do anything for now
			}

		}else if(action == "verify"){
			// check if the user is the owner of the doc
			if(frappe.user.has_role("Staff")){
				if (frm.doc.verifier_name && frm.doc.verifier_designation) {
					// allow administrator to validate
					cur_frm.set_value("verified",1)
					cur_frm.set_value("status","Verified")
					cur_frm.set_value("verified_by",frappe.session.user)
					cur_frm.save()
				} else {
					cur_frm.set_value('verified', 0);
					frappe.throw('Fill in <b>Name</b> and <b>Designation</b> fields before verifying');
				}
			}
			else if(frm.doc.owner){
				// check if the current user is also the owner
				if(frappe.session.user == frm.doc.owner){
					if (frm.doc.verifier_name && frm.doc.verifier_designation) {
						// allow administrator to validate
						cur_frm.set_value("verified",1)
						cur_frm.set_value("status","Verified")
						cur_frm.set_value("verified_by",frappe.session.user)
						cur_frm.save()
					} else {
						cur_frm.set_value('verified', 0);
						frappe.throw('Fill in <b>Name</b> and <b>Designation</b> fields before verifying');
					}
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}else{
				if(frappe.session.user == frm.doc.created_by){
					// allow user to validate their organization
					if (frm.doc.verifier_name && frm.doc.verifier_designation) {
						// allow administrator to validate
						cur_frm.set_value("verified",1)
						cur_frm.set_value("status","Verified")
						cur_frm.set_value("verified_by",frappe.session.user)
						cur_frm.save()
					} else {
						cur_frm.set_value('verified', 0);
						frappe.throw('Fill in <b>Name</b> and <b>Designation</b> fields before verifying');
					}
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}

		}else if(action == "validate"){
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Staff")){
				// the user has privillages allow
				// check if the if its validated
				if(frm.doc.verified == 1){
					cur_frm.set_value("validated",1)
					cur_frm.set_value("status","Validated")
					cur_frm.set_value("validated_by",frappe.session.user)
					cur_frm.save()
				}else{
					frappe.throw("Form need to be verified before validation")
				}
				
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
	
		}else if(action == "approve"){
			// check if the user has the Role "Documents Approver"
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Documents Approver")){
				// the user has privillages allow
				// check if the if its validated
				if(frm.doc.validated == 1){
					cur_frm.set_value("approved",1)
					cur_frm.set_value("status","Approved")
					cur_frm.set_value("approved_by",frappe.session.user)
					cur_frm.save()
				}else{
					frappe.throw("Form need to be validated before approval")
				}
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
		}
	}else{
		frappe.throw("Please save the document first")
	}
}

function goToPage(){
	frappe.prompt([
		{'fieldname': 'page', 'fieldtype': 'Int', 'label': 'Go to page', 'reqd': 1}  
	],
	function(values){
		if(values.page <= list_of_sections.length &&  values.page > 0 ){
			// set the current page
			cur_frm.set_value("previous_section",list_of_sections[values.page -2])
			cur_frm.set_value("current_section",list_of_sections[values.page-1])
			cur_frm.set_value("next_section",list_of_sections[values.page ])

			// // save the form in order to go to that page
			cur_frm.save()

		}else{
			msgprint("Please provide a page betweeen 1 and "+String(list_of_sections.length))
		}
	},
	'Go to page',
	'Go'
	)
}

// function that sets custom buttons
function add_custom_buttons(button_name,action){
	cur_frm.add_custom_button(__(button_name), function(){

		// // only show this buttons to users with administrative privillages
		// if(frappe.user.has_role("Project Admin")){
		if(action=="Unverify"){
			unverify_button()
		}else if(action == "Unvalidate"){
			unvalidate_button()
		}else if(action=="Edit"){
			// check user is allowed to edit
			if(cur_frm.doc.verified){
				frappe.throw("You cannot edit a form that is already verified \
				,contact the admin for assistance")
			}else{
				// make the forms readble for user
				cur_frm.set_value("viewing_information",0)
				cur_frm.set_value("current_section",list_of_sections[0])
				cur_frm.save()
			}
		}else if(action = "Go To Page"){
			goToPage()
		}

	},__("Project Renewal Menu"));
}


// function that sets custom buttons
function add_go_to_buttons(button_name,action){
	cur_frm.add_custom_button(__(button_name), function(){

		if(action = "Go To Page"){
			frappe.prompt([
				{'fieldname': 'page', 'fieldtype': 'Int', 'label': 'Go to screen', 'reqd': 1}  
			],
			function(values){
				if(values.page <= list_of_sections.length &&  values.page > 0 ){
					// set the current page
					cur_frm.set_value("previous_section",list_of_sections[values.page -2])
					cur_frm.set_value("current_section",list_of_sections[values.page-1])
					cur_frm.set_value("next_section",list_of_sections[values.page ])

					// // save the form in order to go to that page
					cur_frm.save()

				}else{
					msgprint("Please provide a page betweeen 1 and "+String(list_of_sections.length))
				}
			},
			'Go to screen',
			'Go'
			)
		}

	},__("Go to screen"));
}

// function that unverifies and unvalidates the form
function unverify_button(){
	cur_frm.set_value("validated","")
	cur_frm.set_value("validated_by","")
	cur_frm.set_value("verified","")
	cur_frm.set_value("verified_by","")
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Pending Verification")
	cur_frm.save()
}

// function that unverifies and unapprovess the form
function unvalidate_button(){
	cur_frm.set_value("validated","")
	cur_frm.set_value("validated_by","")
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Pending Validation")
	cur_frm.save()
}

function renew_button(){
	frappe.throw("Functionality Under Development")
}

// function filter relevant fields
function filter_fields(){
	// filter organization for specific user
	if(frappe.user.has_role("Staff")){
		// do not filter the organizations for administrator
		// PROJECT TITLE SELECTION SECTION
		cur_frm.set_query("project_title_selection_field", function() {
			return {
				"filters": {
					"master_project": 1,
				}
			}
		});

		// Sub Project Title filter
		cur_frm.set_query("sub_project_title_1", function() {
			// get_the users organization
			return {
				"filters": {
					"sub_project":1
				}
			}
		});
	}else{
		// filter organization based on those assigned to user
		cur_frm.set_query("select_your_organization", function() {
			return {
				"filters": {
					"user":frappe.session.user
				}
			}
		});

		// get the organization of logged in user
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Organization",
					filters: {
						user:frappe.session.user
					},
			fields:["*"]
			},
			callback: function(response) {
				if(response.message.length == 0){
					frappe.msgprint("You have not registered an organization")
					location.replace("/profile_organization")
				}else{
					var users_org = response.message[0].name

					// PROJECT TITLE SELECTION SECTION
					cur_frm.set_query("project_title_selection_field", function() {
						return {
							"filters": {
								"master_project": 1,
								"your_organization":users_org
							}
						}
					});

					// Sub Project Title filter
					cur_frm.set_query("sub_project_title_1", function() {
						return {
							"filters": {
								"sub_project": 1,
								"your_organization":users_org,
								"status":"Complete"
							}
						}
					});	
				}
			}	
		});		
	}	
}

// function that determines which section the user should see
function navigation_function(frm,list_of_sections){
	// check if the form is complete
	// if(frm.doc.validated){
	// 	// hide the navigation section
	// 	frm.toggle_display("navigation_section", false)
	// 	// make fields read only for basic users
	// 	if(frappe.session)
	// 	if(frappe.user.has_role("Project Admin")){
	// 		// else do not make form read only
	// 	}
	// 	else{
	// 		// make form readonly
	// 		cur_frm.fields.forEach(function(l){ cur_frm.set_df_property(l.df.fieldname, "read_only", 1); })
	// 	}
	// }else{
	// 	// hide all the fields
	// 	hide_unhide_fields(frm, list_of_sections, false)
	// 	// check the fields to unhide
	// 	if(frm.doc.current_section){
	// 		// unhide the current section
	// 		frm.toggle_display(frm.doc.current_section,true)
	// 	}else{
	// 		// set the current section as the first section
	// 		frm.doc.current_section = list_of_sections[0]
	// 		frm.doc.next_section = list_of_sections[1]
	// 		frm.doc.previous_section = ""
			
	// 		frm.toggle_display(frm.doc.current_section,true)
	// 	}
	// }

	// fill in the section page numbers
	var number_of_sections = list_of_sections.length
	var current_section_index = list_of_sections.indexOf(cur_frm.doc.current_section)
	// fill in the section page numbers
	cur_frm.set_value("pagesection","Screen: "+String(parseInt(current_section_index)+1)+" of "+String(number_of_sections))
	

	if(cur_frm.doc.viewing_information){
		// allow user to see all section
		hide_unhide_fields(frm, list_of_all_view_form_sections, true)
	}else{
		// hide all the fields
		hide_unhide_fields(frm, list_of_all_sections, false)

		// if form has a current section
		if(frm.doc.current_section){
			// unhide the current section
			frm.toggle_display(frm.doc.current_section,true)
		}else{
			frm.doc.current_section = list_of_sections[0]
			frm.doc.next_section = list_of_sections[1]

			// refresh form
			cur_frm.refresh()
		}

		// fill in the section page numbers
		if(frm.doc.current_section){
			// do nothing for now
			var number_of_sections = list_of_sections.length
			var current_section_index = list_of_sections.indexOf(cur_frm.doc.current_section)
			// fill in the section page numbers
			cur_frm.set_value("pagesection","Screen: "+String(parseInt(current_section_index)+1)+" of "+String(number_of_sections))
		}else{
			cur_frm.set_value("pagesection","Screen: 1")
		}
	}
}


function exitButton(){
	
	cur_frm.add_custom_button('Exit', function(){
		
		if(frappe.user.has_role('Staff') || frappe.user.has_role('Super Admin')){
			window.location = '/desk#List/Project%20Renewal%20Form/List'
		} else{
			window.location = '/profile_project'
		}
	})
	
}

// function that unhides fields fields,sections and buttons based on user's role
function options_based_hide_unhide(frm){

	// determine whether to show the project management menu
	if(frappe.user.has_role("Staff")){
		add_custom_buttons("Unverify Form","Unverify")
	}

	if(frappe.user.has_role("Documents Approver")){
		add_custom_buttons("Unvalidate Form","Unvalidate")
	}

	// allow all users to seee this menu items
	add_custom_buttons("Edit Form","Edit")
	add_custom_buttons("Go to screen","Go To Page")

	// PROJECT OWNERSHIP && ACTION TYPE SECTION
	if(frm.doc.action_select_field == "Project Renewal" || frm.doc.action_select_field == "Project Details Update"){
		// unhide related fields
		cur_frm.toggle_display("inst_select_project", true);
		
	}else{
		// clear and hide related feels
		cur_frm.toggle_display("inst_select_project", false);
	}

	// Project Suggestion Section
	if(cur_frm.doc.yes_project_is_in_list || cur_frm.doc.no_project_not_in_list){
		// unhide the instruction and button
		cur_frm.toggle_display("project_title_conf_jnst", true);
		cur_frm.toggle_display("i_have_selected_the_correct_project_title_from_the_table_above", true);
	}else{
		// hide the instruction and button
		cur_frm.toggle_display("project_title_conf_jnst", false);
		cur_frm.toggle_display("i_have_selected_the_correct_project_title_from_the_table_above", false);
	}

	// approval sections
	if(frappe.user.has_role("Staff")){
		// check if form is already verified
		if(cur_frm.doc.verified == 1){
			// show all fields
			cur_frm.toggle_display("validation", true);
			// check if user has the role of documents approver
			if(frappe.user.has_role("Documents Approver")){
				cur_frm.toggle_display("project_approval_section", true);
			}else{
				cur_frm.toggle_display("project_approval_section", false);
			}
		}else{
			// hide the validation ,approval section
			cur_frm.toggle_display("validation", false);
			cur_frm.toggle_display("project_approval_section", false);
		}
	}else{
		// hide the validation ,approval section
		cur_frm.toggle_display("validation", false);
		cur_frm.toggle_display("project_approval_section", false);
	}	

	// Add exit button to forms of basic users.
	if (frappe.user_roles.includes('Staff') || frappe.user_roles.includes('Super Admin')) {
		// Dont add exit button
		// exitButton()
	} else {
		exitButton()
	} 
}	

// function that asks the users to confirm and runs different based on the answer
function confirm_function(confirm_message,yes_func,no_funct){
	frappe.confirm(
		confirm_message,
		function(){
			yes_func()	
		},
		function(){
			no_funct()
		}
	)
}

// function that determines the next section the user need to fill and unhides it
function unhide_next_section(frm,steps){
	// unhide_next_field(list_of_fields)
	var current_section_index = list_of_sections.indexOf(frm.doc.current_section)
	var next_user_section = list_of_sections[current_section_index + steps]
	var future_user_section = list_of_sections[current_section_index + steps+1]

	// set new previous,current and next sections
	if(future_user_section){
		// set direction value
		cur_frm.set_value("direction","Forward")
		// set the other navigation fields
		cur_frm.set_value("previous_section",frm.doc.current_section)
		cur_frm.set_value("current_section",next_user_section)
		cur_frm.set_value("next_section",future_user_section)
		// unhide the save and continue button
		frm.toggle_display("save_and_continue", true)
	}else{
		// check already on the last page
		if(list_of_sections.indexOf(frm.doc.current_section) == list_of_sections.length - 1){
			msgprint("You have reached the end of the form")
		}else{
			// set direction value
			cur_frm.set_value("direction","Forward")
			// set the other navigation fields
			cur_frm.set_value("previous_section",frm.doc.current_section)
			cur_frm.set_value("current_section",next_user_section)
			cur_frm.set_value("next_section","")
		}
	}
	// save the form
	cur_frm.save()
}

// function that determines the previous section the user filled and unhides it
function unhide_previous_section(frm){
	// unhide_next_field(list_of_fields)
	var current_section_index = list_of_sections.indexOf(frm.doc.current_section)
	var current_user_section = list_of_sections[current_section_index - 1]
	var previous_user_section = list_of_sections[current_section_index - 2]

	// set new previous,current and next sections
	if(previous_user_section){
		// set direction value
		cur_frm.set_value("direction","Backward")
		// set the other navigation fields
		cur_frm.set_value("previous_section",previous_user_section)
		cur_frm.set_value("current_section",current_user_section)
		cur_frm.set_value("next_section",frm.doc.current_section)
	}else{

		// check already on the last page
		if(list_of_sections.indexOf(frm.doc.current_section) == 0){
			msgprint("You Reached the Beginning of the Form")
		}else{
			// set direction value
			cur_frm.set_value("direction","Backward")
			// set the other navigation fields
			cur_frm.set_value("previous_section","")
			cur_frm.set_value("next_section",frm.doc.current_section)
			cur_frm.set_value("current_section",current_user_section)
		}
	}
	
	// save the form
	cur_frm.save()
}

// function that checks to ensure that all the required fields are
// given before an action is undertaken e.g verification, validation etc.
function check_required_fields(action){
	var return_value = true
	if(action == "verification"){
		// fields required before verification
		var verification_required_fields = [
			{"field_name":"* Project Start Date","field_value":cur_frm.doc.project_start_date},
			{"field_name":"* Project End Date","field_value":cur_frm.doc.project_end_date},
		]

		// loop through checking if all the required fields are given
		verification_required_fields.forEach(function(required_field){
			if(required_field["field_value"]){
				// do nothing
			}else{
				return_value = false
				frappe.msgprint(`The field ${required_field['field_name']} is required before verification`)
			}
		})
	}else if (action == 'validation'){

	}else if (action == 'approval'){

	}
	
	// return the return value
	return return_value
}

var check_required_fields_for_section_a = function(frm){
	if(frm.doc.yes_destination_organization){
		if(frm.doc.source_organization_table.length >0){
			// check if the user confirmed that they have added all the sources
			if(frm.doc.all_sources_filled_check){
				return {status:true,steps:1}
			}else{
				frappe.throw("Please confirm that you have added all the sources by clicking on the confirm check box below the sources table")
			}
			
		}else{
			frappe.throw("You need to add your source organization(s) in the table ,in order to continue")
		}
		
	}else if(frm.doc.no_destination_organization){
		frappe.set_route("List","Project Registration Form");
	}
	else{
		frappe.throw("You need to select either 'Yes' or 'No' in order to continue")
	}
}

var check_required_fields_for_source_suggestion = function(frm){
	// check if the user has selected either 'yes' or 'no'
	if(frm.doc.yes_found_source_match || frm.doc.no_no_source_match){
		return {status:true,steps:1}
	}else{
		frappe.throw("You Need to Select 'Yes' or 'No' In Order to Continue")
	}
}


var check_required_fields_for_section_b_section = function(frm){
	if(frm.doc.total_implementor_check || frm.doc.partial_implementor_check || frm.doc.not_implementor_check){
		return {status:true,steps:1}
	}else{
		frappe.throw("You Need To Select One of the Three Option in This Section, In Order to Continue")
	}
}

var check_required_fields_section_break_1 = function(frm){
	// check if organization has been given
	if(cur_frm.doc.select_your_organization && cur_frm.doc.organization_acronym){
		if(cur_frm.doc.sub_project_title_1){
			return {status:true,steps:1}
		}else{
			frappe.throw("Please select the subproject title in order to continue")
		}
	}else{
		frappe.throw("Please provide an organization and acronymn in order to continue")
	}
}

var check_required_fields_project_details_section = function(frm){
	if(frm.doc.project_start_date && frm.doc.project_end_date){
		return {status:true,steps:1}
	}else{
		frappe.throw("Please Enter the Start and End Date of The Project In Order to Continue")
	}
}

var check_required_fields_project_amount_section = function(frm){
	// check if the user has confirmed amounts
	if(frm.doc.confirm_total_amounts){
		return {status:true,steps:1}
	}else{
		frappe.throw("You Need to Confirm The Total Amounts In Order to Continue"+"<hr>"+"To Confirm Please Tick the Field 'The Total Amounts Above Correctly Reflect Those Entered in The Previous Section?'")
	}
}

var check_required_fields_destination_implementing_and_reporting_organization_section =function(frm){
	// check if the user has confirmed to have added all implementors
	if(frm.doc.all_implementors_added_check){
		return {status:true,steps:1}
	}else{
		frappe.throw("You need to confirm that all implementing organization have been given in order to continue"+"<hr>"+"to confirm please tick the field 'All the Implementing Organizations Have Been Given?'")
	}
}

var check_required_fields_implementing__partners_section_section =function(frm){
	return {status:true,steps:1}	
}

var check_required_fields_project_description_section = function(frm){
	if(frm.doc.description){
		return {status:true,steps:1}
	}else{
		frappe.throw("Please Add Project Description In Order to Continue")
	}
}

var check_required_pillar_alignment_section = function(frm){
	return {status:true,steps:1}
}

var check_required_fields_section_break_18 = function(frm){
	return {status:true,steps:1}
}


var check_required_fields_priority_programs_section_section = function(frm){
	return {status:true,steps:1}
}

var check_required_fields_relevant_mdas_section_section = function(frm){
	return {status:true,steps:1}
}

var check_required_fields_relevant_ndp_outcome_by_selected_priority_program_section = function(frm){
	return {status:true,steps:1}
}

var check_required_fields_project_location_section = function(frm){
	// check the user has added project locations
	return {status:true,steps:1}
	// if(frm.doc.project_location_table.length > 0){
	// 	return {status:true,steps:1}
	// }else{
	// 	frappe.throw("You Need to Add Project Locations In The Table In Order to Continue ")
	// }
}

var check_required_fields_ndp_aligment_section = function(frm){
	return {status:true,steps:1}
}

var check_required_fields_attachments_section_section = function(frm){
	var return_value = true

	var required_attachments = [
		{"field":"Project's Log Frame",value:frm.doc.attach_the_projects_log_frame},
		// add more required attachments fields below
	]
	// ensure that all the required attachements are adde
	required_attachments.forEach(function(attachment_field){
		if(attachment_field["value"]){
			// do nothing
		}else{
			return_value = false
			frappe.throw("Please Attach the "+attachment_field["field"]+" In Order to Continue")
		}
	})
	
	if(return_value){
		// mark the form as complete
		return {status:true,steps:1}
	}
}

var check_required_fields_verification_section_section = function(frm){
	return {status:true,steps:1}
}

var check_required_fields_verification_section_c_section = function(frm){
	// check if user clicked either Yes or No
	if(frm.doc.co_implementors_no){
		return {status:true,steps:1}
	}else if(frm.doc.co_implementors_yes){
		// check if the user has confirmed co-implementors
		if(frm.doc.confirm_coimplementors){
			return {status:true,steps:1}
		}else{
			frappe.throw("You Havent Confirmed Addition of Co-Implementing Partners \
			Tick on  'Confirm , I Have Added All The Co-Implementing Organizations I Know of' in Order to Continue")
		}
	}else{
		frappe.throw("You Need to Select Either 'Yes' or 'No' in Order to Continue")
	}
}

var check_required_pillar_alignment_section = function(frm){
	// check if the user has confirmed pillars
	if(cur_frm.doc.confirm_pillar_selection){
		// return true
		return {status:true,steps:1}
	}else{
		// unmark the confirm sectors check
		cur_frm.set_value("confirm_pillar_selection","")
		frappe.throw("You Need to Confirm That You Have Added All Pillars In Order To Continue <hr>\
		To Do This Tick the Field 'Confirm, I have Ticked Against All the Pillars the Project Contributes'")
	}
}

var check_required_sector_alignment_section_section = function(frm){
	// check if the user has confirmed pillars
	if(cur_frm.doc.confirm_sector_selection){
		// return true
		return {status:true,steps:1}
	}else{
		// unmark the confirm sectors check
		cur_frm.set_value("confirm_sector_selection","")
		frappe.throw("You Need to Confirm That You Have Added All Sectors In Order To Continue <hr>\
		To Do This Tick the Field 'Confirm, I Have Selected All the Sectors My Project Contributes To'")
	}
}

var check_required_priority_programs_section = function(frm){
	// check if the user has confirmed pillars
	if(cur_frm.doc.confirm_priority_programs && cur_frm.doc.priority_programs_table.length != 0){
		// return true
		return {status:true,steps:1}
	} else if(cur_frm.doc.no_ndp_check){
		return {status:true, steps:1}
	}
	else{
		// unmark the confirm sectors check
		cur_frm.set_value("confirm_priority_programs","")
		frappe.throw("You need to confirm that you have ticked against all NDP priority programs <hr>\
		To Do This Tick the Field 'Confirm, I Have Selected All The Priority Programs That My Project Contributes To' <hr>\
		If there are none check the field 'None of the above'")
	}
}

var check_required_mdas_section = function(frm){
	// check if the user has confirmed pillars
	if(cur_frm.doc.confirm_mdas){
		// return true
		return {status:true,steps:1}
	} else if(cur_frm.doc.no_gi_check){
		return {status:true, steps:1}
	}
	else{
		// unmark the confirm sectors check
		cur_frm.set_value("confirm_mdas","")
		frappe.throw("You need to confirm that you have ticked against all NDP priority programs <hr>\
		To do this tick the field 'Confirm, I Have Selected All the Relevant Lead MDAs In The Table Above' <hr>\
		If there are none check the field 'None of the above'")
	}
}

var check_required_relevant_ndp_outcome = function(frm){
	// check if the user has confirmed pillars
	if(cur_frm.doc.confirm_outcomes){
		// return true
		return {status:true,steps:1}
	} else if(cur_frm.doc.no_outcome_check){
		return {status:true, steps:1}
	}
	else{
		// unmark the confirm sectors check
		cur_frm.set_value("confirm_outcomes","")
		frappe.throw("You need to confirm that you have added all sectors in order to continue <hr>\
		to do this tick the field 'Confirm, I Have Ticked Against All The Relevant Outcome The Project Contributes To' <hr>\
		If there are none, check the field 'None of the above'")
	}
}





var check_required_fields_other_project_title_selection_options_section = function(frm){
	if(frm.doc.yes_in_select_field){
		// Check if the user selected a project title
		if(frm.doc.project_title_selection_field){
			// check if the user selected their organization
			if(frm.doc.select_your_organization){
				return {status:true,steps:1}
			}else{
				frappe.throw("You Need To Select Your Organization in the 'Organization' Field In Order to Continue")
			}	
			
		}else{
			// user did not selet a project title
			frappe.throw("If The Project Title Exist in The Select Field Above You Need to Select It in Order to Continue")
		}
		
	}else if(frm.doc.no_not_in_select_field_check){
		// check is user entered a new project title
		if(frm.doc.new_project_title){
			// check if the user entered a project code
			if(frm.doc.new_project_code){
				// user entered code
				// check if the user selected their organization
				if(frm.doc.select_your_organization){
					return {status:true,steps:1}
				}else{
					frappe.throw("You Need To Select Your Organization in the 'Organization' Field In Order to Continue")
				}
			}else{
				frappe.throw("You Need to Enter the New Project Code In Order to Continue")
			}
		}else{
			frappe.throw("Since Your Project Title is Not in Select Field Above,You Need to Enter a New Project Title Name in the `New Project Title` Field In Order to Continue")
		}
		
	}else{
		frappe.throw("You Need to Select Either 'Yes' or 'No' in Order to Continue")
	}
}

var check_required_fields_action_confirmation_section_section = function(frm){
	if (frm.doc.confirm_action_select){
		return {status:true,steps:1}
	}else{
		frappe.throw("You Need to Choose an Action in The 'Select an Action' In Order to Continue")
	}
}

var validation_function_per_section ={
	"section_break_1":check_required_fields_section_break_1,
	"section_a_section":check_required_fields_for_section_a,
	"source_suggestion":check_required_fields_for_source_suggestion,
	"section_b_section":check_required_fields_for_section_b_section,
	"project_details_section":check_required_fields_project_details_section,
	"project_amount_section":check_required_fields_project_amount_section,
	"destination_implementing_and_reporting_organization_section":check_required_fields_destination_implementing_and_reporting_organization_section,
	"implementing__partners_section_section":check_required_fields_implementing__partners_section_section,
	"project_description_section":check_required_fields_project_description_section,
	"pillar_alignment_section":check_required_pillar_alignment_section,
	"sector_alignment_section_section":check_required_sector_alignment_section_section,
	"priority_programs_section_section": check_required_priority_programs_section, //check_required_fields_priority_programs_section_section,
	"relevant_mdas_section_section": check_required_mdas_section, //check_required_fields_relevant_mdas_section_section,
	"relevant_ndp_outcome_by_selected_priority_program_section":check_required_relevant_ndp_outcome, //check_required_fields_relevant_ndp_outcome_by_selected_priority_program_section,
	"project_location_section":check_required_fields_project_location_section,
	"section_break_18":check_required_fields_section_break_18,
	"ndp_aligment_section":check_required_fields_ndp_aligment_section,
	"attachments_section_section":check_required_fields_attachments_section_section,
	"verification_section_section":check_required_fields_verification_section_section,
	"section_c_section":check_required_fields_verification_section_c_section,
	"other_project_title_selection_options_section":check_required_fields_other_project_title_selection_options_section,
	"action_confirmation_section_section":check_required_fields_action_confirmation_section_section
}

function make_fields_read_only(list_of_fields){
	if(frappe.user.has_role("Super Admin")){
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",0)	
		})
	}
	else if(frappe.user.has_role("Staff")){
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",1)	
		})
	}else{
		if(cur_frm.doc.verified == 1 || cur_frm.doc.viewing_information){
			// check if user has validation rights
			if(frappe.user.has_role("Super Admin")){
				list_of_fields.forEach(function(v,i){
					cur_frm.set_df_property(v,"read_only",0)	
				})
			}else{
				// make fields read only
				list_of_fields.forEach(function(v,i){
					cur_frm.set_df_property(v,"read_only",1)
				})
			}
		}else{
			list_of_fields.forEach(function(v,i){
				cur_frm.set_df_property(v,"read_only",0)	
			})
		}
	}	
}


/* end of the general functions section
// =================================================================================================
/* This section  contains functions that are triggered by the form action refresh or
reload to perform various action*/


frappe.ui.form.on('Project Renewal Form', {
	onload: function(frm) {

	// 	navigation_function(frm,list_of_sections)
	// 	filter_fields()
	// 	options_based_hide_unhide()

	// do not allow users to add rows in fields they are not supposed to add tables to 
	cur_frm.get_field("source_suggestion_table").grid.cannot_add_rows = true;
	cur_frm.get_field("pillar_alignment_table").grid.cannot_add_rows = true;
	cur_frm.get_field("sector_alignment_table").grid.cannot_add_rows = true;
	cur_frm.get_field("priority_programs_table").grid.cannot_add_rows = true;
	cur_frm.get_field("relevant_mda_table").grid.cannot_add_rows = true;
	cur_frm.get_field("relevant_outcomes__table").grid.cannot_add_rows = true;
	}
});

frappe.ui.form.on('Project Renewal Form', {
	refresh: function(frm) {
		// do not allow users to add rows in fields they are not supposed to add tables to 
		cur_frm.get_field("source_suggestion_table").grid.cannot_add_rows = true;
		cur_frm.get_field("pillar_alignment_table").grid.cannot_add_rows = true;
		cur_frm.get_field("sector_alignment_table").grid.cannot_add_rows = true;
		cur_frm.get_field("priority_programs_table").grid.cannot_add_rows = true;
		cur_frm.get_field("relevant_mda_table").grid.cannot_add_rows = true;
		cur_frm.get_field("relevant_outcomes__table").grid.cannot_add_rows = true;

		//Determines current section user should see
		navigation_function(frm,list_of_sections)
		// determine if user can see special section/fields
		options_based_hide_unhide(frm)
		// set query for fields
		filter_fields()
		// document is verefied
		make_fields_read_only(list_of_project_fields)

	}
});

frappe.ui.form.on('Project Renewal Form', {
	after_save: function(frm) {
		// check if the user is a basic user and the status is verified
		if(frappe.user.has_role("Organization Login")){
			if(cur_frm.doc.status == "Verified"){
				// redirect to profile page
				redirect_url('/profile_project')
			}
		}
	}
});


/*****************************************************************************************
Project Ownership Section ie. section_break_1 */

// function that is called when the your_organization field
frappe.ui.form.on("Project Renewal Form", "select_your_organization", function(frm){ 
	if(cur_frm.doc.select_your_organization){
		// call the organization
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Organization",
					filters: {
						name:cur_frm.doc.select_your_organization
					},
			fields:["*"]
			},
			callback: function(response) {
				// get the first organization
				cur_frm.set_value("organization_acronym",response.message[0]["acronym"])
			}	
		});
	}
})


// function called when the user selects a sub_project_title_1
frappe.ui.form.on("Project Renewal Form", "sub_project_title_1", function(frm){ 
	if(cur_frm.doc.sub_project_title_1){
		// get the master project
		// call the organization
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Project",
					filters: {
						name:cur_frm.doc.sub_project_title_1
					},
			fields:["*"]
			},
			callback: function(response) {
				var sub_project_doc =  response.message[0]
				cur_frm.set_value("project_title_selection_field",sub_project_doc["link_to_master_project"])
				cur_frm.set_value("new_project_code",sub_project_doc["project_code"])
				cur_frm.set_value("select_your_organization",sub_project_doc["your_organization"])
				cur_frm.set_value("organization_acronym",sub_project_doc["organization_acronym"])
				findPhase()
			}	
		});



	}else{
		// clear the project field
		cur_frm.set_value("project_title_selection_field")
	}
})

function findPhase(){
	frappe.call({
		method: 'frappe.client.get_list',
		args: 	{
			doctype: "Project Renewal Form",
			filters: {
			},
		fields: ['phase_title'],
		},
		callback: function(response){
			var projectNames = []
			for(var i = 0; i < response.message.length; i++){
				projectNames.push(response.message[i].phase_title);
			}
			var projectPhase = 2
			var projectPhaseTitle = cur_frm.doc.sub_project_title_1 + `-Phase ${projectPhase}`;

			// if(projectNames.includes(cur_frm.doc.sub_project_title_1)){
			if (cur_frm.doc.sub_project_title_1.includes('-Phase ')) {
				var projectPhase = cur_frm.doc.sub_project_title_1.split(' ')
				var phaseNumber = projectPhase[projectPhase.length - 1]
				phaseNumber++;
				projectPhase[projectPhase.length - 1 ] = phaseNumber;
				var projectTitle = cur_frm.doc.sub_project_title_1
				projectPhaseTitle = projectPhase.join(' ')
				
				// set phase title and values
				cur_frm.set_value('phase_title', projectPhaseTitle);
				cur_frm.set_value('phase', phaseNumber)

			} else {	
				// set phase title and values on the form	
				cur_frm.set_value('phase_title', projectPhaseTitle);
				cur_frm.set_value('phase', projectPhase)
			}						
		}
	})
}

// if a user clicks a project
frappe.ui.form.on("Project Renewal Form", "project_title_selection_field", function(frm){ 
	if(cur_frm.doc.project_title_selection_field){
		
		// call the organization
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Project",
					filters: {
						name:cur_frm.doc.project_title_selection_field
					},
			fields:["*"]
			},
			callback: function(response) {
				cur_frm.set_value("new_project_code",response.message[0]["project_code"])

				// refresh the page to hide unhide the page when the field is clicked
				cur_frm.refresh()
			}	
		});

	}else{
		// clear the project name and code fields
		cur_frm.set_value("project_title_selection_field","")
		cur_frm.set_value("new_project_code","")
	}
});

/*****************************************************************************************
Section A */

// function called when  the No field is clicked under Section A
frappe.ui.form.on("Project Renewal Form", "no_destination_organization", function(frm){ 
	// check if user selected the choice
	if(frm.doc.no_destination_organization){
		// unselect the no option if it already selected
		if(frm.doc.yes_destination_organization){
			cur_frm.set_value("yes_destination_organization","")
		}

		// throw and error telling the user they are not allowed to create a project
		var confirm_message = "Only Organizations That Received Funds From Organizations Outside Somaliland Can Create A Project" + "<hr>"+"Do You Still Want to Create the Project Anyway,Make an Inqury?"
		var yes_func = function (){
			// redirect to inquiry form
			frappe.set_route("List", "Inquiry");
		}
		
		var no_func = function(){
			cur_frm.set_value("delete_project",1)
			cur_frm.save()
			// redirect to Project Form List
			frappe.set_route("List","Project Form");
		}
		// confirm if the user wants to file an inquiry
		confirm_function(confirm_message,yes_func,no_func)
	}
})

// function called when  the yes field is clicked under Section A
frappe.ui.form.on("Project Renewal Form", "yes_destination_organization", function(frm){ 
	// check if user selected the choice
	if(frm.doc.yes_destination_organization){
		// unselect the no option if it already selected
		if(frm.doc.no_destination_organization){
			cur_frm.set_value("no_destination_organization","")
		}
	}
})

// function that checks the source organization type selected
frappe.ui.form.on("Source Organization", "source_organization_type", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn];
	
	// clear all the other relevant fields
	child.enter_organization_type == ""
	cur_frm.set_value("all_sources_filled_check",0)

	cur_frm.refresh_fields();
});

// function that a source organization type has been selected
frappe.ui.form.on("Source Organization", "enter_name_of_source_organization", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn];

});

// function that a source organization type has been selected
frappe.ui.form.on("Source Organization", "source_organization_name", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn];

	// if source name is given
	if(child.source_organization_name){

		// get the type from the source
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Organization",
					filters: {
						name:child.source_organization_name
					},
			fields:["*"]
			},
			callback: function(response) {
				
				// get the first organization
				var fetched_organization = response.message[0]
			
				// place the correct type in the row
				child.source_organization_type = fetched_organization["organization_type"]
				// unmark does not exist  as well as other relevant fields
				child.source_organization_does_not_exist_in_the_list_above = 0
				child.enter_organization_type =""
				cur_frm.set_value("all_sources_filled_check",0)
				cur_frm.refresh_fields();
			}	
		});
	}
});

// function that clears the source name if the user clicks on did not find it in select field
frappe.ui.form.on("Source Organization", "source_organization_does_not_exist_in_the_list_above", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn];
	
	if(child.source_organization_does_not_exist_in_the_list_above){
		// clear the source name
		child.source_organization_name =""
		// refresh fields in order to reflect the latest changes
		cur_frm.refresh_fields(); 
	}
});

// function that a source organization type has been selected
frappe.ui.form.on("Source Organization", "enter_name_of_source_organization", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn];

	// if(child.enter_name_of_source_organization){
	// 	// ensure a type is given first
	// 	if(child.source_organization_type || child.enter_organization_type ){
	// 		// allow	
	// 	}else{
	// 		child.enter_name_of_source_organization == ""
	// 		cur_frm.refresh_fields();
	// 		frappe.throw("Please give organization type in the field above in order to continue")
	// 	}
	// }else{
	// 	// do not do anything
	// }
});

// function that a source organization has been selected
frappe.ui.form.on("Source Organization", "funding_status_select_one", function(frm,cdt,cdn){ 
	// var child = locals[cdt][cdn];

	// if(child.funding_status_select_one){
	// 	// check if a souce is given
	// 	if(child.source_organization_name || child.enter_name_of_source_organization){
	// 		// allow
	// 	}else{
	// 		child.funding_status_select_one = ""
	// 		cur_frm.refresh_fields();
	// 		cur_frm.set_value("all_sources_filled_check",0)
	// 		frappe.throw("Give The Source Organization First")
	// 	}	
	// }

});

// function that is triggered when the amount field is clicked in the Source Organization Table
frappe.ui.form.on("Source Organization", "amount", function(frm, cdt, cdn) {
	var child = locals[cdt][cdn];

	// check if a funging status has been selected
	if(child.funding_status_select_one == "&lt; Select One&gt;"){
		cur_frm.set_value("all_sources_filled_check",0)
		frappe.throw("Please Select a Funding Status First")
	}else{
		var source_details = frm.doc.source_organization_table;
		var total_amount = 0
		var committed_amount = 0
		var pipeline_amount = 0
		// var operational_amount = 0
		for(var i in source_details) {
			total_amount += source_details[i].amount
			if(source_details[i].funding_status_select_one == "Pipeline"){
				pipeline_amount += source_details[i].amount
			}else if(source_details[i].funding_status_select_one == "Committed"){
				committed_amount += source_details[i].amount
			}
			// total_moisture_weight = total_moisture_weight + ((material_details[i].material_weight*material_details[i].moisture)/100)
		}

		var funding_gap = total_amount - committed_amount
	
		// set the different values
		frm.set_value("total_project_amount_in_usd",total_amount)
		// frm.set_value("total_project_amount",total_amount)
		frm.set_value("amount_commited",committed_amount)
		frm.set_value("amount_in_pipeline",pipeline_amount)
		frm.set_value('funding_gap_in_usd', funding_gap)
		// frm.set_value("operational_amount",operational_amount)
	}
});

frappe.ui.form.on('Source Organization', 'funds_outside_sl_yes', function(cur_frm, cdt, cdn){
	frappe.model.set_value(cdt, cdn, 'funds_outside_sl_no', 0)
})

frappe.ui.form.on('Source Organization', 'funds_outside_sl_no', function(cur_frm, cdt, cdn){
	frappe.model.set_value(cdt, cdn, 'funds_outside_sl_yes', 0)
})

// function called when  all_sources_filled_check  check is clicked
frappe.ui.form.on("Project Registration Form", "all_sources_filled_check", function(frm){
	
	if(frm.doc.all_sources_filled_check){
		// check if the table has any sources
		if(frm.doc.source_organization_table.length >0){
			var source_given = false
			if(frm.doc.all_sources_filled_check){
				var row_index = 0
				$.each(frm.doc.source_organization_table, function(i,v){
					row_index += 1
					// check if the user gave a source type
					if(v["source_organization_type"] || v['enter_funder_type'] || v['other_specify_name']){
						
						// check is the user gave a source organization
						if(v["source_organization_name"] || v["enter_name_of_source_organization"] || v['other_specify_name']){
							// check if the user gave funding status
							if(v["funding_status_select_one"]){
								// check if the user has given an amount
								if(v["amount"]){
									// check if the user has given an amount
									source_given = true
								}else{
									// do nothing
									cur_frm.set_value("all_sources_filled_check","")
									frappe.throw("You Have Not Added An Amount In Row "+row_index+"<hr> \
									Please Add an Amount In "+row_index+" In Order to Continue")
								}
							}else{
								// do nothing
								cur_frm.set_value("all_sources_filled_check","")
								frappe.throw("You Have Not Selected A Funding Status In Row "+row_index+"<hr> \
								Please Select A Funding Status In "+row_index+" In Order to Continue")
							}
						}else{
							// do nothing
							cur_frm.set_value("all_sources_filled_check","")
							frappe.throw("You have not added source organization in row "+row_index+"<hr> \
							Please Add You Source Name or 'enter name of source organization' in row "+row_index+" In Order to Continue")
						}
					}else{
						// do nothing
						cur_frm.set_value("all_sources_filled_check","")
						frappe.throw("You have not selected and organization type in row"+row_index+"<hr> \
						Please Add You Source Name or 'Enter Name of Source Organization' in Row "+row_index+" In Order to Continue")
					}
				});
			
				if(source_given){
					// do nothing
				}else{
					cur_frm.set_value("all_sources_filled_check","")
					frappe.throw("You have not added any source organization(s) to the table<hr> \
					please add 'source organization type' or 'enter the type of source organization' in order to continue")
				}
			}
		}else{
			cur_frm.set_value("all_sources_filled_check","")
			frappe.throw("You Have Not Added Any Source Organization(s) To the Table<hr> \
			Please Add Source Organization(s) to the Table In Order to Continue")
		}
	}
})

/*****************************************************************************************
SOURCE ORGANIZATION SUGGESTION SECTION */

// function called when  the 'Yes' found a match under source organization
frappe.ui.form.on("Project Renewal Form", "yes_found_source_match", function(frm,cdn,cdt){

	if(frm.doc.yes_found_source_match){
		// unset the no option
		cur_frm.set_value("no_no_source_match",0)

		var source_selected = false
		// check if any of the matches have been clicked
		$.each(frm.doc.source_suggestion_table,function(i,v){
			if(v["yes"]){
				source_selected = true
				
				// get given source in source table
				frappe.call({
					method: "frappe.client.get_list",
					args: 	{
							parent:"Project Renewal Form",
							doctype: "Source Organization",
							filters: {
								enter_name_of_source_organization:v.given__source_name,
								parent:cur_frm.doc.name
							},
					fields:["*"]
					},
					callback: function(response) {

						$.each(response.message,function(j,k){
							frappe.model.set_value("Source Organization",k.name, "source_organization_name",v["did_you_mean"])
							frappe.model.set_value("Source Organization",k.name, "source_organization_does_not_exist_in_the_list_above",0)
							frappe.model.set_value("Source Organization",k.name, "enter_name_of_source_organization","")
							frappe.model.set_value("Source Organization",k.name, "enter_acronym_of_source_organization_eg_un_for_united_nations","")
						});
					}	
				});
			}
		})

		if(source_selected){
			// allow user to continue
		}else{
			cur_frm.set_value("yes_found_source_match","")
			frappe.throw("Please tick 'Yes' against the match in the table in order to continue")
		}
	}
})

// function called when  the 'Yes' found a match under source organization
frappe.ui.form.on("Project Renewal Form", "no_no_source_match", function(frm,cdn,cdt){

	if(frm.doc.no_no_source_match){
		cur_frm.set_value("yes_found_source_match",0)
	}else{
		// continue
	}
		
})


// function called when  the confirmation button is clicked
frappe.ui.form.on("Project Renewal Form", "i_have_selected_the_correct_project_title_from_the_table_above", function(frm,cdn,cdt){
	if(cur_frm.doc.yes_project_is_in_list){

		// run the same script as the one for the 'yes_found_source_match' field
		if(frm.doc.yes_found_source_match){
			var source_selected = false
			// check if any of the matches have been clicked
			$.each(frm.doc.source_suggestion_table,function(i,v){
				if(v["yes"]){
					source_selected = true
					
					// get given source in source table
					frappe.call({
						method: "frappe.client.get_list",
						args: 	{
								parent:"Project Registration Form",
								doctype: "Source Organization",
								filters: {
									enter_name_of_source_organization:v.given__source_name,
									parent:cur_frm.doc.name
								},
						fields:["*"]
						},
						callback: function(response) {
	
							$.each(response.message,function(j,k){
								frappe.model.set_value("Source Organization",k.name, "source_organization_name",v["did_you_mean"])
								frappe.model.set_value("Source Organization",k.name, "source_organization_does_not_exist_in_the_list_above",0)
								frappe.model.set_value("Source Organization",k.name, "enter_name_of_source_organization","")
								frappe.model.set_value("Source Organization",k.name, "enter_acronym_of_source_organization_eg_un_for_united_nations","")
							});
						}	
					});
				}
			})
	
			if(source_selected){
				// allow user to continue
			}else{
				cur_frm.set_value("yes_found_source_match","")
				frappe.throw("Please tick 'Yes' against the match in the table in order to continue")
			}
		}

	}else if(cur_frm.doc.no_project_not_in_list){
		// check if the user provided a name and code for the code for the master project
		if(cur_frm.doc.new_project_title && cur_frm.doc.new_project_code ){
			// allow the user to continue
		}else{
			frappe.throw("Please provide the project name and project code in "+String(list_of_sections.indexOf("section_break_1") +1)+" in order to continue")
		}
	}
})


/*****************************************************************************************
Section B*/
// function called when  the Yes, total implementation check is clicked under Section B
frappe.ui.form.on("Project Renewal Form", "total_implementor_check", function(frm){ 
	// check if user selected total implementation
	if(frm.doc.total_implementor_check){

		// confirm that the user gave the organization name and acronym
		if(cur_frm.doc.select_your_organization && cur_frm.doc.organization_acronym){
			// allow the user to continue
		}else{
			cur_frm.set_value("total_implementor_check","")
			frappe.throw("Please provide the name and acronym of your organization in screen 1 in order to continue")
		}
			
		// unselect the other options
		if(frm.doc.partial_implementor_check){
			cur_frm.set_value("partial_implementor_check","")
		}
		if(frm.doc.not_implementor_check){
			cur_frm.set_value("not_implementor_check","")
		}
	}
})

// function called when  the Yes,partial implementation check is clicked under Section A
frappe.ui.form.on("Project Renewal Form", "partial_implementor_check", function(frm){ 
	// check if user selected total implementation
	if(frm.doc.partial_implementor_check){

		// confirm that the user gave the organization name and acronym
		if(cur_frm.doc.select_your_organization && cur_frm.doc.organization_acronym){
			// allow the user to continue
		}else{
			cur_frm.set_value("partial_implementor_check","")
			frappe.throw("Please provide the name and acronym of your organization in "+String(list_of_sections.indexOf("section_break_1") +1)+" in order to continue")
		}
		
		// unselect the other options
		if(frm.doc.total_implementor_check){
			cur_frm.set_value("total_implementor_check","")
		}
		if(frm.doc.not_implementor_check){
			cur_frm.set_value("not_implementor_check","")
		}
	}
})

// function called when  the No, only other partners are implementing check is clicked under Section A
frappe.ui.form.on("Project Renewal Form", "not_implementor_check", function(frm){ 
	// check if user selected total implementation
	if(frm.doc.not_implementor_check){

		// confirm that the user gave the organization name and acronym
		if(cur_frm.doc.select_your_organization && cur_frm.doc.organization_acronym){
			// allow the user to continue
		}else{
			cur_frm.set_value("not_implementor_check","")
			frappe.throw("Please provide the name and acronym of your organization in "+String(list_of_sections.indexOf("section_break_1") +1)+" in order to continue")
		}
		
		// unselect the other options
		if(frm.doc.total_implementor_check){
			cur_frm.set_value("total_implementor_check","")
		}
		if(frm.doc.partial_implementor_check){
			cur_frm.set_value("partial_implementor_check","")
		}
	}
})


/*****************************************************************************************
Implementing and Reporting Organization */


// function triggered when the Implementing Organization field is clicked
frappe.ui.form.on("Destination Implementing Reporting", "implementing_organization", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn];
	
});


// function triggered when the reporting_obligation field is clicked
frappe.ui.form.on("Destination Implementing Reporting", "reporting_obligation", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn];

	// check if implementing organization has been given
	if(child.reporting_obligation == 1){
		// check if the implementing_organization has been given
		if(child.implementing_organization){	
			// user selected an existing implementing organization
			child.reporting_organization = child.implementing_organization
			cur_frm.refresh_fields();
	
		}else if(child.enter_name_of_implementing_organization && child.enter_acronym){
			// the user selected and organization that does not exist yet
			// allow user to click
		}
		else{
			child.reporting_obligation = 0
			cur_frm.refresh_fields();
			frappe.throw("Please select an implementing organization first")
		}
	}
});


// function triggered when the 'Amount' field is clicked
frappe.ui.form.on("Destination Implementing Reporting", "amount", function(frm,cdt,cdn){ 
	var child = locals[cdt][cdn];
	if(child.amount){
		// check if the implementing_organization has been given
		if(child.implementing_organization){
			// allow th user to enter amount 
		}else if(child.enter_name_of_implementing_organization && child.enter_acronym){
			// allow th user to enter amount 
		}
		else{
			child.amount = ""
			cur_frm.refresh_fields();
			frappe.throw("You need to provide and implementing organization before filling the amount table")
		}
	}
});


// function triggered when the 'all_implementors_added_check' field is clicked
frappe.ui.form.on("Project Renewal Form", "all_implementors_added_check", function(frm,cdt,cdn){ 

	if(frm.doc.all_implementors_added_check){
		// first check that all the required fields for each row is given
		var all_fields_given = true
		var project_amount_patners = 0
		$.each(frm.doc.destination_organization_table,function(i,v){
			
			// if implementing_organization is given
			if(v["implementing_organization"] || v["enter_name_of_implementing_organization"]){
				// allow the user to continue
			}else{
				cur_frm.set_value("all_implementors_added_check","")
				frappe.throw("Please add an organization name in row "+String(i+1)+" in order to continue")
			}

			// if amount is given
			if(v["amount"]){
				// count the amount
				project_amount_patners += v["amount"]
			}else{
				cur_frm.set_value("all_implementors_added_check","")
				frappe.throw("Please add amount in row "+String(i+1)+" in order to continue")
			}

			
		})


		// check length of implementors table
		if(frm.doc.destination_organization_table.length > 0){
			if(frm.doc.total_implementor_check){
				// there should be only one implementing partner
				if(frm.doc.destination_organization_table.length == 1 ){
					// check if the implementing_organization has been given in screen one
					if(frm.doc.select_your_organization){
						// check if organization is part of it
						var organization_is_implementing = false
						$.each(frm.doc.destination_organization_table,function(i,v){
							if(v["implementing_organization"] == frm.doc.select_your_organization){
								organization_is_implementing = true
							}
						})

					}else{
						cur_frm.set_value("all_implementors_added_check","")
						frappe.throw("Please provide the name of your organization in "+String(list_of_sections.indexOf("section_break_1") +1)+" in order to continue")
					}
				

					if(organization_is_implementing){
						// allow user to continue
					}else{
						cur_frm.set_value("all_implementors_added_check","")
						frappe.throw("Your organization should be the only organization \
						in the table as it is fully  implementing the project")
					}
				}else {
					cur_frm.set_value("all_implementors_added_check","")
					frappe.throw("Your organization should be the only organization \
					in the table above as it is fully implementing the project")
				}

			}else if(frm.doc.partial_implementor_check){
				// there should be atleast two implementing parteners
				if(frm.doc.destination_organization_table.length > 1){
					// check if organization is part of it
					var organization_is_implementing = false
					$.each(frm.doc.destination_organization_table,function(i,v){
						if(v["implementing_organization"] == frm.doc.select_your_organization){
							organization_is_implementing = true
						}
					})

					if(organization_is_implementing){
						// allow user to continue
					}else{
						cur_frm.set_value("all_implementors_added_check","")
						frappe.throw("Since your organization is partially implementing the project \
						your organization needs to be among the implementing partners in the table")
					}
				}else{
					cur_frm.set_value("all_implementors_added_check","")
					frappe.throw("You need to add atleast two implementing \
					partners to the table where one is your organization (Partial implementation)")
				}

			}else if(frm.doc.not_implementor_check){
				// there should be atleast 1
				if(frm.doc.destination_organization_table.length > 0){
					// allow user to continue
				}else{
					cur_frm.set_value("all_implementors_added_check","")
					frappe.throw("You have not addes any implementing patner in the table")
				}
			}else{
				// the user has not made any implemention option
				cur_frm.set_value("all_implementors_added_check","")
				frappe.throw("Please select an implementation option in screen "+String(list_of_sections.indexOf("section_b_section") +1)+" in order to continue")
			}
		}else{
			cur_frm.set_value("all_implementors_added_check","")
			frappe.throw("You have not added any implementing organizations in table")
		}


		// ensure that the pateners project amount is not more than the total project amount
		if(project_amount_patners<= cur_frm.doc.total_project_amount_in_usd){
			// allow user to continue
		}else{
			cur_frm.set_value("all_implementors_added_check","")
			frappe.throw("The total amount given in in the table is more than the total project amount")
		}
	}
})


/*****************************************************************************************
implementing partners suggestion section */
// function called when  the 'found_implementors_match' field is clicked 
frappe.ui.form.on("Project Renewal Form", "found_implementors_match", function(frm){ 
	// check if the user has selected any match
	if(frm.doc.found_implementors_match){
		var implementor_selected = false
		// check if any of the matches have been clicked
		$.each(frm.doc.implementing_partners_suggestion_table,function(i,v){
			if(v["yes"]){
				implementor_selected = true
				
				// get given implementor in implementors table
				frappe.call({
					method: "frappe.client.get_list",
					args: 	{
							parent:"Project Renewal Form",
							doctype: "Destination Implementing Reporting",
							filters: {
								enter_name_of_implementing_organization:v.given_name,
								parent:cur_frm.doc.name
							},
					fields:["*"]
					},
					callback: function(response) {

						$.each(response.message,function(j,k){
							frappe.model.set_value("Destination Implementing Reporting",k.name, "implementing_organization",v["did_you_mean"])
							frappe.model.set_value("Destination Implementing Reporting",k.name, "implementing_organization_is_not_in_the_list_above",0)
							frappe.model.set_value("Destination Implementing Reporting",k.name, "enter_name_of_implementing_organization","")
							frappe.model.set_value("Destination Implementing Reporting",k.name, "enter_acronym","")
						});
					}	
				});
			}
		})

		if(implementor_selected){
			// allow user to continue
		}else{
			cur_frm.set_value("found_implementors_match","")
			frappe.throw("Please tick 'Yes' against the match in the table in order to continue")
		}
	}
})

/*****************************************************************************************
co implementing__partners_section_section suggestion section */

frappe.ui.form.on("CoImplementing Partners Table", "yes", function(frm,cdt,cdn){ 

	var child = locals[cdt][cdn];

	if(child.yes){
		// check if the users has selected an organization
		if(child.organization_name){
			// allow the user
			child.no = 0
			cur_frm.refresh_fields()
		}else{
			child.yes =  0
			cur_frm.refresh_fields()
			frappe.throw("You have not selected any organization in the organization name field")
		}
	}
})

frappe.ui.form.on("CoImplementing Partners Table", "no", function(frm,cdt,cdn){ 

	var child = locals[cdt][cdn];

	if(child.no){
		// check if the users has selected an organization
		if(child.organization_name){
			// allow the user
			child.yes = 0
			cur_frm.refresh_fields()
		}else{
			// allow the user to continue
		}
	}
})


/*****************************************************************************************
CO-IMPLEMENTING PARTNERS SECTION */

// function called when  the co_implementors_yes check is clicked under CO-IMPLEMENTING PARTNERS SECTION
frappe.ui.form.on("Project Renewal Form", "co_implementors_yes", function(frm){ 
	// check if user selected total implementation
	if(frm.doc.co_implementors_yes){
		// clear confirm box
		cur_frm.set_value("confirm_coimplementors","")

		// unselect the other option
		if(frm.doc.co_implementors_no){
			cur_frm.set_value("co_implementors_no","")
			cur_frm.refresh_fields()
		}
	}else{
		// clear confirm box
		cur_frm.set_value("confirm_coimplementors","")
	}
})

// function called when  the co_implementors_no check is clicked under CO-IMPLEMENTING PARTNERS SECTION
frappe.ui.form.on("Project Renewal Form", "co_implementors_no", function(frm){ 
	// check if user selected total implementation
	if(frm.doc.co_implementors_no){
		// clear confirm box
		cur_frm.set_value("confirm_coimplementors","")

		// unselect the other option
		if(frm.doc.co_implementors_yes){
			cur_frm.set_value("co_implementors_yes","")
		}

		// clear the co-implementors table
		cur_frm.clear_table("co_implementing_table");
		cur_frm.refresh_fields();
	}else{
		// clear confirm box
		cur_frm.set_value("confirm_coimplementors","")
	}
})

// function triggered when the 'confirm_coimplementors' under the Co-Implentors Table
frappe.ui.form.on("Project Renewal Form", "confirm_coimplementors", function(frm,cdt,cdn){ 
	
	if(frm.doc.confirm_coimplementors){
		// if user selected yes
		if(frm.doc.co_implementors_yes){
			// check if the user has added co-implementing_organizations
			if(frm.doc.co_implementing_table.length >0){
				// loop through checking that all required 
				var row_index = 0
				$.each(frm.doc.co_implementing_table, function(i,v){
					row_index +=1
					// check if the user gave organization_name or clicked no in list
					if(v["organization_name"] || v["no"]){
						// check if the user gave clicked not in the list
						if(v["no"]){
							// check that user gives new organization name
							if(v["co_implementors_field"] && v["co_implementor_acronym"]){
								// do nothing
							}else{
								cur_frm.set_value("confirm_coimplementors","")
								frappe.throw("Please provide the name and acronym of the organization in row "+row_index+" in order to continue")
							}
						}
					}else{
						cur_frm.set_value("confirm_coimplementors","")
						frappe.throw("Please add an organization name in row "+row_index+"<hr> \
						select an 'organization name' or tick 'No' in order to continue")
					}
				})
			}else{
				cur_frm.set_value("confirm_coimplementors","")
				frappe.throw("You have not added any co-implemeting organizations in the table above")
			}
		}
	}
});

/***********************************************************************************************
PROJECT TITLE SELECTION SECTION */

// function called when  the 'Yes' check is clicked under OTHER PROJECT TITLE SELECTION OPTIONS 
frappe.ui.form.on("Project Renewal Form", "yes_in_select_field", function(frm){ 
	if(frm.doc.yes_in_select_field){
		// check if th user has selected a project
		if(frm.doc.project_title_selection_field){
			// unselect the other option
			if(frm.doc.no_not_in_select_field_check){
				cur_frm.set_value("no_not_in_select_field_check","")
			}

			// remove the name on enter project title name if the user has already entered it
			if(frm.doc.new_project_title){
				cur_frm.set_value("new_project_title","")
			}
		}else{
			cur_frm.set_value("yes_in_select_field","")
			frappe.throw("Please Select The Project Title in the Select Field Above First, In Order to Continue")
		}	
	}
})

// function called when the 'No' check is clicked under OTHER PROJECT TITLE SELECTION OPTIONS
frappe.ui.form.on("Project Renewal Form", "no_not_in_select_field_check", function(frm){ 
	// check if user selected total implementation
	if(frm.doc.no_not_in_select_field_check){
		// check if the user has selected a project
		if(frm.doc.project_title_selection_field){
			cur_frm.set_value("no_not_in_select_field_check","")
			frappe.throw("You Want to Select 'No' But You Have Selected A Project in the 'Project Title' Field <hr>\
			If The Selected Title is Not Correct Remove it In Order to Select 'No'")
		}else{
			// unselect the other option
			if(frm.doc.yes_in_select_field){
				cur_frm.set_value("yes_in_select_field","")
			}
		}
	}
})



// function called when the 'i_have_selected_the_correct_project_title_from_the_table_above field'  clicked under 
frappe.ui.form.on("Project Renewal Form", "i_have_selected_the_correct_project_title_from_the_table_above", function(frm){ 

	// check if user is selecting
	if(frm.doc.i_have_selected_the_correct_project_title_from_the_table_above){
		// check if user confirmed selection
		if(frm.doc.yes_project_is_in_list){
			// check if the user clicked any project
			var suggestion_selected = false
			var number_of_selections = 0
			$.each(frm.doc.project_titles_table, function(i,v){
				if(v["yes"]){
					suggestion_selected = true
					number_of_selections += 1
				}
			})

			// check if  a suggestion was selected
			if(suggestion_selected){
				// check if user selected only one
				if(number_of_selections ==1){
					return {status:true,steps:1}
				}else{
					cur_frm.set_value("i_have_selected_the_correct_project_title_from_the_table_above",0)
					frappe.throw("You Can Only Select on Project in the Table Above Make the Necessary Corrections In Order to Continue")
				}
			}else{
				cur_frm.set_value("i_have_selected_the_correct_project_title_from_the_table_above",0)
				frappe.throw("You Need To Select A Project That Matches You Project In Order To Continue")
			}
		}else if(frm.doc.no_project_not_in_list){
			return {status:true,steps:1}
		}else{
			frappe.throw("You Need to Select Either 'Yes' or 'No' in Order to Continue")
		}	
	}
})

/*****************************************************************************************
PROJECT TITLES SUGGESTION SECTION */

// function called when  the co_implementors_no check is clicked under Section C
frappe.ui.form.on("Project Renewal Form", "yes_project_is_in_list", function(frm){ 
	// check if user selected total implementation
	if(frm.doc.yes_project_is_in_list){
		// unselect the other option
		if(frm.doc.no_project_not_in_list){
			cur_frm.set_value("no_project_not_in_list","")
		}

		// check if there are any suggestion in the table
		if(frm.doc.project_titles_table.length == 0 ){
			// do not allow the user to select yes
			if(frm.doc.yes_project_is_in_list == 1){
				cur_frm.set_value("yes_project_is_in_list",0)
				cur_frm.set_value("i_have_selected_the_correct_project_title_from_the_table_above","")
				frappe.throw("You cannot select 'Yes' when there are no project title suggestions in suggestion table")
			}
		}else{

			var found_project_selection = false
			var number_of_projects_selected = 0
			// set the correct project title in the project title field
			$.each(frm.doc.project_titles_table,function(i,v){
				if(v['yes']){
					found_project_selection = true
					number_of_projects_selected += 1
					// set the value to the project
					cur_frm.set_value("project_title_selection_field",v['project_title'])
				}
			})

			// check if the user selected any organization
			if(found_project_selection){
				// check if the user selected only one project
				if(number_of_projects_selected == 1){
					// allow the user to continue
				}else{
					cur_frm.set_value("yes_project_is_in_list",0)
					frappe.throw("You can only select one project that matches your project title")
				}

			}else{
				cur_frm.set_value("yes_project_is_in_list",0)
				frappe.throw("Please select a match for your project in the table above in order to continue")
			}
		}
	}

	// refresh the form in order to hide/unhide fields
	cur_frm.refresh()
})

// function called when the co_implementors_no check is clicked under Section C
frappe.ui.form.on("Project Renewal Form", "no_project_not_in_list", function(frm){ 
	// check if user selected total implementation
	if(frm.doc.no_project_not_in_list){
		// unselect the other option
		if(frm.doc.yes_project_is_in_list){
			cur_frm.set_value("yes_project_is_in_list",0)
			cur_frm.set_value("i_have_selected_the_correct_project_title_from_the_table_above","")
		}
	}

	// refresh the form in order to hide/unhide fields
	cur_frm.refresh()
})

/*****************************************************************************************
CORRECT TITLE SELECTION SECTION */



/*****************************************************************************************
GENERAL PROJECT DETAILS SECTION */

// function called when  the Project Title field clicked under GENERAL PROJECT DETAILS SECTION
frappe.ui.form.on("Project Renewal Form", "project_name", function(frm){
	// do nothing for now
})


/*****************************************************************************************
PILLAR ALIGNMENT SECTION */

// function called when  the confirm_pillars_check field clicked under PILLAR ALIGNMENT SECTION
frappe.ui.form.on("Project Renewal Form", "confirm_pillar_selection", function(frm){
	// check if the user has selected any pillar
	var pillar_selected = false

	if(frm.doc.confirm_pillar_selection){
		$.each(frm.doc.pillar_alignment_table, function(i,v){
			if(v["yes"]){
				pillar_selected = true
			}else{
				// do nothing
			}
		});
	
		if(pillar_selected){
			// allow user to continues
		}else{
			// check if there is any pillar in the table
			if(frm.doc.pillar_alignment_table.length == 0){
				cur_frm.set_value("confirm_pillar_selection","")
				frappe.throw("You Have Not Ticked Any Pillar In The Table"+"<hr>"+"Please Tick 'Yes' Against Pillars That The Organization Contributes to In Order to Continue")
			
			}else{
				cur_frm.set_value("confirm_pillar_selection","")
				frappe.throw("You Have Not Ticked Any Pillar In The Table"+"<hr>"+"Please Tick 'Yes' Against Pillars That The Organization Contributes to In Order to Continue")
			}	
		}
	}
})

frappe.ui.form.on("Project Renewal Form", "confirm_sector_selection", function(frm){

	var sector_selected = false

	if(frm.doc.confirm_sector_selection){
		$.each(frm.doc.sector_alignment_table, function(i,v){
			if(v["yes"]){
				sector_selected = true
			}else{
				// do nothing
			}
		});
	
		if(sector_selected){
			// allow user to continues
		}else{
			// check if there is any sector in the table
			if(frm.doc.sector_alignment_table.length == 0){
				cur_frm.set_value("confirm_sector_selection","")
				frappe.throw("You have not ticked any sector in the table"+"<hr>"+"please tick 'Yes' against pillars that the organization contributes to in order to continue")
			
			}else{
				cur_frm.set_value("confirm_sector_selection","")
				frappe.throw("You have not ticked any sector in the table"+"<hr>"+"please tick 'Yes' against pillars that the organization contributes to in order to continue")
			}	
		}
	}
})

frappe.ui.form.on("Project Renewal Form", "confirm_priority_programs", function(frm){
	var priority_selected = false

	if(frm.doc.confirm_priority_programs){
		cur_frm.set_value('no_ndp_check', '')
		$.each(frm.doc.priority_programs_table, function(i,v){
			if(v["yes"]){
				priority_selected = true
			}else{
				// do nothing
			}
		});
	
		if(priority_selected){
			// allow user to continues
		}else{
			// check if there is any pillar in the table
			if(frm.doc.priority_programs_table.length == 0){
				cur_frm.set_value('no_ndp_check', '')
				cur_frm.set_value("confirm_priority_programs","")
				frappe.throw("You have not ticked any priority program in the table"+"<hr>"+"please tick 'Yes' against a priority program that the organization contributes to in order to continue")
			
			}else{
				cur_frm.set_value('no_ndp_check', '')
				cur_frm.set_value("confirm_priority_programs","")
				frappe.throw("You have not ticked any priority program in the table"+"<hr>"+"please tick 'Yes' against a priority program that the organization contributes to in order to continue")
			}	
		}
	}
})

frappe.ui.form.on("Project Renewal Form", "confirm_mdas", function(frm){
	var mda_selected = false

	if(frm.doc.confirm_mdas){
		cur_frm.set_value('no_gi_check', '')
		$.each(frm.doc.relevant_mda_table, function(i,v){
			if(v["lead_mda"]){
				mda_selected = true
			}else{
				// do nothing
			}
		});
	
		if(mda_selected){
			// allow user to continues
		}else{
			// check if there is any pillar in the table
			if(frm.doc.relevant_mda_table.length == 0){
				cur_frm.set_value('no_gi_check', '')
				cur_frm.set_value("confirm_mdas","")
				frappe.throw("You have not ticked  against any government institution in the table"+"<hr>"+"please tick 'Yes' against a government institution under whose mandate your organization operates in order to continue")
			
			}else{
				cur_frm.set_value('no_gi_check', '')
				cur_frm.set_value("confirm_mdas","")
				frappe.throw("You have not ticked  against any government institution in the table"+"<hr>"+"please tick 'Yes' against a government institution under whose mandate your organization operates in order to continue")
			}	
		}
	}
})


frappe.ui.form.on("Project Renewal Form", "confirm_outcomes", function(frm){
	var outcome_selected = false

	if(frm.doc.confirm_outcomes){
		cur_frm.set_value('no_outcome_check', '')
		$.each(frm.doc.relevant_outcomes__table, function(i,v){
			if(v["yes"]){
				outcome_selected = true
			}else{
				// do nothing
			}
		});
	
		if(outcome_selected){
			// allow user to continues
		}else{
			// check if there is any pillar in the table
			if(frm.doc.relevant_outcomes__table.length == 0){
				cur_frm.set_value('no_outcome_check', '')
				cur_frm.set_value("confirm_outcomes","")
				frappe.throw("You have not ticked any outcomes in the table"+"<hr>"+"please tick 'Yes' against an outcome your organization contributes to in order to continue")
			
			}else{
				cur_frm.set_value('no_outcome_check', '')
				cur_frm.set_value("confirm_outcomes","")
				frappe.throw("You have not ticked any outcomes in the table"+"<hr>"+"please tick 'Yes' against an outcome your organization contributes to in order to continue")
			}	
		}
	}
})


/*****************************************************************************************
SECTOR ALIGNMENT SECTION */

// function called when  the confirm_pillars_check field clicked under PILLAR ALIGNMENT SECTION
frappe.ui.form.on("Project Renewal Form", "confirm_sector_selection", function(frm){
	// check if the user has selected any pillar
	var sector_selected = false

	if(frm.doc.confirm_sector_selection){
		$.each(frm.doc.sector_alignment_table, function(i,v){
			if(v["yes"]){
				sector_selected = true
			}else{
				// do nothing
			}
		});
	
		if(sector_selected){
			// allow user to continues
		}else{
			// check if there is any pillar in the table
			if(frm.doc.sector_alignment_table.length == 0){
				// do nothing
			}else{
				cur_frm.set_value("confirm_sector_selection","")
				frappe.throw("You Have Not Ticked Any Sector In The Table"+"<hr>"+"Please Tick 'Yes' Against Sector That The Organization Contributes to In Order to Continue")
			}	
		}
	}
})


/*****************************************************************************************
PROJECT LOCATION SECTION */

// frappe.ui.form.on("Project Location", "region", function (doc, grid_row) {
//     cur_frm.set_query("score", function () {
//         return {
//             "filters": {
//                 "kind": "Child"
//             }
//         };
//     });
// })



/*****************************************************************************************
PROJECT DESCRIPTION SECTION */

// add trigger code for the project description section here

/*******************************************************************************************
ATTACHMENTS SECTION */

/*******************************************************************************************
VERIFICATION/VALIDATION/APPROVAL SECTION */

frappe.ui.form.on("Project Renewal Form", "view_all_information", function(frm){
	cur_frm.set_value("viewing_information",1)
	cur_frm.save()
})

frappe.ui.form.on("Project Renewal Form", "edit_information", function(frm){
	// check user is allowed to edit
	if(cur_frm.doc.verified){
		frappe.throw("You cannot edit a form that is already verified \
		,contact the admin for assistance")
	}else{
		// make the forms readble for user
		cur_frm.set_value("viewing_information",0)
		cur_frm.set_value("current_section",list_of_sections[0])
		cur_frm.save()
	}
})

frappe.ui.form.on("Project Renewal Form", "mark_information_as_checked", function(frm){
	check_privillages(frm,"checked")
})

frappe.ui.form.on("Project Renewal Form", "verify", function(frm){
	// check if the user has marked the form as checked
	if(cur_frm.doc.information_checked){
		// check if required fields are filled
		var required_fields_status = check_required_fields('verification')

		if(required_fields_status){
			// confirm if the user want to validate information
			frappe.confirm(
				"Please ensure that all the information you have given is correct before verifying <hr> \
				if you are sure you are ready to verify click 'Yes' otherwise click 'No'",
				function(){
					// validate the form
					check_privillages(frm,"verify")
				},
				function(){
					// do nothing since the user is not sure
				}
			)
		}else{
			cur_frm.set_value('status','Pending Verification')
			cur_frm.set_value('verified',0)
		}

	}else{
		frappe.throw("You need to mark the information as checked before verifying them")
	}
})

frappe.ui.form.on("Project Renewal Form", "validate_button", function(frm){
	check_privillages(frm,"validate")
})

frappe.ui.form.on("Project Renewal Form", "approve_project", function(frm){
	check_privillages(frm,"approve")
})


/*****************************************************************************************
Navigation Section */

// function called when  the Previous Button field clicked under Navigation Section
frappe.ui.form.on("Project Renewal Form", "previous_section_button", function(frm){
	// go to the next section	
	unhide_previous_section(frm)
})

// function called when  the Save and Continue field clicked under Navigation Section
frappe.ui.form.on("Project Renewal Form", "save_and_continue", function(frm){ 
	// go to the next section
	// ensure to requirements for the section are complete
	var section_function = validation_function_per_section[frm.doc.current_section] 
	if(section_function(frm)["status"]){
		unhide_next_section(frm,section_function(frm)["steps"])
	}	
	
})

// function called when  the Project Name field is clicked
frappe.ui.form.on("Project Renewal Form", "project_name", function(frm){ 
	if(frm.doc.project_name){
		cur_frm.set_value("selected_project_name",frm.doc.project_name)
	}else{
		// did user click tha the name is not available in the list above
		if(frm.doc.project_name_is_not_in_the_list_above == 1){
			// check if the entered name is available
			if(frm.doc.enter_new_project_name){
				cur_frm.set_value("selected_project_name",frm.doc.enter_new_project_name)	
			}else{
				cur_frm.set_value("selected_project_name","")
			}
		}else{
			cur_frm.set_value("selected_project_name","")
		}
	}
});

// function called when  the Project Name field is clicked
frappe.ui.form.on("Project Renewal Form", "enter_new_project_name", function(frm){ 
	if(frm.doc.enter_new_project_name){
		cur_frm.set_value("selected_project_name",frm.doc.enter_new_project_name)
	}	
});

// function called when  the project_name_is_not_in_the_list_above is clicked
frappe.ui.form.on("Project Renewal Form", "project_name_is_not_in_the_list_above", function(frm){ 
	if(frm.doc.project_name_is_not_in_the_list_above == 1){
		// hide the project_name field
		frm.toggle_display("project_name", false)

		// add the available name if entered name exists
		if(frm.doc.enter_new_project_name){
			cur_frm.set_value("selected_project_name",frm.doc.enter_new_project_name)
		}
		
	}else if(frm.doc.project_name_is_not_in_the_list_above == 0){
		frm.toggle_display("project_name", true)
		cur_frm.set_value("selected_project_name",frm.doc.project_name)
	}
});


// function called when  the project_name_is_not_in_the_list_above is clicked
frappe.ui.form.on("Project Renewal Form", "your_organization", function(frm){
	// check if the organization is approved
	if(frm.doc.your_organization){
		frappe.call({
			method: "frappe.client.get_list",
			args: 	{
					doctype: "Organization",
					filters: {
						name:frm.doc.your_organization
					},
			fields:["*"]
			},
			callback: function(response) {	
				// get the first organization
				var fetched_organization = response.message[0]
				cur_frm.set_value("organization_acronym",fetched_organization.acronym)
			}	
		});
	}else{
		// no organization was selected
		cur_frm.set_value("organization_acronym","")
	}
});


// function called when  the 'Go to screen' button is clicked
frappe.ui.form.on("Project Renewal Form", "go_to_page", function(frm){
	// call the go to page function
	goToPage()
});
