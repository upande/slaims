# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import datetime

class UmbrellaorConsortiumOrganizationRenewalForm(Document):
	def validate(self):
		# fun the validation Function
		validation_function(self)
		
		# add correct renewal year
		add_correct_renewal_year(self)

		# load renewal data from organization
		load_renewal_data_from_organization(self)
	
		# add pillars to the pillars table
		add_pillars_to_table(self)

		# add sectors to the sectors table
		add_sectors_to_table(self)

		# add themes to themes table
		add_themes_to_table(self)

		# upgrade the status for imported documents
		if self.imported:
			upgrade_status_for_imported_documents(self)

	def on_update(self):
		# update the information to main doctype
		map_data_to_organization(self)

def validation_function(self):
	'''
	Function that checks that all the required fields 
	are given
	'''
	# verification fields
	list_of_verification_fields = [
		{"field":self.full_name_of_the_organization,"name":"Name of Organization"},
		{"field":self.acronym,"name":"Acronym"},
		{"field":self.status,"name":"Status"},

		# add more validation required fields below
		
	]

	# validation fields
	list_of_validation_fields = [
		{"field":self.full_name_of_the_organization,"name":"Name of Organization"},
		{"field":self.acronym,"name":"Acronym"},
		{"field":self.status,"name":"Status"}

		# add more validation required fields below
	]

	# check approval fields
	list_of_approval_fields = [
		# add fields required for approval below
	]

	if self.status == "Verified":
		# loop through list of saving fields
		for field in list_of_verification_fields:
			if field["field"]:
				pass
			else:
				# value for the field is not given throw an errror
				frappe.throw("The Field '{}' is Required Before Verification".format(field["name"]))

	if self.status == "Validated":
		# loop through list of saving fields
		for field in list_of_validation_fields:
			if field["field"]:
				pass
			else:
				# value for the field is not given throw an errror
				frappe.throw("The Field '{}' is Required Before Verification".format(field["name"]))

	if self.status == "Approved":
		# loop through list of saving fields
		for field in list_of_approval_fields:
			if field["field"]:
				pass
			else:
				# value for the field is not given throw an errror
				frappe.throw("The Field '{}' is Required Before Verification".format(field["name"]))

	# check if previous financial years was closed
	linked_organization_doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
	if linked_organization_doc.financial_year_not_closed:
		frappe.throw("The previous Financial year for your organization was not closed <hr> \
			Please fill all the required forms for the year {} and contant Admin for assisistance\
			".format(linked_organization_doc.current_registration_year))

def add_correct_renewal_year(self):
	'''
	Function that gets the current organization 
	year and hence the next year i.e renewal year
	using years ranks
	'''
	# check year exist
	if self.year_of_registration: #in this case the year of renewal
		pass
	else:
		try:
			# get the current registration year of the organization
			organization_doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
			# its current year rank
			current_year_doc = frappe.get_doc("Year",str(organization_doc.current_registration_year))
			renewal_year_rank = current_year_doc.rank + 1
			# get a list of years with a specific rank
			list_of_years_matching_rank = frappe.get_list("Year",
				fields=["name"],
				filters = {
					"rank":renewal_year_rank
			})
			# place the renewal year in the form
			self.year_of_registration = list_of_years_matching_rank[0].name
		except:
			# if an error occurs trying placing the global year as the renewal year
			try:
				global_setting_doc = frappe.get_single("Global Settings")
				self.year_of_registration = global_setting_doc.current_financial_year
			except:
				frappe.throw("A global financial year not defined please contact Admin for assistance")

def add_pillars_to_table(self):
	'''
	Function that collects all the pillars and places them 
	in the Pillars table
	'''
	if self.current_section == "pillar_alignment_section":
		# hold all marked pillars
		pillars_holder = []
		for item in self.pillar_alignment_table:
			if item.yes:
				pillars_holder.append(item.pillar)

		# firt clear the pillars table
		self.pillar_alignment_table = []

		# get all the pillars
		list_of_pillars = frappe.get_list("NDP",
			fields=["name","description_somali"],
			filters = {
				"type":"Pillar"
		})

		for pillar in list_of_pillars:
			# check if that pillar is marked
			if pillar["name"] in pillars_holder:
				if pillar["description_somali"]:
					self.append("pillar_alignment_table", {
						"pillar_local":pillar["description_somali"],
						"pillar":pillar["name"],
						"yes":1
					})	
				else:
					self.append("pillar_alignment_table", {
						"pillar_local":pillar["name"],
						"pillar":pillar["name"],
						"yes":1
					})
			else:
				if pillar["description_somali"]:
					self.append("pillar_alignment_table", {
						"pillar_local":pillar["description_somali"],
						"pillar":pillar["name"]
					})	
				else:
					self.append("pillar_alignment_table", {
						"pillar_local":pillar["name"],
						"pillar":pillar["name"]
					})



def add_sectors_to_table(self):
	'''
	Function that collects Sectors Related to Pillars Selected
	in the pillars section and places them in the Sectors table
	'''
	if self.current_section == "section_alignment_section":
		# hold all marked sectors
		sectors_holder = []
		for item in self.section_alignment_table:
			if item.yes:
				sectors_holder.append(item.sector)

		# first clear the sector table
		self.section_alignment_table = []

		# get the relavant sectors
		list_of_relevant_sectors = []

		# get sectors for each selected pillar
		for pillar in self.pillar_alignment_table:
			# get all the sectors for each ticked pillar
			if pillar.yes:
				sectors_for_pillar = frappe.get_list("NDP",
					fields=["name","parent_ndp","description_somali"],
					filters = {
						"type":"Sector",
						"parent_ndp":pillar.pillar
				})

				# append the sectors to the list of pillars
				list_of_relevant_sectors += sectors_for_pillar

		# append the sectors to the sectors table
		for sector in list_of_relevant_sectors:
			# check if that sector is marked
			if sector["name"] in sectors_holder:
				# check if lcoal description exists
				if sector["description_somali"]:
					self.append("section_alignment_table", {
						"sector_local":sector["description_somali"],
						"pillar":sector["parent_ndp"],
						"sector":sector["name"],
						"yes":1
					})
				else:
					self.append("section_alignment_table", {
						"sector_local":sector["name"],
						"pillar":sector["parent_ndp"],
						"sector":sector["name"],
						"yes":1
					})
			else:
				# check if lcoal description exists
				if sector["description_somali"]:
					self.append("section_alignment_table", {
						"sector_local":sector["description_somali"],
						"pillar":sector["parent_ndp"],
						"sector":sector["name"]
					})
				else:
					self.append("section_alignment_table", {
						"sector_local":sector["name"],
						"pillar":sector["parent_ndp"],
						"sector":sector["name"]
					})


def add_themes_to_table(self):
	'''
	Function that collects Themes Related to Sectors Selected
	in the Sectors section and places them in the Themes table
	'''
	if self.current_section == "themes_alignment_section":
		# hold marked themes
		themes_holder = []
		for item in self.themes_alignment_table:
			if item.yes:
				themes_holder.append(item.theme)

		# first clear the pillars table
		self.themes_alignment_table = []
		# holds a list of relevant themes
		list_of_relevant_themes = []

		# get sectors for each selected pillar
		for sector in self.section_alignment_table:
			# get all the themes for each ticked sector
			if sector.yes:
				themes_for_sector = frappe.get_list("NDP",
					fields=["name","parent_ndp","description_somali"],
					filters = {
						"type":"Theme",
						"parent_ndp":sector.sector
				})

				# append the sectors to the list of sectors
				list_of_relevant_themes += themes_for_sector

		# append the themes to the themes table
		for theme in list_of_relevant_themes:
			# check if that sector is marked
			if theme["name"] in themes_holder:
				if theme["description_somali"]:
					self.append("themes_alignment_table", {
						"sector":theme["parent_ndp"],
						"theme_local":theme["description_somali"],
						"theme":theme["name"],
						"yes":1
					})
				else:
					self.append("themes_alignment_table", {
						"sector":theme["parent_ndp"],
						"theme_local":theme["name"],
						"theme":theme["name"],
						"yes":1
					})
			else:
				if theme["description_somali"]:
					self.append("themes_alignment_table", {
						"sector":theme["parent_ndp"],
						"theme_local":theme["description_somali"],
						"theme":theme["name"]
					})
				else:
					self.append("themes_alignment_table", {
						"sector":theme["parent_ndp"],
						"theme_local":theme["name"],
						"theme":theme["name"]
					})

def check_if_organization_exists(self):
	'''
	Checks if an organization exists else create one
	'''
	# create an organization
	doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
	doc.name = self.full_name_of_the_organization
	doc.status = self.status
	doc.organization_type = "Umbrella or Consortium Organization"
	doc.full_name_of_the_organization = self.full_name_of_the_organization
	doc.country_of_origin = self.country_of_origin

	if doc:
		doc.save(ignore_permissions = True)
	else:
		doc.insert(ignore_permissions = True)

def upgrade_status_for_imported_documents(self):
	if self.status == "Admin Verified":
		# progress the form to Verified for imported data
		self.status = "Verified"

	elif self.status == "Verified":
		# progress the form to Validated for imported data
		self.status = "Validated"

	elif self.status == "Validated":
		# progress the form to Approved for imported data
		self.status = "Approved"

def map_data_to_organization(self):
	'''
	Function that Updates the details of the organization
	to the organization form once the form is approved
	'''
	# check if the organization has been verified
	if self.status == "Verified" or  self.status == "Admin Approved":
		check_if_organization_exists(self)

	if self.status == "Validated":
		# get related organization
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
		doc.status == "Validated"
		doc.save()

	# if approved update details to linked organization
	if(self.status == "Approved" or self.status == "Imported"):
		# get related organization
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
		
		# ORGANIZATION TYPE SECTION
		doc.status = "Approved"

		# GENERAL ORGANIZATION DETAILS SECTION
		doc.full_name_of_the_organization = self.full_name_of_the_organization
		doc.acronym = self.acronym
		doc.country_of_origin = self.country_of_origin
		doc.year_of_first_registration = self.year_of_first_registration
		doc.organization_registration_number = self.organization_registration_number

		# ADDRESS OF MAIN OFFICE SECTION
		doc.construction = self.construction
		doc.region = self.state_local_1
		doc.district = self.district_umbrella
		doc.street_local_1 = self.country_code_umbrealla
		doc.country_code_umbrealla = self.country_code_umbrealla
		doc.telephone_umbrella = self.telephone_umbrella
		doc.complete_organization_address =  self.complete_organization_address
		doc.email_umbrella =  self.email_umbrella

		# Name of person registering section
		doc.name_of_person_registering =  self.name_of_person_registering
		doc.code_person_registering =  self.code_person_registering
		doc.tel_person_registering =  self.tel_person_registering_1
		doc.complete_tel_registering =  self.complete_tel_registering
		doc.email_person_registering =  self.email_person_registering

		# Laws Section
		doc.yes_laws =  self.yes_laws
		doc.no_laws =  self.no_laws

		# operation level section
		doc.national_tax =  self.national_tax
		doc.regional_tax =  self.regional_tax
		doc.district_level =  self.district_level
		
		# organization description section
		doc.orgnization_description = self.brief_organization_description
		
		# Pillar Alignment Section
		doc.pillar_alignment_table = []
		for pillar in self.pillar_alignment_table:
			if pillar.yes:
				doc.append("pillar_alignment_table", {
					"pillar": pillar.pillar,
					"yes": pillar.yes
				})

		# Sectors Alignment Section
		doc.section_alignment_table = []
		for sector in self.section_alignment_table:
			if sector.yes:
				doc.append("section_alignment_table", {
					"pillar": sector.pillar,
					"sector":sector.sector,
					"yes": sector.yes
				})

		# Themes Alignment Table
		doc.themes_alignment_table = []
		for theme in self.themes_alignment_table:
			if theme.yes:
				doc.append("themes_alignment_table", {
					"sector":theme.sector,
					"theme": theme.theme,
					"yes": theme.yes
				})

		# Geographical area(s) of operation
		doc.marodijeh_check = self.marodijeh_check
		doc.hargeisa = self.hargeisa
		doc.gabiley = self.gabiley
		doc.baligubadle = self.baligubadle
		doc.salahlay = self.salahlay

		doc.sanag_check = self.sanag_check
		doc.erigavo = self.erigavo
		doc.badhan = self.badhan
		doc.las_qoray = self.las_qoray
		doc.ela_fweyn = self.ela_fweyn
		doc.dhahar = self.dhahar
		doc.gar_adag = self.gar_adag
		
		doc.sool_check = self.sool_check
		doc.las_anod = self.las_anod
		doc.hudun = self.hudun
		doc.taleh = self.taleh
		doc.aynabo = self.aynabo

		doc.togdheer_check = self.togdheer_check
		doc.burao = self.burao
		doc.odwayne = self.odwayne
		doc.buhodle = self.buhodle

		doc.awdal_check = self.awdal_check
		doc.borama = self.borama
		doc.zeila = self.zeila
		doc.baki = self.baki
		doc.lughaya = self.lughaya

		doc.sahil_check = self.sahil_check
		doc.berbera = self.berbera
		doc.sheekh = self.sheekh

		doc.hargeisa_region = self.hargeisa_region

		# Consortium members
		doc.consortium_members = []
		for member in self.consortium_members:
			doc.append("consortium_members", {
				"name_of_organization":member.name_of_organization,
				"registration_no": member.registration_no,
				"contact_person": member.contact_person,
				"country_code": member.country_code,
				"telephone": member.telephone,
				"complete_telephone": member.complete_telephone,
				"email": member.email
			})

		# International communication section
		doc.international_communication = []
		for communication in self.international_communication:
			doc.append("international_communication", {
				"member_name":communication.member_name,
				"selected_assignment": communication.selected_assignment,
				"level_education": communication.level_education,
				"telephone": communication.telephone,
				"email": communication.email
			})

		# more information section
		doc.foundation_date = self.foundation_date
		doc.foundation_venue = self.foundation_venue
		doc.yes_conved_by_mopnd = self.yes_conved_by_mopnd

		doc.convening_members_of_mopnd = []
		for member in self.convening_members_of_mopnd:
			doc.append("convening_members_of_mopnd", {
				"magaca":member.magaca,
			})
		
		doc.no_conved_by_mopnd = self.no_conved_by_mopnd
		doc.yes_consultative_committee = self.yes_consultative_committee

		doc.consultative_committee = []
		for member in self.consultative_committee:
			doc.append("consultative_committee", {
				"member_name":member.member_name,
			})

		doc.not_consultative_committee = self.not_consultative_committee

		'''
		# attachment section
		doc.attachments_table = []
		all_attachments_table = 'attachments_table'

		# attach organization logo
		doc.append(all_attachments_table, {
				'attachment_name': "Logo",
				'attachment':self.attach_logo_local_1,
				'field_name':'attach_logo_local_1'
			}),

		# attach by laws
		for association_rule in self.association_rules:
			doc.append(all_attachments_table, {
				'attachment_name': association_rule.attachment_name,
				'attachment': association_rule.attachment,
				'field_name':'association_rules'
			}), 

		for director in self.board_of_directors:
			doc.append(all_attachments_table, {
				'attachment_name': director.attachment_name,
				'attachment': director.attachment,
				'field_name':'board_of_directors'
			}), 

		for member in self.proof_of_members:
			doc.append(all_attachments_table, {
				'attachment_name': member.attachment_name,
				'attachment': member.attachment,
				'field_name':'proof_of_members'
			}), 

		for cv in self.members_cv:
			doc.append(all_attachments_table, {
				'attachment_name': cv.attachment_name,
				'attachment': cv.attachment,
				'field_name':'members_cv'
			}), 

		for written_request in self.written_request:
			doc.append(all_attachments_table, {
				'attachment_name': written_request.attachment_name,
				'attachment': written_request.attachment,
				'field_name':'written_request'
			})

		for prop in self.property_list:
			doc.append(all_attachments_table, {
				'attachment_name': prop.attachment_name,
				'attachment': prop.attachment,
				'field_name':'property_list'
			})

		for plan in self.next_year_plan:
			doc.append(all_attachments_table, {
				'attachment_name': plan.attachment_name,
				'attachment': plan.attachment,
				'field_name':'next_year_plan'
			})

		
		for performance in self.last_year_performance:
			doc.append(all_attachments_table, {
				'attachment_name': performance.attachment_name,
				'attachment': performance.attachment,
				'field_name':'last_year_performance'
			})

		# attach written request
		for organization_1000 in self.organization_1000:
			doc.append(all_attachments_table, {
				'attachment_name': organization_1000.attachment_name,
				'attachment': organization_1000.attachment,
				'field_name':'organization_1000'
			})

		for other in self.attach_other:
			doc.append(all_attachments_table, {
				'attachment_name': other.attachment_name,
				'attachment': other.attachment,
				'field_name':'attach_other'
			})
		'''
		# add the form as a child table to organization
		doc.append("attached_organization_forms_table", {
			"name_of_form":self.name,
			"year_of_form":self.year_of_registration
		})

		# save the added changes
		doc.save()

def load_renewal_data_from_organization(self):
	if self.data_pulled_from_main:
		pass
	else:
		# get related organization
		doc = frappe.get_doc("Organization", self.full_name_of_the_organization)
		
		# GENERAL ORGANIZATION DETAILS SECTION
		self.full_name_of_the_organization = doc.full_name_of_the_organization
		self.acronym = doc.acronym
		self.country_of_origin = doc.country_of_origin
		self.year_of_first_registration = doc.year_of_first_registration 
		self.organization_registration_number = doc.organization_registration_number

		# ADDRESS OF MAIN OFFICE SECTION
		self.construction = doc.construction
		self.state_local_1 = doc.region
		self.district_umbrella = doc.district
		self.street_local_1 = doc.street_local_1
		self.country_code_umbrealla = doc.country_code_umbrealla
		self.telephone_umbrella = doc.telephone_umbrella
		self.complete_organization_address =  doc.complete_organization_address
		self.email_umbrella =  doc.email_umbrella

		# Name of person registering section
		self.name_of_person_registering =  doc.name_of_person_registering
		self.code_person_registering =  doc.code_person_registering
		self.tel_person_registering_1 =  doc.telephone_umbrella
		self.complete_tel_registering =  doc.complete_tel_registering
		self.email_person_registering =  doc.email_person_registering

		# Laws Section
		self.yes_laws =  doc.yes_laws
		self.no_laws =  doc.no_laws

		# operation level section
		self.national_tax =  doc.national_tax
		self.regional_tax =  doc.regional_tax
		self.district_level =  doc.district_level
			
		# organization description section
		self.brief_organization_description = doc.orgnization_description

		# Pillar Alignment Section
		self.pillar_alignment_table = []

		list_of_pillars = frappe.get_list("Pillar Alignment Table",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"pillar_alignment_table",
						"parenttype":"Organization",
				})

		for pillar in list_of_pillars:
			ndp_doc = frappe.get_doc("NDP",pillar.pillar)
			self.append("pillar_alignment_table", {
				"pillar_local": ndp_doc.description_somali,
				"pillar": pillar.pillar,
				"yes": pillar.yes
			})
		

		# Sectors Alignment Section
		self.section_alignment_table = []
		list_of_sectors = frappe.get_list("Sector Alignment Table",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"section_alignment_table",
						"parenttype":"Organization",
				})

		for sector in list_of_sectors:
			ndp_doc = frappe.get_doc("NDP", sector.sector)
			if ndp_doc.description_somali:
				self.append("section_alignment_table", {
					"sector_local": ndp_doc.description_somali,
					"pillar": sector.pillar,
					"sector":sector.sector,
					"yes": sector.yes
				})
			else:
				self.append("section_alignment_table", {
					"sector_local": ndp_doc.name,
					"pillar": sector.pillar,
					"sector":sector.sector,
					"yes": sector.yes
				})

		# Themes Alignment Table
		self.themes_alignment_table = []
		list_of_themes = frappe.get_list("Themes Alignment Table",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"themes_alignment_table",
						"parenttype":"Organization",
				})


		for theme in list_of_themes:
			ndp_doc = frappe.get_doc("NDP", theme.theme)
			if ndp_doc.description_somali:
				self.append("themes_alignment_table", {
					"sector":theme.sector,
					"theme": theme.theme,
					"theme_local": ndp_doc.description_somali,
					"yes": theme.yes
				})
			else:
				self.append("themes_alignment_table", {
					"sector":theme.sector,
					"theme": theme.theme,
					"theme_local": ndp_doc.name,
					"yes": theme.yes
				})

		# Geographical area(s) of operation
		self.marodijeh_check = doc.marodijeh_check
		self.hargeisa = doc.hargeisa
		self.gabiley = doc.gabiley
		self.baligubadle = doc.baligubadle
		self.salahlay = doc.salahlay

		self.sanag_check = doc.sanag_check
		self.erigavo = doc.erigavo
		self.badhan = doc.badhan
		self.las_qoray = doc.las_qoray
		self.ela_fweyn = doc.ela_fweyn
		self.dhahar = doc.dhahar
		self.gar_adag = doc.gar_adag
		
		self.sool_check = doc.sool_check
		self.las_anod = doc.las_anod
		self.hudun = doc.hudun
		self.taleh = doc.taleh
		self.aynabo = doc.aynabo

		self.togdheer_check = doc.togdheer_check
		self.burao = doc.burao
		self.odwayne = doc.odwayne
		self.buhodle = doc.buhodle

		self.awdal_check = doc.awdal_check
		self.borama = doc.borama
		self.zeila = doc.zeila
		self.baki = doc.baki
		self.lughaya = doc.lughaya

		self.sahil_check = doc.sahil_check
		self.berbera = doc.berbera
		self.sheekh = doc.sheekh

		self.hargeisa_region = doc.hargeisa_region
		
		# Accounting and reporting period
		self.closing_month = doc.closing_month
		self.closing_day = doc.closing_day

		# Total Number of Staff
		self.managerial_or_program_staff = doc.managerial_or_program_staff
		self.admin_or_support_staff = doc.admin_or_support_staff
		self.managerial_or_program_staff_2 = doc.managerial_or_program_staff_2
		self.admin_or_support_staff_2 = doc.admin_or_support_staff_2

		# Consortium members
		list_of_consortium_members = frappe.get_list("Consortium Members",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"consortium_members",
						"parenttype":"Organization",
				})

		for consortium_member in list_of_consortium_members:
			self.append("consortium_members", {
				"name_of_organization":consortium_member.name_of_organization,
				"registration_no": consortium_member.registration_no,
				"contact_person": consortium_member.contact_person,
				"country_code": consortium_member.country_code,
				"telephone": consortium_member.telephone,
				"complete_telephone": consortium_member.complete_telephone,
				"email": consortium_member.email
			})

		# International communication section
		self.international_communication = []
		list_of_international_communication = frappe.get_list("International Communication Table Consortium",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"international_communication",
						"parenttype":"Organization",
				})

		for international_communication in list_of_international_communication:
			self.append("international_communication", {
				"member_name":international_communication.member_name,
				"selected_assignment": international_communication.selected_assignment,
				"level_education": international_communication.level_education,
				"telephone": international_communication.telephone,
				"email": international_communication.email
			})

		# more information section
		self.foundation_date = doc.foundation_date
		self.foundation_venue = doc.foundation_venue
		self.yes_conved_by_mopnd = doc.yes_conved_by_mopnd

		# map convening members of mopnd
		self.convening_members_of_mopnd = []
		list_of_convening_members_of_mopnd = frappe.get_list("Convening Members of Mopnd",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"convening_members_of_mopnd",
						"parenttype":"Organization",
				})

		for convening_member in list_of_convening_members_of_mopnd:
			self.append("convening_members_of_mopnd", {
				"magaca":convening_member.magaca,
			})
		
		self.no_conved_by_mopnd = doc.no_conved_by_mopnd
		self.yes_consultative_committee = doc.yes_consultative_committee

		# map consultative members of MoPND
		self.consultative_committee = []
		list_of_consultative_committee = frappe.get_list("Consultative Committee",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"consultative_committee",
						"parenttype":"Organization",
				})

		for consultative_committee in list_of_consultative_committee:
			self.append("consultative_committee", {
				"member_name":consultative_committee.member_name,
			})
		self.not_consultative_committee = doc.not_consultative_committee

		# Newsletter of migrants or appeed in Ururka
		self.communicate_members = []
		list_of_members = frappe.get_list("Communicate members",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"communicate_members",
						"parenttype":"Organization",
				})

		for member in list_of_members:
			self.append("communicate_members", {
				"name1": member.name1,
				"tel_no":member.tel_no,
				"education_level": member.education_level,
				"email": member.email
			})

		'''
		# Attachments Section
		doc.attach_logo_local_1 = self.attach_logo_local_1
		doc.association_rules = self.association_rules

		doc.board_of_directors = []
		for director in self.board_of_directors:
			doc.append('board_of_directors', {
				'attachment_name': director.attachment_name,
				'attachment': director.attachment,
			}), 

		doc.proof_of_members = []
		for member in self.proof_of_members:
			doc.append('proof_of_members', {
				'attachment_name': member.attachment_name,
				'attachment': member.attachment,
			}), 

		doc.members_cv = []
		for cv in self.members_cv:
			doc.append('members_cv', {
				'attachment_name': cv.attachment_name,
				'attachment': cv.attachment,
			}), 

		# doc.written_request = self.written_request
		doc.property_list = []
		for prop in self.property_list:
			doc.append('property_list', {
				'attachment_name': prop.attachment_name,
				'attachment': prop.attachment,
			})

		doc.next_year_plan = []
		for plan in self.next_year_plan:
			doc.append('next_year_plan', {
				'attachment_name': plan.attachment_name,
				'attachment': plan.attachment,
			})

		doc.last_year_performance = []
		for performance in self.last_year_performance:
			doc.append('last_year_performance', {
				'attachment_name': performance.attachment_name,
				'attachment': performance.attachment,
			})
		doc.organization_1000 = self.organization_1000

		doc.attach_other = []
		for other in self.attach_other:
			doc.append('attach_other', {
				'attachment_name': other.attachment_name,
				'attachment': other.attachment,
			})
		'''
		# add the form as a child table to organization
		doc.append("attached_organization_forms_table", {
			"name_of_form":self.name,
			"year_of_form":self.year_of_registration
		})

		# mark the form that data has been imported
		self.data_pulled_from_main = 1

