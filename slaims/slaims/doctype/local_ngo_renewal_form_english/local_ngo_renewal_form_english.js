// Copyright (c) 2019, upande and contributors
// For license information, please see license.txt

// list of organization form sections in organization
var list_of_sections = {
	"Local Organization": [
		"general_organizaiton_details",
		"organization_address",
		"registering_persons_details_section",
		"operation_level_section",
		"organization_description_section_section",
		"pillar_alignment_section",
		"section_alignment_section",
		"themes_alignment_section",
		"geographical_area",
		"chapter_2",
		"staff_details_section",
		"attachments_section_section",
		"verification"
	],
	
	"All": [
		"organization_type_section_section",
		"general_organizaiton_details",
		"organization_address",
		"registering_persons_details_section",
		"operation_level_section",
		"organization_description_section_section",
		"pillar_alignment_section",
		"section_alignment_section",
		"themes_alignment_section",
		"geographical_area",
		"staff_details_section",
		"chapter_2",
		"attachments_section_section",
		"verification","validation","approval"
	]
}

var list_of_all_fields = [
	// organization details section
	"full_name_of_the_organization","year_of_first_registration",
	"organization_registration_number",

	// organization address section
	// address in origin country
	"state_local_1", "city", "street_local_1",
	
	// Registering person details section
	"name_of_person_registering", "code_person_registering", "tel_person_registering", "email_person_registering",

	// Operation level section
	"national_tax", "regional_tax",
	
	// Organization description section
	"brief_organization_description",
	
	// Pillar alignment section
	"pillar_alignment_table", "confirm_pillars_check",
	
	// Section alignment section
	"section_alignment_table", "confirm_sectors_check",
	
	// Themes alignment section
	"themes_alignment_table", "confirm_themes_check", 
	
	// Geographical areas section
	"marodijeh_check", "hargeisa", "gabiley", "baligubadle", "salahlay", "sanag_check",
	"erigavo", "badhan", "las_qoray", "ela_fweyn", "dhahar", "gar_adag", "hargeisa_region",
	"sool_check", "las_anod", "hudun", "taleh", "aynabo", "togdheer_check", "burao",
	"odwayne", "buhodle", "awdal_check", "borama", "zeila", "baki", "lughaya", "sahil_check",
	"berbera", "sheekh",

	// Community group section
	"communicate_members", "staff_details_section",
	
	// Attachments section
	"attach_logo_local_1",

	// Verification and approval section
	"verifier_name","verifier_designation","verification_date",
	
	// Navigation section
	"save_and_continue", "next_section", "direction"
	
]

// function that sets custom buttons
function add_custom_buttons(button_name,action){
	cur_frm.add_custom_button(__(button_name), function(){

		// only show this buttons to users with administrative privillages
		if(action=="Unverify"){
			unverify_button()
		}else if (action == "Unvalidate"){
			unvalidate_button()
		}else if(action=="Edit"){
			// check user is allowed to edit
			if(cur_frm.doc.verified){
					frappe.throw("You cannot edit a form that is already verified \
					,contact the admin for assistance")
			}else{
				// make the forms readble for user
				cur_frm.set_value("viewing_information",0)
				cur_frm.set_value("current_section",list_of_sections[0])
				cur_frm.save()
			}
		}else if(action = "Go To Page"){
			goToPage()
		}

	},__("Form Menu"));
}

// function that unverifies and unvalidates the form
function unverify_button(){
	cur_frm.set_value("information_checked",0)
	cur_frm.set_value("verifier_name","")
	cur_frm.set_value("verifier_designation","")
	cur_frm.set_value("verification_date","")
	cur_frm.set_value("verified","")
	cur_frm.set_value("validated","")
	cur_frm.set_value("validated_by","")
	cur_frm.set_value("verified","")
	cur_frm.set_value("information_checked_by_staff",0)
	cur_frm.set_value("information_checked_by","")
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Pending Verification")
	cur_frm.save()
}

// function that unvalidates the form
function unvalidate_button(){
	cur_frm.set_value("information_checked_by_staff",0)
	cur_frm.set_value("information_checked_by","")
	cur_frm.set_value("validated","")
	cur_frm.set_value("validated_by","")
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Pending Validation")
	cur_frm.save()
}

// function that unverifies and unapprovess the form
function unapprove_button(){
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Verified")
	cur_frm.save()
}

/*function that toogles field to hide or unhide*/
function hide_unhide_fields(frm, list_of_fields, hide_or_unhide) {
	for (var i = 0; i < list_of_fields.length; i++) {
		frm.toggle_display(list_of_fields[i], hide_or_unhide)
	}
}

// function that redirect the user to a given url
function redirect_url(new_url){
	// allow the user to view page
	window.location = new_url
}

// function that determines which section the user should see
function navigation_function(frm,list_of_sections){
	
	if(cur_frm.doc.viewing_information){
		// allow user to see all section
		hide_unhide_fields(frm, list_of_sections["All"], true)
	}else{
		// hide all the fields
		hide_unhide_fields(frm, list_of_sections["All"], false)

		// if form has a current section
		if(frm.doc.current_section){
			// unhide the current section
			frm.toggle_display(frm.doc.current_section,true)
		}else{
			frm.doc.current_section = list_of_sections["Local Organization"][0]
			frm.doc.next_section = list_of_sections["Local Organization"][1]

			// refresh form
			cur_frm.refresh()
		}

		// fill in the section page numbers
		if(frm.doc.current_section){
			// do nothing for now
			var number_of_sections = list_of_sections["Local Organization"].length
			var current_section_index = list_of_sections["Local Organization"].indexOf(cur_frm.doc.current_section)
			// fill in the section page numbers
			cur_frm.set_value("pagesection","Screen: "+String(parseInt(current_section_index)+1)+" of "+String(number_of_sections))
		}else{
			cur_frm.set_value("pagesection","Screen: 1")
		}
	}
}


// function that determines the next section the user need to fill and unhides it
function unhide_next_section(frm,steps){
	var current_section_index = list_of_sections["Local Organization"].indexOf(cur_frm.doc.current_section)

	var current_user_section = list_of_sections["Local Organization"][current_section_index]
	var next_user_section = list_of_sections["Local Organization"][current_section_index + steps]
	var future_user_section = list_of_sections["Local Organization"][current_section_index + steps+1]

	// set next sections
	if(next_user_section){
		cur_frm.set_value("previous_section",current_user_section)
		cur_frm.set_value("current_section",next_user_section)
		cur_frm.set_value("next_section",future_user_section)
	}else{
		msgprint("You Reached the End of the Form")
	}

	// set screen numbers
	
	// save the form
	cur_frm.save()
}

// function that determines the previous section the user filled and unhides it
function unhide_previous_section(frm){
	// unhide_next_field(list_of_fields)	
	var current_section_index = list_of_sections["Local Organization"].indexOf(frm.doc.current_section)

	var current_user_section = list_of_sections["Local Organization"][current_section_index - 1]
	var previous_user_section = list_of_sections["Local Organization"][current_section_index - 2]
	var future_user_section = list_of_sections["Local Organization"][current_section_index]

	// set next sections
	if(current_user_section){
		cur_frm.set_value("previous_section",previous_user_section)
		cur_frm.set_value("current_section",current_user_section)
		cur_frm.set_value("next_section",future_user_section)
	}else{
		msgprint("You Reached the Beginning of the Form")
	}

	// save the form
	cur_frm.save()
}


var check_required_organization_type_section_section = function(){
	return {status:true,steps:1}
}

var check_required_general_organizaiton_details = function(){
	// check has given all the required fields
	if(cur_frm.doc.full_name_of_the_organization && cur_frm.doc.acronym ){
		return {status:true,steps:1}	
	}else{
		frappe.throw("Please Give Values for The Fields 'Name','Acronym' In Order to Continue")
	}
}

var check_required_organization_address = function(){
	return {status:true,steps:1}
}

var check_required_pillar_alignment_section = function(){
	return {status:true,steps:1}
}

var check_required_section_alignment_section = function(){
	return {status:true,steps:1}
}

var check_required_themes_alignment_section = function(){
	return {status:true,steps:1}
}

var check_required_geographical_area = function(){
	return {status:true,steps:1}
}

var check_required_attachments_section_section = function(){
	return {status:true,steps:1}
}

var check_required_verification = function(){
	return {status:true,steps:1}
}

var check_required_verification = function(){
	return {status:true,steps:1}
}



var check_required_registering_persons_details_section = function(){
	return {status:true,steps:1}
}

var check_required_operation_level_section = function(){
	return {status:true,steps:1}
}

var check_required_chapter_2 = function(){
	return {status:true,steps:1}
}

var check_staff_details_section = function(){
	return {status:true, steps:1};
};

var check_required_organization_description_section_section = function(){
	return {status:true,steps:1}
}

var validation_function_per_section ={
	"general_organizaiton_details":check_required_general_organizaiton_details,
	"organization_address":check_required_organization_address,
	"organization_type_section_section":check_required_organization_type_section_section,
	"general_organizaiton_details":check_required_general_organizaiton_details,
	"organization_address":check_required_organization_address,
	"registering_persons_details_section":check_required_registering_persons_details_section,
	"operation_level_section":check_required_operation_level_section,
	"organization_description_section_section":check_required_organization_description_section_section,
	"pillar_alignment_section": check_required_pillar_alignment_section,
	"section_alignment_section": check_required_section_alignment_section,
	"themes_alignment_section": check_required_themes_alignment_section,
	"geographical_area":check_required_geographical_area,
	"chapter_2":check_required_chapter_2,
	"staff_details_section": check_staff_details_section,
	"attachments_section_section":check_required_attachments_section_section,
	"verification":check_required_verification
}

// function filter relevant fields
function filter_fields(){

	// filter organization for specific user and organization type
	if(frappe.user.has_role("Staff")){
		// filter organization based on status only as pending renewal
		cur_frm.set_query("full_name_of_the_organization", function() {
			return {
				"filters": {
					"organization_type": "Local NGO",
					"status":"Pending Renewal"
				}
			}
		});
	}else{
		// filter organization based on those assigned to user
		cur_frm.set_query("full_name_of_the_organization", function() {
			return {
				"filters": {
					"organization_type": "Local NGO",
					"user":frappe.session.user,
					"status":"Pending Renewal"
				}
			}
		});
	}

	// filter regions
	cur_frm.set_query("state_local_1", function() {
		return {
			"filters": {
				"type": "Region",
				name:["!=","Central/Hargeisa"]
			}
		}
	});

	// filter districts
	cur_frm.set_query("district", function() {
		return {
			"filters": {
				"type": "District"
			}
		}
	});

}

function exitButton(){
	
	cur_frm.add_custom_button('Exit', function(){
		
		if(frappe.user.has_role('Staff') || frappe.user.has_role('Super Admin')){
			window.location = '/desk#List/Local%20NGO%20Renewal%20Form%20English/List'
		} else{
			window.location = '/profile_organization'
		}
	})
	
}

// function that hides or unhides certain fields based on what the user selects while filling the form
function options_based_hide_unhide(){
	if(frappe.user.has_role("Staff")){
		add_custom_buttons("Unverify Form","Unverify")
	}

	if(frappe.user.has_role("Documents Approver")){
		add_custom_buttons("Unvalidate Form","Unvalidate")
	}

	// allow all users to seee this menu items
	add_custom_buttons("Edit Form","Edit")
	add_custom_buttons("Go to screen","Go To Page")

	// approval sections
	if(frappe.user.has_role("Staff")){
		// check if form is already verified
		if(cur_frm.doc.verified == 1){
			// show all fields
			cur_frm.toggle_display("validation", true);
			// check if user has the role of documents approver
			if(frappe.user.has_role("Documents Approver")){
				cur_frm.toggle_display("approval", true);
			}else{
				cur_frm.toggle_display("approval", false);
			}
		}else{
			// hide the validation ,approval section
			cur_frm.toggle_display("validation", false);
			cur_frm.toggle_display("approval", false);
		}
	}else{
		// hide the validation ,approval section
		cur_frm.toggle_display("validation", false);
		cur_frm.toggle_display("approval", false);
	}

	// Add exit button to forms of basic users.
	if (frappe.user_roles.includes('Staff') || frappe.user_roles.includes('Super Admin')) {
		// Dont add exit button
		// exitButton()
	} else {
		exitButton()
	}
}

// Function to navigate through sections
function goToPage(){
	frappe.prompt([
		{'fieldname': 'page', 'fieldtype': 'Int', 'label': 'Go to page', 'reqd': 1}  
	],
	function(values){
		if(values.page <= list_of_sections['Local Organization'].length &&  values.page > 0 ){
			// set the current page
			cur_frm.set_value("previous_section",list_of_sections['Local Organization'][values.page -2])
			cur_frm.set_value("current_section",list_of_sections['Local Organization'][values.page-1])
			cur_frm.set_value("next_section",list_of_sections['Local Organization'][values.page ])
	
			// save the form in order to go to that page
			cur_frm.save()
	
		}else{
			msgprint("Please provide a page betweeen 1 and "+String(list_of_sections['Local Organization'].length))
		}
	},
	'Go to page',
	'Go'
	)
	}

function make_fields_read_only(list_of_fields){
	if(frappe.user.has_role("Super Admin")){
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",0)	
		})
	}
	else if(frappe.user.has_role("Staff")){
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",1)	
		})
	}else{
		if(cur_frm.doc.verified == 1 || cur_frm.doc.viewing_information){
			// check if user has validation rights
			if(frappe.user.has_role("Super Admin")){
				list_of_fields.forEach(function(v,i){
					cur_frm.set_df_property(v,"read_only",0)	
				})
			}else{
				list_of_fields.forEach(function(v,i){
					cur_frm.set_df_property(v,"read_only",1)	
				})
			}
		}else{
			list_of_fields.forEach(function(v,i){
				cur_frm.set_df_property(v,"read_only",0)	
			})
		}
	}	
}

// function that determines if  user has privillages to Validite , approve,verify info
function check_privillages(frm,action){
	// check if doc is saved
	if(frm.doc.__islocal ? 0 : 1){
		// check privillages absed on action
		if(action == "checked"){
			// check if the user is the owner of the doc
			if(frappe.user.has_role("Staff")){
				// allow administrator to mark as checked
				cur_frm.set_value("information_checked",1)
				cur_frm.save()
			}
			else if(frm.doc.owner){
				// check if the current user is also the owner
				if(frappe.session.user == frm.doc.owner){
					// allow administrator to validate
					cur_frm.set_value("information_checked",1)
					cur_frm.save()
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}else{
				// do not do anything for now
			}

		}else if(action == "verify"){
			// check if the user is the owner of the doc
			if(frappe.user.has_role("Staff")){
				if (frm.doc.verifier_name && frm.doc.verifier_designation && frm.doc.verification_date) {
					// allow administrator to validate
					cur_frm.set_value("verified",1)
					cur_frm.set_value("status","Verified")
					cur_frm.save()
				} else {
					cur_frm.set_value('verified', 0);
					frappe.throw('Fill in <b>Name</b> ,<b>Designation</b> and <b>Date</b> fields before verifying');
				}
				
			}
			else if(frm.doc.owner){
				// check if the current user is also the owner
				if(frappe.session.user == frm.doc.owner){
					if (frm.doc.verifier_name && frm.doc.verifier_designation && frm.doc.verification_date) {
						// allow user to validate their organization
						cur_frm.set_value("verified",1);
						cur_frm.set_value("status","Verified");
						cur_frm.save();
					} else {
						cur_frm.set_value('verified', 0);
						frappe.throw('Fill in <b>Name</b> ,<b>Designation</b> and <b>Date</b> fields before verifying');
					}
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}else{
				if(frappe.session.user == frm.doc.created_by){
					if (frm.doc.verifier_name && frm.doc.verifier_designation && frm.doc.verification_date) {
						// allow user to validate their organization
						cur_frm.set_value("verified",1)
						cur_frm.set_value("status","Verified")
						cur_frm.save()
					} else {
						cur_frm.set_value('verified', 0)
						frappe.throw('Fill in <b>Name</b> ,<b>Designation</b> and <b>Date</b> fields before verifying');

					}
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}

		}else if(action == "checked_by_staff"){
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Staff")){
				// the user has privillages allow
				// check if the if its validated
				if(frm.doc.verified == 1){
					// information_checked_by_mopnd_staff
					cur_frm.set_value("information_checked_by_staff",1)
					cur_frm.set_value("information_checked_by",frappe.session.user)
					cur_frm.save()
				}else{
					cur_frm.set_value("information_checked_by_staff",0)
					cur_frm.set_value("information_checked_by","")
					frappe.throw("Form need to be Verified before Staff can Mark it as Checked")
				}	
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
		}else if(action == "validate"){
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Staff")){
				// check if staff has marked the form as checked
				if(frm.doc.information_checked_by_staff == 1){
					cur_frm.set_value("validated",1)
					cur_frm.set_value("status","Validated")
					cur_frm.set_value("validated_by",frappe.session.user)
					cur_frm.save()
				}else{
					frappe.throw("Form need to be Checked by Staff before Validation")
				}
				
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}

		}else if(action == "validate"){
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Staff")){
				// the user has privillages allow
				// check if the if its validated
				if(frm.doc.verified == 1){
					cur_frm.set_value("validated",1)
					cur_frm.set_value("status","Validated")
					cur_frm.set_value("validated_by",frappe.session.user)
					cur_frm.save()
				}else{
					frappe.throw("Form need to be verified before validation")
				}
				
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
	
		}else if(action == "approve"){
			// check if the user has the Role "Documents Approver"
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Documents Approver")){
				// the user has privillages allow
				// check if the if its validated
				if(frm.doc.validated == 1){
					cur_frm.set_value("approved",1)
					cur_frm.set_value("status","Approved")
					cur_frm.set_value("approved_by",frappe.session.user)
					cur_frm.save()
				}else{
					frappe.throw("Form need to be validated before approval")
				}
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
		}
	}else{
		frappe.throw("Please save the document first")
	}
}

/*****************************************************************************************/
// Trigger based function

// Autofill acronym on selection of full name of the organization

frappe.ui.form.on("Local NGO Renewal Form English",{
	full_name_of_the_organization:function(){
	if(cur_frm.doc.full_name_of_the_organization){
		// call the organization
		frappe.call({
			method: "frappe.client.get_list",
			args:   {
					doctype: "Organization",
					filters: {
						name:cur_frm.doc.full_name_of_the_organization
					},
			fields:["*"]
			},
			callback: function(response) {
				cur_frm.set_value("acronym",response.message[0]["acronym"])
				cur_frm.set_value("country_of_origin",response.message[0]["country_of_origin"])
				// now save the form
				cur_frm.save()
				
			} 
		})
	} else{
		cur_frm.set_value("acronym", "")
		cur_frm.set_value("country_of_origin", "")
	}
  }
});


frappe.ui.form.on('Local NGO Renewal Form English', {
	refresh: function(frm) {
		navigation_function(frm,list_of_sections)
		filter_fields()
		options_based_hide_unhide()

		cur_frm.get_field("pillar_alignment_table").grid.cannot_add_rows = true;
		cur_frm.get_field("section_alignment_table").grid.cannot_add_rows = true;
		cur_frm.get_field("themes_alignment_table").grid.cannot_add_rows = true;
		
		// Make fields read only based on status and role
		make_fields_read_only(list_of_all_fields)
	}
});

frappe.ui.form.on('Local NGO Renewal Form English', {
	after_save: function(frm) {
		// check if user is not part of the admin
		if(frappe.user.has_role("Staff") || frappe.user.has_role("Super Admin") || frappe.user.has_role("Documents Approver")){
			// do no do anything
		}else{
			// check if the user is a basic user and the status is verified
			if(frappe.user.has_role("Organization Login")){
				if(cur_frm.doc.status == "Verified"){
					// redirect to profile page
					setTimeout(function(){redirect_url('/profile_organization');}, 950);
				}
			}
		}
	}
});

cur_frm.cscript.custom_refresh = function() {
    if(frappe.user.has_role("Staff")) {
		// show button
    }else{
		cur_frm.appframe.buttons.Menu.remove();
	}
}

/*****************************************************************************************
PILLAR ALIGNMENT SECTION */

// function called when  the confirm_pillars_check field clicked under PILLAR ALIGNMENT SECTION
frappe.ui.form.on("Local NGO Renewal Form English", "confirm_pillars_check", function(frm){
	// check if the user has selected any pillar
	var pillar_selected = false

	if(frm.doc.confirm_pillars_check){
		$.each(frm.doc.pillar_alignment_table, function(i,v){
			if(v["yes"]){
				pillar_selected = true
			}else{
				// do nothing
			}
		});
	
		if(pillar_selected){
			// allow user to continues
		}else{
			// check if there is any pillar in the table
			if(frm.doc.pillar_alignment_table.length == 0){
				// do nothing
			}else{
				cur_frm.set_value("confirm_pillars_check","")
				frappe.throw("You Have Not Ticked Any Pillar In The Table"+"<hr>"+"Please Tick 'Yes' Against Pillars That The Organization Contributes to In Order to Continue")
			}	
		}
	}
})


/*****************************************************************************************
SECTION ALIGNMENT SECTION */

// function called when  the confirm_sectors_check field clicked under SECTION ALIGNMENT SECTION
frappe.ui.form.on("Local NGO Renewal Form English", "confirm_sectors_check", function(frm){
	// check if the user has selected any pillar
	var selectors_selected = false

	if(frm.doc.confirm_sectors_check){
		$.each(frm.doc.section_alignment_table, function(i,v){
			if(v["yes"]){
				selectors_selected = true
			}else{
				// do nothing
			}
		});
	
		if(selectors_selected){
			// allow user to continues
		}else{
			// check if there is any sector in the table
			if(frm.doc.section_alignment_table.length == 0){
				// do nothing
			}else{
				cur_frm.set_value("confirm_sectors_check","")
				frappe.throw("You Have Not Ticked Any Sector In The Table"+"<hr>"+"Please Tick 'Yes' Against Sectors That The Organization Contributes to In Order to Continue")
			}
		}
	}
})

/*****************************************************************************************
THEMES ALIGNMENT SECTION */

// function called when  the confirm_themes_check field clicked under THEMES ALIGNMENT SECTION
frappe.ui.form.on("Local NGO Renewal Form English", "confirm_themes_check", function(frm){
	// check if the user has selected any pillar
	var themes_selected = false

	if(frm.doc.confirm_themes_check){
		$.each(frm.doc.themes_alignment_table, function(i,v){
			if(v["yes"]){
				themes_selected = true
			}else{
				// do nothing
			}
		});
	
		if(themes_selected){
			// allow user to continues
		}else{
			// check if there is any themes in the table
			if(frm.doc.themes_alignment_table.length == 0){
				// do nothing
			}else{
				cur_frm.set_value("confirm_themes_check","")
				frappe.throw("You Have Not Ticked Any Themes In The Table"+"<hr>"+"Please Tick 'Yes' Against Themes That The Organization Contributes to In Order to Continue")
			}
		}
	}
})

/*****************************************************************************************
Organization Activity Section */
frappe.ui.form.on("Local NGO Renewal Form English", "national_tax", function(frm){
	if(cur_frm.doc.national_tax){
		cur_frm.set_value("regional_tax","")
	}
})

frappe.ui.form.on("Local NGO Renewal Form English", "regional_tax", function(frm){
	if(cur_frm.doc.regional_tax){
		cur_frm.set_value("national_tax","")
	}
})

/******************************************************************************************/
frappe.ui.form.on("Local NGO Renewal Form English", "view_all_information", function(frm){
	cur_frm.set_value("viewing_information",1)
	cur_frm.save()
})

frappe.ui.form.on("Local NGO Renewal Form English", "edit_information", function(frm){
	// check user is allowed to edit
	if(cur_frm.doc.verified){
		frappe.throw("You cannot edit a form that is already verified \
		,contact the admin for assistance")
	}else{
		// make the forms readble for user
		cur_frm.set_value("viewing_information",0)
		var current_list_of_sections = list_of_sections['Local Organization']
		cur_frm.set_value("current_section",current_list_of_sections[current_list_of_sections.length - 1])
		cur_frm.save()
	}
})

frappe.ui.form.on("Local NGO Renewal Form English", "mark_information_as_checked", function(frm){
	check_privillages(frm,"checked")
})

frappe.ui.form.on("Local NGO Renewal Form English", "user_verify", function(frm){
	// check if the user has marked the information as checked
	if(cur_frm.doc.information_checked){
		frappe.confirm(
			"Please ensure that all the information you have given is correct before verifying <hr> \
			if you are sure you are ready to verify click <b>Yes</b> otherwise click <b>No</b>",
			function(){
				check_privillages(frm,"verify")	
				cur_frm.set_value('viewing_information',1)
			},
			function(){
				// do nothing since the user is not sure
			}
		)
	}else{
		frappe.throw("Please mark the information as checked before verification")
	}
})

frappe.ui.form.on("Local NGO Renewal Form English", "mark_information_as_checked_by_staff", function(frm){
	check_privillages(frm,"checked_by_staff")
})

frappe.ui.form.on("Local NGO Renewal Form English", "user_validate", function(frm){
	check_privillages(frm,"validate")
})

frappe.ui.form.on("Local NGO Renewal Form English", "approve", function(frm){
	check_privillages(frm,"approve")
})

/*****************************************************************************************
Navigation Section */

// function called when  the Previous Button field clicked under Navigation Section
frappe.ui.form.on("Local NGO Renewal Form English", "previous_section_button", function(frm){
	// go to the next section	
	unhide_previous_section(frm)
})

// function called when  the Save and Continue field clicked under Navigation Section
frappe.ui.form.on("Local NGO Renewal Form English", "save_and_continue", function(frm){ 

	// ensure to requirements for the section are complete
	var section_function = validation_function_per_section[frm.doc.current_section] 
	if(section_function()["status"]){
		unhide_next_section(cur_frm,section_function()["steps"])
	}

})

// Function called on clicking go to screen button
frappe.ui.form.on("Local NGO Renewal Form English", "go_to_page", function(frm){
	// call the go to page function
	goToPage()
});

