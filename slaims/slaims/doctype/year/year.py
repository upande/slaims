# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt
from __future__ import unicode_literals

# std lib import
import datetime
import frappe
from frappe.model.document import Document

class Year(Document):
	'''
	Class that handles transactions on the 
	Year Doctype
	'''
	def validate(self):
		# check that all the fields are given
		check_required_fields(self)
		# check other requirements for the years
		check_field_requirements(self)
		# assign the year a rank
		assign_a_rank(self)

def check_required_fields(self):
	'''
	Function that checks that all the required 
	fields are given 
	Input:
		self i.e the document
	Output:
		No Error - pass
		Error  - throws an alert message to the user
	'''
	list_of_required_fields = [
		{"field_name":"Year","field_value":self.year},
		{"field_name":"Start Date","field_value":self.start_date},
		{"field_name":"End Date","field_value":self.end_date},
		{"field_name":"Submission Deadline","field_value":self.submission_deadline},
		{"field_name":"Year Closing Date","field_value":self.year_closing_date},	
	]

	for field in list_of_required_fields:
		if field["field_value"]:
			pass
		else:
			frappe.throw("You not given any value for \
				the field <strong>{}</strong>".format(field["field_name"]))

def check_field_requirements(self):
	'''
	Check other requirements that the fields should 
	have in order to save the document
	'''
	# start date should be less that end date
	if self.start_date < self.end_date:
		pass
	else:
		frappe.throw("The <strong>Start Date</strong> \
			should be an earlier date in comparison to \
				the <strong>End Date</strong>")


	# ensure the period does not overlap with the duration of
	# another year
	if type(self.start_date) and type(self.end_date):
		new_start_date = self.start_date
		new_end_date = self.end_date
	else:
		new_start_date = datetime.datetime.strptime(self.start_date, '%Y-%m-%d')
		new_end_date = datetime.datetime.strptime(self.end_date, '%Y-%m-%d')
	
	# get overlapping dates
	years_with_overlapping_dates = frappe.db.sql("""select name from \
		tabYear where '{}' between start_date and end_date || \
		'{}' between start_date and end_date""".format(new_start_date,new_end_date))
	
	if len(years_with_overlapping_dates) > 0:
		# loop to check if any is not the same as the current year
		for year in years_with_overlapping_dates:
			if year[0] != self.name:
				frappe.throw("<strong>Start</strong> and <strong>End Dates</strong> \
					of the year overlap with those of the year's <strong>{}</strong>\
					".format(year[0]))

	# submission deadline should be greter than startdate but not closing date
	if self.submission_deadline > self.start_date and self.submission_deadline < \
		self.year_closing_date:
		pass
	else:
		frappe.throw("Submission Deadline should be later than Start Date but earlier \
			than the Year Closing Date")

	# closing date should be greater than startdate
	if self.year_closing_date <= self.start_date:
		frappe.throw("Year Closing Date should be later than Start Date but earlier \
			than the Year Closing Date")
	

def assign_a_rank(self):
	'''
	Assign a rank to the current year
	'''
	if self.rank:
		pass
	else:
		# retirve rank from global settings
		global_settings = frappe.db.sql("""select \
			value from `tabSingles` where doctype = \
			'Global Settings' && field='current_year_rank'""")
		current_global_rank = global_settings[0][0]
		if self.rank:
			pass
		else:
			self.rank = int(current_global_rank) +1