// Copyright (c) 2019, upande and contributors
// For license information, please see license.txt



// global variables
var field_to_hide_unhide = {
	admin_fields: ["verification_approval_section"],
	all: ["verification_approval_section"],
}

// ================================================================================================
/* This section contains code from the general functions section
which are called is the form triggered functions section*/

// function hide_unhide_on_refresh(frm) {
// 	if(frappe.user.has_role("Project Admin")){
// 		// unhide the validation section
// 		hide_function(frm, field_to_hide_unhide, "admin_fields")
// 	}else{

// 	}
			
// 	function hide_function(frm, field_to_hide_unhide, allowed_fields) {
// 		var hide_fields = field_to_hide_unhide["all"]
// 		var unhide_fields = field_to_hide_unhide[allowed_fields]
// 		// hide or unhide fields
// 		hide_unhide_fields(frm, hide_fields, false)
// 		hide_unhide_fields(frm, unhide_fields, true)
// 	}

// 	/*function that toogles field to hide or unhide*/
// 	function hide_unhide_fields(frm, list_of_fields, hide_or_unhide) {
// 		for (var i = 0; i < list_of_fields.length; i++) {
// 			frm.toggle_display(list_of_fields[i], hide_or_unhide)
// 		}
// 	}
// }


// function that determines if  user has privillages to Validite , approve,verify info
function check_privillages(frm,action){
	// check if doc is saved
	if(frm.doc.__islocal ? 0 : 1){
		// check privillages absed on action
		if(action == "verify"){
			// check if the user is has the role "Project Admin"
			if(frappe.user.has_role("Project Admin")){
				// allow administrator to validate
				cur_frm.set_value("verified",1)
				cur_frm.set_value("status","Verified")
				cur_frm.save()
			}else{
				frappe.throw("You Do Not Have Enough Permisions To Perform This Action")
			}

		}else if(action == "approve"){
			// check if the user is has the role "Project Admin"
			if(frappe.user.has_role("Project Admin")){
				// allow administrator to validate
				cur_frm.set_value("approved",1)
				cur_frm.set_value("status","Approved")
				cur_frm.save()
			}else{
				frappe.throw("You Do Not Have Enough Permisions To Perform This Action")
			}
		}
	}else{
		frappe.throw("Please Save the Document First")
	}
}

// function that sets custom buttons
function add_custom_buttons(button_name,action){
	cur_frm.add_custom_button(__(button_name), function(){
		if(action=="Unverify"){
			cur_frm.set_value("verified",0)
			cur_frm.set_value("verified_by","")
			cur_frm.set_value("approved",0)
			cur_frm.set_value("approved_by","")
			cur_frm.set_value("status","Pending Verification")
			cur_frm.save()
		}else if(action == "Unapprove"){
			cur_frm.set_value("verified",0)
			cur_frm.set_value("verified_by","")
			cur_frm.set_value("approved",0)
			cur_frm.set_value("approved_by","")
			cur_frm.set_value("status","Pending Verification")
			cur_frm.save()
		}
	},__("Project Title Menu"));
}

/* end of the general functions section
// =================================================================================================
/* This section  contains functions that are triggered by the form action refresh or
reload to perform various action*/
frappe.ui.form.on('Project Title', {
	refresh: function(frm) {
		// check fields
		if(frappe.user.has_role("Project Admin")){
			// unhide the validation section
			frm.toggle_display("verification_approval_section", true)
			// add custom buttons
			add_custom_buttons("Unverify","Unverify")
			add_custom_buttons("Unapprove","Unapprove")
		}else{
			frm.toggle_display("verification_approval_section", false)
		}
	}
});


// function that validates the form when the validate button is clicked
frappe.ui.form.on("Project Title", "verify", function(frm){ 
	check_privillages(frm,"verify")
});

// function that validates the form when the validate button is clicked
frappe.ui.form.on("Project Title", "approve", function(frm){ 
	check_privillages(frm,"approve")
});