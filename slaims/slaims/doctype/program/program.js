// Copyright (c) 2019, upande and contributors
// For license information, please see license.txt

frappe.ui.form.on('Program', {
	refresh: function(frm) {

	}
});


// function called when  program code field is filled
frappe.ui.form.on("Program", "program_code", function(frm){
	if(frm.doc.program_code){
		// clear the visible_program_code field 
		cur_frm.set_value("visible_program_code","")

		// add new program code
		cur_frm.set_value("visible_program_code",frm.doc.program_code)
	}else{
		// clear the visible_program_code field 
		cur_frm.set_value("visible_program_code","")
	}
})


// function called when  Program field is filled
frappe.ui.form.on("Program", "program", function(frm){
	if(frm.doc.program){
		// clear the trancate field and add new trancated value
		cur_frm.set_value("program_title","")

		// get the program value
		var trancated_value = frm.doc.program.slice(0,135)
		cur_frm.set_value("program_title",trancated_value+"...")
	}else{
		// clear the program_title field 
		cur_frm.set_value("program_title","")
	}
})