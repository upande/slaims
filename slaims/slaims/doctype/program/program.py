# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class Program(Document):
	def validate(self):
		''''
		Function that validates an entered program
		'''
		truncate_long_titles(self)


def truncate_long_titles(self):
	'''
	Function that truncates the title if its longer than 100 
	characters
	'''
	if len(self.program_title)>100:
		new_title = self.program_title[:20]
		self.program_title  = new_title