// Copyright (c) 2019, upande and contributors
// For license information, please see license.txt

// list of all the fields under NPRE
var list_of_fields = [
	"full_name_of_the_organization","year","yes","no",

	// verification section
	"verifier_name","verifier_designation",
]

// function filter relevant fields
function filter_fields(){

	if(frappe.user.has_role("Staff")){
		// do not filter organization for staff
	}else{
		// filter organization based on those assigned to user
		cur_frm.set_query("full_name_of_the_organization", function() {
			return {
				"filters": {
					"user":frappe.session.user
				}
			}
		});
	}
}

// function that sets custom buttons
function add_custom_buttons(button_name,action){
	cur_frm.add_custom_button(__(button_name), function(){

		// only show this buttons to users with administrative privillages
		if(action=="Unverify"){
			unverify_button()
		}else if (action == "Unvalidate"){
			unvalidate_button()
		}else if(action=="Edit"){
			// check user is allowed to edit
			if(cur_frm.doc.verified){
				frappe.throw("You cannot edit a form that is already verified \
				,contact the admin for assistance")
			}else{
				// make the forms readble for user
				cur_frm.set_value("viewing_information",0)
				cur_frm.set_value("current_section",list_of_sections[0])
				cur_frm.save()
			}
		}else if(action = "Go To Page"){
			goToPage()
		}

	},__("Form Menu"));
}

// function that unverifies and unvalidates the form
function unverify_button(){
	cur_frm.set_value("information_checked",0)
	cur_frm.set_value("verifier_name","")
	cur_frm.set_value("verifier_designation","")
	cur_frm.set_value("verification_date","")
	cur_frm.set_value("verified","")
	cur_frm.set_value("validated","")
	cur_frm.set_value("validated_by","")
	cur_frm.set_value("verified","")
	cur_frm.set_value("information_checked_by_staff",0)
	cur_frm.set_value("information_checked_by","")
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Pending Verification")
	cur_frm.save()
}

// function that unvalidates the form
function unvalidate_button(){
	cur_frm.set_value("information_checked_by_staff",0)
	cur_frm.set_value("information_checked_by","")
	cur_frm.set_value("validated","")
	cur_frm.set_value("validated_by","")
	cur_frm.set_value("approved","")
	cur_frm.set_value("approved_by","")
	cur_frm.set_value("status","Pending Validation")
	cur_frm.save()
}

function exitButton(){
	cur_frm.add_custom_button('Exit', function(){
		
		if(frappe.user.has_role('Staff') || frappe.user.has_role('Super Admin')){
			window.location = '/desk#List/Non%20Project%20Related%20Expenditure/List'
		} else{
			window.location = '/profile_expenditure'
		}
	})
}

function redirect_url(new_url){
    // allow the user to view page
    window.location = new_url
}

function make_fields_read_only(list_of_fields){
	if(frappe.user.has_role("Super Admin")){
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",0)	
		})
	}
	else if(frappe.user.has_role("Staff")){
		list_of_fields.forEach(function(v,i){
			cur_frm.set_df_property(v,"read_only",1)	
		})
	}else{
		if(cur_frm.doc.verified == 1 || cur_frm.doc.viewing_information){
			// check if user has validation rights
			if(frappe.user.has_role("Super Admin")){
				list_of_fields.forEach(function(v,i){
					cur_frm.set_df_property(v,"read_only",0)	
				})
			}else{
				list_of_fields.forEach(function(v,i){
					cur_frm.set_df_property(v,"read_only",1)	
				})
			}
		}else{
			list_of_fields.forEach(function(v,i){
				cur_frm.set_df_property(v,"read_only",0)	
			})
		}
	}	
}



// function that unhides fields fields,sections and buttons based on user's role
function options_based_hide_unhide(frm){
	// adding buttons
	if(frappe.user.has_role("Staff")){
		add_custom_buttons("Unverify Form","Unverify")
	}

	if(frappe.user.has_role("Documents Approver")){
		add_custom_buttons("Unvalidate Form","Unvalidate")
	}
	
	// approval sections
	if(frappe.user.has_role("Staff")){
		// check if form is already verified
		if(cur_frm.doc.verified == 1){
			// show all fields
			cur_frm.toggle_display("validation", true);
			// check if user has the role of documents approver
			if(frappe.user.has_role("Documents Approver")){
				cur_frm.toggle_display("validation_and_approval_section_section", true);
			}else{
				cur_frm.toggle_display("validation_and_approval_section_section", false);
			}
		}else{
			// hide the validation ,approval section
			cur_frm.toggle_display("validation", false);
			cur_frm.toggle_display("validation_and_approval_section_section", false);
		}
	}else{
		// hide the validation ,approval section
		cur_frm.toggle_display("validation", false);
		cur_frm.toggle_display("validation_and_approval_section_section", false);
	}
}	

// function that determines if  user has privillages to Validite , approve,verify info
function check_privillages(frm,action){
	// check if doc is saved
	if(frm.doc.__islocal ? 0 : 1){
		// check privillages absed on action
		if(action == "checked"){
			// check if the user is the owner of the doc
			if(frappe.user.has_role("Staff")){
				// allow administrator to mark as checked
				cur_frm.set_value("information_checked",1)
				cur_frm.save()
			}
			else if(frm.doc.owner){
				// check if the current user is also the owner
				if(frappe.session.user == frm.doc.owner){
					// allow administrator to validate
					cur_frm.set_value("information_checked",1)
					cur_frm.save()
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}else{
				// do not do anything for now
			}

		}else if(action == "verify"){
			// check if the user is the owner of the doc
			if(frappe.user.has_role("Staff")){
				if (frm.doc.verifier_name && frm.doc.verifier_designation && cur_frm.doc.verification_date) {
					// allow administrator to validate
					cur_frm.set_value("verified",1)
					cur_frm.set_value("status","Verified")
					cur_frm.save()
				} else {
					cur_frm.set_value('verified', 0);
					frappe.throw('Fill in <b>Name</b>,<b>Designation</b> and <b>Date</b> fields before verifying');
				}
				
			}
			else if(frm.doc.owner){
				// check if the current user is also the owner
				if(frappe.session.user == frm.doc.owner){
					if (frm.doc.verifier_name && frm.doc.verifier_designation && cur_frm.doc.verification_date) {
						// allow user to validate their organization
						cur_frm.set_value("verified",1);
						cur_frm.set_value("status","Verified");
						cur_frm.save();
					} else {
						cur_frm.set_value('verified', 0);
						frappe.throw('Fill in <b>Name</b>,<b>Designation</b> and <b>Date</b> fields before verifying');
					}
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}else{
				if(frappe.session.user == frm.doc.created_by){
					if (frm.doc.verifier_name && frm.doc.verifier_designation && cur_frm.doc.verification_date) {
						// allow user to validate their organization
						cur_frm.set_value("verified",1)
						cur_frm.set_value("status","Verified")
						cur_frm.save()
					} else {
						cur_frm.set_value('verified', 0)
						frappe.throw('Fill in <b>Name</b>,<b>Designation</b> and <b>Date</b> fields before verifying');

					}
				}else{
					frappe.throw("You cannot verify a plan that does not belong to you organization contact the administrator for assistance")
				}
			}
		}else if(action == "checked_by_staff"){
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Staff")){
				// the user has privillages allow
				// check if the if its validated
				if(frm.doc.verified == 1){
					// information_checked_by_mopnd_staff
					cur_frm.set_value("information_checked_by_staff",1)
					cur_frm.set_value("information_checked_by",frappe.session.user)
					cur_frm.save()
				}else{
					cur_frm.set_value("information_checked_by_staff",0)
					cur_frm.set_value("information_checked_by","")
					frappe.throw("Form need to be Verified before Staff can Mark it as Checked")
				}	
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
		}else if(action == "validate"){
			// check if the user is has the role "Organization Admin"
			if(frappe.user.has_role("Staff")){
				// check if staff has marked the form as checked
				if(frm.doc.information_checked_by_staff == 1){
					cur_frm.set_value("validated",1)
					cur_frm.set_value("status","Validated")
					cur_frm.set_value("validated_by",frappe.session.user)
					cur_frm.save()
				}else{
					frappe.throw("Form need to be Checked by Staff before Validation")
				}
				
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
		}else if(action == "approve"){
			// check if the user has the Role "Documents Approver"
			if(frappe.user.has_role("Documents Approver")){
				// the user has privillages allow
				// check if the if its validated
				if(frm.doc.validated == 1){
					cur_frm.set_value("approved",1)
					cur_frm.set_value("status","Approved")
					cur_frm.set_value("approved_by",frappe.session.user)
					cur_frm.save()
				}else{
					frappe.throw("Form need to be validated before approval")
				}
			}else{
				frappe.throw("You do not have sufficient permissions to perform this action")
			}
		}
	}else{
		frappe.throw("Please save the document first")
	}
}


frappe.ui.form.on('Non Project Related Expenditure',{
	refresh:function(frm){
		options_based_hide_unhide(frm)

		// check fields to make read only
		make_fields_read_only(list_of_fields)

		// Add exit button to forms of basic users.
		if (frappe.user_roles.includes('Staff') || frappe.user_roles.includes('Super Admin')) {
			// Dont add exit button
		} else {
			exitButton()
			// set orgnization that is linked to the organization
			frappe.call({
				method: "frappe.client.get_list",
				args: 	{
						doctype: "Organization",
						filters: {
							user:frappe.session.user
						},
				fields:["*"]
				},
				callback: function(response) {
					if(response.message.length>0){
						// set the organization field name
						cur_frm.set_value('full_name_of_the_organization',response.message[0].name)
						
					}
				}	
			});
		}
	}
})

frappe.ui.form.on('Non Project Related Expenditure', {
	onload: function(frm) {
		cur_frm.get_field("disbursement_grid_total").grid.cannot_add_rows = true;
	}
});

frappe.ui.form.on('Non Project Related Expenditure', {
	after_save: function(frm) {
		// check if user is not part of the admin
		if(frappe.user.has_role("Staff") || frappe.user.has_role("Super Admin") || frappe.user.has_role("Documents Approver")){
			// do no do anything
		}else{
			// check if the user is a basic user and the status is verified
			if(frappe.user.has_role("Organization Login")){
				if(cur_frm.doc.status == "Verified"){
					// redirect to profile page
					// redirect_url('/profile_project')
					setTimeout(function(){redirect_url('/profile_expenditure');}, 950);
				}
			}
		}
	}
});

frappe.ui.form.on("Non Project Related Expenditure", "yes", function(frm){
	if(cur_frm.doc.yes){
		// unmark the no check field
		cur_frm.set_value("no",0)
	}
});

frappe.ui.form.on("Non Project Related Expenditure", "no", function(frm){
	if(cur_frm.doc.no){
		// unmark the yes check field
		cur_frm.set_value("yes",0)
	}
});

frappe.ui.form.on("NPRE Sectors", "capital_investment", 
function(frm, cdt, cdn){
	var totalemp = 0

	var row = locals[cdt][cdn];
	if(row.capital_investment){
		totalemp += row.capital_investment
	}
	
	if(row.operating_costs){
		totalemp += row.operating_costs
	}
	
	row.total = totalemp

	frm.refresh_fields("disbursement_grid")
});


frappe.ui.form.on("NPRE Sectors", "operating_costs", 
function(frm, cdt, cdn){
	var totalemp = 0
	var row = locals[cdt][cdn];

	if(row.capital_investment){
		totalemp += row.capital_investment
	}

	if(row.operating_costs){
		totalemp += row.operating_costs
	}
	
	row.total = totalemp

	frm.refresh_fields("disbursement_grid")
});

// filters ndp to show only sectors
cur_frm.fields_dict["disbursement_grid"].grid.get_field("sector").get_query = function(doc) {
    return {
        filters: {
			"type":"Sector"
        }
    }
}

// filters administrative units to show only Regions
cur_frm.fields_dict["disbursement_grid"].grid.get_field("region").get_query = function(doc) {
    return {
        filters: {
			"type":"Region"
        }
    }
}

// Verification, Validation and Approval Section
frappe.ui.form.on("Non Project Related Expenditure", "mark_information_as_checked", function(frm){
	check_privillages(frm,"checked")
})

frappe.ui.form.on("Non Project Related Expenditure", "user_verify", function(frm){
	// check if infomation is marked as checked
	if(cur_frm.doc.information_checked){
		// confirm if the user want to validate information
		frappe.confirm(
			"Please ensure that all the information you have given is correct before verifying <hr> \
			if you are sure you are ready to verify click 'Yes' otherwise click 'No'",
			function(){
				// validate the form
				check_privillages(frm,"verify")
			},
			function(){
				// do nothing since the user is not sure
			}
		)
	}else{
		frappe.throw("Please mark the information as checked first")
	}
})

frappe.ui.form.on("Non Project Related Expenditure", "mark_information_as_checked_by_staff", function(frm){
	check_privillages(frm,"checked_by_staff")
})

frappe.ui.form.on("Non Project Related Expenditure", "user_validate", function(frm){
	check_privillages(frm,"validate")
})

frappe.ui.form.on("Non Project Related Expenditure", "approve", function(frm){
	check_privillages(frm,"approve")
})