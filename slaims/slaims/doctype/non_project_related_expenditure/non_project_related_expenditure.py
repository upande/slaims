# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document

class NonProjectRelatedExpenditure(Document):
	def validate(self):
		'''
		Function validates values before the document is
		saved
		'''
		# check if the user marked either yes or no
		if self.yes:
			# check if the user added any expenditure 
			if len(self.disbursement_grid) > 0 :
				pass
			else:
				frappe.throw("You have not added any expenditure amount to the table")
		elif self.no:
			pass
		else:
			frappe.throw("You have not stated whether or not your organization had any \
				None Project related expenditure in the previous year")

		# calculate the totals fr
		calculate_sectors_n_regions_total(self)

def calculate_sectors_n_regions_total(self):
	'''
	Function that calculates the total given in 
	for each sector
	'''
	sector_totals = {}
	region_totals = {}
	total_npre_amount = 0
	# add all unique sectors to the sectors table
	for row in self.disbursement_grid:
		# add keys to the sectors total dictionary
		try:
			# check if the key has been added
			sector_totals[row.sector]
		except:
			sector_totals[row.sector] = {"capital_investment":0,"operating_costs":0,"total":0}

		# add keys to the region_totals
		try:
			# check if the key has been added
			region_totals[row.region]
		except:
			region_totals[row.region] = {"capital_investment":0,"operating_costs":0,"total":0}

		# now add the totals for every row
		if row.capital_investment:
			sector_totals[row.sector]["capital_investment"] += row.capital_investment
		if row.operating_costs:
			sector_totals[row.sector]["operating_costs"] += row.operating_costs
		if row.total:
			sector_totals[row.sector]["total"] += row.total

		# now add totals for every region

		if row.capital_investment:
			region_totals[row.region]["capital_investment"] += row.capital_investment
		if row.operating_costs:
			region_totals[row.region]["operating_costs"] += row.operating_costs
		if row.operating_costs:
			region_totals[row.region]["total"] += row.total

	# place the amounts in the sectors table
	self.disbursement_total_sectors = []
	for key in  sector_totals.keys():
		self.append("disbursement_total_sectors", {
			"sector":key,
			"capital_investment":sector_totals[key]["capital_investment"],
			"operating_costs":sector_totals[key]["operating_costs"],
			"total":sector_totals[key]["total"]
		})
		# since the total npre amount is the same for both regional and 
		# sector distribution table lets use the sector table to 
		# get total npre amount
		total_npre_amount += sector_totals[key]["total"]
	# place the total npre amount in the total npre amount table
	self.total_npre_amount = total_npre_amount

	# place the amounts in the regions table
	self.disbursement_grid_total = []
	for key in  region_totals.keys():
		self.append("disbursement_grid_total", {
			"region":key,
			"capital_investment":region_totals[key]["capital_investment"],
			"operating_costs":region_totals[key]["operating_costs"],
			"total":region_totals[key]["total"]
		})