# -*- coding: utf-8 -*-
# Copyright (c) 2019, upande and contributors
# For license information, please see license.txt

from __future__ import unicode_literals
import frappe
from frappe.model.document import Document
import datetime


class SomalilandGovernmentInstitutionRenewalForm(Document):
	def validate(self):
		# run validation function
		validation_function(self)

		# add correct renewal year
		add_correct_renewal_year(self)	

		# load renewal data from organization
		load_renewal_data_from_organization(self)	

		# add contact persons
		add_contact_spaces(self)

		# add sectors
		add_sectors(self)


	def on_update(self):
		# update information to main doctypes
		map_data_to_organization(self)

def validation_function(self):
	'''
	Function that checks that all the required fields 
	are given
	'''

	# verification fields
	list_of_verification_fields = [
		{"field":self.full_name_of_the_organization,"name":"Name of Organization"},
		{"field":self.acronym,"name":"Acronym"},
		{"field":self.status,"name":"Status"},

		# add more validation required fields below
		
	]

	# validation fields
	list_of_validation_fields = [
		{"field":self.full_name_of_the_organization,"name":"Name of Organization"},
		{"field":self.acronym,"name":"Acronym"},
		{"field":self.status,"name":"Status"}

		# add more validation required fields below
	]

	

	# check approval fields
	list_of_approval_fields = [
		# add fields required for approval below
	]


	if self.status == "Verified":
		# loop through list of saving fields
		for field in list_of_verification_fields:
			if field["field"]:
				pass
			else:
				# value for the field is not given throw an errror
				frappe.throw("The Field '{}' is Required Before Verification".format(field["name"]))

	if self.status == "Validated":
		# loop through list of saving fields
		for field in list_of_validation_fields:
			if field["field"]:
				pass
			else:
				# value for the field is not given throw an errror
				frappe.throw("The Field '{}' is Required Before Verification".format(field["name"]))

	if self.status == "Approved":
		# loop through list of saving fields
		for field in list_of_approval_fields:
			if field["field"]:
				pass
			else:
				# value for the field is not given throw an errror
				frappe.throw("The Field '{}' is Required Before Verification".format(field["name"]))

	# check if previous financial years was closed
	linked_organization_doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
	if linked_organization_doc.financial_year_not_closed:
		frappe.throw("The previous Financial year for your organization was not closed <hr> \
			Please fill all the required forms for the year {} and contant Admin for assisistance\
			".format(linked_organization_doc.current_registration_year))

def add_correct_renewal_year(self):
	'''
	Function that gets the current organization 
	year and hence the next year i.e renewal year
	using years ranks
	'''
	# check year exist
	if self.year_of_registration: #in this case the year of renewal
		pass
	else:
		try:
			# get the current registration year of the organization
			organization_doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
			# its current year rank
			current_year_doc = frappe.get_doc("Year",str(organization_doc.current_registration_year))
			renewal_year_rank = current_year_doc.rank + 1
			# get a list of years with a specific rank
			list_of_years_matching_rank = frappe.get_list("Year",
				fields=["name"],
				filters = {
					"rank":renewal_year_rank
			})
			# place the renewal year in the form
			self.year_of_registration = list_of_years_matching_rank[0].name
		except:
			# if an error occurs trying placing the global year as the renewal year
			try:
				global_setting_doc = frappe.get_single("Global Settings")
				self.year_of_registration = global_setting_doc.current_financial_year
			except:
				frappe.throw("A global financial year not defined please contact Admin for assistance")

def add_contact_spaces(self):
	'''
	Add the Correct Number of Spaces for Connection
	'''
	if len(self.alternative_contact_person) < 2:
		rows_to_add = 2 - len(self.alternative_contact_person)
		for i in range(rows_to_add):
			self.append("alternative_contact_person", {
				# do not add any fields
			})


def add_sectors(self):
	'''
	Add the Correct Number of Sectors
	'''
	if self.current_section == "department_responsible_section":
		# get all the sectors
		list_of_sectors = frappe.get_list("NDP",
			fields=["name"],
			filters = {
				"type":"Sector"
		})

		if len(self.sector__alignment_table) == len(list_of_sectors):
			pass
		else:
			# add the sectors to the table
			for sector in list_of_sectors:
				self.append("sector__alignment_table", {
					"sector":sector["name"]
				})


def check_if_organization_exists(self):
	'''
	Checks if an organization exists else create one
	'''

	# create an organization
	doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
	
	doc.name = self.full_name_of_the_organization
	doc.status = self.status
	doc.organization_type = "Somaliland Government Institution"
	doc.full_name_of_the_organization = self.full_name_of_the_organization
	if doc:
		doc.save(ignore_permissions = True)
	else:
		doc.insert(ignore_permissions = True)


def map_data_to_organization(self):
	'''
	Function that Updates the details of the organization
	to the organization form once the form is approved
	'''
	# check if the organization has been verified
	if self.status == "Verified" or self.status == "Admin Approved":
		check_if_organization_exists(self)

	if self.status == "Validated":
		# get related organization
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
		doc.status = "Validated"
		doc.save()

	# if approved update details to linked organization
	if(self.status == "Approved" or self.status == "Imported"):
		# get related organization
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
		
		# ORGANIZATION TYPE SECTION
		doc.status = "Approved"

		# GENERAL ORGANIZATION DETAILS SECTION
		doc.full_name_of_the_organization = self.full_name_of_the_organization
		doc.acronym = self.acronym

		# ADDRESS OF MAIN OFFICE SECTION
		doc.area_neighborhood = self.area_neighborhood
		doc.town = self.town
		doc.region = self.region
		doc.district = self.district

		# Director Address Section
		doc.full_name = self.director_full_name
		doc.director_country_code = self.country_code
		doc.tel_no_1 = self.tel_no
		doc.complete_director_tel_no = self.complete_tel_no

		# Three Alternative Contact Persons Section
		doc.alternative_contact_person = []

		for contact in self.alternative_contact_person:
			doc.append("alternative_contact_person", {
				"full_name": contact.full_name,
				"title":contact.title,
				"tel_no":contact.tel_no,
				"email":contact.email
			})
		
		# Department responsible for coordination issues
		doc.sector__alignment_table = []

		for sector in self.sector__alignment_table:
			doc.append("sector__alignment_table", {
				"sector": sector.sector,
				"yes":sector.yes,
			})

		# Sector coordination fora attended
		doc.coordinating_sectors_table = []

		for fora in self.coordinating_sectors_table:
			doc.append("coordinating_sectors_table", {
				"fora_attended": fora.fora_attanded,
				"your_organization_the_chair_of_this_scf": fora.your_organization_the_chair_of_this_scf
			})
		
		# add the form as a child table to organization
		doc.append("attached_organization_forms_table", {
			"name_of_form":self.name,
			"year_of_form":self.year_of_registration
		})

		# save the added changes
		doc.save()

def load_renewal_data_from_organization(self):
	if self.data_pulled_from_main:
		pass
	else:
		doc = frappe.get_doc("Organization",self.full_name_of_the_organization)
		
		# GENERAL ORGANIZATION DETAILS SECTION
		self.full_name_of_the_organization = doc.full_name_of_the_organization
		self.acronym = doc.acronym
		self.year_of_first_registration = doc.organization_year_registered 
		self.organization_registration_number = doc.organization_registration_number

		# ADDRESS OF MAIN OFFICE SECTION
		self.area_neighborhood = doc.area_neighborhood
		self.town = doc.town
		self.region = doc.region
		self.district = doc.district

		# Director Address Section
		self.director_full_name =  doc.full_name
		self.country_code =  doc.director_country_code
		self.tel_no =  doc.tel_no_1
		self.complete_tel_no =  doc.complete_director_tel_no

		# Three Alternative Contact Persons Section
		self.alternative_contact_person = []

		# get the list from the linked table
		list_of_contacts = frappe.get_list("Alternative Contact Persons",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"alternative_contact_person",
						"parenttype":"Organization",
				})
		
		for contact in list_of_contacts:
			self.append("alternative_contact_person", {
				"full_name": contact.full_name,
				"title":contact.title,
				"tel_no":contact.tel_no,
				"email":contact.email
			})
		
		# Department responsible for coordination issues
		self.sector__alignment_table = []

		# Get list of sectors
		list_of_sectors = frappe.get_list("Department Responsible Table",
					fields=["*"],
					filters = {
						"parent":doc.full_name_of_the_organization,
						"parentfield":"sector__alignment_table",
						"parenttype":"Organization",
				})

		for sector in list_of_sectors:
			self.append("sector__alignment_table", {
				"sector": sector.sector,
				"yes":sector.yes,
			})

		# Fora attended 
		self.coordinating_sectors_table = []
		# get required list
		list_of_fora = frappe.get_list("Cordinating Sectors Table",
			fields=["*"],
			filters = {
				"parent":doc.full_name_of_the_organization,
				"parentfield":"coordinating_sectors_table",
				"parenttype":"Organization",
				})

		for fora in list_of_fora:
			self.append("coordinating_sectors_table", {
				"fora_attanded": fora.fora_attanded,
				"your_organization_the_chair_of_this_scf": fora.your_organization_the_chair_of_this_scf
			})

		# self.append()
		# # Department responsible for coordination issues
		# self.sector__alignment_table = []

		# # Get list of sectors
		# list_of_departments = frappe.get_list("Department Responsible Table",
		# 			fields=["*"],
		# 			filters = {
		# 				"parent":doc.full_name_of_the_organization,
		# 				"parentfield":"section_alignment_table",
		# 				"parenttype":"Organization",
		# 		})

		# for department in list_of_departments:
		# 	doc.append("attached_organization_forms_table", {
		# 		"name_of_form":self.name,
		# 		"year_of_form":self.year_of_registration
		# 	})

		# mark the form that data has been imported
		self.data_pulled_from_main = 1
		