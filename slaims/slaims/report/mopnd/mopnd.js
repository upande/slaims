// Copyright (c) 2016, upande and contributors
// For license information, please see license.txt
/* eslint-disable */

frappe.query_reports["MoPND"] = {
	"filters": [
		{"fieldname":"organization",
			"label": __("Organization"),
			"fieldtype": "Link",
			"options": "Organization",
			// "default":
		},
		{"fieldname":"year",
			"label": __("Year"),
			"fieldtype": "Link",
			"options": "Year",
			"default":get_current_year()
		},
		{"fieldname":"completion_status",
			"label": __("Completion Status"),
			"fieldtype": "Select",
			"options": ["Complete","Incomplete"],
			"default":"Incomplete"
		}
	],

	onload: function(report) {
        report.page.add_inner_button(__("Apply"), function() {
			var filters = report.get_values();
			console.log(filters)
            // frappe.set_route('query-report', 'Accounts Receivable Summary', {company: filters.company});
        });
    }
}



function get_current_year(){
	var today = new Date()
	return today.getFullYear()
}


 