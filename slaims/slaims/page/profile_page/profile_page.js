frappe.pages['profile-page'].on_page_load = function(wrapper) {

	redirectFunction()

	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Loading Profile ...',
		single_column: true
	});
}

function redirectFunction() {
	location.replace("/profile_organization")
}