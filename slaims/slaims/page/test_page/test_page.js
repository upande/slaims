frappe.pages['test-page'].on_page_load = function(wrapper) {
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: __('Test Page'),
		single_column: true
	});

	// page.add_menu_item('Projects', () => frappe.set_route('List', 'Project'))
	// page.add_menu_item('Project1', () => test_function())

	wrapper.test_page = new frappe.TestFunnel(wrapper);
	
}


frappe.TestFunnel = class TestFunnel{
	constructor(wrapper){
		var me = this;

		// gives time for canvas to get width and height
		setTimeout(function(){
			me.setup(wrapper);
			// me.get_data();
		},0);
	}

	setup(wrapper){
		var me = this;

		// add organization name field year field and completion
		this.organization_name = wrapper.page.add_field({
			"fieldtype":"Link","fieldname":"organization_name","options":"Organization",
			"label":__("Organization"),
		})

		this.year = wrapper.page.add_field({
			"fieldtype":"Link","fieldname":"year","options":"Year",
			"label":__("Year"),
		})

		this.completion_status = wrapper.page.add_field({
			"fieldtype":"Select","fieldname":"completion_status","options":["","Complete","Incomplete"],
			"label":__("Completion Status"),
		})

		this.html_field = wrapper.page.add_field({
			"fieldtype":"HTML","fieldname":"html_field",
			"label":__("HTML"),
		})

		// this.full_name_of_the_organization = wrapper.page.add_field({
		// 	"fieldtype":"Link","fieldname":"full_name_of_the_organization","options":"Organization",
		// 	"label":__("Organization"),
		// })

		// this.elements = {
		// 	layout: $(wrapper).find(".layout-main"),
		// 	from_date: wrapper.page.add_date(__("From Date")),
		// 	to_date: wrapper.page.add_date(__("To Date")),
		// 	chart: wrapper.page.add_select(__("Chart"), [{value: 'sales_funnel', label:__("Sales Funnel")},
		// 		{value: 'sales_pipeline', label:__("Sales Pipeline")},
		// 		{value: 'opp_by_lead_source', label:__("Opportunities by lead source")}]),
		// 	refresh_btn: wrapper.page.set_primary_action(__("Refresh"),
		// 		function() {  }, "fa fa-refresh"),
		// };

		// this.elements.no_data = $('<div class="alert alert-warning">' + __("No Data") + '</div>')
		// 	.toggle(false)
		// 	.appendTo(this.elements.layout);

		// this.elements.funnel_wrapper = $('<div class="funnel-wrapper text-center"></div>')
		// 	.appendTo(this.elements.layout);

		this.year = get_current_year();
		this.html_field = "<h1>Hello World</h1>"

		// bind resize
		// $(window).resize(function() {
		// 	// me.render();
		// });
	}
}


function get_current_year(){
	var today = new Date()
	return today.getFullYear()
}

