frappe.pages['mopnd-interface'].on_page_load = function(wrapper) {
	// redirect to MNOPD interace
	redirectFunction()
	
	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Loading MoPND Interface ...',
		single_column: true
	});
}

function redirectFunction() {
	location.replace("/mopnd_interface")
}