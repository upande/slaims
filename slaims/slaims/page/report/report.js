frappe.pages['report'].on_page_load = function(wrapper) {
	
	redirectFunction()

	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Report',
		single_column: true
	});
}

function redirectFunction() {
	location.replace("/report_home")
}