frappe.pages['dashboard'].on_page_load = function(wrapper) {
	
	redirectFunction()

	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Dashboard',
		single_column: true
	});
}

function redirectFunction() {
	location.replace("/dashboard")
}