frappe.pages['help-page'].on_page_load = function(wrapper) {
	redirectFunction()

	var page = frappe.ui.make_app_page({
		parent: wrapper,
		title: 'Loading Help Page ..',
		single_column: true
	});
}

function redirectFunction() {
	location.replace("/help_page")
}